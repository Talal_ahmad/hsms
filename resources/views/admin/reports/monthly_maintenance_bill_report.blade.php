@extends('admin.layouts.master')

@section('title', 'Maintenance Bill Monthly Report')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">OPUS</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Maintenance Bill Monthly Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            {{-- @dd($data->first()); --}}
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{ url('/monthly_maintenance_bill_report') }}">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label for="fromdate">From</label>
                                        <input type="date" name="fromdate" id="fromdate" class="form-control"
                                            value="{{ $request->fromdate }}">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="todate">To</label>
                                            <input type="date" name="todate" id="todate" class="form-control"
                                                value="{{ $request->todate ? $request->todate : date('Y-m-d') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="properties">Filter By Property</label>
                                        <select name="properties[]" id="properties" multiple class="form-select select2"
                                            data-placeholder="Select Property">
                                            <option value="">Select Property</option>
                                            @foreach ($properties as $property)
                                                <option value="{{ $property->id }}"
                                                    {{ is_array(request('properties')) && in_array($property->id, request('properties')) ? 'selected' : '' }}>
                                                    {{ $property->property_number }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="possesion">Filter By Possesion</label>
                                        <select name="possesion" id="possesion" class="form-select select2"
                                            data-placeholder="Select Status">
                                            <option value="">Select Status</option>
                                            <option value='all' {{ 'all' == request('possesion') ? 'selected' : '' }}>All
                                            </option>
                                            <option value='owner' {{ 'owner' == request('possesion') ? 'selected' : '' }}>
                                                Given</option>
                                            <option value='tenant'
                                                {{ 'tenant' == request('possesion') ? 'selected' : '-' }}>
                                                Not Given(Rented)</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{ url('/monthly_maintenance_bill_report') }}" type="button"
                                            class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                    <div class="col-8">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card pb-1">
                        <table class="table px-1" id="dataTable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Sr.#</th>
                                    <th>Unit #</th>
                                    <th>Client Names</th>
                                    <th>Final Area(SFT)</th>
                                    <th>Unit Type</th>
                                    <th>Bill Issued On</th>
                                    @foreach ($collection_heads as $key => $collection_head)
                                        <th>{{ $collection_head }}</th>
                                    @endforeach
                                    {{-- <th>Electricity</th>
                                    <th>Genset</th>
                                    <th>Other Community Charges</th> --}}
                                    <th>Total COMMON COMMON AREA UTILITIES & COMMUNITY SERVICES</th>
                                    {{-- <th>Genset Consumption Inside the unit</th>
                                    <th>House Keeping Services</th>
                                    <th>Repair & Keeping Maintenance</th> --}}
                                    <th>Total Bill Without PRA Sales Tax</th>
                                    <th>PRA Sales Tax</th>
                                    {{-- <th>Electricity Consumption Inside the unit</th> --}}
                                    <th>Total Amount Receivable</th>
                                    <th>Arrears</th>
                                    <th>Total Receivable with Arrears</th>
                                    <th>Amount Received on Date</th>
                                    <th>Total Amount Received</th>
                                    <th>Total Waived Off</th>
                                    <th>Balance Due</th>
                                    <th>Remarks</th>
                                    <th>Possession Given/Not Given</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($monthly_bills as $monthly_bill)
                                    <tr>
                                        <td></td>
                                        <td>{{ $monthly_bill['property_id'] }}</td>
                                        <td>{{ $monthly_bill['property_number'] }}</td>
                                        <td>{{ $monthly_bill['name'] }}</td>
                                        <td>{{ $monthly_bill['final_area_sft'] }}</td>
                                        <td>{{ $monthly_bill['unit_type'] }}</td>
                                        <td>{{ $monthly_bill['bill_issued_on'] }}</td>

                                        {{-- @foreach ($collection_heads as $collection_head)
                                            <td>{{ in_array($collection_head, $monthly_bill['collection_heads_and_calculation_units']) ? 'name here' : '-' }}
                                            </td>
                                        @endforeach --}}

                                        @php
                                            $total_common_area_utilities_and_total_bill_without_pra_sales_tax = 0;
                                            $pra_tax_rate = 0;
                                        @endphp

                                        @foreach ($collection_heads as $key => $collection_head)
                                            @php
                                                $calculation_unit =
                                                    $monthly_bill['collection_heads_and_calculation_units'][
                                                        $collection_head
                                                    ] ?? null;
                                                $common_area_utilities = $calculation_unit
                                                    ? ($calculation_unit == 'square_feet'
                                                        ? $monthly_bill['collection_heads_payment_plan_details_amount'][
                                                                $collection_head
                                                            ] * $monthly_bill['final_area_sft']
                                                        : ($calculation_unit == 'kanal'
                                                            ? $monthly_bill[
                                                                    'collection_heads_payment_plan_details_amount'
                                                                ][$collection_head] * $monthly_bill['final_area_kanal']
                                                            : ($calculation_unit == 'marla'
                                                                ? $monthly_bill[
                                                                        'collection_heads_payment_plan_details_amount'
                                                                    ][$collection_head] *
                                                                    $monthly_bill['final_area_marla']
                                                                : $monthly_bill[
                                                                    'collection_heads_payment_plan_details_amount'
                                                                ][$collection_head])))
                                                    : '-';
                                            @endphp
                                            {{-- @php
                                                $common_area_utilities = in_array(
                                                    $collection_head,
                                                    $monthly_bill['collection_heads_and_calculation_units'],
                                                )
                                                    ? (array_search(
                                                        $collection_head,
                                                        $monthly_bill['collection_heads_and_calculation_units'],
                                                    ) == 'square_feet'
                                                        ? $monthly_bill['collection_heads_payment_plan_details_amount'][
                                                                $collection_head
                                                            ] * $monthly_bill['final_area_sft']
                                                        : (array_search(
                                                            $collection_head,
                                                            $monthly_bill['collection_heads_and_calculation_units'],
                                                        ) == 'kanal'
                                                            ? $monthly_bill[
                                                                    'collection_heads_payment_plan_details_amount'
                                                                ][$collection_head] * $monthly_bill['final_area_kanal']
                                                            : (array_search(
                                                                $collection_head,
                                                                $monthly_bill['collection_heads_and_calculation_units'],
                                                            ) == 'marla'
                                                                ? $monthly_bill[
                                                                        'collection_heads_payment_plan_details_amount'
                                                                    ][$collection_head] *
                                                                    $monthly_bill['final_area_marla']
                                                                : $monthly_bill[
                                                                    'collection_heads_payment_plan_details_amount'
                                                                ][$collection_head])))
                                                    : '-';
                                            @endphp --}}

                                            <td>
                                                PKR {{ number_format((float) $common_area_utilities, 3) }}/-
                                            </td>
                                            @php
                                                array_search(
                                                    $collection_head,
                                                    $monthly_bill['collection_heads_and_calculation_units'],
                                                ) == 'fixed'
                                                    ? 0
                                                    : ($pra_tax_rate += (float) $common_area_utilities);
                                                $total_common_area_utilities_and_total_bill_without_pra_sales_tax += (float) $common_area_utilities;
                                            @endphp
                                        @endforeach

                                        <td>PKR
                                            {{ number_format($total_common_area_utilities_and_total_bill_without_pra_sales_tax, 3) }}/-
                                        </td>
                                        <td>PKR
                                            {{ number_format($total_common_area_utilities_and_total_bill_without_pra_sales_tax, 3) }}/-
                                        </td>
                                        <td>{{ '(' . $monthly_bill['pra_tax_rate'] . '%) ' . number_format(($monthly_bill['pra_tax_rate'] / 100) * $pra_tax_rate, 3) }}
                                        </td>
                                        <td>PKR {{ number_format($monthly_bill['total_amount_receivable'], 3) }}/-</td>
                                        <td>PKR {{ number_format($monthly_bill['arrears'], 3) }}/-</td>
                                        <td>PKR
                                            {{ number_format($monthly_bill['total_amount_receivable_with_arrears'], 3) }}/-
                                        </td>
                                        <td>{{ $monthly_bill['amount_received_on_date'] }}</td>
                                        <td>PKR {{ number_format($monthly_bill['total_amount_received'], 3) }}/-</td>
                                        <td>PKR {{ number_format($monthly_bill['total_waived_off'], 3) }}/-</td>
                                        <td>PKR {{ number_format($monthly_bill['balance_due'], 3) }}/-</td>
                                        <td>{{ $monthly_bill['remarks'] }}</td>
                                        <td>{{ $monthly_bill['possession_given/not_given'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                ordering: true,
                // ajax: '{{ route('monthly_maintenance_bill_report') }}',
                // columns: [{
                //         data: 'responsive_id'
                //     },
                //     {
                //         data: 'property_id'
                //     },
                //     {
                //         data: 'property_number'
                //     },
                //     {
                //         data: 'name',
                //     },
                //     {
                //         data: 'final_area_sft'
                //     },
                //     {
                //         data: 'unit_type'
                //     },
                //     {
                //         data: 'bill_issued_on'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     },
                //     {
                //         data: '-'
                //     }
                // ],
                "columnDefs": [{
                        className: 'control',
                        orderable: false,
                        targets: 0,
                        searchable: false
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                // displayLength: 10,
                pageLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',

                    buttons: [{

                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        previous: 'Previous',
                        next: 'Next'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">Maintenance Bill Monthly Report</h6>');
            $('.app').attr('id', 'app-export');

        });
    </script>
@endsection
