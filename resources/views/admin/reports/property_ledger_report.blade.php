@extends('admin.layouts.master')

@section('title', 'Property Reports')
@section('content')
    <section class="content-wrapper container-xxl p-0">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">HSMS</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Property Report
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="content-body">
            {{-- @dd($data->first()); --}}
            <div class="row">
                <div class="col-12">
                    <div class="card mb-1">
                        <div class="card-body">
                            <form id="search_form" action="{{ url('/property_ledger') }}">
                                <div class="row">
                                    <div class="col-md-6 col-12">
                                        <label for="fromdate">From</label>
                                        <input type="date" name="fromdate" id="fromdate" class="form-control"
                                            value="">
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <div class="mb-1">
                                            <label for="todate">To</label>
                                            <input type="date" name="todate" id="todate" class="form-control"
                                                value="{{ date('Y-m-d') }}">
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="customer">Customer</label>
                                        <select name="customer" id="customer" class="form-select select2"
                                            data-placeholder="Select Customer" aria-selected="{{ request('customer') }}">
                                            <option value="">Select Customer</option>
                                            {{-- @foreach ($vehicleMoverscustomers as $customer)
                                                    <option value="{{ $customer->id }}"
                                                        {{ $customer->id == request('customer') ? 'selected' : '' }}>
                                                        {{ $customer->name }}</option>
                                                @endforeach --}}
                                        </select>
                                    </div>
                                    <div class="col-md-6 col-12">
                                        <label for="status">Status</label>
                                        <select name="status" id="status" class="form-select select2"
                                            data-placeholder="Select Status" aria-selected="{{ request('blnumber') }}">
                                            <option value="">Select Status</option>
                                            {{-- <option value='Delivered'
                                                {{ 'Delivered' == request('status') ? 'selected' : '' }}>Delivered</option>
                                            <option selected value='Pending' {{ 'Pending' == request('status') || (!request('status') && request('status') != 'All')  ? 'selected' : '' }}>
                                                Pending</option>
                                            <option value='All' {{ 'All' == request('status') ? 'selected' : '' }}>All
                                            </option> --}}
                                        </select>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12 text-end">
                                        <a href="{{ url('/property_ledger') }}" type="button"
                                            class="btn btn-danger mt-1">Reset</a>
                                        <button type="submit" class="btn btn-primary mt-1">Apply</button>
                                    </div>
                                    <div class="col-8">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-12">
                    <div class="card pb-1">
                        <table class="table px-1" id="dataTable">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Date</th>
                                    <th>Vou. Ref.</th>
                                    <th>Transaction Narration</th>
                                    <th>Ref Chq/PO No.</th>
                                    <th>Ref Chq/PO Date</th>
                                    <th>Dr. Amt</th>
                                    <th>Cr. Amt</th>
                                    <th>Closing Balance</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td></td>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                    <th>-</th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable({
                ordering: true,
                "columnDefs": [{
                    className: 'control',
                    orderable: false,
                    targets: 0
                }],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                // displayLength: 10,
                pageLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',

                    buttons: [{

                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            footer: true,
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">Vehicle Report</h6>');
            $('.app').attr('id', 'app-export');

        });
    </script>
@endsection
