<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>
        {{ $property->city_name . ' - ' . $property->society_name . ' - ' . $property->block_name . ' - ' . $plan_name }}
    </title>
    <style>
        h1,
        h2,
        h3,
        h4,
        h5,
        h6 {
            color: #896A00 !important;
        }

        h2 {
            font-size: 2rem !important;
        }

        .theme_color {
            background-color: #8B6801 !important;
        }

        .theme_color_2 {
            background-color: #FFC719 !important;
        }

        p {
            color: #87734F;
        }

        .border {
            border-color: #896A00 !important;
        }

        b {
            font-weight: 1200 !important;
        }

        .table {
            border-color: #FFC719 !important;
        }

        .theme_color3 {
            color: #896A00 !important;
        }

        .striped_table {
            --bs-table-striped-color: #896A00 !important;
            --bs-table-active-color: #896A00 !important;
            --bs-table-striped-bg: #FFF3D0 !important;
        }

        .striped_table2 {
            --bs-table-striped-color: #896A00 !important;
            --bs-table-active-color: #896A00 !important;
            --bs-table-striped-bg: #F6C17F !important;
        }

        .bg_pink {
            background-color: #FFF3D0 !important;
        }

        .text-dark {
            color: #000 !important;
        }

        .table_dark_header {
            background-color: #D8D8D8 !important;
        }

        .lh_custom {
            line-height: 2.25 !important;
        }

        .w-90 {
            width: 90% !important;
        }

        .border-top {
            border-color: #896A00 !important;
        }

        .container-xxl {
            max-width: 1140px;
            margin-left: auto;
            margin-right: auto;
            padding-left: 15px;
            padding-right: 15px;
        }

        .p-0 {
            padding: 0;
        }

        .pb-5 {
            padding-bottom: 5rem;
        }

        .mt-5 {
            margin-top: 3rem !important;
        }

        .row::after {
            content: "";
            display: table;
            clear: both;
        }

        .row {
            width: 100%;
            margin-left: -15px;
            margin-right: -15px;
        }

        .row::after {
            content: "";
            display: table;
            clear: both;
        }

        .col-10 {
            float: left;
            width: 83.333333%;
            max-width: 83.333333%;
            margin-right: auto;
        }

        .col-2 {
            float: left;
            width: 16.666667%;
            margin-left: auto;
        }

        .align-items-center::after {
            content: "";
            display: table;
            clear: both;
        }

        .align-items-center>* {
            float: left;
        }

        .border {
            border: 1px solid #000;
        }

        .border-2 {
            border-width: 2px;
        }

        .fw-bolder {
            font-weight: bold;
        }

        .text-center {
            text-align: center;
        }

        .fs-2 {
            font-size: 1.5rem;
        }

        .text-end {
            text-align: right;
        }

        *,
        ::after,
        ::before {
            box-sizing: border-box;
        }

        body {
            font-family: sans-serif;
        }

        .px-2 {
            padding-left: 0.5rem !important;
            padding-right: 0.5rem !important;
        }

        .w-50 {
            width: 50%;
        }

        .row-cols-3>* {
            width: 33.3%;
            /* Adjust the width as per your requirement */
            float: left;
            /* margin-right: 20px; */
            /* Add some spacing between the columns */
            /* box-sizing: border-box; */
            /* Include padding and border in the width calculation */
        }

        .row-cols-3::after {
            content: "";
            display: table;
            clear: both;
            /* Clear the float to properly contain the floated elements */
        }

        .py-2 {
            padding-top: 2rem;
            padding-bottom: 2rem;
        }

        .px-1 {
            padding-left: 1rem;
            padding-right: 1rem;
        }

        .fs-5 {
            font-size: 1.25rem;
        }

        .d-flex::after {
            content: "";
            display: table;
            clear: both;
        }

        .d-flex>* {
            float: left;
        }

        .justify-content-between> :first-child {
            margin-right: auto;
        }

        .justify-content-between::after {
            content: "";
            display: table;
            clear: both;
        }

        .row-cols-2::after {
            content: "";
            display: table;
            clear: both;
        }

        .row-cols-2>* {
            float: left;
            width: 48%;
            /* Adjust as needed */
        }

        .mt-3 {
            margin-top: 2rem;
            /* Adjust as needed */
        }

        .margin-right-2 {
            margin-right: 1%;
        }

        .margin-right-3 {
            margin-right: 6%;
        }

        .margin-left-2 {
            margin-left: 1%;
        }

        /* Table */
        .table {
            width: 100%;
            margin-bottom: 1rem;
            color: #212529;
            border-collapse: collapse;
        }

        /* Table Header */
        .table th,
        .table td {
            padding: 0.75rem;
            vertical-align: top;
            border-top: 1px solid #dee2e6;
        }

        /* Striped Rows */
        .table-striped tbody tr:nth-of-type(odd) {
            background-color: rgba(0, 0, 0, 0.05);
        }

        /* Bordered Table */
        .table-bordered th,
        .table-bordered td {
            border: 1px solid #dee2e6;
        }

        /* Custom Striped Table */
        .striped_table tbody tr:nth-of-type(odd) {
            background-color: #FFF3D0 !important;
            /* Adjust background color as needed */
        }

        /* Custom Striped Table 2 */
        .striped_table2 tbody tr:nth-of-type(odd) {
            background-color: #F6C17F !important;
            /* Adjust background color as needed */
        }

        .text-white {
            color: white;
        }

        .text-end {
            text-align: right;
        }

        .m-0 {
            margin: 0;
        }

        /* Define the hover effect for table rows */
        .table-hover tbody tr:hover {
            background-color: #f5f5f5;
            /* Change to the desired background color */
        }

        .text-danger {
            color: red;
        }

        .mt-2 {
            margin-top: 0.5rem;
        }

        .fs-4 {
            font-size: 1.5rem;
        }

        .ps-2 {
            padding-left: 0.5rem;
        }

        .fs-5 {
            font-size: 1.25rem;
        }

        .px-1 {
            padding-left: 0.25rem;
            padding-right: 0.25rem;
        }

        .pt-1 {
            padding-top: 0.25rem;
        }

        .py-1 {
            padding-top: 0.25rem;
            padding-bottom: 0.25rem;
        }

        .mx-auto {
            margin-left: auto;
            margin-right: auto;
        }

        .p-1 {
            padding: 0.25rem !important;
        }

        .pb-0 {
            padding-bottom: 0 !important;
        }

        .border-1 {
            border-width: 1px;
        }

        .border-black {
            border-color: #000 !important;
        }

        .table-borderless {
            border: none !important;
        }

        .text-decoration-underline {
            text-decoration: underline !important;
        }

        div {
            display: block;
            unicode-bidi: isolate;
        }

        p {
            margin-top: 0;
            margin-bottom: 1rem;
        }

        .pe-0 {
            padding-right: 0 !important;
        }

        .py-0 {
            padding-top: 0 !important;
            padding-bottom: 0 !important;
        }

        .pe-5 {
            padding-right: 1.25rem !important;
            /* Assuming 1rem = 16px */
        }

        .fs-3 {
            font-size: 1.875rem !important;
            /* Assuming 1rem = 16px */
        }

        .border-top {
            border-top: 1px solid #000;
        }

        .same-height {
            height: 15.8rem;
        }
    </style>
    </style>
</head>

<body>
    <section class="content-wrapper container-xxl p-0 pb-5 mt-5">
        <div class="row align-items-center border border-2 ">
            <h2 class="fw-bolder col-10 text-center fs-2 p-0"><b>Monthly Charges Bill</b></h2>
            <div class="text-end col-2 px-2">
                <img class="w-50" src="{{ asset('image/opus_log(2).jpg') }}" alt="The opus">
            </div>
        </div>
        <div class="row row-cols-3 p-0">
            <div class="border border-2 py-2 px-1 same-height">
                <p class=" fw-bolder fs-5">{{ $assigned_to }} </p>
                <p class=" fw-bolder fs-5">{{ 'Unit No: ' . $property->property_number }}</p>
                <p class=" fw-bolder fs-5">{{ 'Unit Area: ' . $property->landAreaSqfeet . ' SFT' }}</p>
            </div>
            <div class="border border-2 py-2 px-1 same-height">
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 5rem;">BILL MONTH:</p>
                    <p class=" fw-bolder fs-5">{{ $hmonth }}-{{ $year }}</p>
                </div>
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 4rem;">BILL AMOUNT:&nbsp;</p>
                    <p class=" fw-bolder fs-5">{{ number_format($total_amount_within_due_date) }}</p>
                </div>
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 6rem;">ARREARS:&nbsp;&nbsp;</p>
                    <p class=" fw-bolder fs-5">{!! $arrears > 0 ? number_format($arrears) : '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-' !!}</p>
                </div>
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 2rem;">TOTAL PAYABLE:&nbsp;&nbsp;</p>
                    <p class=" fw-bolder fs-5">
                        {{-- {{ number_format(round($total_amount_within_due_date + $arrears + ($tax_rate / 100) * ($total_amount_within_due_date + $arrears))) }} --}}
                        {{ number_format(round($total_amount_within_due_date + $arrears)) }}
                    </p>
                </div>
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 2rem;"><small>AFTER DUE
                            DATE:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</small>
                    </p>
                    <p class=" fw-bolder fs-5">
                        <small>{{ number_format($total_amount_within_due_date + $arrears + $total_amount_after_due_date) }}</small>
                    </p>
                </div>
            </div>
            <div class="border border-2 py-2 px-1 same-height">
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 5rem;">BILL Date:&nbsp;</p>
                    <p class=" fw-bolder fs-5">{{ $issue_date }}</p>
                </div>
                <div class=" d-flex justify-content-between ">
                    <p class=" fw-bolder fs-5" style="margin-right: 5rem;">DUE DATE:</p>
                    <p class=" fw-bolder fs-5">{{ $due_date }}</p>
                </div>
            </div>
        </div>
        <div class="row row-cols-2 mt-3">
            <div class="margin-right-2" style="height: 585px !important;">
                <table style="break-inside: avoid;" class="table table-striped table-bordered striped_table">
                    <thead>
                        <tr>
                            <th class="theme_color border border-2 fw-bolder text-white "
                                style="font-size: 15px; white-space: nowrap !important;" scope="col">COMMON AREA
                                UTILITIES & COMMUNITY SERVICES</th>
                            <th class="theme_color border border-2 fw-bolder text-white text-end" scope="col">Amount
                                (PKR)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $sum = 0;
                        ?>
                        @foreach ($data as $index => $pay)
                            <tr>
                                <td class="theme_color3 fw-bolder ">
                                    {{ '' . $pay['name'] }}
                                </td>

                                <td class="theme_color3 fw-bolder text-end ">
                                    {{ number_format($pay['amount']) }}
                                </td>
                            </tr>
                            <?php $sum = $sum + $pay['amount'];
                            ?>
                        @endforeach
                    </tbody>
                </table>
                <div class=" border border-2 p-1 d-flex justify-content-between bg_pink">
                    <p class="m-0 fw-bolder theme_color3">TOTAL BILL FOR COMMON AREAS:&nbsp;&nbsp;&nbsp;&nbsp;</p>
                    <p class="m-0 fw-bolder theme_color3 " style="padding-left: 135px">{{ number_format($sum) }}</p>
                </div>
                <div style="margin-top: 3%">
                    <div class=" d-flex justify-content-between px-1">
                        <p class="m-0 fw-bolder theme_color3" style="margin-left: 1%">Total Reimbursement without
                            {{ $taxation_body }}:</p>
                        <p class="m-0 fw-bolder theme_color3" style="padding-left: 143px">{{ number_format($sum) }}</p>
                    </div>
                    <div class="pt-1 d-flex justify-content-between px-1" style="margin-top: 2%">
                        <p class="m-0 fw-bolder theme_color3" style="margin-left: 1%">{{ $taxation_body }} - Sales Tax
                            ({{ $tax_rate . '%' }})
                            of
                            Reimbursable Amount:&nbsp;&nbsp;&nbsp;</p>
                        <p class="m-0 fw-bolder theme_color3" style="padding-left: 5px">
                            {{ number_format(round($sum * ($tax_rate / 100))) }}</p>
                    </div>
                    <div class="mt-1 row row-cols-2 theme_color fw-bolder py-1 align-items-center mx-auto"
                        style="margin-top: 2%">
                        <p class="m-0 fw-bolder text-white" style="margin-left: 2%">TOTAL PAYABLE:</p>
                        <p class="m-0 fw-bolder text-white " style="padding-left: 193px">
                            {{ number_format(round($sum + ($tax_rate / 100) * $sum)) }}</p>
                    </div>
                </div>
                <div style="margin-left: 2%; margin-top: 3%;">
                    <div class="border border-1 border-black p-1 pb-0">
                        <p class=" fw-bolder text-dark fs-4"><i>Dear Resident,</i></p>
                        <p class=" text-dark "><i>Bill
                                can be paid either through Cross Cheques/Pay Orders in favor of
                                <i class=" fw-bolder text-dark ">"EMPIRE PROPERTIES"</i> or for
                                IBFT/Direct Transfers bank detail are as follows;</i></p>
                        <i>
                            <table style="break-inside: avoid; text-align: left;"
                                class="table table-borderless text-dark fw-bolder mt-4">
                                <thead>
                                    <tr>
                                        <th class=" table_dark_header" scope="col">A/C Title:</th>
                                        <th class=" table_dark_header" scope="col">Empire Properties</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <th class=" text-decoration-underline " scope="row">Account#</th>
                                        <td>(Br Code 0266) 010755944</td>
                                    </tr>
                                    <tr>
                                        <th class=" text-decoration-underline pe-0 py-0" scope="row">Bank Name & Br:
                                        </th>
                                        <td class="py-0">Meezan Bank Limited Mini Market Branch Lahore</td>
                                    </tr>
                                    <tr>
                                        <th class=" text-decoration-underline " scope="row">IBAN:</th>
                                        <td>PK62MEZN0002660107559944</td>
                                    </tr>
                                </tbody>
                            </table>
                        </i>
                    </div>
                </div>
            </div>
            <div class="margin-left-2">
                <div class="theme_color border border-2 text-center p-1 m-0 fw-bolder">
                    <span class=" text-white">PAID BILL HISTORY</span>
                </div>
                <table style="break-inside: avoid; margin-bottom: 0;" class="table table-hover table-bordered ">
                    <thead>
                        <tr>
                            <th style="background-color: #FFC719" scope="col">Month</th>
                            <th style="background-color: #FFC719" scope="col">Bill Amount</th>
                            <th style="background-color: #FFC719" scope="col">Bill Paid</th>
                            <th style="background-color: #FFC719" scope="col">Waived OFF</th>
                            <th style="background-color: #FFC719" scope="col">Balance</th>
                        </tr>
                    </thead>
                    <tbody>
                        @for ($i = 6; $i < 12; $i++)
                            <tr>
                                <th scope="row" style="font-size: 15px; white-space: nowrap !important;"
                                    class="jan">{{ $months[$i]['month_name'] }}-{{ $months[$i]['year'] - 1 }}</th>
                                <td class="text-center">
                                    {{ $months[$i]['balance'] == 0 ? '-' : number_format($months[$i]['balance']) }}
                                </td>
                                <td class="text-center">
                                    {{ $months[$i]['bill_paid'] == 0 ? '-' : $months[$i]['bill_paid'] }}</td>
                                <td class="text-danger fw-bolder text-center">
                                    {{ $months[$i]['waived_off'] == 0 ? '-' : $months[$i]['waived_off'] }}</td>
                                <td class="text-center">
                                    {{ $months[$i]['total'] == 0 ? '-' : number_format($months[$i]['total']) }}</td>
                            </tr>
                        @endfor

                        @for ($i = 0; $i < 6; $i++)
                            <tr>
                                <th scope="row" style="font-size: 15px; white-space: nowrap !important;"
                                    class="jan">{{ $months[$i]['month_name'] }}-{{ $months[$i]['year'] }}</th>
                                <td class="text-center">
                                    {{-- {{ $months[$i]['balance'] == 0 ? '-' : number_format($months[$i]['balance']) }} --}}
                                    {{ $months[$i]['balance'] == 0 ? '-' : '-' }}
                                </td>
                                <td class="text-center">
                                    {{-- {{ $months[$i]['bill_paid'] == 0 ? '-' : $months[$i]['bill_paid'] }}</td> --}}
                                    {{ $months[$i]['bill_paid'] == 0 ? '-' : '-' }}</td>
                                <td class="text-danger fw-bolder text-center">
                                    {{-- {{ $months[$i]['waived_off'] == 0 ? '-' : $months[$i]['waived_off'] }}</td> --}}
                                    {{ $months[$i]['waived_off'] == 0 ? '-' : '-' }}</td>
                                <td class="text-center">
                                    {{-- {{ $months[$i]['total'] == 0 ? '-' : number_format($months[$i]['total']) }}</td> --}}
                                    {{ $months[$i]['total'] == 0 ? '-' : '-' }}</td>
                            </tr>
                        @endfor
                    </tbody>
                </table>
            </div>
        </div>
        <div class=" w-90 mt-5">
            <p class=" fs-3 text-dark "><b>Notes:</b></p>
            <p class=" text-dark fw-bolder lh_custom">- The insurance policy is for the building only and does not
                cover
                personal effects and belongings such as
                but not limited to furniture, fixture and other personal valuables inside the apartment.</p>
            <p class="text-dark fw-bolder "> - As Pool facility
                is not active, common charges are being applied at reduced rates and are subject to change.</p>
            <p class="text-dark fw-bolder "> - Kindly pay
                your bill before due date. After due date a 10% Surcharge will apply.</p>
            <div class="mt-4">
                <p class="text-dark fw-bolder "><b class=" fs-5 text-dark ">CONTACT US:</b>
                    IN CASE OF ANY FURTHER DETAILS ABOUT YOUR BILL PLEASE CONTACT US ON THE BELOW MENTIONED
                    NUMBER/EMAIL:
                </p>
                <div class=" d-flex">
                    <p class="text-dark fw-bolder pe-5">CELL: +92 300 0454710 / +92 300 0454720</p>
                    <p class="text-dark fw-bolder ps-5">EMAIL: THEOPUSBILLING@GMAIL.COM</p>
                </div>
            </div>
        </div>
    </section>
    <footer class=" border-top">
        <p class=" text-center text-dark fw-bolder pt-1 text-decoration-underline ">This is a system generated bill and
            does not require signature unless altered.</p>
    </footer>
</body>

</html>
