<!DOCTYPE html>
<html>

<head>
    <meta name="viewport" content="width=device-width, initial-scale=1 text/html; charset=utf-8"
        http-equiv="Content-Type" />
    {{-- <link rel="stylesheet" href="{{asset('urdu-fonts/nafees-nastaleeq-webfont.eot')}}">
    <link rel="stylesheet" href="{{asset('urdu-fonts/nafees-nastaleeq-webfontd41d.eot?#iefix')}}">
    <link rel="stylesheet" href="{{asset('urdu-fonts/nafees-nastaleeq-webfont.woff')}}">
    <link rel="stylesheet" href="{{asset('urdu-fonts/Nafees-Nastaleeq.ttf')}}">
    <link rel="stylesheet" href="{{asset('urdu-fonts/nafees-nastaleeq-webfont.svg')}}"> --}}
    <title>
        {{ $property->city_name . '-' . $property->society_name . '-' . $property->block_name . '-' . $plan_name }}
    </title>
    <style>
        @font-face {
            font-family: 'Your custom font name';
            src: url(asset('urdu-fonts/nafees-nastaleeq-webfont.eot'));
            src: url(asset('urdu-fonts/nafees-nastaleeq-webfontd41d.eot?#iefix'));
            src: url(asset('urdu-fonts/nafees-nastaleeq-webfont.woff'));
            src: url(asset('urdu-fonts/Nafees-Nastaleeq.ttf'));
            src: url(asset('urdu-fonts/nafees-nastaleeq-webfont.svg'));
            font-weight: 400;
            font-style: normal;
        }

        .urdu,
        .select-urdu {

            font-family: 'Jameel Noori Nastaleeq', 'nafees-nastaleeq', 'nafees nastaleeq';
            text-align-last: center;
        }

        .select-urdu option {
            direction: rtl;
        }

        form .selected-urdu {
            font-family: 'Jameel Noori Nastaleeq', 'nafees-nastaleeq', 'nafees nastaleeq';
            direction: rtl;
        }

        * {
            box-sizing: border-box;
            background-color: white !important;
        }

        /* Create four equal columns that floats next to each other */
        .column {
            float: left;
            width: 25%;
            overflow: hidden;
            padding-right: 5px;
            /* Should be removed. Only for demonstration */
        }

        /* Clear floats after the columns */
        .row:after {
            content: "";
            display: table;
            clear: both;
        }

        .custom-font {
            font-weight: bold;
        }

        /* table.blueTable {
                background-color: #ffffff;
                width: 100%;
                text-align: left;
                border-collapse: collapse;
            } */

        table.blueTable td,
        table.blueTable th {
            border: 1px solid #020202;
            /* border-spacing: 0px; */

        }

        table {
            border: 1px solid #020202;
            /* border-collapse: collapse; */
        }

        table.blueTable tbody td {
            font-size: 11px;
        }

        table.blueTable tr:nth-child(even) {
            /* background: #ffffff; */
        }

        table.blueTable thead {
            background: #1c6ea4;
            background: -moz-linear-gradient(top, #5592bb 0%, #327cad 66%, #1c6ea4 100%);
            background: -webkit-linear-gradient(top, #5592bb 0%, #327cad 66%, #1c6ea4 100%);
            background: linear-gradient(to bottom, #5592bb 0%, #327cad 66%, #1c6ea4 100%);
            /* border-bottom: 2px solid #444444;*/
        }

        table th,
        table td {
            padding: 0.10rem !important;
        }

        table.blueTable thead th {
            font-size: 13px;
            font-weight: bold;
            /* color: #1a237e; */
            /* border-left: 2px solid #d0e4f5;*/
        }

        table.blueTable tfoot td {
            font-size: 12px;
        }

        table.blueTable tfoot .links {
            text-align: right;
        }
    </style>
</head>


<body>
    <div class="row">
        <div class="column" style="background-color: #aaa; ">
            <div class="flex">
                <div>
                    <img src="{{ asset($property->image) }}" alt="" width="50" height="50" />
                </div>
                <div style="left: 50px; position:  absolute; top: 5px;bottom: 20px;">
                    <h3 style="padding: 0;margin:0;padding-bottom: 15px;">
                        {{ optional($property)->bank_name }}
                    </h3>
                    <h6 class="urdu" style="margin: 0px; margin-right: 12px; margin-top: -11px; font-size:12px">
                        {{ optional($property)->branch_narration }}</h6>
                </div>
            </div>
            <table class="blueTable">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Account Title</td>
                        <td colspan="2">{{ optional($property)->account_title }}</td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap;">Account No</td>

                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ optional($property)->account_no }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" style="white-space: nowrap;">Challan No</td>
                        <td style="text-align: center;" class="custom-font">{{ $challan_no }}</td>
                        <td class="custom-font"
                            style="color: white; background-color: black !important; text-align: center;white-space: nowrap;">
                            Customer Copy
                        </td>
                    </tr>
                    <tr>
                        <td class="custom-font">NAME</td>
                        <td colspan="2" style="border-right-color: black;">{{ $prop->name }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">PLOT / HOUSE NO</td>
                        {{-- <td></td> --}}
                        <td colspan="2" class="custom-font" style="text-align: center;">Area</td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="2">
                            <p style="font-size:14px; margin:0;text-align: center;"> {{ $property->property_number }}
                            </p>
                            <p style="font-size:9px; margin:0">{{ $property->block_name }}</p>
                        </td>
                        {{-- <td></td> --}}
                        <td colspan="2" rowspan="2" style="text-align: center;" class="custom-font">
                            <p style="margin:0">
                                @if (!empty($property->landAreaKanal))
                                    {{ $property->landAreaKanal }} Kanal
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaMarla))
                                    {{ $property->landAreaMarla }} Marla
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaSqfeet))
                                    {{ $property->landAreaSqfeet }} Sqfeet
                                @endif
                            </p>
                        </td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size:small; padding:8px">Billing Month</td>
                        <td class="custom-font" style="font-size:medium; text-align: center;" colspan="2">
                            <b>{{ $hmonth }}-{{ $year }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;background-color: grey !important;" class="custom-font">Sr No.
                        </td>
                        <td style="background-color: grey !important;white-space: nowrap;" class="custom-font">Detail of
                            Charges</td>
                        <td style="text-align: center; background-color: grey !important;" class="custom-font">
                            Amount</td>
                    </tr>
                    <?php $sum1 = 0;
                    $i = 1; ?>
                    @foreach ($data as $index => $pay)
                        <tr>
                            <td style="text-align: center;" class="custom-font">{{ $i++ }}</td>
                            <td style="padding:2px 10px !important;">{{ $pay['name'] }}</td>
                            <td style="text-align: right;">{{ number_format((float) $pay['amount'], 2, '.', '') }}</td>
                    @endforeach
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">Total
                            Amount</td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount) }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">Arrears
                        </td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date - $total_amount) }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Issue Date</td>
                        <td style="text-align: center;" class="custom-font">
                            {{ $issue_date }}</td>
                        <!-- <td style="text-align: center;" class="custom-font">
                            {{ ($issueDate = \Carbon\Carbon::now()->startOfMonth()->addMonth())->format('d-m-Y') }}</td> -->

                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Due Date</td>
                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $due_date }}</td>
                        <!-- <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $issueDate->addDays(9)->format('d-m-Y') }}</td> -->
                    </tr>
                    <tr>
                        <?php
                        $amount_in_words1 = new \Riskihajar\Terbilang\Terbilang();
                        $amount_in_words1 = $amount_in_words1->make($total_amount_within_due_date); ?>
                        <td colspan="3">{{ ucwords($amount_in_words1) . ' Rupees Only ' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable within Due Date</td>
                        <td colspan="2" style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date) }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable after Due Date</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($total_amount_within_due_date + $total_amount_after_due_date) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="margin-top:10px;" class="flex">
                <table style=" border: none">
                    <tr>
                        <td style="font-size:xx-small">DEPOSITOR'S <br> SIGNATURE</td>
                        <td style="font-size:small" class="custom-font">SOCIETY SIGN & STAMP</td>
                    </tr>
                </table>
                <div>
                    <p style="margin:0;"><b>Disclaimer:</b>Errors and Omissions are rectifiable.</p>
                    <p style="margin:0;">This is Water Bill Only.</p>
                </div>
            </div>
        </div>
        <div class="column" style="background-color: #aaa; ">
            <div class="flex">
                <div>
                    <img src="{{ asset($property->image) }}" alt="" width="50" height="50" />
                </div>
                <div style="left: 50px; position:  absolute; top: 5px;bottom: 20px;">
                    <h3 style="padding: 0;margin:0;padding-bottom: 15px;">
                        {{ optional($property)->bank_name }}
                    </h3>
                    <h6 class="urdu" style="margin: 0px; margin-right: 12px; margin-top: -11px; font-size:12px">
                        {{ optional($property)->branch_narration }}</h6>
                </div>
            </div>
            <table class="blueTable">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Account Title</td>
                        <td colspan="2">{{ optional($property)->account_title }}</td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap;">Account No</td>

                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ optional($property)->account_no }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" style="white-space: nowrap;">Challan No</td>
                        <td style="text-align: center;" class="custom-font">{{ $challan_no }}</td>
                        <td class="custom-font"
                            style="color: white; background-color: black !important; text-align: center;white-space: nowrap;">
                            Bank Copy</td>
                    </tr>
                    <tr>
                        <td class="custom-font">NAME</td>
                        <td colspan="2" style="border-right-color: black;">{{ $prop->name }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">PLOT / HOUSE NO</td>
                        {{-- <td></td> --}}
                        <td colspan="2" class="custom-font" style="text-align: center;">Area</td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="2">
                            <p style="font-size:14px; margin:0;text-align: center;"> {{ $property->property_number }}
                            </p>
                            <p style="font-size:9px; margin:0">{{ $property->block_name }}</p>
                        </td>
                        {{-- <td></td> --}}
                        <td colspan="2" rowspan="2" style="text-align: center;" class="custom-font">
                            <p style="margin:0">
                                @if (!empty($property->landAreaKanal))
                                    {{ $property->landAreaKanal }} Kanal
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaMarla))
                                    {{ $property->landAreaMarla }} Marla
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaSqfeet))
                                    {{ $property->landAreaSqfeet }} Sqfeet
                                @endif
                            </p>
                        </td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size:small; padding:8px">Billing Month</td>
                        <td class="custom-font" style="font-size:medium; text-align: center;" colspan="2">
                            <b>{{ $hmonth }}-{{ $year }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;background-color: grey !important;" class="custom-font">Sr No.
                        </td>
                        <td style="background-color: grey !important;white-space: nowrap;" class="custom-font">Detail
                            of Charges</td>
                        <td style="text-align: center; background-color: grey !important;" class="custom-font">
                            Amount</td>
                    </tr>
                    <?php $sum1 = 0;
                    $i = 1; ?>
                    @foreach ($data as $index => $pay)
                        <tr>
                            <td style="text-align: center;" class="custom-font">{{ $i++ }}</td>
                            <td style="padding:2px 10px !important;">{{ $pay['name'] }}</td>
                            <td style="text-align: right;">{{ number_format((float) $pay['amount'], 2, '.', '') }}
                            </td>
                    @endforeach
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">
                            Total
                            Amount</td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount) }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">
                            Arrears
                        </td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date - $total_amount) }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Issue Date</td>
                        <td style="text-align: center;" class="custom-font">
                            {{ $issue_date }}</td>
                        <!-- <td style="text-align: center;" class="custom-font">
                        {{ ($issueDate = \Carbon\Carbon::now()->startOfMonth()->addMonth())->format('d-m-Y') }}</td> -->

                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Due Date</td>
                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $due_date }}</td>
                        <!-- <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $issueDate->addDays(9)->format('d-m-Y') }}</td> -->
                    </tr>
                    <tr>
                        <?php
                        $amount_in_words1 = new \Riskihajar\Terbilang\Terbilang();
                        $amount_in_words1 = $amount_in_words1->make($total_amount_within_due_date); ?>
                        <td colspan="3">{{ ucwords($amount_in_words1) . ' Rupees Only ' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable within Due Date</td>
                        <td colspan="2" style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date) }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable after Due Date</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($total_amount_within_due_date + $total_amount_after_due_date) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="margin-top:10px;" class="flex">
                <table style=" border: none">
                    <tr>
                        <td style="font-size:xx-small">DEPOSITOR'S <br> SIGNATURE</td>
                        <td style="font-size:small" class="custom-font">SOCIETY SIGN & STAMP</td>
                    </tr>
                </table>
                <div>
                    <p style="margin:0;"><b>Disclaimer:</b>Errors and Omissions are rectifiable.</p>
                    <p style="margin:0;">This is Water Bill Only.</p>
                </div>
            </div>
        </div>
        <div class="column" style="background-color: #aaa; ">
            <div class="flex">
                <div>
                    <img src="{{ asset($property->image) }}" alt="" width="50" height="50" />
                </div>
                <div style="left: 50px; position:  absolute; top: 5px;bottom: 20px;">
                    <h3 style="padding: 0;margin:0;padding-bottom: 15px;">
                        {{ optional($property)->bank_name }}
                    </h3>
                    <h6 class="urdu" style="margin: 0px; margin-right: 12px; margin-top: -11px; font-size:12px">
                        {{ optional($property)->branch_narration }}</h6>
                </div>
            </div>
            <table class="blueTable">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Account Title</td>
                        <td colspan="2">{{ optional($property)->account_title }}</td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap;">Account No</td>

                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ optional($property)->account_no }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" style="white-space: nowrap;">Challan No</td>
                        <td style="text-align: center;" class="custom-font">{{ $challan_no }}</td>
                        <td class="custom-font"
                            style="color: white; background-color: black !important; text-align: center;white-space: nowrap;">
                            Headoffice
                            Copy</td>
                    </tr>
                    <tr>
                        <td class="custom-font">NAME</td>
                        <td colspan="2" style="border-right-color: black;">{{ $prop->name }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">PLOT / HOUSE NO</td>
                        {{-- <td></td> --}}
                        <td colspan="2" class="custom-font" style="text-align: center;">Area</td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="2">
                            <p style="font-size:14px; margin:0;text-align: center;"> {{ $property->property_number }}
                            </p>
                            <p style="font-size:9px; margin:0">{{ $property->block_name }}</p>
                        </td>
                        {{-- <td></td> --}}
                        <td colspan="2" rowspan="2" style="text-align: center;" class="custom-font">
                            <p style="margin:0">
                                @if (!empty($property->landAreaKanal))
                                    {{ $property->landAreaKanal }} Kanal
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaMarla))
                                    {{ $property->landAreaMarla }} Marla
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaSqfeet))
                                    {{ $property->landAreaSqfeet }} Sqfeet
                                @endif
                            </p>
                        </td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size:small; padding:8px">Billing Month</td>
                        <td class="custom-font" style="font-size:medium; text-align: center;" colspan="2">
                            <b>{{ $hmonth }}-{{ $year }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;background-color: grey !important;" class="custom-font">Sr No.
                        </td>
                        <td style="background-color: grey !important;white-space: nowrap;" class="custom-font">Detail
                            of Charges</td>
                        <td style="text-align: center; background-color: grey !important;" class="custom-font">
                            Amount</td>
                    </tr>
                    <?php $sum1 = 0;
                    $i = 1; ?>
                    @foreach ($data as $index => $pay)
                        <tr>
                            <td style="text-align: center;" class="custom-font">{{ $i++ }}</td>
                            <td style="padding:2px 10px !important;">{{ $pay['name'] }}</td>
                            <td style="text-align: right;">{{ number_format((float) $pay['amount'], 2, '.', '') }}
                            </td>
                    @endforeach
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">
                            Total
                            Amount</td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount) }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">
                            Arrears
                        </td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date - $total_amount) }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Issue Date</td>
                        <td style="text-align: center;" class="custom-font">
                            {{ $issue_date }}</td>
                        <!-- <td style="text-align: center;" class="custom-font">
                        {{ ($issueDate = \Carbon\Carbon::now()->startOfMonth()->addMonth())->format('d-m-Y') }}</td> -->

                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Due Date</td>
                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $due_date }}</td>
                        <!-- <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $issueDate->addDays(9)->format('d-m-Y') }}</td> -->
                    </tr>
                    <tr>
                        <?php
                        $amount_in_words1 = new \Riskihajar\Terbilang\Terbilang();
                        $amount_in_words1 = $amount_in_words1->make($total_amount_within_due_date); ?>
                        <td colspan="3">{{ ucwords($amount_in_words1) . ' Rupees Only ' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable within Due Date</td>
                        <td colspan="2" style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date) }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable after Due Date</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($total_amount_within_due_date + $total_amount_after_due_date) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="margin-top:10px;" class="flex">
                <table style=" border: none">
                    <tr>
                        <td style="font-size:xx-small">DEPOSITOR'S <br> SIGNATURE</td>
                        <td style="font-size:small" class="custom-font">SOCIETY SIGN & STAMP</td>
                    </tr>
                </table>
                <div>
                    <p style="margin:0;"><b>Disclaimer:</b>Errors and Omissions are rectifiable.</p>
                    <p style="margin:0;">This is Water Bill Only.</p>
                </div>
            </div>
        </div>
        <div class="column" style="background-color: #aaa; ">
            <div class="flex">
                <div>
                    <img src="{{ asset($property->image) }}" alt="" width="50" height="50" />
                </div>
                <div style="left: 50px; position:  absolute; top: 5px;bottom: 20px;">
                    <h3 style="padding: 0;margin:0;padding-bottom: 15px">
                        {{ optional($property)->bank_name }}
                    </h3>
                    <h6 class="urdu" style="margin: 0px; margin-right: 12px; margin-top: -11px; font-size:12px">
                        {{ optional($property)->branch_narration }}</h6>
                </div>
            </div>
            <table class="blueTable">
                <thead></thead>
                <tbody>
                    <tr>
                        <td>Account Title</td>
                        <td colspan="2">{{ optional($property)->account_title }}</td>
                    </tr>
                    <tr>
                        <td style="white-space: nowrap;">Account No</td>

                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ optional($property)->account_no }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" style="white-space: nowrap;">Challan No</td>
                        <td style="text-align: center;" class="custom-font">{{ $challan_no }}</td>
                        <td class="custom-font"
                            style="color: white; background-color: black !important; text-align: center;white-space: nowrap;">
                            Site Office
                            Copy</td>
                    </tr>
                    <tr>
                        <td class="custom-font">NAME</td>
                        <td colspan="2" style="border-right-color: black;">{{ $prop->name }}</td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">PLOT / HOUSE NO</td>
                        {{-- <td></td> --}}
                        <td colspan="2" class="custom-font" style="text-align: center;">Area</td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>
                        <td colspan="2" rowspan="2">
                            <p style="font-size:14px; margin:0;text-align: center;"> {{ $property->property_number }}
                            </p>
                            <p style="font-size:9px; margin:0">{{ $property->block_name }}</p>
                        </td>
                        {{-- <td></td> --}}
                        <td colspan="2" rowspan="2" style="text-align: center;" class="custom-font">
                            <p style="margin:0">
                                @if (!empty($property->landAreaKanal))
                                    {{ $property->landAreaKanal }} Kanal
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaMarla))
                                    {{ $property->landAreaMarla }} Marla
                                @endif
                            </p>
                            <p style="margin:0">
                                @if (!empty($property->landAreaSqfeet))
                                    {{ $property->landAreaSqfeet }} Sqfeet
                                @endif
                            </p>
                        </td>
                        {{-- <td></td> --}}
                    </tr>
                    <tr>

                    </tr>
                    <tr>
                        <td colspan="2" style="text-align: center; font-size:small; padding:8px">Billing Month</td>
                        <td class="custom-font" style="font-size:medium; text-align: center;" colspan="2">
                            <b>{{ $hmonth }}-{{ $year }}</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: center;background-color: grey !important;" class="custom-font">Sr No.
                        </td>
                        <td style="background-color: grey !important;white-space: nowrap;" class="custom-font">Detail
                            of Charges</td>
                        <td style="text-align: center; background-color: grey !important;" class="custom-font">
                            Amount</td>
                    </tr>
                    <?php $sum1 = 0;
                    $i = 1; ?>
                    @foreach ($data as $index => $pay)
                        <tr>
                            <td style="text-align: center;" class="custom-font">{{ $i++ }}</td>
                            <td style="padding:2px 10px !important;">{{ $pay['name'] }}</td>
                            <td style="text-align: right;">{{ number_format((float) $pay['amount'], 2, '.', '') }}
                            </td>
                    @endforeach
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">
                            Total
                            Amount</td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount) }}</td>
                    </tr>
                    <tr>
                        <td class="custom-font" colspan="2" style="text-align: center; letter-spacing: 1px; ">
                            Arrears
                        </td>
                        <td style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date - $total_amount) }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Issue Date</td>
                        <td style="text-align: center;" class="custom-font">
                            {{ $issue_date }}</td>
                        <!-- <td style="text-align: center;" class="custom-font">
                        {{ ($issueDate = \Carbon\Carbon::now()->startOfMonth()->addMonth())->format('d-m-Y') }}</td> -->

                    </tr>
                    <tr>
                        <td colspan="2" class="custom-font">Due Date</td>
                        <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $due_date }}</td>
                        <!-- <td colspan="2" style="text-align: center;" class="custom-font">
                            {{ $issueDate->addDays(9)->format('d-m-Y') }}</td> -->
                    </tr>
                    <tr>
                        <?php
                        $amount_in_words1 = new \Riskihajar\Terbilang\Terbilang();
                        $amount_in_words1 = $amount_in_words1->make($total_amount_within_due_date); ?>
                        <td colspan="3">{{ ucwords($amount_in_words1) . ' Rupees Only ' }}</td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable within Due Date</td>
                        <td colspan="2" style="text-align: right;" class="custom-font">
                            {{ number_format($total_amount_within_due_date) }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">Payable after Due Date</td>
                        <td colspan="2" style="text-align: right;">
                            {{ number_format($total_amount_within_due_date + $total_amount_after_due_date) }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div style="margin-top:10px;" class="flex">
                <table style=" border: none">
                    <tr>
                        <td style="font-size:xx-small">DEPOSITOR'S <br> SIGNATURE</td>
                        <td style="font-size:small" class="custom-font">SOCIETY SIGN & STAMP</td>
                    </tr>
                </table>
                <div>
                    <p style="margin:0;"><b>Disclaimer:</b>Errors and Omissions are rectifiable.</p>
                    <p style="margin:0;">This is Water Bill Only.</p>
                </div>
            </div>
        </div>
    </div>
</body>


</html>
