@extends('admin.layouts.master')

@section('title', 'Dashboard')
<style>
    #bill_pie_chart {
        width: 70% !important;
        height: 90% !important;
    }

    .chart_legend_item {
        white-space: nowrap;
    }

    .legend-color-box {
        width: 20px;
        height: 20px;
        margin-right: 8px;
    }

    .card-subtitle {
        cursor: default;
    }

    tbody>tr>td {
        text-align: center;
    }

    .child>.child {
        text-align: start !important;
    }
</style>
@section('content')
    <section class="content-wrapper container-xxl p-0">
        {{-- <div class="content-header row"></div> --}}
        <div class="content-body">
            <h1>Dashboard</h1>
            <div class=" row m-xxl-1 justify-content-md-around m-0">
                <div class="px-1 col-xxl-3 col-md-5">
                    <div class="card card-statistics">
                        <div class="card-header pb-1">
                            <h4 class="card-title">Total Properties</h4>
                        </div>
                        <div class="card-body statistics-body">
                            <div class="row">
                                <div class="">
                                    <div class="d-flex flex-row">
                                        <div class="avatar bg-light-info me-2">
                                            <div class="avatar-content">
                                                <a href="#" style="color: #00cfe8 !important;">
                                                    {{-- <i data-feather="user" class="avatar-icon"></i> --}}
                                                    <i class="fa fa-building fs-3 " aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="my-auto">
                                            <h4 class="fw-bolder mb-0">{{ $total_properties }}</h4>
                                            <p class="card-text font-small-3 mb-0">Properties</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class=" col-xxl-6 col-md-12 d-none d-xl-block">
                    <div class="card">
                        <div class="card-body row row-cols-2 p-1">
                            <div class="pe-1">
                                <h5 class="card-title text-center">{{ date('F-Y') }}</h5>
                                {{-- <h6 class="card-subtitle mb-1 text-body-secondary text-center">( <span
                                        title="Total Amount Within Due Date">PKR {{ explode('-', ) }}/-</span> )</h6> --}}
                                <h6 class="card-subtitle mb-1 text-body-secondary text-center">( <span
                                        title="Total Amount Within Due Date">PKR
                                        {{ $current_month_total_amount_within_due_date }}/-</span> ) + ( <span
                                        title="Total Amount After Due Date">PKR
                                        {{ $current_month_total_amount_after_due_date }}/-</span> )</h6>
                                <hr>
                                <div class="card-text">
                                    <div class=" d-flex mb-1">
                                        <h4 class="text-success">Paid:</h4>&nbsp;<span class=" fs-5">PKR
                                            {{ $current_month_total_paid_amount }}/-</span>
                                    </div>
                                    <div class=" d-flex">
                                        <h4 class="" style="color: rgb(246, 136, 136)">
                                            {{-- {{ $current_month_total_outstanding_amount >= 0 ? 'Outstanding:' : 'Advance:' }} --}}
                                            Outstanding:
                                        </h4>&nbsp;<span class=" fs-5">PKR
                                            {{-- {{ abs($current_month_total_outstanding_amount) }}/-</span> --}}
                                            {{ $current_month_total_outstanding_amount }}/-</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ps-1">
                                <h5 class="card-title text-center">Arrears</h5>
                                <h6 class="card-subtitle mb-1 text-body-secondary text-center">( <span
                                        title="Total Arrears Amount Within Due Date">PKR
                                        {{ $arrears_total_amount_within_due_date }}/-</span> ) + ( <span
                                        title="Total Arrears Amount After Due Date">PKR
                                        {{ $arrears_total_amount_after_due_date }}/-</span> )</h6>
                                <hr>
                                <div class="card-text">
                                    <div class=" d-flex mb-1">
                                        <h4 class="text-success">Paid:</h4>&nbsp;<span class=" fs-5">PKR
                                            {{ $arrears_total_paid_amount }}/-</span>
                                    </div>
                                    <div class=" d-flex">
                                        <h4 class="" style="color: rgb(246, 136, 136)">
                                            {{-- {{ $arrears_total_outstanding_amount >= 0 ? 'Outstanding:' : 'Advance:' }}</h4> --}}
                                            Outstanding:</h4>
                                        &nbsp;<span class=" fs-5">PKR
                                            {{ $arrears_total_outstanding_amount }}/-</span>
                                        {{-- {{ abs($arrears_total_outstanding_amount) }}/-</span> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xxl-3 col-md-5 d-flex" id="chart-container">
                    <canvas class="me-2 ms-0" id="bill_pie_chart" role="img" aria-label="Bill Doughnut Chart"></canvas>
                    <div class="chart_legend my-auto" id="chart_legend"></div>
                </div>
                <div class=" col-xxl-6 col-md-12 d-md-block d-xl-none">
                    <div class="card">
                        <div class="card-body row row-cols-2 p-1">
                            <div class="pe-1">
                                <h5 class="card-title text-center">{{ date('F-Y') }}</h5>
                                <h6 class="card-subtitle mb-1 text-body-secondary text-center">( <span
                                        title="Total Amount Within Due Date">PKR
                                        {{ $current_month_total_amount_within_due_date }}/-</span> ) + ( <span
                                        title="Total Amount After Due Date">PKR
                                        {{ $current_month_total_amount_after_due_date }}/-</span> )</h6>
                                <hr>
                                <div class="card-text">
                                    <div class=" d-flex mb-1">
                                        <h4 class="text-success">Paid:</h4>&nbsp;<span class=" fs-5">PKR
                                            {{ $current_month_total_paid_amount }}/-</span>
                                    </div>
                                    <div class=" d-flex">
                                        <h4 class="" style="color: rgb(246, 136, 136)">Outstanding:</h4>&nbsp;<span
                                            class=" fs-5">PKR {{ $current_month_total_outstanding_amount }}/-</span>
                                    </div>
                                </div>
                            </div>
                            <div class="ps-1">
                                <h5 class="card-title text-center">Arrears</h5>
                                <h6 class="card-subtitle mb-1 text-body-secondary text-center">( <span
                                        title="Total Arrears Amount Within Due Date">PKR
                                        {{ $arrears_total_amount_within_due_date }}/-</span> ) + ( <span
                                        title="Total Arrears Amount After Due Date">PKR
                                        {{ $arrears_total_amount_after_due_date }}/-</span> )</h6>
                                <hr>
                                <div class="card-text">
                                    <div class=" d-flex mb-1">
                                        <h4 class="text-success">Paid:</h4>&nbsp;<span class=" fs-5">PKR
                                            {{ $arrears_total_paid_amount }}/-</span>
                                    </div>
                                    <div class=" d-flex">
                                        <h4 class="" style="color: rgb(246, 136, 136)">
                                            {{-- {{ $arrears_total_outstanding_amount >= 0 ? 'Outstanding:' : 'Advance:' }}</h4> --}}
                                            Outstanding:</h4>
                                        &nbsp;<span class=" fs-5">PKR
                                            {{-- {{ abs($arrears_total_outstanding_amount) }}/-</span> --}}
                                            {{ $arrears_total_outstanding_amount }}/-</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div>
                <canvas class="w-100 h-75" id="balance_chart" role="img" aria-label="Yearly Balance Chart"></canvas>
            </div>
            <div class="row mt-3">
                <div class="col-12">
                    <div class="card pb-2">
                        <table class="datatables-basic table" id="dataTable">
                            <thead>
                                <tr>
                                    <th class="not_include"></th>
                                    <th class="text-center">id</th>
                                    <th class="text-center">Property Number</th>
                                    <th class="text-center">Name (Phone No.)</th>
                                    <th class="text-center">January</th>
                                    <th class="text-center">February</th>
                                    <th class="text-center">March</th>
                                    <th class="text-center">April</th>
                                    <th class="text-center">May</th>
                                    <th class="text-center">June</th>
                                    <th class="text-center">July</th>
                                    <th class="text-center">August</th>
                                    <th class="text-center">September</th>
                                    <th class="text-center">October</th>
                                    <th class="text-center">November</th>
                                    <th class="text-center">December</th>
                                    <th class="text-center">Total Paid Amount</th>
                                    <th class="text-center">Total Outstanding Amount</th>
                                </tr>
                            </thead>
                            <tbody></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
@section('custom-scripts-foot')
    <script>
        $(document).ready(function() {

            const balance_chart = document.getElementById('balance_chart').getContext('2d');
            const data = {
                labels: [
                    'January', 'February', 'March', 'April', 'May', 'June',
                    'July', 'August', 'September', 'October', 'November', 'December'
                ],
                datasets: [{
                        label: 'Total',
                        data: [
                            @foreach ($monthly_total_amounts as $monthly_total_amount)
                                {{ $monthly_total_amount }},
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(0, 0, 255, 0.2)', 'rgba(0, 0, 238, 0.3)', 'rgba(0, 0, 221, 0.4)',
                            'rgba(0, 0, 204, 0.5)', 'rgba(0, 0, 187, 0.4)', 'rgba(0, 0, 170, 0.3)',
                            'rgba(0, 0, 153, 0.2)', 'rgba(0, 0, 136, 0.5)', 'rgba(0, 0, 119, 0.4)',
                            'rgba(0, 0, 102, 0.3)', 'rgba(0, 0, 85, 0.2)', 'rgba(0, 0, 68, 0.5)'
                        ],
                        borderColor: [
                            'rgb(0, 0, 255)', 'rgb(0, 0, 238)', 'rgb(0, 0, 221)', 'rgb(0, 0, 204)',
                            'rgb(0, 0, 187)', 'rgb(0, 0, 170)',
                            'rgb(0, 0, 153)', 'rgb(0, 0, 136)', 'rgb(0, 0, 119)', 'rgb(0, 0, 102)',
                            'rgb(0, 0, 85)', 'rgb(0, 0, 68)'
                        ],
                        borderWidth: 2,
                        borderRadius: 8
                    },
                    {
                        label: 'Paid',
                        data: [
                            @foreach ($all_months_total_paid_amounts as $all_months_total_paid_amount)
                                {{ $all_months_total_paid_amount }},
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(0, 148, 0, 0.4)', 'rgba(0, 188, 0, 0.3)', 'rgba(0, 200, 0, 0.4)',
                            'rgba(0, 225, 0, 0.4)', 'rgba(0, 118, 0, 0.4)', 'rgba(0, 100, 0, 0.4)',
                            'rgba(0, 160, 30, 0.5)', 'rgb(0, 180, 60, 0.5)', 'rgba(0, 200, 80, 0.5)',
                            'rgba(0, 250, 10, 0.4)', 'rgba(0, 250, 40, 0.5)', 'rgba(0, 225, 70, 0.5)'
                        ],
                        borderColor: [
                            'rgb(0, 148, 0)', 'rgb(0, 188, 0)', 'rgb(0, 200, 0)', 'rgb(0, 225, 0)',
                            'rgb(0, 128, 0)', 'rgb(0, 100, 0)',
                            'rgb(0, 160, 30)', 'rgb(0, 180, 60)', 'rgb(0, 200, 80)', 'rgb(0, 250, 10)',
                            'rgb(0, 250, 40)', 'rgb(0, 225, 70)'
                        ],
                        borderWidth: 2,
                        borderRadius: 8
                    },
                    {
                        label: 'Outstanding',
                        data: [
                            @foreach ($all_months_total_outstanding_amounts as $all_months_total_outstanding_amount)
                                {{ $all_months_total_outstanding_amount }},
                            @endforeach
                        ],
                        backgroundColor: [
                            'rgba(242, 15, 15, 0.4)', 'rgba(250, 40, 40, 0.5)',
                            'rgba(252, 66, 66, 0.5)', 'rgba(232, 77, 77, 0.5)',
                            'rgba(250, 102, 102, 0.5)', 'rgba(193, 60, 60, 0.5)',
                            'rgba(148, 24, 24, 0.5)', 'rgba(252, 96, 96, 0.5)',
                            'rgba(217, 11, 11, 0.4)', 'rgba(242, 15, 15, 0.5)', 'rgba(212, 19, 8, 0.4)',
                            'rgba(255, 0, 0, 0.2)'
                        ],
                        borderColor: [
                            'rgba(242, 15, 15)', 'rgb(250, 40, 40)', 'rgb(252, 66, 66)',
                            'rgb(232, 77, 77)', 'rgb(250, 102, 102)', 'rgb(163, 60, 60)',
                            'rgb(168, 24, 24)', 'rgb(252, 96, 96)', 'rgb(217, 11, 11)',
                            'rgb(242, 15, 15)', 'rgb(212, 19, 8)', 'rgb(255, 0, 0)'
                        ],
                        borderWidth: 2,
                        borderRadius: 8
                    }
                ]
            };

            new Chart(balance_chart, {
                type: 'bar',
                data: data,
                options: {
                    responsive: true,
                    maintainAspectRatio: false,
                    scales: {
                        y: {
                            beginAtZero: true,
                            grid: {
                                color: 'rgba(128, 128, 128, 0.3)'
                            },
                            ticks: {
                                color: 'rgba(112, 112, 112, 0.9)'
                            }
                        },
                        x: {
                            grid: {
                                color: 'rgba(128, 128, 128, 0.3)'
                            },
                            ticks: {
                                color: 'rgb(112,112,112, 1)'
                            }
                        }
                    },
                    plugins: {
                        legend: {
                            position: 'top',
                            title: {
                                display: true,
                                text: 'Yearly Balance Chart',
                                color: 'rgb(144,144,144)',
                                font: {
                                    size: 25,
                                    weight: 'normal'
                                }
                            },
                            labels: {
                                color: 'rgb(144,144,144)',
                                font: {
                                    size: 14,
                                    weight: 'bold'
                                }
                            }
                        },
                        tooltip: {
                            callbacks: {
                                title: function(tooltipItems) {
                                    return tooltipItems[0].label;
                                },
                                label: function(tooltipItem) {
                                    const total = data.datasets[0].data[tooltipItem.dataIndex];
                                    const paid = data.datasets[1].data[tooltipItem.dataIndex];
                                    const outstanding = data.datasets[2].data[tooltipItem.dataIndex];
                                    return [
                                        `Total: ${total}`,
                                        `Paid: ${paid}`,
                                        `Outstanding: ${outstanding}`
                                    ];
                                }
                            }
                        }
                    }
                }
            });
            const bill_pie_chart = document.getElementById('bill_pie_chart').getContext('2d');

            const pie_chart = new Chart(bill_pie_chart, {
                type: 'pie',
                data: {
                    labels: [
                        'Paid',
                        'Partially Paid',
                        'Unpaid'
                    ],
                    datasets: [{
                        label: 'Total Bills',
                        data: [
                            {{ $total_paid_bills }},
                            {{ $total_partially_paid_bills }},
                            {{ $total_unpaid_bills }}
                        ],
                        backgroundColor: [
                            'rgba(1, 135, 73, 0.7)',
                            'rgba(255, 193, 7, 0.7)',
                            'rgba(220, 53, 69, 0.7)'
                        ],
                        borderColor: [
                            '#018749',
                            '#ffc107',
                            '#dc3545'
                        ],
                        borderWidth: 4,
                        hoverOffset: 3
                    }]
                },
                options: {
                    plugins: {
                        legend: {
                            display: false,
                        },
                        title: {
                            display: true,
                            text: 'Bills Pie Chart',
                            color: 'rgb(144,144,144)',
                            font: {
                                size: 22,
                                weight: 'normal'
                            },
                            padding: {
                                top: 0,
                            }
                        }
                    }
                }
            });

            const legendContainer = document.getElementById('chart_legend');
            pie_chart.data.labels.forEach((label,
                index) => {
                const legendItem = document.createElement('div');
                legendItem.classList.add('chart_legend_item', 'd-flex', 'mb-1');
                const colorBox = document.createElement('div');
                colorBox.classList.add('legend-color-box');
                colorBox.style.backgroundColor = pie_chart.data.datasets[0].backgroundColor[index];
                legendItem.appendChild(colorBox);
                const labelText = document.createTextNode(label);
                legendItem.appendChild(labelText);
                legendContainer.appendChild(legendItem);
            });

            $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ordering: true,
                ajax: '{{ route('dashboard') }}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        searchable: false
                    },
                    {
                        data: 'property_number'
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            return `${data.name} (${data.phone_number})`;
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '01';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '02';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '03';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '04';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '05';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '06';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '07';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '08';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '09';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '10';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '11';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            const billingMonth = '12';
                            const billingData = full.billing_month_is_paid;

                            if (billingMonth in billingData) {
                                return billingData[billingMonth] == 1 ?
                                    '<span class="badge bg-success">Bill Paid</span>' :
                                    '<span class="badge bg-danger">Bill Unpaid</span>';
                            } else {
                                return '<span class="badge bg-secondary">No Bill Available</span>';
                            }
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            return `PKR ${data.total_paid_amount.toFixed(2)}/-`;
                        }
                    },
                    {
                        data: null,
                        render: function(data, type, full) {
                            return `(<span>${(data.total_amount_within_due_date - data.total_paid_amount)}</span> + <span title="Total Amount After Due Date">${data.total_amount_after_due_date}</span>)`;
                        }
                    }
                ],
                "columnDefs": [{
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-1',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                }],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Properties</h6>');
        });

        setTimeout(function() {
            toastr['success'](
                'You have successfully logged in.',
                '👋 Welcome {{ Auth::user()->name }}!', {
                    closeButton: true,
                    tapToDismiss: false
                }
            );
        }, 2000);
    </script>
@endsection
