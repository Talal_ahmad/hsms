@extends('admin.layouts.master')
@section('title', 'Bank')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Bank</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Bank
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>id</th>
                                <th>Bank Name</th>
                                <th>GL</th>
                                <th>Branch</th>
                                <th>Account Title</th>
                                <th>Account Number</th>
                                <th>City</th>
                                <th>Society</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Bank</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_add_form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="bank_name">Bank Name</label>
                                        <input type="text" class="form-control dt-full-name" id="bank_name"
                                            placeholder="Enter Bank Name" name="bank_name" aria-label="bank_name" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="branch">Branch Name</label>
                                        <input type="text" class="form-control dt-full-name" id="branch"
                                            placeholder="Enter Branch Name" name="branch" aria-label="branch" />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="branch_phone_no">Branch Phone N0:</label>
                                    <div class="input-group input-group-merge">
                                        <input type="text" class="form-control" maxlength="10"
                                            placeholder="Enter Branch Phone No:" name="branch_phone_no"
                                            id="branch_phone_no" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="branch_address">Branch Address</label>
                                        <input type="text" class="form-control dt-full-name" id="branch_address"
                                            placeholder="Enter Branch Address" name="branch_address"
                                            aria-label="branch_address" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="branch_narration">Narration on Bill</label>
                                        <input type="text" class="form-control dt-full-name" id="branch_narration"
                                            placeholder="Enter Branch Narration" name="branch_narration"
                                            aria-label="branch_narration" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="account_title">Account Title</label>
                                        <input type="text" class="form-control dt-full-name" id="account_title"
                                            placeholder="Enter Account Title" name="account_title"
                                            aria-label="account_title" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="account_no">Account Number</label>
                                        <input type="text" class="form-control dt-full-name" id="account_no"
                                            placeholder="Enter Account Number" name="account_no"
                                            aria-label="account_no" />
                                    </div>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic">Select GL</label>
                                    <select class="select2 select form-select my_select" name="gl_no"
                                        id="select2-basic">
                                        @foreach ($gl_code as $gl)
                                            <option value = "{{ $gl->account_code . '|' . $gl->account_name }}">
                                                {{ $gl->account_code . ' ' . $gl->account_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic1">City</label>
                                    <select class="select2 select form-select my_select" name="city_id"
                                        id="select2-basic1">
                                        @foreach ($cities as $city)
                                            <option id="{{ $city->id }}" value="{{ $city->id }}">
                                                {{ $city->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic2">Society</label>
                                    <select class="select2 select form-select my_select" name="society_id"
                                        id="select2-basic2">
                                        @foreach ($societies as $society)
                                            <option id="{{ $society->id }}" value="{{ $society->id }}">
                                                {{ $society->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="image">Select Image:</label>
                                        <input type="file" name="image" id="image" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal to update record --}}
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update Bank </h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_update_form" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="bank_name_edit">Bank Name</label>
                                        <input type="text" class="form-control dt-full-name" id="bank_name_edit"
                                            placeholder="Enter Bank Name" name="bank_name" aria-label="bank_name" />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="branch_edit">Branch Name</label>
                                        <input type="text" class="form-control dt-full-name" id="branch_edit"
                                            placeholder="Enter Branch Name" name="branch" aria-label="branch" />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="branch_phone_no_edit">Branch Phone N0:</label>
                                    <div class="input-group input-group-merge">
                                        <input type="text" class="form-control" maxlength="10"
                                            placeholder="Enter Branch Phone No:" name="branch_phone_no"
                                            id="branch_phone_no_edit" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="branch_address_edit">Branch Address</label>
                                        <input type="text" class="form-control dt-full-name" id="branch_address_edit"
                                            placeholder="Enter Branch Address" name="branch_address"
                                            aria-label="branch_address" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="branch_narration_edit">Narration on Bill</label>
                                        <input type="text" class="form-control dt-full-name"
                                            id="branch_narration_edit" placeholder="Enter Branch Narration"
                                            name="branch_narration" aria-label="branch_narration" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="account_title_edit">Account Title</label>
                                        <input type="text" class="form-control dt-full-name" id="account_title_edit"
                                            placeholder="Enter Account Title" name="account_title"
                                            aria-label="account_title" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="account_no_edit">Account Number</label>
                                        <input type="text" class="form-control dt-full-name" id="account_no_edit"
                                            placeholder="Enter Account Number" name="account_no"
                                            aria-label="account_no" />
                                    </div>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-edit">Select GL</label>
                                    <select class="select2 select form-select my_select" name="gl_no"
                                        id="select2-basic-edit">
                                        @foreach ($gl_code as $gl)
                                            <option value = "{{ $gl->account_code . '|' . $gl->account_name }}">
                                                {{ $gl->account_code . ' ' . $gl->account_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-edit1">City</label>
                                    <select class="select2 select form-select my_select" name="city_id"
                                        id="select2-basic_edit1">
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}">
                                                {{ $city->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-edit2">Society</label>
                                    <select class="select2 select form-select my_select" name="society_id"
                                        id="select2-basic-edit2">
                                        @foreach ($societies as $society)
                                            <option value="{{ $society->id }}">
                                                {{ $society->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="image_edit">Select Image:</label>
                                        <input type="file" name="image" id="image_edit" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var Table = '';
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('bank.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id'
                    },
                    {
                        data: 'bank_name'
                    },
                    {
                        data: 'gl_no'
                    },
                    {
                        data: 'branch'
                    },
                    {
                        data: 'account_title'
                    },
                    {
                        data: 'account_no'
                    },
                    {
                        data: 'city_name'
                    },
                    {
                        data: 'society_name'
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('billing_bank_edit')
                                btn +=
                                    `<a href="javascript:;" class="item-edit" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('billing_bank_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add Bank',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Banks</h6>');
            $('#body_add_form').on('submit', function(event) {
                event.preventDefault();
                $.ajax({
                    url: '{!! route('bank.store') !!}',
                    method: 'POST',
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#body_add_form')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Bank has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
            $('#body_update_form').on('submit', function(event) {
                event.preventDefault();
                let societyId = $('#edit-id-2').val();
                let formdata = $(this).serialize() + "&image=" + $('#image_edit').val();
                $.ajax({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        '_method': 'PUT'
                    },
                    url: "{{ url('/billing/bank') }}" + "/" + societyId,
                    method: 'PUT',
                    data: JSON.stringify(formdata),
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#body_update_form')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Bank has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/billing/bank') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    $('#bank_name_edit').val(response.data.bank_name);
                    $('#branch_edit').val(response.data.branch);
                    $('#branch_phone_no_edit').val(response.data.branch_phone_no);
                    $('#branch_address_edit').val(response.data.branch_address);
                    $('#branch_narration_edit').val(response.data.branch_narration);
                    $('#account_title_edit').val(response.data.account_title);
                    $('#account_no_edit').val(response.data.account_no);
                    $('#select2-basic-edit').val(response.data.gl_no).select2();
                    $('#select2-basic-edit1').val(response.data.city_id).select2();
                    $('#select2-basic-edit2').val(response.data.society_id).select2();
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }
    </script>
@endsection
