@extends('admin.layouts.master')
@section('title', 'POS')
@section('styles')
    <style>
        ::placeholder {
            font-size: 1rem !important;
        }

        .bg-theme {
            background: #2f3349 !important;
        }

        .carousel-control-prev,
        .carousel-control-next {
            top: -8px !important;
        }
    </style>
@endsection
@section('content')
    <div class="content-header row">
        <div class="content-header-left col-md-9 col-12 mb-2">
            <div class="row breadcrumbs-top">
                <div class="col-12">
                    <h2 class="content-header-title float-start mb-0">Category</h2>
                    <div class="breadcrumb-wrapper">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                            </li>
                            <li class="breadcrumb-item active">Categories
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-3">
            <div class=" card p-1 rounded">
                <div class=" d-flex align-items-center ">
                    <div class="input-group">
                        <span class="input-group-text">
                            <button class="btn p-0">
                                <i class="fa fa-search fs-4" aria-hidden="true"></i>
                            </button>
                        </span>
                        <input type="search" placeholder="Search" class="form-control py-1" />
                    </div>
                </div>
                <?php
                $j = 0;
                ?>
                @foreach ($categories as $category)
                    @if ($category['services'] == null)
                        @continue
                    @else
                        <div>
                            <div class="my-2">
                                <h3 class="m-0 mt-4 fs-4">{{ $category['category_name'] }}</h3>
                            </div>
                            <div id="carouselExampleIndicators{{ $j }}" class="carousel slide">
                                <div class="carousel-inner mt-3 pb-4">
                                    <?php
                                    $i = 0;
                                    ?>
                                    @foreach ($category['services'] as $service)
                                        <div class="carousel-item {{ $i == 0 ? 'active' : 0 }}">
                                            <div class="row row-cols-1">
                                                <div>
                                                    <div class=" rounded p-1 ">
                                                        <img height="300px" src="{{ asset('image/' . $service['img']) }}"
                                                            class="card-img-top" alt="...">
                                                        <div class="card-body text-center">
                                                            <h5 class="card-title text-light bg-dark rounded py-1">
                                                                {{ $service['name'] }}
                                                            </h5>
                                                            <a onclick="addToCart({{ $service['id'] }})"
                                                                class="btn btn-primary">Add To
                                                                Cart</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                        $i++;
                                        ?>
                                    @endforeach
                                </div>
                                <div class="carousel-indicators pt-5 mb-0">
                                    <button type="button" data-bs-target="#carouselExampleIndicators{{ $j }}"
                                        data-bs-slide-to="0" class="active" aria-current="true"
                                        aria-label="Slide 1"></button>
                                    <button type="button" data-bs-target="#carouselExampleIndicators{{ $j }}"
                                        data-bs-slide-to="1" aria-label="Slide 2"></button>
                                    <button type="button" data-bs-target="#carouselExampleIndicators{{ $j }}"
                                        data-bs-slide-to="2" aria-label="Slide 3"></button>
                                </div>
                                <button class="carousel-control-prev" type="button"
                                    data-bs-target="#carouselExampleIndicators{{ $j }}" data-bs-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Previous</span>
                                </button>
                                <button class="carousel-control-next" type="button"
                                    data-bs-target="#carouselExampleIndicators{{ $j }}" data-bs-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="visually-hidden">Next</span>
                                </button>
                            </div>
                        </div>
                    @endif
                    <?php
                    $j = $j + 1;
                    ?>
                @endforeach

            </div>
        </div>
        <div class="col-9">
            <div class=" card p-1 pb-4 rounded ">
                <div class="table-responsive">
                    <table class="table table-hover table-bordered" id="item_cart">
                        <thead>
                            <tr>
                                <th scope="col">Food Item</th>
                                <th scope="col">Price</th>
                                <th scope="col">GST(%)</th>
                                <th scope="col">Quantity</th>
                                <th scope="col">Discount</th>
                                <th scope="col">Total Price</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody id="tbody_cart">
                        </tbody>
                    </table>
                </div>
                <div class=" mx-5 mt-4">
                    <div class=" row row-cols-2 ">
                        <p class=" fw-bolder ">SubTotal:</p>
                        <p class=" text-center" id="sub_total">-</p>
                        <p class=" fw-bolder ">Tax:</p>
                        <p class=" text-center" id="tax">-</p>
                        <p class=" fw-bolder ">Service Charges:</p>
                        <p class=" text-center">-</p>
                        <p class=" fw-bolder">Discount:</p>
                        <p class=" text-center" id="discount">-</p>
                    </div>
                    <div class=" border p-2 border-3 rounded d-flex align-items-center justify-content-center ">
                        <p class=" fw-bolder fs-3 m-0">Grand Total: &nbsp;</p>
                        <p class=" fs-4 text-start m-0" id="grand_total">-</p>
                    </div>
                    <form id="add_form">
                        @csrf
                        @method('POST')
                        <div class="mt-2">
                            <input class="form-check-input" type="radio" name="cash_checkbox" id="cash_checkbox"
                                value="cash" required />
                            <label class="fs-5" for="cash_checkbox">Cash</label>
                        </div>
                        <div class="mt-2">
                            <input class="form-check-input" type="radio" name="cash_checkbox" id="credit"
                                value="credit" required />
                            <label class="fs-5" for="credit">Credit</label>
                        </div>
                        <div class="mt-2 text-center ">
                            <button type="submit" class=" btn btn-primary fw-bold fs-5">Make
                                Order</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts-foot')
    <script>
        $(document).ready(function() {
            setTimeout(function() {
                showCart();
            }, 1000);

            $("#tbody_cart").on("change", "input[type='number']", function() {
                var quantity = $(this).val();
                var itemId = $(this).attr("id");
                $.ajax({
                    url: "{{ url('/pos') }}" + "/" + itemId + "/" + "show",
                    method: "GET",
                    data: {
                        itemId: itemId,
                        quantity: quantity
                    },
                    success: function(response) {
                        cartData(response);
                    },
                    error: function(xhr) {
                        console.log(xhr);
                    }
                });
            });
            $('#add_form').submit(function(e) {
                e.preventDefault();
                let is_credit = $('input[name="cash_checkbox"]:checked').val();
                $.ajax({
                    url: '{!! route('pos.store') !!}',
                    type: "POST",
                    data: {
                        _token: '{{ csrf_token() }}',
                        check: is_credit,
                    },
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'Can not place the order.'
                            })
                        } else {
                            Toast.fire({
                                icon: 'success',
                                title: 'Your Order Has Been Placed!'
                            })
                        }
                        $('#add_form')[0].reset();
                        showCart();
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'Can not place the order.'
                            })
                        }
                    }
                });
            })
        });

        function showCart() {
            $.ajax({
                url: "{!! route('pos.create') !!}",
                method: 'GET',
                success: function(response) {
                    cartData(response);
                },
                error: function(xhr) {
                    console.log('error');
                }
            });
        }

        function addToCart(id) {
            $.ajax({
                url: "{{ url('/pos') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    cartData(response);
                },
                error: function(xhr) {}
            });
        }

        function cartData(response) {
            var table = document.getElementById("tbody_cart");
            table.innerHTML = "";
            let sub_total = 0;
            let tax = 0;
            let discount = 0;
            let grand_total = 0;
            response.data.forEach(item => {
                sub_total += item.sub_total;
                tax += item.gst_amount;
                discount += item.discount_amount;
                grand_total += item.total_amount;
                var newRow = `<tr>` +
                    `<th scope='row'>` + item.name + `</th>` +
                    `<td>$` + item.price + `</td>` +
                    `<td>` + item.gst + `%</td>` +
                    `<td><input type='number' id="${item.id}" min='0' value='${item.quantity}' class='form-control form-floating'></td>` +
                    `<td>` + item.discount + `</td>` +
                    `<td>$` + item.total_amount.toFixed(2) + `</td>` +
                    `<td class='text-center'><a onclick="remove_item(${item.id})" class='delete-item'><i class='fa fa-trash text-danger' aria-hidden='true'></i></a></td>` +
                    `</tr>`;

                $("#tbody_cart").append(newRow);
            });
            $('#sub_total').text(sub_total);
            $('#tax').text(tax);
            $('#discount').text(discount);
            $('#grand_total').text('$' + grand_total);
        }

        function remove_item(id) {
            $.ajax({
                url: "{{ url('pos') }}" + "/" + id,
                method: 'DELETE',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(response) {
                    if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'Could not remove the item!'
                        })
                    } else if (response.code == 300) {
                        $.alert({
                            icon: 'far fa-times-circle',
                            title: 'Oops!',
                            content: response.message,
                            type: 'red',
                            buttons: {
                                Okay: {
                                    text: 'Okay',
                                    btnClass: 'btn-red',
                                }
                            }
                        });
                    } else {
                        Toast.fire({
                            icon: 'success',
                            title: 'Item has been removed Successfully!'
                        })
                    }
                    showCart();
                },
                error: function(xhr) {
                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                    if (xhr.status === 422) {

                        var errors = "";
                        //loop through error
                        $.each(xhr.responseJSON.errors, function(i, val) {
                            //loop through inner array for each keys
                            $.each(val, function(i, val1) {
                                toastr['error'](
                                    val1, {
                                        closeButton: true,
                                        tapToDismiss: false,
                                        rtl: isRtl
                                    });
                            })
                        })
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'Could not remove the item!'
                        })
                    }

                }
            });
        }
    </script>
@endsection
