<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>Studio | POS - Customer Order System</title>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta name="description" content />
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content />
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('app-assets/images/ico/favicon.ico') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/main.css') }}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.5.2/css/all.min.css"
        integrity="sha512-SnH5WK+bZxgPHs44uWIX+LLJAJ9/2PkPKZ5QiAj6Ta86w+fsb2TkcmfRyVX3pBnMFcV7oQPJkl9QevSCWr3W6A=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="
        https://cdn.jsdelivr.net/npm/pace-js@1.2.4/pace-theme-default.min.css
        "
        rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('app-assets/css/core/menu/menu-types/vertical-menu.css') }}">
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
</head>

<body class="pace-top">
    <div id="app" class="app app-content-full-height app-without-sidebar app-without-header">
        <div id="content" class="app-content p-0">
            <div class="pos pos-with-menu pos-with-sidebar" id="pos">
                <div class="pos-container">
                    <div class="pos-menu">
                        <div class="logo">
                            <a>
                                <div class="logo-img"><i class="fa fa-bowl-rice"></i></div>
                                <div class="logo-text">THE OPUS</div>
                            </a>
                        </div>

                        <div class="nav-container">
                            <div class="h-100">
                                <ul class="nav nav-tabs nav-left flex-column" role="tablist">
                                    <?php
                                    $i = 0;
                                    ?>
                                    @foreach ($categories as $category)
                                        @if ($category['services'] == null)
                                            @continue
                                        @else
                                            <li class="nav-item">
                                                <a class="nav-link {{ $i == 0 ? 'active' : $i }}"
                                                    href="{{ '#cat' . $i }}" data-bs-toggle="tab"
                                                    aria-controls="{{ 'cat' . $i }}"
                                                    aria-selected="{{ $i == 0 ? 'true' : 'false' }}">
                                                    <i
                                                        class="fa fa-fw fa-drumstick-bite"></i>{{ $category['category_name'] }}
                                                </a>
                                            </li>
                                        @endif
                                        <?php
                                        $i++;
                                        ?>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="pos-content">
                        <div class="row mt-3">
                            <div class="col-md-6 col-sm-12">
                                <label for="property">Property</label>
                                <select class="js-select2 w-100 form-select property_no" placeholder="Select Property"
                                    name="property_id" id="property">
                                    @foreach ($properties as $property)
                                        <option value = "{{ $property->id }}">
                                            {{ $property->property_number . '/ ' . $property->city_name . '/ ' . $property->society_name . '/ ' . $property->block_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <label for="owner">Property Owner / Tenant</label>
                                <select class="js-select2 w-100 form-select " placeholder="Select Property Owner"
                                    name="owner_id" id="owner" disabled>
                                    <option></option>
                                </select>
                            </div>
                            <div class="col-md-6 col-sm-12 mt-2">
                                <input class="w-100 form-control " type="search" name="search" id="search"
                                    placeholder="Search Property Here">
                            </div>
                        </div>
                        <div class="pos-content-container h-100">
                            <div class="tab-content">
                                <?php
                                $i = 0;
                                ?>
                                @foreach ($categories as $category)
                                    @if ($category['services'] == null)
                                        @continue
                                    @else
                                        <div class="tab-pane {{ $i == 0 ? 'active' : $i }}" id="{{ 'cat' . $i }}"
                                            aria-labelledby="{{ 'cat' . $i }}">
                                            <div class="row gx-4">
                                                @foreach ($category['services'] as $service)
                                                    <div class="col-xxl-3 col-xl-4 col-lg-6 col-md-4 col-sm-6 pb-4"
                                                        data-type="{{ $service['name'] }}">
                                                        <a href="#" class="pos-product" data-bs-toggle="modal"
                                                            onclick="open_modal({{ $service['id'] }})">
                                                            {{-- data-bs-target="#modalPosItem" --}}
                                                            <div class="img"
                                                                style="background-image: url('{{ asset('image/' . $service['img']) }}');">
                                                            </div>
                                                            <div class="info">
                                                                <div class="title">{{ $service['name'] }}&reg;
                                                                </div>
                                                                <div class="desc">{{ $service['description'] }}
                                                                </div>
                                                                <div class="price">{{ $service['price'] }}</div>
                                                            </div>
                                                        </a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        </div>
                                    @endif
                                    <?php
                                    $i++;
                                    ?>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="pos-sidebar" id="pos-sidebar">
                        <div class="h-100 d-flex flex-column p-0">
                            <div class="pos-sidebar-header">
                                <div class="back-btn">
                                    <button type="button" class="btn">
                                        <i class="fa fa-chevron-left"></i>
                                    </button>
                                </div>
                                <div class="icon"><i class="fa fa-plate-wheat"></i></div>
                                <div class="title">Orders</div>
                            </div>
                            <div class="pos-sidebar-nav small">
                                <ul class="nav nav-tabs nav-fill">
                                    <li class="nav-item">
                                        <a class="nav-link active" href="#" data-bs-toggle="tab"
                                            data-bs-target="#newOrderTab">New Order(<span
                                                id="num_of_items">0</span>)</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="pos-sidebar-body tab-content">
                                <div class="tab-pane fade h-100 show active" id="newOrderTab">
                                </div>
                            </div>

                            <div class="pos-sidebar-footer">
                                <div class="d-flex align-items-center mb-2">
                                    <div>Subtotal</div>
                                    <div class="flex-1 text-end h6 mb-0" id="final_subtotal">0</div>
                                </div>
                                <div class="d-flex align-items-center mb-2">
                                    <div>Taxes</div>
                                    <div class="flex-1 text-end h6 mb-0" id="final_tax">0</div>
                                </div>
                                <div class="d-flex align-items-center mb-2">
                                    <div>Discount</div>
                                    <div class="flex-1 text-end h6 mb-0" id="final_discount">0</div>
                                </div>
                                <div class="d-flex align-items-center mb-2">
                                    <label for="purchase_type">Purchase Type</label>
                                    <div class="flex-1 text-end h6 mb-0 align-items-baseline ">
                                        <span id="cash">
                                            <input class="m-1" type="radio" name="purchase_type"
                                                id="cash_radio">Cash
                                        </span>
                                        <span id="online">
                                            <input class="m-1" type="radio" name="purchase_type"
                                                id="online_radio">Online
                                        </span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center">

                                    Credit<input class="m-1 form-check " type="checkbox" name="credit"
                                        id="credit">
                                </div>
                                <hr class="opacity-1 my-10px" />
                                <div class="d-flex align-items-center mb-2">
                                    <div>Total</div>
                                    <div class="flex-1 text-end h4 mb-0" id="final_amount"></div>
                                </div>
                                <div class="mt-3">
                                    <div class="d-flex">
                                        <a href="#"
                                            class="btn btn-theme flex-fill d-flex align-items-center justify-content-center"
                                            onclick="place_order()">
                                            <span>
                                                <i class="fa fa-cash-register fa-lg my-10px d-block"></i>
                                                <span class="small fw-semibold">Submit Order</span>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <a href="#" class="pos-mobile-sidebar-toggler" data-toggle-class="pos-mobile-sidebar-toggled"
                data-toggle-target="#pos">
                <i class="fa fa-shopping-bag"></i>
                <span class="badge">5</span>
            </a>
        </div>
    </div>

    <div class="modal modal-pos fade" id="modalPosItem">
        <div class="modal-dialog modal-lg">
            <div class="modal-content border-0">
                <a href="#" data-bs-dismiss="modal" class="btn-close position-absolute top-0 end-0 m-4"></a>
                <div class="modal-pos-product">
                    <div class="modal-pos-product-img">
                        <div class="img" id="service_image">
                        </div>
                    </div>
                    <div class="modal-pos-product-info">
                        <div class="fs-4 fw-semibold" id="service_name"></div>
                        <div class="text-body text-opacity-50 mb-2" id="service_description">
                        </div>
                        <div class="fs-3 fw-bold mb-3" id="service_price"></div>
                        <input type="hidden" id="service_id" />
                        <div class="d-flex mb-3">
                            <a href="#" class="btn btn-secondary" onclick="reduce_quantity()"><i
                                    class="fa fa-minus"></i></a>
                            <input type="text" id="service_quantity"
                                class="form-control w-50px fw-bold mx-2 text-center" name="quantity"
                                value="1" />
                            <a href="#" class="btn btn-secondary" onclick="increase_quantity()"><i
                                    class="fa fa-plus"></i></a>
                        </div>
                        <hr class="opacity-1" />
                        <div class="mb-2">
                            <div class="d-flex">
                                <p class="fw-bold">GST (%):</p>
                                <p id="service_gst"></p>
                            </div>
                        </div>
                        <div class="mb-2">
                            <div class="d-flex">
                                <p class="fw-bold">Discount (%):&nbsp;</p>
                                <p id="service_discount"></p>
                            </div>
                        </div>
                        <hr class="opacity-1" />
                        <div class="row">
                            <div class="col-4">
                                <a href="#" class="btn btn-default fw-semibold mb-0 d-block py-3"
                                    data-bs-dismiss="modal">Cancel</a>
                            </div>
                            <div class="col-8">
                                <a href="#" onclick="addToCart()"
                                    class="btn btn-theme fw-semibold d-flex justify-content-center align-items-center py-3 m-0">Add
                                    to
                                    cart <i class="fa fa-plus ms-2 my-n3"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.1/jquery.min.js"
        integrity="sha512-v2CJ7UaYy4JwqLDIrZUI/4hqeoQieOmAZNXBeQyjo21dadnwR+8ZaIJVT8EE2iyI61OV8e6M8PP2/4hpQINQ/g=="
        crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.3/dist/js/bootstrap.bundle.min.js"></script>
    <script src="{{ asset('app-assets/js/scripts/components/components-modals.js') }}"></script>
    <script src="{{ asset('app-assets/js/scripts/components/components-navs.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/pace-js@1.2.4/pace.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>


    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var creditCheckbox = document.getElementById('credit');
            var cashRadio = document.getElementById('cash');
            var onlineRadio = document.getElementById('online');

            creditCheckbox.addEventListener('change', function() {
                if (creditCheckbox.checked) {
                    cashRadio.style.display = 'none';
                    onlineRadio.style.display = 'none';
                } else {
                    cashRadio.style.display = 'inline';
                    onlineRadio.style.display = 'inline';
                }
            });
        });
    </script>
    <script>
        var itemsArray = [];
        $(document).ready(function() {
            setTimeout(function() {
                showCart();
            }, 1000);
        });

        function open_modal(id) {
            $('#modalPosItem').modal('show');
            $.ajax({
                url: "{{ url('/pos') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    var imgUrl = '{{ asset('image/') }}' + '/' + response.data.img;
                    $('#service_image').css('background-image', 'url(' + imgUrl + ')');
                    $('#service_id').val(response.data.id);
                    $('#service_discount').html('&nbsp &nbsp' + response.data.discount);
                    $('#service_price').html(response.data.price + '  PKR');
                    $('#service_description').text(response.data.description);
                    $('#service_gst').html('&nbsp &nbsp' + response.data.gst);
                    $('#service_name').text(response.data.name);
                },
                error: function(xhr) {}
            });
        }

        function reduce_quantity() {
            let value = $('#service_quantity').val();
            if (value > 1) {
                value--;
                $('#service_quantity').val(value);
            }
        }

        function increase_quantity() {
            let value = $('#service_quantity').val();
            $('#service_quantity').val(++value);
        }

        function reduce_quantity_from_cart(id) {
            let value = $(`#cart_quantity${id}`).val();
            if (value > 1) {
                value--;
                $(`#cart_quantity${id}`).val(value);
                $.ajax({
                    url: "{{ url('/pos/updateQuantity') }}" + "/" + id,
                    method: 'POST',
                    data: {
                        _token: '{{ csrf_token() }}',
                        quantity: value,
                    },
                    success: function(response) {
                        showCart();
                    },
                    error: function(xhr) {}
                });
            }
        }

        function increase_quantity_from_cart(id) {
            let value = $(`#cart_quantity${id}`).val();
            $(`#cart_quantity${id}`).val(++value);
            $.ajax({
                url: "{{ url('/pos/updateQuantity') }}" + "/" + id,
                method: 'POST',
                data: {
                    _token: '{{ csrf_token() }}',
                    quantity: value,
                },
                success: function(response) {
                    showCart();
                },
                error: function(xhr) {}
            });
        }

        function addToCart() {
            $('#modalPosItem').modal('hide');
            $.ajax({
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    '_method': 'post'
                },
                url: "{{ url('/pos/addtocart') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    service_id: $('#service_id').val(),
                    quantity: $('#service_quantity').val(),
                },
                success: function(response) {
                    cartData(response);
                },
                error: function(xhr) {}
            });
        }

        function cartData(response) {
            $('#newOrderTab').empty();
            itemsArray = [];
            let sub_total = 0;
            let tax = 0;
            let discount = 0;
            let grand_total = 0;
            response.data.forEach(item => {
                var currentItem = {
                    id: item.id,
                    category_id: item.category_id,
                    service_id: item.service_id,
                    quantity: item.quantity,
                };
                itemsArray.push(currentItem);
                sub_total += item.sub_total;
                tax += item.gst_amount;
                discount += item.discount_amount;
                grand_total += item.total_amount;
                let img_Url = '{{ asset('image/') }}' + '/' + item.img;
                var $newOrder = $('<div>').addClass('pos-order').html(`
                        <div class="pos-order-product">
                            <div class="img"
                                style="background-image: url(${img_Url});">
                            </div>
                            <div class="flex-1">
                                <div class="h6 mb-1">${item.name}</div>
                                <div class="small">${item.price}</div>
                                <div class="d-flex">
                                    <a href="#" class="btn btn-secondary btn-sm" onclick="reduce_quantity_from_cart(${item.id})"><i
                                            class="fa fa-minus"></i></a>
                                    <input type="text" id="cart_quantity${item.id}"
                                        class="form-control w-50px form-control-sm mx-2 bg-white bg-opacity-25 bg-white bg-opacity-25 text-center"
                                        value="${item.quantity}" />
                                    <a href="#" class="btn btn-secondary btn-sm" onclick="increase_quantity_from_cart(${item.id})"><i
                                            class="fa fa-plus"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="pos-order-price d-flex flex-column">
                            <div class="flex-1">${item.price}</div>
                            <div class="text-end">
                                <a href="#" class="btn btn-default btn-sm text-danger" onclick="remove_item(${item.id})"><i
                                        class="fa fa-trash"></i></a>
                            </div>
                        </div>
                    `);
                $('.tab-content').find('#newOrderTab').append($newOrder);
            });
            $('#num_of_items').text(itemsArray.length);
            $('#final_subtotal').text(sub_total);
            $('#final_tax').text(tax);
            $('#final_discount').text(discount);
            $('#final_amount').text(grand_total);
        }

        function showCart() {
            $.ajax({
                url: "{!! route('pos.create') !!}",
                method: 'GET',
                success: function(response) {
                    cartData(response);
                },
                error: function(xhr) {
                    console.log('error');
                }
            });
        }

        function remove_item(id) {
            var indexToRemove = itemsArray.findIndex(item => item.id === id);
            if (indexToRemove !== -1) {
                itemsArray.splice(indexToRemove, 1);
            }
            $.ajax({
                url: "{{ url('pos') }}" + "/" + id,
                method: 'DELETE',
                data: {
                    _token: '{{ csrf_token() }}',
                },
                success: function(response) {
                    showCart();
                },
                error: function(xhr) {
                    console.log('error');
                }
            });
        }

        function place_order() {
            let type = 'credit';
            if ($('#credit').is(':checked')) {
                type = 'credit';
            } else {
                if ($('#cash_radio').is(':checked')) {
                    type = 'cash';
                }
                if ($('#online_radio').is(':checked')) {
                    type = 'online';
                }
            }
            $.ajax({
                type: "post",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    '_method': 'post'
                },
                url: "{{ url('billing/service-bills') }}",
                data: {
                    _token: '{{ csrf_token() }}',
                    prop_id: $('#property').val(),
                    assigned_to: $('#owner').val(),
                    cart: itemsArray,
                    subtotal: $('#final_subtotal').text(),
                    tax: $('#final_tax').text(),
                    discount: $('#final_discount').text(),
                    payment_type: type,
                },
                success: function(response) {
                    showCart();
                    window.location.reload();
                },
                error: function(xhr) {}
            });
        }

        $('.property_no').on('change', function() {
            var data = $(".property_no option:selected").val();
            if (data) {
                $.ajax({
                    url: "{{ url('/pos') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "new_owner"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#owner');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var owner = response.data[i];
                            selectElement.append(
                                `<option value="${owner.id}"> ${owner.name} S/O or D/O ${owner.father_name} / ${owner.cnic}</option>`
                            );
                        }
                        selectElement.select2();
                        $("#owner").prop('disabled', false);

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#owner').val(null).trigger('change');
                $("#owner").prop('disabled', true);
            }
        });
    </script>
</body>

</html>
