@extends('admin.layouts.master')
@section('title', 'Users')

@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Users</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Users
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Name</th>
                                <th>City</th>
                                <th>Society</th>
                                <th>Blocks</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>

        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add User</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="add_user">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                {{-- <div class="col-12 mb-1">
                                <label class="form-label" for="user_type">Type</label>
                                <select class="select2 form-select" name="type" id="user_type">
                                    <option value="1">Employee</option>
                                    <option value="2">Self-Portal User</option>
                                </select>
                            </div> --}}
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="user-name">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="user-name"
                                            placeholder="Enter Name" name="name" aria-label="user-name" required />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="user-email">Email</label>
                                        <input type="email" class="form-control dt-full-name" id="user-email"
                                            placeholder="Enter Email" name="email" aria-label="user-email" required />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="user-password">Password</label>
                                        <input type="password" class="form-control dt-full-name" id="user-password"
                                            placeholder="Enter Name" name="password" aria-label="user-password" />
                                    </div>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple">Roles</label>
                                    <select class="select2 form-select" name="role_id[]" id="select2-multiple" multiple>
                                        <optgroup label="User Roles">
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-1">City</label>
                                    <select class="select2 form-select" name="city_id[]" id="select2-multiple-1" multiple>
                                        <optgroup label="Cities">
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}">
                                                    {{ $city->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-2">Society</label>
                                    <select class="select2 form-select" name="society_id[]" id="select2-multiple-2"
                                        multiple>
                                        <optgroup label="Societies">
                                            @foreach ($societies as $society)
                                                <option value="{{ $society->id }}">
                                                    {{ $society->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-3">Block</label>
                                    <select class="select2 form-select" name="block_id[]" id="select2-multiple-3" multiple>
                                        <optgroup label="Blocks">
                                            @foreach ($blocks as $block)
                                                <option value="{{ $block->id }}">
                                                    {{ $block->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal to update record --}}
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update User</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_user">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="user-name">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="user-name-edit"
                                            placeholder="Enter Name" name="name" aria-label="user-name-edit"
                                            required />
                                        <input type="hidden" value="" name="user_id" id="edit-id-2">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="user-email">Email</label>
                                        <input type="email" class="form-control dt-full-name" id="user-email-edit"
                                            placeholder="Enter Email" name="email" aria-label="user-email-edit"
                                            required />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="user-password">Password</label>
                                        <input type="password" class="form-control dt-full-name" id="user-password-edit"
                                            placeholder="Enter Name" name="password" aria-label="user-password-edit" />
                                    </div>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-edit">Roles</label>
                                    <select class="select2 form-select" name="role_id[]" id="select2-multiple-edit"
                                        multiple>
                                        <optgroup label="User Roles">
                                            @foreach ($roles as $role)
                                                <option value="{{ $role->id }}">{{ $role->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-1-edit">City</label>
                                    <select class="select2 form-select" name="city_id[]" id="select2-multiple-1-edit"
                                        multiple>
                                        <optgroup label="Cities">
                                            @foreach ($cities as $city)
                                                <option value="{{ $city->id }}">
                                                    {{ $city->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-2-edit">Society</label>
                                    <select class="select2 form-select" name="society_id[]" id="select2-multiple-2-edit"
                                        multiple>
                                        <optgroup label="Societies">
                                            @foreach ($societies as $society)
                                                <option value="{{ $society->id }}">
                                                    {{ $society->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-3-edit">Block</label>
                                    <select class="select2 form-select" name="block_id[]" id="select2-multiple-3-edit"
                                        multiple>
                                        <optgroup label="Blocks">
                                            @foreach ($blocks as $block)
                                                <option value="{{ $block->id }}">
                                                    {{ $block->name }}
                                                </option>
                                            @endforeach
                                        </optgroup>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var Table = '';
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                responsive: true,
                ordering: false,
                ajax: '{!! route('users.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'city_name'
                    },
                    {
                        data: 'society_name'
                    },
                    {
                        data: 'block_name'
                    },
                    {
                        data: ''
                    },
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('users_edit')
                                btn +=
                                    `<a href="#" class="item-edit" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('users_delete')
                                btn += '<a href="javascript:;" onclick="delete_item(' + full
                                    .id +
                                    ')">' +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('users_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'ADD USER',
                            className: 'create-new btn btn-primary me-2',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                    @endcan

                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Users</h6>');

            $('#add_user').on('submit', function(event) {
                event.preventDefault();
                let formdata = $(this).serializeArray();

                $.ajax({
                    url: '{!! route('users.store') !!}',
                    method: 'POST',
                    data: formdata,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_user')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'User has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });

            $('#edit_user').on('submit', function(event) {
                event.preventDefault();

                let unit_Id = $('#edit-id-2').val();
                let formData = $(this).serializeArray();
                $.ajax({
                    url: "{{ url('/users/') }}" + "/" + unit_Id,
                    method: 'PUT',
                    data: formData,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#edit_user')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'User has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/users/') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    $('#user-name-edit').val(response.data.name);
                    $('#user-email-edit').val(response.data.email);
                    $('#select2-multiple-edit').val(JSON.parse(response.role)).select2();
                    $('#select2-multiple-1-edit').val(JSON.parse(response.city)).select2();
                    $('#select2-multiple-2-edit').val(JSON.parse(response.society)).select2();
                    $('#select2-multiple-3-edit').val(JSON.parse(response.block)).select2();
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('users/') }}" + "/" + id,
                                method: 'DELETE',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Society has been Deleted Successfully!'
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
