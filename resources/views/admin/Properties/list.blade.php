@extends('admin.layouts.master')
@section('title', 'List')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Property</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Properties
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <form {{-- action="route('add.store')" --}} id="add_form">
                @method('GET')
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-1">City</label>
                                    <select class="select2 select form-select my_select select2-basic-1" name="city_id"
                                        id="select2-basic-1">
                                        <option value="">Select City</option>
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-2">Phase</label>
                                    <select class="select2 select form-select my_select select2-basic-2" disabled
                                        name="society_id" id="select2-basic-2">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-3">Block</label>
                                    <select class="select2 select form-select my_select select2-basic-3" disabled
                                        name="block_id" id="select2-basic-3">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic">Billing Type</label>
                                    <select class="select2 select form-select my_select" name="type_id" id="select2-basic">
                                        <option></option>
                                        @foreach ($property_types as $property_type)
                                            <option value="{{ $property_type->id }}">{{ $property_type->type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-prop">Property Type</label>
                                    <select class="select2 select form-select my_select" name="typesProperty"
                                        id="select2-basic-prop">
                                        <option></option>
                                        <option value = "plot">Plot</option>
                                        <option value = "house">House</option>
                                        <option value = "commercial">Commercial</option>
                                        <option value = "apartment">Apartment</option>
                                    </select>
                                </div>
                                <div class="col-4">
                                    <div class="mb-1">
                                        <label class="form-label" for="property_number">Plot No</label>
                                        <input type="text" class="form-control dt-full-name" id="property_number"
                                            placeholder="Enter Property Number" name="property_number" aria-label="type" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="mb-1">
                                        <label class="form-label" for="owner_name">Owner Name</label>
                                        <input type="text" class="form-control dt-full-name" id="owner_name"
                                            placeholder="Owner Name" name="owner_name" aria-label="owner_name" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary apply_filter">
                        APPLY FILTER
                    </button>
                </div>
            </form>
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>City</th>
                                <th>Society</th>
                                <th>Block</th>
                                <th>Owners Name</th>
                                <th>Property Number</th>
                                <th>Property Type</th>
                                <th>Billing Type</th>
                                <th>Land Area/Kanal</th>
                                <th>Land Area/Marla</th>
                                <th>Land Area/Sq.feet</th>
                                <th>Covered Area</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        $(document).ready(function() {
            var Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: {
                    url: '{!! route('list.index') !!}',
                    type: 'GET',
                    data: function(q) {
                        q.society_id = $('#select2-basic-2').val();
                        q.city_id = $('#select2-basic-1').val();
                        q.block_id = $('#select2-basic-3').val();
                        q.type_id = $('#select2-basic').val();
                        q.typesProperty = $('#select2-basic-prop').val();
                        q.property_number = $('#property_number').val();
                        q.owner_name = $('#owner_name').val();
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'city_name',
                        name: 'cities.name'
                    },
                    {
                        data: 'society_name',
                        name: 'societies.name'
                    },
                    {
                        data: 'block_name',
                        name: 'property_blocks.name'
                    },
                    {
                        data: 'owner_names',
                        name: 'property_owners.name'
                    },
                    {
                        data: 'property_number',
                        name: 'properties.property_number'
                    },
                    {
                        data: 'typesProperty',
                        name: 'properties.typesProperty',
                        render: function(data, type, row, meta) {
                            return data.charAt(0).toUpperCase() + data.slice(1).toLowerCase();
                        }
                    },
                    {
                        data: 'type_name',
                        name: 'property_types.type'
                    },
                    {
                        data: 'landAreaKanal',
                        name: 'properties.landAreaKanal'
                    },
                    {
                        data: 'landAreaMarla',
                        name: 'properties.landAreaMarla'
                    },
                    {
                        data: 'landAreaSqfeet',
                        name: 'properties.landAreaSqfeet'
                    },
                    {
                        data: 'covered_area',
                        name: 'properties.covered_area'
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '<div class="d-flex py-1">';
                            @can('properties_list_edit')
                                btn +=
                                    `<form action="{{ url('properties/add/${full.id}/edit') }}" method="POST" target="_blank">` +
                                    '@csrf' +
                                    `<button type="submit" class="btn btn-link p-0"><i class="text-success">${feather.icons['edit'].toSvg({ class: 'font-medium-4' })}</i></button>` +
                                    '</form>';
                            @endcan
                            // btn +=
                            //     `<a href="" class="item-view">` +
                            //     feather.icons['download'].toSvg({
                            //         class: 'font-medium-4'
                            //     }) +
                            //     '</a>';
                            @can('properties_list_view')
                                btn +=
                                    `<a href="{{ url('properties/list/${full.id}') }}" target="_blank">` +
                                    feather.icons['eye'].toSvg({
                                        class: 'font-medium-4 text-success'
                                    }) +
                                    '</a>';
                            @endcan
                            btn += '</div>';
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }, ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });


            $(document).on('click', '.member', function() {
                var anchor = $('<a>', {
                    href: '{{ route('member_add.index') }}',
                    style: 'display:none;'
                });

                $('body').append(anchor);

                anchor[0].click();

                anchor.remove();
            });

            $('div.head-label').html('<h6 class="mb-0">List of Properties</h6>');

            $('#add_form').on('submit', function(event) {
                event.preventDefault();
                Table.draw();
            });
        });
        $('.select2-basic-1').on('change', function() {
            var data = $(".select2-basic-1 option:selected").val();
            if (data) {
                $("#select2-basic-2").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/list') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "society"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-2');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var society = response.data[i];
                            selectElement.append('<option value="' + society.id + '">' + society.name +
                                '</option>');
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-2').val(null).trigger('change');
                $("#select2-basic-2").prop('disabled', true);
            }
        });
        $('.select2-basic-2').on('change', function() {
            var data = $(".select2-basic-2 option:selected").val();
            if (data) {
                $("#select2-basic-3").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/list') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "block"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-3');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var block = response.data[i];
                            selectElement.append('<option value="' + block.id + '">' + block.name +
                                '</option>');
                        }
                        selectElement.select2();
                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-3').val(null).trigger('change');
                $("#select2-basic-3").prop('disabled', true);
            }
        });
    </script>
@endsection
