@extends('admin.layouts.master')
@section('title', 'Property View')
@section('content')
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <strong class="p-1">
                        <h3>Property Information</h3>
                    </strong>
                </div>
            </div>
        </div>
        <div class="card">
            <div class="row px-1">
                <div class="col-6 py-1">
                    <div class="row">
                        <div class="col-12 py-1">
                            <div class="card card-transaction">
                                <div class="card-header">
                                    <h4 class="card-title">Property Details</h4>
                                </div>
                                <div class="card-body px-3">
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>City:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->city_name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->city_name ?? 'N/A' }}

                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Society:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->society_name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->society_name ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Block:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->block_name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->block_name ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Property Number:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->property_number) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->property_number ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Property Type:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->typesProperty) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->typesProperty ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Land Area / Kanal:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->landAreaKanal) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->landAreaKanal ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Land Area / Marla:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->landAreaMarla) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->landAreaMarla ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Land Area / Square Feet:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->landAreaSqfeet) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->landAreaSqfeet ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Coverred Area:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->covered_area) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->covered_area ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Extra Area:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->extra_area) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->extra_area ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 py-1">
                            <div class="card card-transaction">
                                <div class="card-header">
                                    <h4 class="card-title">Owner Details</h4>
                                </div>
                                <div class="card-body px-3">
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Name:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->name ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Father Name:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->father_name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->father_name ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Old CNIC:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->owner_cnic_old) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->owner_cnic_old ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>CNIC:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->cnic) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->cnic ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>CNIC Expiry Date:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->cnicExpiryDate) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->cnicExpiryDate ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Address:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($data[0]->address) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $data[0]->address ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 py-1">
                            <div class="card card-transaction">
                                <div class="card-header">
                                    <h4 class="card-title">Tenant Details</h4>
                                </div>
                                <div class="card-body px-3">
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Name:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_name ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>CNIC:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_cnic) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_cnic ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Email:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_email) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_email ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Phone:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_phone) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_phone ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Address:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_address) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_address ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Rent:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_rent) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_rent ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Increment:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->tenant_increment) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->tenant_increment ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Tenant Agreement Start Date:</strong>
                                                </h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->from_date) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->from_date ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Tenant Agreement End Date:</strong>
                                                </h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->to_date) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->to_date ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Dealer Name:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->dealer_name) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->dealer_name ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Dealer Address:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->dealer_address) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->dealer_address ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                    <div class="transaction-item">
                                        <div class="d-flex">
                                            <div class="transaction-percentage">
                                                <h6 class="transaction-title"><strong>Dealer Phone:</strong></h6>
                                            </div>
                                        </div>
                                        <div
                                            class="{{ empty($tenant->dealer_phone) ? 'text-danger fw-bolder' : 'text-success fw-bolder' }}">
                                            <strong>
                                                {{ $tenant->dealer_phone ?? 'N/A' }}
                                            </strong>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card card-transaction">
                        <div class="card-header">
                            <h4 class="card-title">Property Documents</h4>
                        </div>
                        <div class="card-body px-3">
                            <div class="row">
                                <div class="col-12 mb-3 px-3">
                                    <button onclick="$('#large1').modal('show')" type="button"
                                        class="btn btn-outline-dark round">
                                        <i data-feather="upload" class="me-25"></i>
                                        <span>Add Document</span>
                                    </button>
                                    <div class="modal fade text-start" id="large1" tabindex="-1"
                                        aria-labelledby="myModalLabel17" aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title" id="myModalLabel17">Add Document</h4>
                                                    <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                        aria-label="Close"></button>
                                                </div>
                                                <form class="form" id="permission_add_form"
                                                    enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="modal-body">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="mb-1">
                                                                    <label class="form-label" for="select2-basic">Select
                                                                        Document Type</label>
                                                                    <select class="select2 select form-select my_select"
                                                                        name="document_type_id" id="select2-basic">
                                                                        @foreach ($document_types as $document)
                                                                            <option value="{{ $document->id }}">
                                                                                {{ $document->type }}
                                                                            </option>
                                                                        @endforeach
                                                                    </select>
                                                                </div>
                                                            </div>
                                                            <div class="col-12">
                                                                <div class="mb-1">
                                                                    <label class="form-label" for="document_image">Pick
                                                                        File
                                                                    </label>
                                                                    <input type="file" name="document_image"
                                                                        id="document_image" class="form-control" required>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="reset" class="btn btn-outline-success"
                                                            data-bs-dismiss="modal" aria-label="Close">
                                                            Cancel
                                                        </button>
                                                        <button type="submit" class="btn btn-primary"
                                                            aria-label="Close">
                                                            Save
                                                        </button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @foreach ($property_documents as $property_document)
                                    <div class="col-6 mb-1">
                                        <div style="text-align: center">
                                            <a style="color: black"
                                                href="{{ route('download.image', ['filename' => $property_document->image]) }}">
                                                <img src="{{ asset('image/' . $property_document->image) }}"
                                                    alt="error" width="250" height="250">
                                                <strong class="px-1">{{ $property_document->name }}</strong>
                                            </a>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        $('#permission_add_form').on('submit', function(event) {
            event.preventDefault();

            $.ajax({
                url: "{{ url('properties/add/') }}" + "/" + {{ $property->id }} + "/doc",
                method: 'POST',
                data: new FormData(this),
                contentType: false,
                processData: false,
                success: function(response, xhr) {
                    console.log('Hello');
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#permission_add_form')[0].reset();
                        $("#large1").modal("hide");
                        Toast.fire({
                            icon: 'success',
                            title: 'Property Document has been Added Successfully!'
                        })
                        setTimeout(function() {
                            // window.location.href = '{!! route('list.index') !!}';
                            location.reload();
                        }, 1);
                    }
                },
                error: function(xhr) {
                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                    if (xhr.status === 422) {

                        var errors = "";
                        //loop through error
                        $.each(xhr.responseJSON.errors, function(i, val) {
                            //loop through inner array for each keys
                            $.each(val, function(i, val1) {
                                toastr['error'](
                                    val1, {
                                        closeButton: true,
                                        tapToDismiss: false,
                                        rtl: isRtl
                                    });
                            })
                        })
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    Table.ajax.reload();
                }
            });
        });
    </script>
@endsection
