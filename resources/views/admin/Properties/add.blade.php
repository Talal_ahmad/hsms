@extends('admin.layouts.master')
@section('title', 'Add')
@section('content')
    <div class="content-body">
        <div class="card">
            <div class="card-bod">
                <div class="row">
                    <!-- Invoice repeater -->
                    <form method="POST" id="add_form" enctype="multipart/form-data">
                        @csrf
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Add New Property</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic">Select Billing Type</label>
                                            <select class="select2 select form-select my_select" name="type_id"
                                                id="select2-basic" required>
                                                <option></option>
                                                @foreach ($property_types as $property_type)
                                                    <option value="{{ $property_type->id }}">{{ $property_type->type }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic-prop">Select Property Type</label>
                                            <select class="select2 select form-select my_select" name="typesProperty"
                                                id="select2-basic-prop" required>
                                                <option></option>
                                                <option value = "plot">Plot</option>
                                                <option value = "house">House</option>
                                                <option value = "commercial">Commercial</option>
                                                <option value = "apartment">Apartment</option>
                                            </select>
                                        </div>
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic-1">Select City</label>
                                            <select class="select2 select form-select my_select select2-basic-1"
                                                name="city_id" id="select2-basic-1" required>
                                                <option></option>
                                                @foreach ($cities as $city)
                                                    <option value="{{ $city->id }}">{{ $city->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic-2">Select Society</label>
                                            <select class="select2 select form-select my_select select2-basic-2" disabled
                                                name="society_id" id="select2-basic-2" required>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic-3">Select Block</label>
                                            <select class="select2 select form-select my_select select2-basic-3" disabled
                                                name="block_id" id="select2-basic-3" required>
                                                <option></option>
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="property_number">Property Number</label>
                                                <input type="text" class="form-control dt-full-name" id="property_number"
                                                    placeholder="Enter Property Number" name="property_number"
                                                    aria-label="type" required />
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="purchased_price">Purchase Price</label>
                                                <input type="number" pattern="[0-9]*" class="form-control dt-full-name"
                                                    id="purchased_price" placeholder="Enter Purchase Price"
                                                    name="purchased_price" aria-label="purchased_price" required />
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="landAreaKanal">Land Area / Kanal</label>
                                                <input type="text" class="form-control dt-full-name" id="landAreaKanal"
                                                    placeholder="Enter Land Area / Kanal" name="landAreaKanal"
                                                    aria-label="landAreaKanal" />
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="landAreaMarla">Land Area / Marla</label>
                                                <input type="text" class="form-control dt-full-name" id="landAreaMarla"
                                                    placeholder="Enter Land Area / Marla" name="landAreaMarla"
                                                    aria-label="landAreaMarla" />
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="landAreaSqfeet">Land Area / Square
                                                    feet</label>
                                                <input type="text" class="form-control dt-full-name" id="landAreaSqfeet"
                                                    placeholder="Enter Land Area / Kanal" name="landAreaSqfeet"
                                                    aria-label="landAreaSqfeet" />
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="covered_area">Covered Area</label>
                                                <input type="text" class="form-control dt-full-name" id="covered_area"
                                                    placeholder="Enter Covered Area" name="covered_area"
                                                    aria-label="covered_area" />
                                            </div>
                                        </div>
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic-edit_1">Select Covered Area
                                                Unit</label>
                                            <select class="select2 select form-select my_select"
                                                name="covered_area_unit_id" id="select2-basic-edit_1">
                                                <option></option>
                                                @foreach ($units as $val)
                                                    <option value="{{ $val->id }}">{{ $val->unit }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="extra_area">Extra Land</label>
                                                <input type="text" class="form-control dt-full-name" id="extra_area"
                                                    placeholder="Enter Extra Land" name="extra_area"
                                                    aria-label="extra_area" />
                                            </div>
                                        </div>
                                        <div class="col-4 mb-1">
                                            <label class="form-label" for="select2-basic-edit">Select Extra Land
                                                Unit</label>
                                            <select class="select2 select form-select my_select" name="extra_area_unit_id"
                                                id="select2-basic-edit">
                                                <option></option>
                                                @foreach ($units as $val)
                                                    <option value="{{ $val->id }}">{{ $val->unit }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="share">Share</label>
                                                <input type="text" class="form-control dt-full-name" id="share"
                                                    placeholder="Enter Share" name="share" aria-label="share" />
                                            </div>
                                        </div>
                                        <div class="col-4">
                                            <div class="mb-1">
                                                <label class="form-label" for="pending_dues">Pending Dues</label>
                                                <input type="text" class="form-control dt-full-name" id="pending_dues"
                                                    placeholder="Enter Pending Dues" name="pending_dues"
                                                    aria-label="pending_dues" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Possession</h4>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="possession_area">Enter Possession
                                                    Area</label>
                                                <input type="text" class="form-control dt-full-name"
                                                    id="possession_area" placeholder="Enter Possession Area"
                                                    name="possession_area" aria-label="possession_area" />
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="allocated_area">Enter Allocated
                                                    Area</label>
                                                <input type="text" class="form-control dt-full-name"
                                                    id="allocated_area" placeholder="Enter Allocated Area"
                                                    name="alloted_area" aria-label="allocated_area" />
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="dimention_length">Dimention
                                                    Length(foot)</label>
                                                <input type="text" class="form-control dt-full-name"
                                                    id="dimention_length" placeholder="Enter Dimension Length"
                                                    name="dimention_length" aria-label="dimention_length" />
                                            </div>
                                        </div>
                                        <div class="col-6">
                                            <div class="mb-1">
                                                <label class="form-label" for="dimention_width">Dimention
                                                    width(foot)</label>
                                                <input type="text" class="form-control dt-full-name"
                                                    id="dimention_width" placeholder="Enter Dimention Width"
                                                    name="dimention_width" aria-label="dimention_width" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <section id="accordion">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="accordionWrapa1" role="tablist" aria-multiselectable="true">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="accordion accordion-margin" id="accordionExample">
                                                    <div class="accordion-item">
                                                        <h2 class="accordion-header" id="headingOne">
                                                            <button class="accordion-button collapsed" type="button"
                                                                data-bs-toggle="collapse" data-bs-target="#accordionOne"
                                                                aria-expanded="false" aria-controls="accordionOne">
                                                                <strong>IF Litigations Is Applicable</strong>
                                                            </button>
                                                        </h2>
                                                        <div id="accordionOne" class="accordion-collapse collapse"
                                                            aria-labelledby="headingOne"
                                                            data-bs-parent="#accordionExample">
                                                            <div class="accordion-body">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="litigation_cout_name">Court Name
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="litigation_cout_name"
                                                                                placeholder="Enter Court Name"
                                                                                name="litigation_cout_name"
                                                                                aria-label="litigation_cout_name" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="litigation_suite">Title Of Suite
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="litigation_suite"
                                                                                placeholder="Enter Title Of Suite"
                                                                                name="litigation_suite"
                                                                                aria-label="litigation_suite" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="litigation_date">Date Of
                                                                                Litigation/Orders
                                                                                Of Court</label>
                                                                            <input type="text" id="litigation_date"
                                                                                class="form-control flatpickr-basic"
                                                                                name="litigation_date"
                                                                                placeholder="YYYY-MM-DD" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="litigation_image">Select Image</label>
                                                                            <input type="file" name="litigation_image"
                                                                                id="litigation_image"
                                                                                class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="litigation_remarks">Remarks
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="litigation_remarks"
                                                                                placeholder="Enter Remarks"
                                                                                name="litigation_remarks"
                                                                                aria-label="litigation_remarks" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-item">
                                                        <h2 class="accordion-header" id="headingTwo">
                                                            <button class="accordion-button collapsed" type="button"
                                                                data-bs-toggle="collapse" data-bs-target="#accordionTwo"
                                                                aria-expanded="false" aria-controls="accordionTwo">
                                                                <strong>IF Mortgage Is Applicable</strong>
                                                            </button>
                                                        </h2>
                                                        <div id="accordionTwo" class="accordion-collapse collapse"
                                                            aria-labelledby="headingTwo"
                                                            data-bs-parent="#accordionExample">
                                                            <div class="accordion-body">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_bank">Bank:
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_bank"
                                                                                placeholder="Enter Bank Name"
                                                                                name="mortage_bank"
                                                                                aria-label="mortage_bank" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_branch">Bank Branch
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_branch"
                                                                                placeholder="Enter Bank Branch"
                                                                                name="mortage_branch"
                                                                                aria-label="mortage_branch" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_document">Document No:
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_document"
                                                                                placeholder="Enter Document No"
                                                                                name="mortage_document"
                                                                                aria-label="mortage_document" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_book">Book No:
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_book"
                                                                                placeholder="Enter Book No"
                                                                                name="mortage_book"
                                                                                aria-label="mortage_book" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_volume">Volume No:
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_volume"
                                                                                placeholder="Enter Volume No"
                                                                                name="mortage_volume"
                                                                                aria-label="mortage_volume" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_pages">Pages:
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_pages"
                                                                                placeholder="Enter Pages"
                                                                                name="mortage_pages"
                                                                                aria-label="mortage_pages" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_sub_register">Sub Registrar
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="mortage_sub_register"
                                                                                placeholder="Enter Sub Registrar"
                                                                                name="mortage_sub_register"
                                                                                aria-label="mortage_sub_register" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_date">Date
                                                                            </label>
                                                                            <input type="text" id="mortage_date"
                                                                                class="form-control flatpickr-basic"
                                                                                name="mortage_date"
                                                                                placeholder="YYYY-MM-DD" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="mortage_image">Select Image</label>
                                                                            <input type="file" name="mortage_image"
                                                                                id="mortage_image" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="accordion-item">
                                                        <h2 class="accordion-header" id="headingThree">
                                                            <button class="accordion-button collapsed" type="button"
                                                                data-bs-toggle="collapse" data-bs-target="#accordionThree"
                                                                aria-expanded="false" aria-controls="accordionThree">
                                                                <strong>IF NAB/Anti Corruption Is Applicable</strong>
                                                            </button>
                                                        </h2>
                                                        <div id="accordionThree" class="accordion-collapse collapse"
                                                            aria-labelledby="headingThree"
                                                            data-bs-parent="#accordionExample">
                                                            <div class="accordion-body">
                                                                <div class="row">
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="nab_detail">Detail
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="nab_detail" placeholder="Enter Detail"
                                                                                name="nab_detail"
                                                                                aria-label="nab_detail" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="nab_image">Select Image</label>
                                                                            <input type="file" name="nab_image"
                                                                                id="nab_image" class="form-control">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="accordion_rent">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div id="accordionWrapa2" role="tablist" aria-multiselectable="true">
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="accordion accordion-margin" id="accordionExample_1">
                                                    <div class="accordion-item">
                                                        <h2 class="accordion-header" id="heading_One">
                                                            <button class="accordion-button collapsed" type="button"
                                                                data-bs-toggle="collapse"
                                                                data-bs-target="#accordion_tenant" aria-expanded="false"
                                                                aria-controls="accordion_tenant">
                                                                <strong>Property Tenant:&nbsp;(If Any)</strong>
                                                            </button>
                                                        </h2>
                                                        <div id="accordion_tenant" class="accordion-collapse collapse"
                                                            aria-labelledby="heading_One"
                                                            data-bs-parent="#accordionExample_1">
                                                            <div class="accordion-body">
                                                                <div class="row">
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="tenant_name">Name
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="tenant_name"
                                                                                placeholder="Enter Tenant Name"
                                                                                name="tenant_name"
                                                                                aria-label="tenant_name" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="tenant_father_name">Father Name
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="tenant_father_name"
                                                                                placeholder="Enter Father Name"
                                                                                name="tenant_father_name"
                                                                                aria-label="tenant_father_name" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3 mb-1">
                                                                        <label class="form-label" for="tenant_cnic">CNIC
                                                                            No./NTN
                                                                            No.</label>
                                                                        <input type="text"
                                                                            class="form-control cnic-mask"
                                                                            oninput="this.value = this.value.replace(/[^0-9]/g, '');"
                                                                            placeholder="Enter Cnic" name="tenant_cnic"
                                                                            id="tenant_cnic" />
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="rental_email">Email</label>
                                                                            <input type="email"
                                                                                class="form-control dt-full-name"
                                                                                id="rental_email"
                                                                                placeholder="Enter Email"
                                                                                name="tenant_email"
                                                                                aria-label="rental_email" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="tenant_cnic_image">Select
                                                                                Image</label>
                                                                            <input type="file" name="tenant_image"
                                                                                id="tenant_cnic_image"
                                                                                class="form-control">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="tanant_address">
                                                                                Address:</label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="tanant_address"
                                                                                placeholder="Enter Current Address"
                                                                                name="tenant_address"
                                                                                aria-label="tanant_address" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <div>
                                                                                <label class="form-label"
                                                                                    for="tanant_phone">
                                                                                    Phone
                                                                                    No./Mobile No.
                                                                                </label>
                                                                            </div>
                                                                            <input type="tel"
                                                                                class="form-control phone_number"
                                                                                placeholder="Enter Mobile No:"
                                                                                id="tanant_phone" name="tenant_phone"
                                                                                aria-label="tanant_phone" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label" for="tenant_rent">
                                                                                Agreed Rent:</label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="tenant_rent"
                                                                                placeholder="Enter Agreed Rent"
                                                                                name="tenant_rent"
                                                                                aria-label="tenant_rent" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-3">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="annual_increment">
                                                                                Annual Increment:</label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="annual_increment"
                                                                                placeholder="Enter Annual Agreement"
                                                                                name="tenant_increment"
                                                                                aria-label="annual_increment" />
                                                                        </div>
                                                                    </div>
                                                                    <strong class="mb-1">Rental Agreement
                                                                        Duration</strong>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="agreement_from_date">From
                                                                                Date:</label>
                                                                            <input type="text" id="agreement_from_date"
                                                                                class="form-control flatpickr-basic"
                                                                                name="tenant_from_date"
                                                                                placeholder="YYYY-MM-DD" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="agreement_to_date">To Date:</label>
                                                                            <input type="text" id="agreement_to_date"
                                                                                class="form-control flatpickr-basic"
                                                                                name="tenant_to_date"
                                                                                placeholder="YYYY-MM-DD" />
                                                                        </div>
                                                                    </div>
                                                                    <strong class="mb-1">Property Dealer Details</strong>
                                                                    <div class="col-4">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="dealer_name">Name
                                                                            </label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="dealer_name"
                                                                                placeholder="Enter Tenant Name"
                                                                                name="dealer_name"
                                                                                aria-label="dealer_name" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="dealer_address">
                                                                                Address:</label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="dealer_address"
                                                                                placeholder="Enter Current Address"
                                                                                name="dealer_address"
                                                                                aria-label="dealer_address" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-4">
                                                                        <div class="mb-1">
                                                                            <div>
                                                                                <label class="form-label"
                                                                                    for="dealer_phone">
                                                                                    Phone
                                                                                    No./Mobile No.
                                                                                </label>
                                                                            </div>
                                                                            <input type="tel"
                                                                                class="form-control phone_number"
                                                                                placeholder="Enter Dealer Mobile No:"
                                                                                id="dealer_phone" name="dealer_phone"
                                                                                aria-label="dealer_phone" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-6">
                                                                        <div class="mb-1">
                                                                            <label class="form-label"
                                                                                for="tenant_remarks">
                                                                                Remarks:</label>
                                                                            <input type="text"
                                                                                class="form-control dt-full-name"
                                                                                id="tenant_remarks"
                                                                                placeholder="Enter Remarks"
                                                                                name="tenant_remarks"
                                                                                aria-label="tenant_remarks" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-12 mb-1">
                                                                        <section id="vuexy-checkbox-color">
                                                                            <div class="row">
                                                                                <div class="col-12">
                                                                                    <div class="card">
                                                                                        <div class="card-body">
                                                                                            <div class="mb-1">
                                                                                                <strong>Assign Bill To
                                                                                                    Tenant</strong>
                                                                                            </div>
                                                                                            <div
                                                                                                class="demo-inline-spacing">
                                                                                                <div
                                                                                                    class="form-check form-check-info">
                                                                                                    <input type="checkbox"
                                                                                                        class="form-check-input"
                                                                                                        id="colorCheck1"
                                                                                                        name="maintenance" />
                                                                                                    <label
                                                                                                        class="form-check-label"
                                                                                                        for="colorCheck1">In-House
                                                                                                        Facilities</label>
                                                                                                </div>
                                                                                                <div
                                                                                                    class="form-check form-check-info">
                                                                                                    <input type="checkbox"
                                                                                                        class="form-check-input"
                                                                                                        id="colorCheck2"
                                                                                                        name="common_area_bill" />
                                                                                                    <label
                                                                                                        class="form-check-label"
                                                                                                        for="colorCheck2">Common
                                                                                                        Area Bill</label>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </section>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <section id="accordion-with-margin">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <div class="accordion accordion-margin" id="accordionMargin">
                                                <div class="accordion-item">
                                                    <h2 class="accordion-header" id="headingMarginOne">
                                                        <button class="accordion-button collapsed" type="button"
                                                            data-bs-toggle="collapse" data-bs-target="#accordionMarginOne"
                                                            aria-expanded="false" aria-controls="accordionMarginOne">
                                                            <strong>Property Owner</strong>
                                                        </button>
                                                    </h2>
                                                    <div id="accordionMarginOne" class="accordion-collapse collapse"
                                                        aria-labelledby="headingMarginOne"
                                                        data-bs-parent="#accordionMargin">
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="card-body">
                                                                    <div class="property_repeat" id="property_repeat">
                                                                        <div data-repeater-list="property_owner">
                                                                            <div data-repeater-item>
                                                                                <h4 class="mb-2"><strong
                                                                                        class="prop_owner">Property Owner
                                                                                        1</strong></h4>
                                                                                <div class="row d-flex align-items-end">
                                                                                    <div class="col-6">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="membership_number">Membership
                                                                                                Number</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="membership_number"
                                                                                                placeholder="Enter Membership Number"
                                                                                                name="membership_number"
                                                                                                aria-label="type"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-6 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="select2-basic-edit">Select
                                                                                            GL</label>
                                                                                        <select
                                                                                            class="select2 select form-select my_select"
                                                                                            name="gl_code"
                                                                                            id="select2-basic-edit">
                                                                                            <option></option>
                                                                                            @foreach ($gl_code as $gl)
                                                                                                <option
                                                                                                    value = "{{ $gl->account_code }}">
                                                                                                    {{ $gl->account_code }}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="full_name">Full
                                                                                                Name</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="full_name"
                                                                                                placeholder="Enter Name"
                                                                                                name="name"
                                                                                                aria-label="type"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="guardian_name">Father/Husband
                                                                                                Name</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="guardian_name"
                                                                                                placeholder="Enter Father/Husband Name"
                                                                                                name="father_name"
                                                                                                aria-label="guardian_name"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="email">Email</label>
                                                                                            <input type="email"
                                                                                                class="form-control dt-full-name"
                                                                                                id="email"
                                                                                                placeholder="Enter Email"
                                                                                                name="email"
                                                                                                aria-label="user-email"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="cnic">CNIC No./NTN
                                                                                            No.</label>
                                                                                        <input type="text"
                                                                                            class="form-control cnic-mask"
                                                                                            oninput="this.value = this.value.replace(/[^0-9]/g, '');"
                                                                                            placeholder="Enter Cnic"
                                                                                            name="cnic" id="cnic"
                                                                                            required />
                                                                                    </div>
                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="cnic_exp_date">CNIC/NTN
                                                                                            EXPIRY DATE:</label>
                                                                                        <input type="text"
                                                                                            id="cnic_exp_date"
                                                                                            class="form-control flatpickr-basic"
                                                                                            name="cnicExpiryDate"
                                                                                            placeholder="YYYY-MM-DD"
                                                                                            required />
                                                                                    </div>

                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="curr_address">Current
                                                                                                Address:</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="curr_address"
                                                                                                placeholder="Enter Current Address"
                                                                                                name="address"
                                                                                                aria-label="curr_adress"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="permanent_address">Permanent
                                                                                                Address:</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="permanent_address"
                                                                                                placeholder="Enter Permanent Address"
                                                                                                name="permanent_address"
                                                                                                aria-label="permanent_address"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1 testing">
                                                                                            <div>
                                                                                                <label
                                                                                                    class="form-label">Phone
                                                                                                    No./Mobile No.
                                                                                                </label>
                                                                                            </div>
                                                                                            <input type="tel"
                                                                                                class="form-control phone_number"
                                                                                                placeholder="Enter Mobile No:"
                                                                                                name="phone_number"
                                                                                                aria-label="phone_number"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <div>
                                                                                                <label
                                                                                                    class="form-label">Secondary
                                                                                                    Phone
                                                                                                    No./Mobile No.
                                                                                                </label>
                                                                                            </div>
                                                                                            <input type="tel"
                                                                                                class="form-control phone_number"
                                                                                                placeholder="Enter Secondary Mobile No:"
                                                                                                name="secondary_phone_number"
                                                                                                aria-label="sec_phone"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="vote_number">Member
                                                                                                Vote</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="vote_number"
                                                                                                placeholder="Enter Member Vote:"
                                                                                                name="member_vote"
                                                                                                aria-label="vote_number"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>

                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="approval_date">Date Of
                                                                                            approval by the Managing
                                                                                            Committee</label>
                                                                                        <input type="text"
                                                                                            id="approval_date"
                                                                                            class="form-control flatpickr-basic"
                                                                                            name="date_approval_managingCommitte"
                                                                                            placeholder="YYYY-MM-DD"
                                                                                            required />
                                                                                    </div>
                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="confirmation_date">Date Of
                                                                                            Confirmation by the General
                                                                                            Body</label>
                                                                                        <input type="text"
                                                                                            id="confirmation_date"
                                                                                            class="form-control flatpickr-basic"
                                                                                            name="date_confirmation_by_GB"
                                                                                            placeholder="YYYY-MM-DD"
                                                                                            required />
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="share_number">Number
                                                                                                Of Shares</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="share_number"
                                                                                                placeholder="Enter Shares Number"
                                                                                                name="no_shares"
                                                                                                aria-label="share_number"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="share_value">Value of
                                                                                                Shares</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="share_value"
                                                                                                placeholder="Enter Value of Share"
                                                                                                name="value_shares"
                                                                                                aria-label="share_value"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4">
                                                                                        <div class="mb-1">
                                                                                            <label class="form-label"
                                                                                                for="share_capital">Share
                                                                                                Capitals</label>
                                                                                            <input type="text"
                                                                                                class="form-control dt-full-name"
                                                                                                id="share_capital"
                                                                                                placeholder="Enter Share Capitals"
                                                                                                name="share_capital"
                                                                                                aria-label="share_capital"
                                                                                                required />
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="member_enroll_date">Date
                                                                                            Of Enrolment as Member</label>
                                                                                        <input type="text"
                                                                                            id="member_enroll_date"
                                                                                            class="form-control flatpickr-basic"
                                                                                            name="date_enrolment_member"
                                                                                            placeholder="YYYY-MM-DD"
                                                                                            required />
                                                                                    </div>
                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="from_date">MemberShip From
                                                                                            Date:</label>
                                                                                        <input type="text"
                                                                                            class="form-control flatpickr-basic"
                                                                                            id="from_date"
                                                                                            name="membership_from_date"
                                                                                            placeholder="YYYY-MM-DD"
                                                                                            required />
                                                                                    </div>
                                                                                    <div class="col-4 mb-1">
                                                                                        <label class="form-label"
                                                                                            for="to_date">MemberShip To
                                                                                            Date:</label>
                                                                                        <input type="text"
                                                                                            id="to_date"
                                                                                            class="form-control flatpickr-basic"
                                                                                            name="membership_to_date"
                                                                                            placeholder="YYYY-MM-DD"
                                                                                            required />
                                                                                    </div>
                                                                                    <div class="col-md-2 col-12 mb-50">
                                                                                        <div class="mb-1">
                                                                                            <button
                                                                                                class="btn btn-outline-danger text-nowrap px-1"
                                                                                                data-repeater-delete
                                                                                                type="button">
                                                                                                <i data-feather="x"
                                                                                                    class="me-25"></i>
                                                                                                <span>Delete</span>
                                                                                            </button>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="row">
                                                                            <div class="col-12">
                                                                                <button class="btn btn-icon btn-primary"
                                                                                    type="button" data-repeater-create>
                                                                                    <i data-feather="plus"
                                                                                        class="me-25"></i>
                                                                                    <span>Add New</span>
                                                                                </button>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" aria-label="Close">
                                Reset
                            </button>
                            <button type="button" id="submit_button" class="btn btn-primary" value="save">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        $(document).ready(function() {
            initializeIntlTelInput('.phone_number');
            $('#submit_button').on('click', function() {
                beforeSubmission();
                $('#add_form').submit();

            });
            $('#add_form').submit(function(e) {
                e.preventDefault();
                let formdata = new FormData(this);
                $.ajax({
                    url: '{!! route('add.store') !!}',
                    method: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#add_form')[0].reset();
                            Toast.fire({
                                    icon: 'success',
                                    title: 'Property has been Added Successfully!'
                                }),
                                setTimeout(function() {
                                    window.location.href = '{!! route('list.index') !!}';
                                }, 1);
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                    }
                });
            })


            var ayat = $('#property_repeat');
            $(ayat).repeater({
                isFirstItemUndeletable: true,
                show: function() {
                    $(this).slideDown();
                    ayat.find('select').next('.select2-container').remove();
                    ayat.find('select').select2();
                    var rowIndex = $(this).index() + 1;
                    destroyIntlTelInput('.phone_number');
                    initializeIntlTelInput('.phone_number');
                    $(this).find(".prop_owner").text('Property Owner ' + rowIndex);
                    var datesCollection = document.getElementsByClassName("flatpickr-basic");
                    var dates = Array.from(datesCollection);
                    dates.forEach(function(date) {
                        new Cleave(date, {
                            date: true,
                            delimiter: '-',
                            datePattern: ['Y', 'm', 'd']
                        })
                        datesCollection.flatpickr();
                    });
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
        });
        $('.select2-basic-1').on('change', function() {
            var data = $(".select2-basic-1 option:selected").val();
            if (data) {
                $("#select2-basic-2").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/add') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "society"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-2');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var society = response.data[i];
                            selectElement.append('<option value="' + society.id + '">' + society.name +
                                '</option>');
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-2').val(null).trigger('change');
                $("#select2-basic-2").prop('disabled', true);
            }
        });
        $('.select2-basic-2').on('change', function() {
            var data = $(".select2-basic-2 option:selected").val();
            if (data) {
                $("#select2-basic-3").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/add') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "block"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-3');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var block = response.data[i];
                            selectElement.append('<option value="' + block.id + '">' + block.name +
                                '</option>');
                        }
                        selectElement.select2();
                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-3').val(null).trigger('change');
                $("#select2-basic-3").prop('disabled', true);
            }
        });

        function initializeIntlTelInput(targetClass) {
            $(targetClass).each(function(i, obj) {
                window.intlTelInput(obj, {
                    allowDropdown: true,
                    autoHideDialCode: true,
                    autoPlaceholder: "polite",
                    separateDialCode: true,
                    customPlaceholder: null,
                    initialCountry: "pk",
                    utilsScript: "https://cdn.jsdelivr.net/npm/intl-tel-input@19.5.3/build/js/utils.js",
                });
            });
        }

        function destroyIntlTelInput(targetClass) {
            $(targetClass).each(function(i, obj) {
                const iti = window.intlTelInputGlobals.getInstance(obj);
                if (iti) {
                    iti.destroy();
                }
            });
        }

        function beforeSubmission() {
            var inputs = document.querySelectorAll('.phone_number');
            inputs.forEach(function(input) {
                updatePhoneNumber(input);
            });
        }

        function updatePhoneNumber(targetClass) {
            let iti = window.intlTelInputGlobals.getInstance(targetClass);
            targetClass.value = iti.getNumber();
        }
    </script>
@endsection
