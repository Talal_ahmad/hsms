@extends('admin.layouts.master')
@section('title', 'Cities')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">City</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Cities
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card pb-2">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th class="not_include"></th>
                                <th>id</th>
                                <th>City</th>
                                <th class="not_include">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add New City</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="modalAddCardNumber">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="fullname"
                                            placeholder="Enter Name" name="name" aria-label="city-name" required />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update City</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_update_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="modalAddCardNumber">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="fullname-edit"
                                            placeholder="Enter Name" name="name" aria-label="city-name" required />
                                        <input type="hidden" id="edit-id" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var rowid;
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                responsive: true,
                ajax: '{!! route('city.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: ''
                    },
                ],
                "columnDefs": [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var btn = '';
                            @can('property_setup_city_edit')
                                btn += `<a href="javascript:;" class="item-edit" onclick="edit('` +
                                    full
                                    .name + `',` + full.id + `)">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('property_setup_city_delete')
                                btn +=
                                    '<a href="javascript:;" id="deleteCity" onclick="delete_item(' +
                                    full
                                    .id +
                                    ')">' +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                "order": [
                    [0, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 10,
                lengthMenu: [10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-1',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('property_setup_city_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New Record',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Cities</h6>');

            $('#permission_add_form').submit(function(e) {
                e.preventDefault();
                $.ajax({
                    url: '{!! route('city.store') !!}',
                    type: "POST",
                    data: new FormData(this),
                    processData: false,
                    contentType: false,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#permission_add_form')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'City has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            })

            $('#permission_update_form').submit(function(e) {
                e.preventDefault();
                let edit = $('#fullname-edit').val();
                var cityId = $('#edit-id').val();
                $.ajax({
                    url: "{{ url('property/city/') }}" + "/" + cityId,
                    method: 'PUT',
                    data: {
                        _token: '{{ csrf_token() }}',
                        name: edit
                    },
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#permission_update_form')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'City has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function edit(name, id) {
            $('#fullname-edit').val(name)
            $('#edit-id').val(id)
            $('#large2').modal('show')
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('property/city/') }}" + "/" + id,
                                method: 'DELETE',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'City has been Deleted Successfully!'
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
