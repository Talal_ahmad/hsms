@extends('admin.layouts.master')
@section('title', 'Collection Head')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Collection Heads</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Collection Heads
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>id</th>
                                <th>Code</th>
                                <th>Name</th>
                                <th>Gl</th>
                                <th>Collection Type</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Collection Heads</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_add_form" enctype="multipart/form-data">
                        @csrf
                        @method('POST')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="code">Code</label>
                                        <input type="text" class="form-control dt-full-name" id="code"
                                            placeholder="Enter Code" name="code" aria-label="code" required />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="name"
                                            placeholder="Enter Name" name="name" aria-label="name" required />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic">Select GL</label>
                                    <select class="select2 select form-select my_select" name="gl_code" id="select2-basic"
                                        required>
                                        @foreach ($gl_code as $gl)
                                            <option value = "{{ $gl->account_code }}">{{ $gl->account_code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="payment_type_id">Select Collection Type</label>
                                    <select class="select2 select form-select my_select" name="payment_type_id"
                                        id="payment_type_id" required>
                                        @foreach ($collections as $collection)
                                            <option value = "{{ $collection->id }}">{{ $collection->type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="tax_rate">Tax Rate:</label>
                                        <input type="text" class="form-control dt-full-name" id="tax_rate"
                                            placeholder="Enter Tax Rate" name="tax_rate" aria-label="tax_rate" />
                                    </div>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="taxation_body">Taxation body</label>
                                    <select class="select2 select form-select my_select" name="taxation_body"
                                        id="taxation_body">
                                        <option></option>
                                        <option value="FBR">FBR</option>
                                        <option value="PRA">PRA</option>
                                        <option value="KPRA">KPRA</option>
                                        <option value="SRB">SRB</option>
                                        <option value="BRA">BRA</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="image_path">Select Image</label>
                                        <input type="file" name="image_path" id="image_path" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form mb-0">
                                        <label for="textarea-counter">Description</label>
                                        <textarea data-length="10000" class="form-control char-textarea" id="textarea-counter" rows="3"
                                            placeholder="Enter Description" name="description" style="height: 100px"></textarea>
                                    </div>
                                    <small class="textarea-counter-value float-end"><span class="char-count">0</span> /
                                        10000 </small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal to update record --}}
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update Collection Heads</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_update_form" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="code-edit">Code</label>
                                        <input type="text" class="form-control dt-full-name" id="code-edit"
                                            placeholder="Enter Code" name="code" aria-label="code" required />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="name-edit">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="name-edit"
                                            placeholder="Enter Name" name="name" aria-label="name" required />
                                    </div>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-edit">Select GL</label>
                                    <select class="select2 select form-select my_select" name="gl_code"
                                        id="select2-basic-edit" required>
                                        @foreach ($gl_code as $gl)
                                            <option value = "{{ $gl->account_code }}">{{ $gl->account_code }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="payment_type_id-edit">Select Collection Type</label>
                                    <select class="select2 select form-select my_select" name="payment_type_id"
                                        id="payment_type_id-edit" required>
                                        @foreach ($collections as $collection)
                                            <option value = "{{ $collection->id }}">{{ $collection->type }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="tax_rate_edit">Tax Rate:</label>
                                        <input type="text" class="form-control dt-full-name" id="tax_rate_edit"
                                            placeholder="Enter Tax Rate" name="tax_rate" aria-label="tax_rate" />
                                    </div>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="taxation_body_edit">Taxation Body</label>
                                    <select class="select2 select form-select my_select" name="taxation_body"
                                        id="taxation_body_edit">
                                        <option></option>
                                        <option value="FBR">FBR</option>
                                        <option value="PRA">PRA</option>
                                        <option value="KPRA">KPRA</option>
                                        <option value="SRB">SRB</option>
                                        <option value="BRA">BRA</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="image_path-edit">Select Image</label>
                                        <input type="file" name="image_path" id="image_path-edits"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form mb-0">
                                        <label for="textarea-counter-edit">Description</label>
                                        <textarea data-length="10000" class="form-control char-textarea" id="textarea-counter-edit" rows="3"
                                            placeholder="Enter Description" name="description" style="height: 100px"></textarea>
                                    </div>
                                    <small class="textarea-counter-value float-end"><span class="char-count">0</span> /
                                        10000 </small>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var Table = '';
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('payment-code.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'code'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'gl_code'
                    },
                    {
                        data: 'type_id'
                    },
                    {
                        data: 'description'
                    },
                    {
                        @can('collection_head_status')
                            data: 'status',
                            "render": function(data, type, full, meta) {
                                if (data == 1) {
                                    let str =
                                        `<span onclick="status(${full.id}, 1)" class="badge bg-info px-2 " style="white-space: nowrap; cursor: pointer;">active</span>`;
                                    return str;
                                } else {
                                    let str =
                                        `<span onclick="status(${full.id}, 0)" class="badge bg-danger px-1" style="white-space: nowrap; cursor: pointer;">inactive</span>`
                                    return str;
                                }
                            }
                        @endcan
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('collection_head_edit')
                                btn +=
                                    `<a href="javascript:;" class="item-edit" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('collection_head_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'ADD Collection Heads',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Collection Heads</h6>');
            $('#body_add_form').on('submit', function(event) {
                event.preventDefault();
                let formdata = new FormData(this);
                $.ajax({
                    url: '{!! route('payment-code.store') !!}',
                    method: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#body_add_form')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Collection Head has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
            $('#body_update_form').on('submit', function(event) {
                event.preventDefault();
                let Id = $('#edit-id-2').val();
                let formData = $(this).serialize();

                $.ajax({
                    url: "{{ url('/collections/payment-code/') }}" + "/" + Id,
                    method: 'PUT',
                    data: formData,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#body_update_form')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Collection Head has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/collections/payment-code/') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    $('#code-edit').val(response.data.code);
                    $('#name-edit').val(response.data.name);
                    $('#select2-basic-edit').val(response.data.gl_code).select2();
                    $('#payment_type_id-edit').val(JSON.parse(response.data.payment_type_id)).select2();
                    $('#textarea-counter-edit').val(response.data.description);
                    $('#tax_rate_edit').val(response.data.tax_rate);
                    $('taxation_body_edit').val(response.data.taxation_body).select2();
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }

        function status(id, check) {
            let msg = check == 1 ? "Inactive" : "Active";
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: `You want to ${msg} this!`,
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('/collections/payment-code/') }}" + "/" + id +
                                    "/status",
                                method: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    check: check,
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: `Collection head has been ${msg} Successfully!`
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
