@extends('admin.layouts.master')
@section('title', 'Collection Type')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Collection Types</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Collection Types
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>id</th>
                                <th>Name</th>
                                <th>Days-Span</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Collection Type</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="collection-type">Collection Type</label>
                                        <input type="text" class="form-control dt-full-name" id="collection-type"
                                            placeholder="Enter Type" name="type" aria-label="collection-type" />
                                    </div>
                                </div>
                                <strong class="col-12 mb-1">IF REQUIRED:</strong>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="num_of_days">Days-Span</label>
                                        <input type="text" class="form-control dt-full-name" id="num_of_days"
                                            placeholder="Enter Type" name="num_of_days" aria-label="num_of_days" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal to update record --}}
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update Collection Type</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_update_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="edit-collection-type">Edit Collection Type</label>
                                        <input type="text" class="form-control dt-full-name" id="edit-collection-type"
                                            placeholder="Enter Type" name="type" aria-label="collection-type" />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="num_of_days_edit">Days-Span</label>
                                        <input type="text" class="form-control dt-full-name" id="num_of_days_edit"
                                            placeholder="Enter Type" name="num_of_days" aria-label="num_of_days" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var Table = '';
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('payment-type.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id'
                    },
                    {
                        data: 'type'
                    },
                    {
                        data: 'num_of_days'
                    },
                    {
                        @can('collection_types_status')
                            data: 'status',
                            "render": function(data, type, full, meta) {
                                if (data == 1) {
                                    let str =
                                        `<span onclick="status(${full.id}, 1)" class="badge bg-info px-2 " style="white-space: nowrap; cursor: pointer;">active</span>`;
                                    return str;
                                } else {
                                    let str =
                                        `<span onclick="status(${full.id}, 0)" class="badge bg-danger px-1" style="white-space: nowrap; cursor: pointer;">inactive</span>`
                                    return str;
                                }
                            }
                        @endcan
                    },
                    {
                        data: ''
                    },
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('collection_types_edit')
                                btn +=
                                    `<a href="javascript:;" class="item-edit" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('collection_types_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New Record',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Collection Types</h6>');

            $('#permission_add_form').on('submit', function(event) {
                event.preventDefault();
                let formdata = new FormData(this);

                $.ajax({
                    url: '{!! route('payment-type.store') !!}',
                    method: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#permission_add_form')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Collection Type has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });

            $('#permission_update_form').on('submit', function(event) {
                event.preventDefault();

                let unit_Id = $('#edit-id-2').val();
                let formdata = new FormData(this);

                $.ajax({
                    type: "post",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        '_method': 'patch'
                    },
                    url: "{{ url('collections/payment-type/') }}" + "/" + unit_Id,
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#permission_update_form')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Collection Type has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/collections/payment-type') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    $('#edit-collection-type').val(response.data.type);
                    $('#num_of_days_edit').val(response.data.num_of_days);
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }

        function status(id, check) {
            let msg = check == 1 ? "Inactive" : "Active";
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: `You want to ${msg} this!`,
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('collections/payment-type/') }}" + "/" + id + "/status",
                                method: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    check: check,
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: `Collection Type has been ${msg} Successfully!`
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
