@extends('admin.layouts.master')
@section('title', 'Collection Plan')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Collection Plans</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Collection Plans
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>id</th>
                                <th>Name</th>
                                <th>Start Date</th>
                                <th>From Date</th>
                                <th>Total Amount</th>
                                <th>Description</th>
                                <th>Assigned By</th>
                                <th>Assigned Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Collection Plans</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_add_form" enctype="multipart/form-data">
                        @csrf
                        @method('POST')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="plan_name">Plan Name</label>
                                        <input type="text" class="form-control dt-full-name" id="plan_name"
                                            placeholder="Enter Plan Name" name="plan_name" aria-label="plan_name" />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount_after_due_date">Amount After Due Date:
                                            ( % )</label>
                                        <input type="text" class="form-control dt-full-name" id="amount_after_due_date"
                                            placeholder="Enter Amount" name="amount_after_due_date"
                                            aria-label="amount_after_due_date" />
                                    </div>
                                </div>
                                <strong class="col-12 mb-1">Implementation Date:</strong>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="from_date">From Date:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="from_date"
                                        name="from_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="to_date">To Date:</label>
                                    <input type="text" id="to_date" class="form-control flatpickr-basic" name="to_date"
                                        placeholder="YYYY-MM-DD" />
                                </div>
                                <strong class="col-12 mb-1">Bill Expiry Date:</strong>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="start_date">Start Date:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="start_date"
                                        name="start_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="end_date">End Date:</label>
                                    <input type="text" id="end_date" class="form-control flatpickr-basic"
                                        name="end_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="image_path">Select Image</label>
                                        <input type="file" name="image_path" id="image_path" class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form mb-0">
                                        <label for="textarea-counter">Description</label>
                                        <textarea data-length="10000" class="form-control char-textarea" id="textarea-counter" rows="3"
                                            placeholder="Enter Description" name="description" style="height: 100px"></textarea>
                                    </div>
                                    <small class="textarea-counter-value float-end"><span class="char-count">0</span> /
                                        10000 </small>
                                </div>
                            </div>
                            <div class="ayat-repeater erp_type_repeater" id="erp_type_repeater">
                                <div data-repeater-list="member_ship_loop">
                                    <div data-repeater-item>
                                        <h3 class="title">Collection Heads 1</h3>
                                        <div class="row d-flex align-items-end">
                                            <div class="col-5 mb-1">
                                                <label class="form-label">Select Collection
                                                    Type</label>
                                                <select class="select2 select form-select my_select"
                                                    name="payment_code_id">
                                                    <option></option>
                                                    @foreach ($collections as $collection)
                                                        <option value = "{{ $collection->id }}">
                                                            {{ $collection->name . ' / ' . $collection->tax_rate . '%' }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-5">
                                                <div class="mb-1">
                                                    <label class="form-label" for="amount">Amount</label>
                                                    <input type="text" class="form-control dt-full-name"
                                                        id="amount" placeholder="Enter Amount" name="amount"
                                                        aria-label="amount" />
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12 mb-50">
                                                <div class="mb-1">
                                                    <button class="btn btn-outline-danger text-nowrap px-1"
                                                        data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-6 mb-1">
                                                <section id="basic-radio">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="demo-inline-spacing">
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="fixed" />
                                                                    <label class="form-check-label">Fixed Amount</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="kanal" />
                                                                    <label class="form-check-label">Area / Kanal</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="marla" />
                                                                    <label class="form-check-label">Area / Marla</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="square_feet" />
                                                                    <label class="form-check-label">Area / Square feet
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-icon btn-primary " type="button" data-repeater-create>
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Add New</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal to update record --}}
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update Collection Plan</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_update_form" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="plan_name-edit">Plan Name</label>
                                        <input type="text" class="form-control dt-full-name" id="plan_name-edit"
                                            placeholder="Enter Plan Name" name="plan_name" aria-label="plan_name" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="amount_after_due_date-edit">Amount After Due
                                            Date: ( % )</label>
                                        <input type="text" class="form-control dt-full-name"
                                            id="amount_after_due_date-edit" placeholder="Enter Amount"
                                            name="amount_after_due_date" aria-label="plan_name" />
                                    </div>
                                </div>
                                <strong class="col-12 mb-1">Implementation Date:</strong>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="from_date-edit">From Date:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="from_date-edit"
                                        name="from_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="to_date-edit">To Date:</label>
                                    <input type="text" id="to_date-edit" class="form-control flatpickr-basic"
                                        name="to_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <strong class="col-12 mb-1">Bill Expiry Date:</strong>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="start_date_edit">Start Date:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="start_date_edit"
                                        name="start_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="end_date_edit">End Date:</label>
                                    <input type="text" id="end_date_edit" class="form-control flatpickr-basic"
                                        name="end_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="image_path-edit">Select Image</label>
                                        <input type="file" name="image_path" id="image_path-edit"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form mb-0">
                                        <label for="textarea-counter-edit">Description</label>
                                        <textarea data-length="10000" class="form-control char-textarea" id="textarea-counter-edit" rows="3"
                                            placeholder="Enter Description" name="description" style="height: 100px"></textarea>
                                    </div>
                                    <small class="textarea-counter-value float-end"><span class="char-count">0</span> /
                                        10000 </small>
                                </div>
                            </div>
                            <div class="ayat-repeater erp_type_repeater" id="erp_type_repeater-edit">
                                <div data-repeater-list="member_ship_loop">
                                    <div data-repeater-item>
                                        <h3 class="title">Collection Heads 1</h3>
                                        <div class="row d-flex align-items-end">
                                            <div class="col-5 mb-1">
                                                <label class="form-label">Select Collection
                                                    Type</label>
                                                <select class="select2 select form-select my_select"
                                                    name="payment_code_id">
                                                    <option></option>
                                                    @foreach ($collections as $collection)
                                                        <option value = "{{ $collection->id }}">
                                                            {{ $collection->name . ' / ' . $collection->tax_rate . '%' }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            <div class="col-5">
                                                <div class="mb-1">
                                                    <label class="form-label" for="amount-edit">Amount</label>
                                                    <input type="text" class="form-control dt-full-name"
                                                        id="amount-edit" placeholder="Enter Amount" name="amount"
                                                        aria-label="amount-edit" />
                                                </div>
                                            </div>
                                            <div class="col-md-2 col-12 mb-50">
                                                <div class="mb-1">
                                                    <button class="btn btn-outline-danger text-nowrap px-1"
                                                        data-repeater-delete type="button">
                                                        <i data-feather="x" class="me-25"></i>
                                                        <span>Delete</span>
                                                    </button>
                                                </div>
                                            </div>
                                            <div class="col-6 mb-1">
                                                <section id="basic-radio">
                                                    <div class="row">
                                                        <div class="col-12">
                                                            <div class="demo-inline-spacing">
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="fixed" />
                                                                    <label class="form-check-label">Fixed Amount</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="kanal" />
                                                                    <label class="form-check-label">Area / Kanal</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="marla" />
                                                                    <label class="form-check-label">Area / Marla</label>
                                                                </div>
                                                                <div class="form-check form-check-inline">
                                                                    <input class="form-check-input" type="radio"
                                                                        name="calculation_unit" value="square_feet" />
                                                                    <label class="form-check-label">Area / Square feet
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-12">
                                        <button class="btn btn-icon btn-primary " type="button" data-repeater-create>
                                            <i data-feather="plus" class="me-25"></i>
                                            <span>Add New</span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="vertical-modal-ex">
        <!-- Modal -->
        <div class="modal fade" id="erp-details" tabindex="-1" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Collection Head</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody id="entry_type_details">
                            </tbody>
                            <tfoot id="details_footer">
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        var Table = '';
        $(document).ready(function() {
            var ayat = $('.erp_type_repeater');
            $(ayat).repeater({
                isFirstItemUndeletable: false,
                show: function() {
                    $(this).slideDown();
                    ayat.find('select').next('.select2-container').remove();
                    ayat.find('select').select2();
                    var rowIndex = $(this).index() + 1;
                    $(this).find(".title").text('Collection Heads ' + rowIndex);
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('payment-plan.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'plan_name'
                    },
                    {
                        data: 'from_date'
                    },
                    {
                        data: 'to_date'
                    },
                    {
                        data: 'total_amount'
                    },
                    {
                        data: 'description'
                    },
                    {
                        data: 'user_name'
                    },
                    {
                        data: 'assigned_date'
                    },
                    {
                        @can('collection_plans_status')
                            data: 'status',
                            "render": function(data, type, full, meta) {
                                if (data == 1) {
                                    let str =
                                        `<span onclick="status(${full.id}, 1)" class="badge bg-info px-2 " style="white-space: nowrap; cursor: pointer;">active</span>`;
                                    return str;
                                } else {
                                    let str =
                                        `<span onclick="status(${full.id}, 0)" class="badge bg-danger px-1" style="white-space: nowrap; cursor: pointer;">inactive</span>`
                                    return str;
                                }
                            }
                        @endcan
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('collection_plans_edit')
                                btn +=
                                    `<a href="javascript:;" class="item-edit" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('collection_plans_view')
                                btn += '<a href="javascript:;" onclick="view_item(' + full
                                    .id +
                                    ')">' +
                                    feather.icons['eye'].toSvg({
                                        class: 'font-medium-4 text-success'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('collection_plans_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'ADD Collection Plans',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Collection Plans</h6>');

            $('#body_add_form').on('submit', function(event) {
                event.preventDefault();
                $(this).find('[type="submit"]').prop('disabled', true);
                let formdata = new FormData(this);
                $.ajax({
                    url: '{!! route('payment-plan.store') !!}',
                    method: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(response) {
                        setTimeout(function() {
                            $('#body_add_form').find('[type="submit"]').prop(
                                'disabled', false);
                        }, 2000);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $("#large1").modal("hide");
                            $('#body_add_form')[0].reset();
                            $('[data-repeater-list="member_ship_loop"]').empty();
                            var newItem = $('<div data-repeater-item></div>');
                            newItem.html(`
                                <h3 class="title">Collection Heads 1</h3>
                                <div class="row d-flex align-items-end">
                                <div class="col-5 mb-1">
                                <label class="form-label">Select Collection Type</label>
                                <select class="select2 select form-select my_select" name="member_ship_loop[0][payment_code_id]">
                                <option></option>
                                ${response.collections.map(function(collection) {
                                return `<option value="${collection.id}">${collection.name} / ${collection.tax_rate}%</option>`;
                                }).join('')}
                                </select>
                                </div>
                                <div class="col-5">
                                <div class="mb-1">
                                <label class="form-label" for="amount-edit">Amount</label>
                                <input type="text" class="form-control dt-full-name" id="amount-edit" placeholder="Enter Amount" name="member_ship_loop[0][amount]" aria-label="amount-edit" />
                                </div>
                                </div>
                                <div class="col-md-2 col-12 mb-50">
                                <div class="mb-1">
                                <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                                    <i data-feather="x" class="me-25"></i>
                                    <span>Delete</span>
                                </button>
                                </div>
                                </div>
                                <div class="col-6 mb-1">
                                <section id="basic-radio">
                                <div class="row">
                                    <div class="col-12">
                                        <div class="demo-inline-spacing">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="member_ship_loop[0][calculation_unit]" value="fixed"/>
                                                <label class="form-check-label">Fixed Amount</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="member_ship_loop[0][calculation_unit]" value="kanal" />
                                                <label class="form-check-label">Area / Kanal</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="member_ship_loop[0][calculation_unit]" value="marla" />
                                                <label class="form-check-label">Area / Marla</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" name="member_ship_loop[0][calculation_unit]" value="square_feet" />
                                                <label class="form-check-label">Area / Square feet </label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                </section>
                                </div>
                                </div>
                        `);

                            $('[data-repeater-list="member_ship_loop"]').append(newItem);
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Collection Plan has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        $('#body_add_form').find('[type="submit"]').prop('disabled', false);
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
            $('#body_update_form').on('submit', function(event) {
                event.preventDefault();
                let Id = $('#edit-id-2').val();
                let formData = $(this).serialize();
                $(this).find('[type="submit"]').prop('disabled', true);
                $.ajax({
                    url: "{{ url('/collections/payment-plan/') }}" + "/" + Id,
                    method: 'PUT',
                    data: formData,
                    success: function(response) {
                        setTimeout(function() {
                            $('#body_update_form').find('[type="submit"]').prop(
                                'disabled', false);
                        }, 2000);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            var repeater = $('#erp_type_repeater-edit');
                            var firstItem = repeater.find('[data-repeater-item]').first();
                            repeater.find('[data-repeater-item]').not(':first').remove();
                            repeater.find('input, select').val('');
                            repeater.find('input[type="radio"]').prop('checked', false);
                            repeater.find('input[type="radio"]').first().prop('checked', true);
                            $('#body_update_form')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Collection Plan has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        $('#body_update_form').find('[type="submit"]').prop('disabled', false);
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });


        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/collections/payment-plan/') }}" + "/" + id + "/edit",
                method: 'GET',
                data: {
                    check: "edit",
                },
                success: function(response) {
                    $('#plan_name-edit').val(response.data[0].plan_name);
                    $('#from_date-edit').val(response.data[0].from_date);
                    $('#to_date-edit').val(response.data[0].to_date);
                    $('#start_date_edit').val(response.data[0].start_date);
                    $('#end_date_edit').val(response.data[0].end_date);
                    $('#amount_after_due_date-edit').val(response.data[0].amount_after_due_date);
                    $('#textarea-counter-edit').val(response.data[0].description);

                    var paymentPlanDetails = response.details;

                    // Clear existing repeater items
                    $('[data-repeater-list="member_ship_loop"]').empty();

                    // Iterate over each PaymentPlanDetail object
                    paymentPlanDetails.forEach(function(detail, index) {
                        var newItem = $('<div data-repeater-item></div>');

                        // Populate the item with the details from PaymentPlanDetail
                        newItem.html(`
        <h3 class="title">Collection Heads ${index + 1}</h3>
        <div class="row d-flex align-items-end">
            <div class="col-5 mb-1">
                <label class="form-label">Select Collection Type</label>
                <select class="select2 select form-select my_select" name="member_ship_loop[${index}][payment_code_id]">
                    <option></option>
                    ${response.collections.map(function(collection) {
                        return `<option value="${collection.id}" ${detail.payment_code_id == collection.id ? 'selected' : ''}>${collection.name} / ${collection.tax_rate}%</option>`;
                    }).join('')}
                </select>
            </div>
            <div class="col-5">
                    <div class="mb-1">
                        <label class="form-label" for="amount-edit">Amount</label>
                        <input type="text" class="form-control dt-full-name" id="amount-edit" placeholder="Enter Amount" name="member_ship_loop[${index}][amount]" aria-label="amount-edit" value="${detail.amount}" />
                    </div>
            </div>
            <div class="col-md-2 col-12 mb-50">
                    <div class="mb-1">
                        <button class="btn btn-outline-danger text-nowrap px-1" data-repeater-delete type="button">
                            <i data-feather="x" class="me-25"></i>
                            <span>Delete</span>
                        </button>
                    </div>
            </div>
            <div class="col-6 mb-1">
                    <section id="basic-radio">
                        <div class="row">
                            <div class="col-12">
                                <div class="demo-inline-spacing">
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="member_ship_loop[${index}][calculation_unit]" value="fixed" ${detail.calculation_unit == 'fixed' ? 'checked' : ''}/>
                                        <label class="form-check-label">Fixed Amount</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="member_ship_loop[${index}][calculation_unit]" value="kanal" ${detail.calculation_unit == 'kanal' ? 'checked' : ''} />
                                        <label class="form-check-label">Area / Kanal</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="member_ship_loop[${index}][calculation_unit]" value="marla" ${detail.calculation_unit == 'marla' ? 'checked' : ''} />
                                        <label class="form-check-label">Area / Marla</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" type="radio" name="member_ship_loop[${index}][calculation_unit]" value="square_feet" ${detail.calculation_unit == 'square_feet' ? 'checked' : ''} />
                                        <label class="form-check-label">Area / Square feet </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>
            </div>
        </div>
    `);

                        // Append the new item to the repeater list
                        $('[data-repeater-list="member_ship_loop"]').append(newItem);
                    });
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }

        function view_item(id) {
            $('#erp-details').modal('show');
            let itemId = id;
            $.ajax({
                url: "{{ url('/collections/payment-plan') }}" + "/" + itemId + "/edit",
                method: 'GET',
                data: {
                    check: "view",
                },
                success: function(response) {
                    table = $('#entry_type_details').html('');
                    foot = $('#details_footer');
                    $('#exampleModalCenterTitle').text(response.data[0].plan_name);
                    foot.html('<div style="display:flex;"><strong>Total Amount:&nbsp;</strong><p>' + response
                        .data[0].total_amount + '</p></div>');
                    $.each(response.details, function(index, item) {
                        var row = $('<tr>');
                        row.append('<td>' + item.head_name + '</td>');
                        row.append('<td>' + item.amount + '</td>');
                        table.append(row);
                    });
                },
                error: function(xhr) {}
            });
        }

        function status(id, check) {
            let msg = check == 1 ? "Inactive" : "Active";
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: `You want to ${msg} this!`,
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('/collections/payment-plan/') }}" + "/" + id +
                                    "/status",
                                method: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    check: check,
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: `Collection head has been ${msg} Successfully!`
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
        $('.select2-basic-1').on('change', function() {
            var data = $(".select2-basic-1 option:selected").val();
            if (data) {
                $("#select2-basic-2").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/add') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "society"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-2');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var society = response.data[i];
                            selectElement.append('<option value="' + society.id + '">' + society.name +
                                '</option>');
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-2').val(null).trigger('change');
                $("#select2-basic-2").prop('disabled', true);
            }
        });
    </script>
@endsection
