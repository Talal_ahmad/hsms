@extends('admin.layouts.master')
@section('title', 'Assign Plan')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Assign Plan To Properties</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Assign Plan To Properties
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>id</th>
                                <th>City</th>
                                <th>Society</th>
                                <th>Property Block</th>
                                <th>Billing Type</th>
                                <th>Plan</th>
                                <th>Assigned By</th>
                                <th>Assigned Date</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Assign Plan</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_add_form">
                        @csrf
                        @method('POST')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-1">City</label>
                                    <select class="select2 select form-select my_select select2-basic-1" name="city_id"
                                        id="select2-basic-1">
                                        <option value="">Select City</option>
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-2">Society</label>
                                    <select class="select2 select form-select my_select select2-basic-2" disabled
                                        name="society_id" id="select2-basic-2">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic-3">Block</label>
                                    <select class="select2 select form-select my_select select2-basic-3" disabled
                                        name="block_id" id="select2-basic-3">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="select2-basic">Billing Type</label>
                                    <select class="select2 billing select form-select my_select" name="type_id"
                                        id="select2-basic">
                                        <option></option>
                                        @foreach ($property_types as $property_type)
                                            <option value="{{ $property_type->id }}">{{ $property_type->type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="property_id">Property</label>
                                    <select class="select2 property select property_no form-select my_select"
                                        name="property_id[]" id="property_id" disabled multiple>
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-basic_second">Payment Plans</label>
                                    <select class="select2 select form-select my_select" name="payment_plan_id"
                                        id="select2-basic_second">
                                        <option></option>
                                        @foreach ($payment_plans as $payment_plan)
                                            <option value="{{ $payment_plan->id }}">{{ $payment_plan->plan_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update Property Plans</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="edit_plan">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-2-edit">Property</label>
                                    <select class="select2 form-select" name="property_id" id="select2-multiple-2-edit">
                                    </select>
                                </div>
                                <div class="col-12 mb-1">
                                    <label class="form-label" for="select2-multiple-3-edit">Payment Plans</label>
                                    <select class="select2 form-select" name="plan_id[]" id="select2-multiple-3-edit">
                                        @foreach ($payment_plans as $plan)
                                            <option value="{{ $plan->id }}">{{ $plan->plan_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
    <div class="vertical-modal-ex">
        <!-- Modal -->
        <div class="modal fade" id="erp-details" tabindex="-1" aria-labelledby="exampleModalCenterTitle"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalCenterTitle">Assigned Payment Plan Properties</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                            <tbody id="entry_type_details">
                            </tbody>
                            <tfoot id="details_footer">
                            </tfoot>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        var Table = '';
        $(document).ready(function() {
            var ayat = $('.erp_type_repeater');
            $(ayat).repeater({
                isFirstItemUndeletable: false,
                show: function() {
                    $(this).slideDown();
                    ayat.find('select').next('.select2-container').remove();
                    ayat.find('select').select2();
                    var rowIndex = $(this).index() + 1;
                    $(this).find(".title").text('Collection Heads ' + rowIndex);
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('assign-plan.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'DT_RowIndex',
                        name: 'DT_RowIndex',
                        orderable: false,
                        searchable: false,
                    },
                    {
                        data: 'city_name'
                    },
                    {
                        data: 'society_name'
                    },
                    {
                        data: 'block_name'
                    },
                    {
                        data: 'type_name'
                    },
                    {
                        data: 'plan_name'
                    },
                    {
                        data: 'user_name'
                    },
                    {
                        data: 'assigned_date'
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('collection_assign_plan_edit')
                                btn +=
                                    `<a href="#" class="item-edit px-1" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('collection_assign_plan_delete')
                                btn += '<a href="javascript:;" onclick="delete_item(' + full
                                    .id +
                                    ')">' +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: [3, 4, 5, 6, 7]
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('collection_assign_plan_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Assign Plan to Properties For Common Area Bills',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Assign Collection Plan To Properties</h6>');

            $('#body_add_form').on('submit', function(event) {
                event.preventDefault();
                let formdata = new FormData(this);
                $.ajax({
                    url: '{!! route('assign-plan.store') !!}',
                    method: 'POST',
                    data: formdata,
                    processData: false,
                    contentType: false,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            // $('#body_add_form')[0].reset();
                            $('#select2-basic-1').val(null).trigger('change');
                            $('#select2-basic-2').val(null).trigger('change');
                            $('#select2-basic-3').val(null).trigger('change');
                            $('#select2-basic_second').val(null).trigger('change');
                            $('.billing').val(null).trigger('change');
                            $('.property').val(null).trigger('change');
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Plan has been Assigned Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });
        $('.select2-basic-1').on('change', function() {
            var data = $(".select2-basic-1 option:selected").val();
            if (data) {
                $("#select2-basic-2").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/list') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "society"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-2');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var society = response.data[i];
                            selectElement.append('<option value="' + society.id + '">' + society.name +
                                '</option>');
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-2').val(null).trigger('change');
                $("#select2-basic-2").prop('disabled', true);
            }
        });
        $('.select2-basic-2').on('change', function() {
            var data = $(".select2-basic-2 option:selected").val();
            if (data) {
                $("#select2-basic-3").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/properties/list') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "block"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-3');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var block = response.data[i];
                            selectElement.append('<option value="' + block.id + '">' + block.name +
                                '</option>');
                        }
                        selectElement.select2();
                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-3').val(null).trigger('change');
                $("#select2-basic-3").prop('disabled', true);
            }
        });
        $('.billing').on('change', function() {
            var data = $(".billing option:selected").val();
            if (data) {
                $("#property_id").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/collection/assign-plan') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "property",
                        'city_id': $(".select2-basic-1 option:selected").val(),
                        'society_id': $(".select2-basic-2 option:selected").val(),
                        'block_id': $(".select2-basic-3 option:selected").val()
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#property_id');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var property = response.data[i];
                            selectElement.append(
                                `<option value="'${property.id}'"> ${property.prop_num} / ${property.city_name} / ${property.society_name} / ${property.block_name}</option>`
                            );
                        }
                        selectElement.select2();
                    },
                    error: function(xhr) {}
                });
            } else {
                $('#property_id').val(null).trigger('change');
                $("#property_id").prop('disabled', true);
            }
        });

        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/collection/assign-plan') }}" + "/" + id + "/edit",
                data: {
                    "check": "view",
                },
                method: 'GET',
                success: function(response) {
                    var selectElement = document.getElementById("select2-multiple-2-edit");
                    var existingOptions = selectElement.querySelectorAll("option");
                    if (existingOptions.length > 0) {
                        existingOptions.forEach(function(option) {
                            option.remove();
                        });
                    }
                    var optionElement = document.createElement("option");
                    optionElement.value = response.data.property_id;
                    optionElement.textContent =
                        `${response.data.flat_number} / ${response.data.city_name} / ${response.data.society_name}`;
                    selectElement.appendChild(optionElement);

                    $('#select2-multiple-3-edit').val(JSON.parse(response.payment_plans)).select2();
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }

        $('#edit_plan').on('submit', function(event) {
            event.preventDefault();

            let unit_Id = $('#select2-multiple-2-edit').val();
            let formData = $(this).serializeArray();
            $.ajax({
                url: "{{ url('/collection/assign-plan') }}" + "/" + unit_Id,
                method: 'PUT',
                data: formData,
                success: function(response) {
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#edit_plan')[0].reset();
                        $("#large2").modal("hide");
                        Table.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Plans have been Updated Successfully!'
                        })
                    }
                },
                error: function(xhr) {
                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                    if (xhr.status === 422) {

                        var errors = "";
                        //loop through error
                        $.each(xhr.responseJSON.errors, function(i, val) {
                            //loop through inner array for each keys
                            $.each(val, function(i, val1) {
                                toastr['error'](
                                    val1, {
                                        closeButton: true,
                                        tapToDismiss: false,
                                        rtl: isRtl
                                    });
                            })
                        })
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    Table.ajax.reload();
                }
            });
        });

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('collection/assign-plan') }}" + "/" + id,
                                method: 'DELETE',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Plan has been Removed Successfully!'
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
