@extends('admin.layouts.master')
@section('title', 'Complains')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Complains</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Complains
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card pb-2">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th class="not_include"></th>
                                <th>id</th>
                                <th>Complain</th>
                                <th>Description</th>
                                <th>Property/House Number</th>
                                <th>City</th>
                                <th>Phase</th>
                                <th>Block</th>
                                <th class="not_include">Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add New Complain</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_add_form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="property_id">Property</label>
                                    <select class="select2 select property_no form-select my_select" name="property_id"
                                        id="property_id">
                                        <option></option>
                                        @foreach ($properties as $property)
                                            <option value = "{{ $property->id }}">
                                                {{ $property->property_number . '/ ' . $property->city_name . '/ ' . $property->society_name . '/ ' . $property->block_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-6 mb-1">
                                    <label class="form-label" for="type">Complain Type</label>
                                    <select class="select2 select form-select my_select" name="comp_type_id" id="type">
                                        <option></option>
                                        @foreach ($complain_types as $complain_type)
                                            <option value = "{{ $complain_type->id }}">
                                                {{ $complain_type->type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="description">Description</label>
                                        <input type="text" class="form-control dt-full-name" id="description"
                                            placeholder="Enter Description" name="description" aria-label="type" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="attachment">Attachment: </label>
                                        <input type="file" name="attachment" id="attachment" class="form-control">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var Table = '';
        $(document).ready(function() {
            var deleteId;
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('complains.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id'
                    },
                    {
                        data: 'complain_type'
                    },
                    {
                        data: 'description'
                    },
                    {
                        data: 'house_number'
                    },
                    {
                        data: 'city_name'
                    },
                    {
                        data: 'society_name'
                    },
                    {
                        data: 'block_name'
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            var btn = '';
                            @can('complains_delete')
                                btn +=
                                    '<a href="javascript:;" id="deleteCity" onclick="delete_item(' +
                                    full
                                    .id +
                                    ')">' +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row pb-1"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('complains_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add New Record',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        },
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });
            $('div.head-label').html('<h6 class="mb-0">List of Complain</h6>');

            $('#permission_add_form').on('submit', function(event) {
                event.preventDefault();
                let formData = new FormData(this);
                $.ajax({
                    url: '{!! route('complains.store') !!}',
                    method: 'POST',
                    data: formData,
                    processData: false,
                    contentType: false,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#permission_add_form')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Complain has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('/complains') }}" + "/" + id,
                                method: 'DELETE',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Complain-Type has been Deleted Successfully!'
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
