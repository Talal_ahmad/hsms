@extends('admin.layouts.master')
@section('title', 'Governing-Body')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Governing Body</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Governing Body
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>id</th>
                                <th>Name</th>
                                <th>Cnic</th>
                                <th>Phone</th>
                                <th>Designation</th>
                                <th>From_data</th>
                                <th>To_date</th>
                                <th>Address</th>
                                <th>Member</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add New Governing Body</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_add_form">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="name"
                                            placeholder="Enter Name" name="name" aria-label="name" />
                                        <input type="hidden" id="edit-id-2" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="cnic">Cnic:</label>
                                    <input type="text" class="form-control cnic-mask" placeholder="Enter Cnic"
                                        name="cnic" id="cnic" />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="phone">Phone N0:</label>
                                    <div class="input-group input-group-merge">
                                        <span class="input-group-text">PK (+92)</span>
                                        <input type="text" class="form-control phone-number-mask" maxlength="10"
                                            placeholder="Enter Phone No:" name="phone" id="phone" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="designation">Designation</label>
                                        <input type="text" class="form-control dt-full-name" id="designation"
                                            placeholder="Enter Designation" name="designation" aria-label="designation" />
                                    </div>
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="start_date">From Date:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="start_date"
                                        name="start_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="end_date">To Date:</label>
                                    <input type="text" id="end_date" class="form-control flatpickr-basic"
                                        name="end_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="adress">Address</label>
                                        <input type="text" class="form-control dt-full-name" id="adress"
                                            placeholder="Enter Address" name="adress" aria-label="adress" />
                                    </div>
                                </div>
                                &nbsp;
                                <div class="form-check px-3">
                                    <input class="form-check-input" type="radio" name="is_member" id="member"
                                        value="active" />
                                    <label class="form-check-label" for="member">Member</label>
                                </div>
                                &nbsp;
                                <div class="form-check px-3">
                                    <input class="form-check-input" type="radio" name="is_member" id="non-member"
                                        value="not-active" />
                                    <label class="form-check-label" for="non-member">Non Member</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        {{-- Modal to update record --}}
        <div class="modal fade text-start" id="large2" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Update Society</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="body_update_form">
                        @csrf
                        @method('PUT')
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="name-edit">Name</label>
                                        <input type="text" class="form-control dt-full-name" id="name-edit"
                                            placeholder="Enter Name" name="name" aria-label="name" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="cnic-edit">Cnic:</label>
                                    <input type="text" class="form-control cnic-mask" placeholder="Enter Cnic"
                                        name="cnic" id="cnic-edit" />
                                </div>
                                <div class="col-6">
                                    <label class="form-label" for="phone-edit">Phone N0:</label>
                                    <div class="input-group input-group-merge">
                                        <span class="input-group-text">PK (+92)</span>
                                        <input type="text" class="form-control phone-number-mask" maxlength="10"
                                            placeholder="Enter Phone No:" name="phone" id="phone-edit" />
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="designation-edit">Designation</label>
                                        <input type="text" class="form-control dt-full-name" id="designation-edit"
                                            placeholder="Enter Designation" name="designation"
                                            aria-label="designation" />
                                    </div>
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="start_date_edit">From Date:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="start_date_edit"
                                        name="start_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="end_date_edit">To Date:</label>
                                    <input type="text" id="end_date_edit" class="form-control flatpickr-basic"
                                        name="end_date" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="adress-edit">Address</label>
                                        <input type="text" class="form-control dt-full-name" id="adress-edit"
                                            placeholder="Enter Address" name="adress" aria-label="adress" />
                                    </div>
                                </div>
                                &nbsp;
                                <div class="form-check px-3">
                                    <input class="form-check-input" type="radio" name="is_member" id="member-edit"
                                        value="active" />
                                    <label class="form-check-label" for="member-edit">Member</label>
                                </div>
                                &nbsp;
                                <div class="form-check px-3">
                                    <input class="form-check-input" type="radio" name="is_member" id="non-member-edit"
                                        value="not-active" />
                                    <label class="form-check-label" for="non-member-edit">Non Member</label>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Cancel
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        var Table = '';
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('governing-body.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'cnic'
                    },
                    {
                        data: 'phone'
                    },
                    {
                        data: 'designation'
                    },
                    {
                        data: 'start_date'
                    },
                    {
                        data: 'end_date'
                    },
                    {
                        data: 'adress'
                    },
                    {
                        data: 'is_member'
                    },
                    {
                        @can('governing_body_status')
                            data: 'status',
                            "render": function(data, type, full, meta) {
                                if (data == 1) {
                                    let str =
                                        `<span onclick="status(${full.id}, 1)" class="badge bg-info px-2 " style="white-space: nowrap; cursor: pointer;">active</span>`;
                                    return str;
                                } else {
                                    let str =
                                        `<span onclick="status(${full.id}, 0)" class="badge bg-danger px-1" style="white-space: nowrap; cursor: pointer;">inactive</span>`
                                    return str;
                                }
                            }
                        @endcan
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('governing_body_edit')
                                btn +=
                                    `<a href="javascript:;" class="item-edit" onclick="edit(${full.id})">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('governing_body_delete')
                                btn += '<a href="javascript:;" onclick="delete_item(' + full
                                    .id +
                                    ')">' +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a>';
                                return btn;
                            @endcan
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('governing_body_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'Add GOVERNING BODIES',
                            className: 'create-new btn btn-primary',
                            attr: {
                                'data-bs-toggle': 'modal',
                                'data-bs-target': '#large1'
                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Governing Bodies</h6>');
            $('#body_add_form').on('submit', function(event) {
                event.preventDefault();
                let formdata = $(this).serialize();
                $.ajax({
                    url: '{!! route('governing-body.store') !!}',
                    method: 'POST',
                    data: formdata,
                    success: function(response, xhr) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#body_add_form')[0].reset();
                            $("#large1").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Governing Body has been Added Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
            $('#body_update_form').on('submit', function(event) {
                event.preventDefault();
                let societyId = $('#edit-id-2').val();
                let formData = $(this).serialize();

                $.ajax({
                    url: "{{ url('/governing-body/') }}" + "/" + societyId,
                    method: 'PUT',
                    data: formData,
                    success: function(response) {
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#body_update_form')[0].reset();
                            $("#large2").modal("hide");
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Governing Body has been Updated Successfully!'
                            })
                        }
                    },
                    error: function(xhr) {
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });

        function edit(id) {
            $('#edit-id-2').val(id);
            $.ajax({
                url: "{{ url('/governing-body/') }}" + "/" + id + "/edit",
                method: 'GET',
                success: function(response) {
                    $('#name-edit').val(response.data.name);
                    $('#cnic-edit').val(response.data.cnic);
                    $('#phone-edit').val(response.data.phone);
                    $('#designation-edit').val(response.data.designation);
                    $('#start_date_edit').val(response.data.start_date);
                    $('#end_date_edit').val(response.data.end_date);
                    $('#adress-edit').val(response.data.adress);
                    $('#member-edit').val(response.data.is_member);
                    $('#non-member-edit').val(response.data.is_member);
                },
                error: function(xhr) {}
            });
            $('#large2').modal('show');
        }

        function delete_item(id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('/governing-body/') }}" + "/" + id,
                                method: 'DELETE',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Governing Body has been Deleted Successfully!'
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }

        function status(id, check) {
            let msg = check == 1 ? "Inactive" : "Active";
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: `You want to ${msg} this!`,
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('governing-body/') }}" + "/" + id + "/status",
                                method: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    check: check,
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: `Governing Body has been ${msg} Successfully!`
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
