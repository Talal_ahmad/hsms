@extends('admin.layouts.master')
@section('title', 'Monthly-Bills')
@section('content')
    <section id="basic-datatable">
        <div class="row">
            <form {{-- action="route('add.store')" --}} id="add_form">
                @method('GET')
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-1">City</label>
                                    <select class="select2 select form-select my_select select2-basic-1" name="city_id"
                                        id="select2-basic-1">
                                        <option value="">Select City</option>
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-2">Phase</label>
                                    <select class="select2 select form-select my_select select2-basic-2" disabled
                                        name="society_id" id="select2-basic-2">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="select2-basic-3">Block</label>
                                    <select class="select2 select form-select my_select select2-basic-3" disabled
                                        name="block_id" id="select2-basic-3">
                                        <option></option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary apply_filter">
                        APPLY FILTER
                    </button>
                </div>
            </form>
            <div class="col-12" id="card_body_datatable">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>City</th>
                                <th>Society</th>
                                <th>Property Block</th>
                                <th>Billing Type</th>
                                <th>Plan</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div id="progressDiv" class="d-flex flex-column align-items-center" style="display: none;">
            <h3><strong id="p_number">Progress : (40%)</strong></h3>
            &nbsp;
            &nbsp;
            &nbsp;
            <div class="progress progress-bar-success" style="width: 25%">
                <div class="progress-bar progress-bar-striped progress-bar-animated" id = "progress_bar" role="progressbar"
                    aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"></div>
            </div>
        </div>
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Billing Information</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_add_form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="billing_month">Billing Month</label>
                                    <input type="number" class="form-control" id="billing_month" name="billing_month"
                                        placeholder="Enter Billing Month e.g: {{ date('m') }}" required />
                                    <input type="hidden" id="bill_id">
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="billing_year">Billing Year:</label>
                                        <input type="number" class="form-control dt-full-name" id="billing_year"
                                            placeholder="Enter Billing Year e.g: {{ date('Y') }}" name="billing_year"
                                            aria-label="billing_year" required />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="selected_plans">Plans</label>
                                        <select class="select2 form-control select2-multiple-plans" name="selected_plans[]"
                                            id="selected_plans" multiple>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Reset
                            </button>
                            <button type="submit" class="btn btn-primary" aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        var Table = '';
        $(document).ready(function() {

            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: {
                    url: '{!! route('monthly-bills.index') !!}',
                    type: 'GET',
                    data: function(q) {
                        q.society_id = $('#select2-basic-2').val();
                        q.city_id = $('#select2-basic-1').val();
                        q.block_id = $('#select2-basic-3').val();
                    }
                },
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'city_name',
                        name: 'cities.name'
                    },
                    {
                        data: 'society_name',
                        name: 'societies.name'
                    },
                    {
                        data: 'block_name',
                        name: 'property_blocks.name'
                    },
                    {
                        data: 'type_name',
                        name: 'property_types.type'
                    },
                    {
                        data: 'plan',
                        name: 'payment_plans.plan_name'
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('billing_monthly_bills_generation')
                                btn +=
                                    `<a href="javascript:;" class="px-2" onclick="generate_pdf(${full.type_id})">` +
                                    feather.icons['send'].toSvg({
                                        class: 'font-medium-4 text-success'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }, ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Bills</h6>');

            $('#add_form').on('submit', function(event) {
                event.preventDefault();
                Table.draw();
                var formdata = $(this).serialize() + "&_token=" + "{{ csrf_token() }}";
            });
            $('#permission_add_form').submit(function(event) {
                event.preventDefault();
                let id = $('#bill_id').val();
                $('#permission_add_form').find('[type="submit"]').prop('disabled', true);
                $.ajax({
                    type: "PUT",
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        '_method': 'PUT'
                    },
                    url: "{{ url('billing/monthly-bills/') }}" + "/" + id,
                    data: {
                        billing_month: $('#billing_month').val(),
                        billing_year: $('#billing_year').val(),
                        plans: $('#selected_plans').val(),
                    },
                    success: function(response, xhr) {
                        setTimeout(function() {
                            $('#permission_add_form').find('[type="submit"]').prop(
                                'disabled', false);
                        }, 2000);
                        if (response.errors) {
                            $.each(response.errors, function(index, value) {
                                Toast.fire({
                                    icon: 'error',
                                    title: value
                                })
                            });
                        } else if (response.error_message) {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        } else {
                            $('#large1').modal('hide');
                            $('#billing_month').val('');
                            $('#billing_year').val('');
                            Table.ajax.reload();
                            Toast.fire({
                                icon: 'success',
                                title: 'Bills Has Been Generated  Successfully!'
                            })
                        }
                        // doAjaxRequest(response.data);
                    },
                    error: function(xhr) {
                        $('#permission_add_form').find('[type="submit"]').prop('disabled',
                            false);
                        var isRtl = $('html').attr('data-textdirection') === 'rtl';
                        if (xhr.status === 422) {

                            var errors = "";
                            //loop through error
                            $.each(xhr.responseJSON.errors, function(i, val) {
                                //loop through inner array for each keys
                                $.each(val, function(i, val1) {
                                    toastr['error'](
                                        val1, {
                                            closeButton: true,
                                            tapToDismiss: false,
                                            rtl: isRtl
                                        });
                                })
                            })
                        } else {
                            Toast.fire({
                                icon: 'error',
                                title: 'An error has been occured! Please Contact Administrator.'
                            })
                        }
                        Table.ajax.reload();
                    }
                });
            });
        });
        $('.select2-basic-1').on('change', function() {
            var data = $(".select2-basic-1 option:selected").val();
            if (data) {
                $("#select2-basic-2").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/billing/monthly-bills') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "society"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-2');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var society = response.data[i];
                            selectElement.append('<option value="' + society.id + '">' + society.name +
                                '</option>');
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-2').val(null).trigger('change');
                $("#select2-basic-2").prop('disabled', true);
            }
        });
        $('.select2-basic-2').on('change', function() {
            var data = $(".select2-basic-2 option:selected").val();
            if (data) {
                $("#select2-basic-3").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/billing/monthly-bills') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "block"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-3');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var block = response.data[i];
                            selectElement.append('<option value="' + block.id + '">' + block.name +
                                '</option>');
                        }
                        selectElement.select2();
                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-3').val(null).trigger('change');
                $("#select2-basic-3").prop('disabled', true);
            }
        });
        0

        function generate_pdf(id) {
            $('#large1').modal('show');
            $('#bill_id').val(id);
            $.ajax({
                url: "{{ url('billing/monthly-bills-get-plans/') }}" + "/" + id,
                type: 'GET',
                data: {
                    id: id, 
                },
                success: function(response) {
                    console.log(response);
                    $('#selected_plans').empty();

                    $('#selected_plans').append($('<option>', {
                        value: '',
                        text: 'Select Plan'
                    }));

                    $.each(response.properties, function(index, property) {
                        $('#selected_plans').append($('<option>', {
                            value: property.property_id,
                            text: property.plan_name
                        }));
                    });

                    $('#selected_plans').select2('destroy').select2({
                        placeholder: 'Select Plan',
                        tags: true
                    });
                },
                error: function(xhr, status, error) {
                    console.log(error);
                }
            });
        }


        // function doAjaxRequest(id) {
        //     var startTime = new Date().getTime();

        //     function recursiveAjax(id) {
        //         $.ajax({
        //             url: "{{ url('billing/monthly-bills/') }}" + "/" + id,
        //             type: 'GET',
        //             data: {
        //                 _token: '{{ csrf_token() }}',
        //             },
        //             success: function(response) {
        //                 console.log(response);
        //                 var progress = response.data.progress ? response.data.progress : 100;
        //                 if (progress < 100) {
        //                     $('#p_number').text('Progress : (' + progress + '% )');
        //                     $('#progress_bar').css('width', progress + '%');
        //                     $('#progressDiv').addClass('d-flex flex-column align-items-center show-progress');
        //                     $('#card_body_datatable').css('display', 'none');
        //                     var currentTime = new Date().getTime();
        //                     if (currentTime - startTime < 600000) { // 10 minutes in milliseconds
        //                         setTimeout(recursiveAjax(id), 1000);
        //                     } else {
        //                         console.log(
        //                             "Error: Progress not completed within 10 minutes."
        //                         );
        //                     }
        //                 } else {
        //                     $('#progressDiv').removeClass('show-progress');
        //                     $('#card_body_datatable').css('display', 'block !important');
        //                 }
        //                 $('#progressDiv').removeClass('show-progress');
        //                 $('#card_body_datatable').css('display', 'block !important');
        //             },
        //             error: function(xhr, status, error) {
        //                 // Handle errors
        //             }
        //         });
        //     }
        //     recursiveAjax(id);
        // }
    </script>
@endsection
