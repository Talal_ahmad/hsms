@extends('admin.layouts.master')
@section('title', 'Property Bills')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Property Bills</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Property Bills
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <form id="add_form">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-3 mb-1">
                                    <label class="form-label" for="select2-basic-1">City</label>
                                    <select class="select2 select form-select my_select select2-basic-1" name="city_id"
                                        id="select2-basic-1">
                                        <option value="">Select City</option>
                                        @foreach ($cities as $city)
                                            <option value="{{ $city->id }}">{{ $city->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-3 mb-1">
                                    <label class="form-label" for="select2-basic-2">Phase</label>
                                    <select class="select2 select form-select my_select select2-basic-2" disabled
                                        name="society_id" id="select2-basic-2">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-3 mb-1">
                                    <label class="form-label" for="select2-basic-3">Block</label>
                                    <select class="select2 select form-select my_select select2-basic-3" disabled
                                        name="block_id" id="select2-basic-3">
                                        <option></option>
                                    </select>
                                </div>
                                <div class="col-3 mb-1">
                                    <label class="form-label" for="select2-basic-4">Bill Type</label>
                                    <select class="select2 select form-select my_select select2-basic-4" name="is_paid"
                                        id="select2-basic-4">
                                        <option></option>
                                        <option value="paid">Paid</option>
                                        <option value="unpaid">Unpaid</option>
                                    </select>
                                </div>
                                <div class="col-4 mb-1">
                                    <label class="form-label" for="property_number">Plot Number</label>
                                    <input type="text" class="form-control dt-full-name" id="property_number"
                                        placeholder="Enter Plot Number" name="property_number"
                                        aria-label="property_number" />
                                </div>
                                <div class="col-md-4 mb-1">
                                    <label class="form-label" for="billing_month">From Month/Year:</label>
                                    <input type="text" class="form-control flatpickr-basic" id="billing_month"
                                        name="billing_month" placeholder="YYYY-MM-DD" />
                                </div>
                                <div class="col-md-4 mb-1">
                                    <label class="form-label" for="billing_year">To Month/Year:</label>
                                    <input type="text" id="billing_year" class="form-control flatpickr-basic"
                                        name="billing_year" placeholder="YYYY-MM-DD" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary apply_filter">
                        APPLY FILTER
                    </button>
                </div>
            </form>
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>City</th>
                                <th>Society</th>
                                <th>Block</th>
                                <th>Plot/House Number</th>
                                <th>Assigned To</th>
                                <th>Challan No</th>
                                <th>Billing Month</th>
                                <th>Bill Amount</th>
                                <th>Amount After Due Date</th>
                                <th>Paid Amount</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="modal fade text-start" id="large1" tabindex="-1" aria-labelledby="myModalLabel17"
            aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel17">Add Payment</h4>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form class="form" id="permission_add_form" enctype="multipart/form-data">
                        @csrf
                        <div class="modal-body">
                            <div class="row">
                                <div class="col-md-6 mb-1">
                                    <label class="form-label" for="payment_date">Payment Date</label>
                                    <input type="text" class="form-control flatpickr-basic" id="payment_date"
                                        name="payment_date" placeholder="YYYY-MM-DD" required />
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="paid_amount">Paid Amount</label>
                                        <div class=" d-flex align-items-center">
                                            <input type="number" class="form-control dt-full-name disabled"
                                                id="paid_amount" placeholder="Enter Paid Amount" name="paid_amount"
                                                aria-label="paid_amount" required disabled />
                                            <span class="ms-1 d-none" id="remaining_amount_info"><i
                                                    class="fa fa-info-circle fs-3" aria-hidden="true"></i></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="waived_off">Waived Off</label>
                                        <input type="number" class="form-control dt-full-name" id="waived_off"
                                            placeholder="Enter Waived Off Amount" name="waived_off"
                                            aria-label="waived_off" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="waived_off_image">Waived Off - Reference</label>
                                        <input type="file" name="waived_off_reference" id="waived_off_image"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="payment_reference">Payment Reference</label>
                                        <input type="text" class="form-control dt-full-name" id="payment_reference"
                                            placeholder="Enter Payment Reference" name="payment_reference"
                                            aria-label="payment_reference" />
                                    </div>
                                </div>
                                <div class="col-6">
                                    <div class="mb-1">
                                        <label class="form-label" for="payment_reference_image">Payment Reference -
                                            attachment</label>
                                        <input type="file" name="paid_slip" id="payment_reference_image"
                                            class="form-control">
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="mb-1">
                                        <label class="form-label" for="remarks">Remarks</label>
                                        <input type="text" class="form-control dt-full-name" id="remarks"
                                            placeholder="Enter Remarks" name="remarks" aria-label="remarks" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset" class="btn btn-outline-success" data-bs-dismiss="modal"
                                aria-label="Close">
                                Reset
                            </button>
                            <button type="submit" id="payment_modal" class="btn btn-primary disabled"
                                aria-label="Close">
                                Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        var dataTable;
        $(document).ready(function() {
            dataTable = $('#dataTable').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: "{!! route('property-bills.index') !!}",
                    type: "GET",
                    data: function(e) {
                        e.city_id = $('#select2-basic-1').val();
                        e.society_id = $('#select2-basic-2').val();
                        e.block_id = $('#select2-basic-3').val();
                        e.is_paid = $('#select2-basic-4').val();
                        // e.is_paid = 1;
                        e.property_number = $('#property_number').val();
                        e.billing_month = $('#billing_month').val();
                        e.billing_year = $('#billing_year').val();
                    }
                },
                // ajax: '{!! route('property-bills.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'city_name',
                        name: 'cities.name'
                    },
                    {
                        data: 'society_name',
                        name: 'societies.name'
                    },
                    {
                        data: 'block_name',
                        name: 'property_blocks.name'
                    },
                    {
                        data: 'plot_number',
                        name: 'properties.property_number'
                    },
                    {
                        data: 'assigned_to',
                        name: 'property_bills.assigned_to'
                    },
                    {
                        data: 'challan_number',
                        name: 'property_bills.challan_no'
                    },
                    {
                        data: 'billing_month',
                        name: 'property_bills.billing_month'
                    },
                    {
                        data: 'total_amount_within_due_date',
                        name: 'property_bills.total_amount_within_due_date'
                    },
                    {
                        data: 'total_amount_after_due_date',
                        "render": function(data, type, full, meta) {
                            return (full.total_amount_within_due_date + full
                                .total_amount_after_due_date);
                        }
                    },
                    {
                        data: 'paid_amount',
                        name: 'property_bills.paid_amount'
                    },
                    {
                        data: 'is_paid',
                        "render": function(data, type, full, meta) {
                            if (data == 1) {
                                let str =
                                    `<span class="badge bg-info px-2 " style="white-space: nowrap; cursor: pointer;">Paid</span>`;
                                return str;
                            } else {
                                let str =
                                    `<span class="badge bg-danger px-1" style="white-space: nowrap; cursor: pointer;">UnPaid</span>`
                                return str;
                            }
                        }
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('billing_properties_bill_payment')
                                if (!full.is_paid) {
                                    btn +=
                                        `<div class="d-flex"><span class="badge bg-danger" style="white-space: nowrap; cursor: pointer;"  onclick="verify_payment(${full.id}, ${full.billing_month}, ${full.billing_year}, ${full.total_amount_within_due_date}, ${full.total_amount_after_due_date}, ${full.paid_amount}, ${full.arrears})">Enter Payment</span>`;

                                } else {
                                    btn +=
                                        `<div class="d-flex"><em class="badge bg-info" style="white-space: nowrap; font-size: 10px;">bill has been paid</em>`;
                                }
                            @endcan
                            @can('billing_properties_bill_download')
                                btn +=
                                    `<a href="javascript:;" class="px-1" onclick="generate_pdf(${full.id}, ${full.billing_month})">` +
                                    // `<a href="javascript:;" class="px-1" onclick="generate_pdf(${full.id})">` +
                                    feather.icons['download'].toSvg({
                                        class: 'font-medium-4 text-success'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('billing_properties_bill_delete')
                                btn +=
                                    `<a href="javascript:;" id="deleteCity" onclick="delete_item(${full.id}, ${full.property_id}, ${full.payment_plan_id})">` +
                                    feather.icons['trash-2'].toSvg({
                                        class: 'font-medium-4 text-danger'
                                    }) +
                                    '</a></div>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                    extend: 'collection',
                    className: 'btn btn-outline-secondary dropdown-toggle me-2',
                    text: feather.icons['share'].toSvg({
                        class: 'font-small-4 me-50'
                    }) + 'Export',
                    buttons: [{
                            extend: 'print',
                            text: feather.icons['printer'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Print',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'csv',
                            text: feather.icons['file-text'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Csv',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'excel',
                            text: feather.icons['file'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Excel',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'pdf',
                            text: feather.icons['clipboard'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Pdf',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        },
                        {
                            extend: 'copy',
                            text: feather.icons['copy'].toSvg({
                                class: 'font-small-4 me-50'
                            }) + 'Copy',
                            className: 'dropdown-item',
                            exportOptions: {
                                columns: ':not(.not_include)'
                            }
                        }
                    ],
                    init: function(api, node, config) {
                        $(node).removeClass('btn-secondary');
                        $(node).parent().removeClass('btn-group');
                        setTimeout(function() {
                            $(node).closest('.dt-buttons').removeClass('btn-group')
                                .addClass('d-inline-flex');
                        }, 50);
                    }
                }, ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $('div.head-label').html('<h6 class="mb-0">List of Bills</h6>');
        });

        $('#add_form').submit(function(event) {
            event.preventDefault();
            // console.log(dataTable.draw());
            dataTable.draw();
        });

        $('.select2-basic-1').on('change', function() {
            var data = $(".select2-basic-1 option:selected").val();
            if (data) {
                $("#select2-basic-2").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/billing/property-bills') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "society"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-2');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var society = response.data[i];
                            selectElement.append('<option value="' + society.id + '">' + society.name +
                                '</option>');
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-2').val(null).trigger('change');
                $("#select2-basic-2").prop('disabled', true);
            }
        });
        $('.select2-basic-2').on('change', function() {
            var data = $(".select2-basic-2 option:selected").val();
            if (data) {
                $("#select2-basic-3").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/billing/property-bills') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "block"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#select2-basic-3');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var block = response.data[i];
                            selectElement.append('<option value="' + block.id + '">' + block.name +
                                '</option>');
                        }
                        selectElement.select2();
                    },
                    error: function(xhr) {}
                });
            } else {
                $('#select2-basic-3').val(null).trigger('change');
                $("#select2-basic-3").prop('disabled', true);
            }
        });

        function generate_pdf(id, billing_month) {
            // function generate_pdf(id) {
            $.ajax({
                url: "{{ url('/billing/property-bills/') }}" + "/" + id,
                method: 'GET',
                data: {
                    id: id,
                    billing_month: billing_month,
                },
                success: function(response, xhr) {
                    window.open(this.url);
                    if (response.errors) {
                        $.each(response.errors, function(index, value) {
                            Toast.fire({
                                icon: 'error',
                                title: value
                            })
                        });
                    } else if (response.error_message) {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    } else {
                        $('#permission_add_form')[0].reset();
                        $("#large1").modal("hide");
                        dataTable.ajax.reload();
                        Toast.fire({
                            icon: 'success',
                            title: 'Bill has been Downloaded!'
                        })
                    }
                },
                error: function(xhr) {
                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                    if (xhr.status === 422) {

                        var errors = "";
                        //loop through error
                        $.each(xhr.responseJSON.errors, function(i, val) {
                            //loop through inner array for each keys
                            $.each(val, function(i, val1) {
                                toastr['error'](
                                    val1, {
                                        closeButton: true,
                                        tapToDismiss: false,
                                        rtl: isRtl
                                    });
                            })
                        })
                    } else {
                        Toast.fire({
                            icon: 'error',
                            title: 'An error has been occured! Please Contact Administrator.'
                        })
                    }
                    dataTable.ajax.reload();
                }
            });
        }

        function verify_payment(id, billing_month, billing_year, total_amount_within_due_date, total_amount_after_due_date,
            paid_amount, arrears) {
            $('#large1').modal('show');

            $('#payment_date').on('change', function() {
                $('#paid_amount').prop('disabled', false);
                $('#remaining_amount_info').removeClass('d-none');
            });

            $('#paid_amount').on('change', function() {

                $payment_date_month = $('#payment_date').val().split('-')[1];
                $payment_date_year = $('#payment_date').val().split('-')[0];
                $paid_amount = $('#paid_amount').val();
                $total_amount_after_due_date = total_amount_within_due_date + arrears + total_amount_after_due_date;
                $correct_payment_amount = false;

                if ($payment_date_month != billing_month && $payment_date_year != billing_year && $paid_amount >
                    $total_amount_after_due_date) {
                    // alert(`IF Case ${billing_month} ${billing_year}`);
                    Swal.fire({
                        icon: "error",
                        title: "Incorrect Payment Amount",
                        text: `Payment amount can't be more than 'Total Amount(PKR ${total_amount_within_due_date + arrears}  + ${total_amount_after_due_date}/-)'`,
                    });
                } else if ($payment_date_month == billing_month && $paid_amount > (total_amount_within_due_date +
                        arrears)) {
                    // alert(`ELSE IF Case ${billing_month} ${billing_year}`);
                    Swal.fire({
                        icon: "error",
                        title: "Incorrect Payment Amount",
                        text: `Payment amount can't be more than 'Total Amount(PKR ${total_amount_within_due_date + arrears}/-)'`,
                    });
                } else {
                    // alert(`ELSE Case ${billing_month} ${billing_year}`);
                    $correct_payment_amount = true;
                    $('#payment_modal').removeClass('disabled');
                }
            });

            $('#remaining_amount_info').on('click', function() {
                $payment_date_month = $('#payment_date').val().split('-')[1];
                $payment_date_year = $('#payment_date').val().split('-')[0];
                if ($payment_date_month == billing_month && $payment_date_year == billing_year) {
                    Swal.fire({
                        icon: "info",
                        title: "Remaining Amount",
                        text: `Your Remaining Amount is PKR ${(total_amount_within_due_date + arrears) - paid_amount}/-`
                    });
                } else {
                    Swal.fire({
                        icon: "info",
                        title: "Remaining Amount",
                        text: `Your Remaining Amount is PKR ${(total_amount_within_due_date + arrears + total_amount_after_due_date) - paid_amount}/-`
                    });
                }
            });

            $('#permission_add_form').submit(function(event) {

                event.preventDefault();
                if ($correct_payment_amount == true) {
                    let formData = new FormData(this);
                    $(this).find('[type="submit"]').prop('disabled', true); 
                    $.ajax({
                        type: "post",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                            '_method': 'post'
                        },
                        url: "{{ url('/billing/property-bills') }}" + "/" + id + "/verify-payment",
                        data: formData,
                        processData: false,
                        contentType: false,
                        success: function(response, xhr) {
                            if (response.errors) {
                                $.each(response.errors, function(index, value) {
                                    Toast.fire({
                                        icon: 'error',
                                        title: value
                                    });
                                });
                            } else if (response.error_message) {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has occurred! You might have entered an invalid challan number...'
                                });
                            } else {
                                $('#permission_add_form')[0].reset();
                                $("#large1").modal("hide");
                                window.location.reload();
                                Toast.fire({
                                    icon: 'success',
                                    title: 'Bill has been paid!'
                                });
                            }
                            // dataTable.ajax.reload();
                            window.location.reload();
                        },
                        error: function(xhr) {
                            var isRtl = $('html').attr('data-textdirection') === 'rtl';
                            if (xhr.status === 422) {
                                var errors = "";
                                //loop through error
                                $.each(xhr.responseJSON.errors, function(i, val) {
                                    //loop through inner array for each keys
                                    $.each(val, function(i, val1) {
                                        toastr['error'](
                                            val1, {
                                                closeButton: true,
                                                tapToDismiss: false,
                                                rtl: isRtl
                                            });
                                    });
                                });
                            } else {
                                Toast.fire({
                                    icon: 'error',
                                    title: 'An error has occurred! You might have entered an invalid challan number...'
                                });
                            }
                        }
                    });
                } else {
                    Swal.fire({
                        icon: "error",
                        title: "Incorrect Payment Amount",
                        text: "Payment amount can't be more than Total Amount",
                    });
                }
            });
        }

        function delete_item(id, property_id, plan_id) {
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: 'Are you sure you want to delete!',
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                type: "delete",
                                headers: {
                                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                                    '_method': 'delete'
                                },
                                url: "{{ url('billing/property-bills') }}" + "/" + id,
                                data: {
                                    _token: '{{ csrf_token() }}',
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        dataTable.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: 'Bill has been Deleted Successfully!'
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    dataTable.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
