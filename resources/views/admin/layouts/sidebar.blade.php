<div class="navbar-header">
    <ul class="nav navbar-nav flex-row">
        <li class="nav-item me-auto">
            <a class="navbar-brand" href="{{ url('/') }}">
                <span class="brand-logo">
                    <svg viewbox="0 0 139 95" version="1.1" xmlns="http://www.w3.org/2000/svg"
                        xmlns:xlink="http://www.w3.org/1999/xlink" height="24">
                        <defs>
                            <lineargradient id="linearGradient-1" x1="100%" y1="10.5120544%" x2="50%"
                                y2="89.4879456%">
                                <stop stop-color="#000000" offset="0%"></stop>
                                <stop stop-color="#FFFFFF" offset="100%"></stop>
                            </lineargradient>
                            <lineargradient id="linearGradient-2" x1="64.0437835%" y1="46.3276743%" x2="37.373316%"
                                y2="100%">
                                <stop stop-color="#EEEEEE" stop-opacity="0" offset="0%"></stop>
                                <stop stop-color="#FFFFFF" offset="100%"></stop>
                            </lineargradient>
                        </defs>
                        <g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                            <g id="Artboard" transform="translate(-400.000000, -178.000000)">
                                <g id="Group" transform="translate(400.000000, 178.000000)">
                                    <path class="text-primary" id="Path"
                                        d="M-5.68434189e-14,2.84217094e-14 L39.1816085,2.84217094e-14 L69.3453773,32.2519224 L101.428699,2.84217094e-14 L138.784583,2.84217094e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L6.71554594,44.4188507 C2.46876683,39.9813776 0.345377275,35.1089553 0.345377275,29.8015838 C0.345377275,24.4942122 0.230251516,14.560351 -5.68434189e-14,2.84217094e-14 Z"
                                        style="fill: currentColor;"></path>
                                    <path id="Path1"
                                        d="M69.3453773,32.2519224 L101.428699,1.42108547e-14 L138.784583,1.42108547e-14 L138.784199,29.8015838 C137.958931,37.3510206 135.784352,42.5567762 132.260463,45.4188507 C128.736573,48.2809251 112.33867,64.5239941 83.0667527,94.1480575 L56.2750821,94.1480575 L32.8435758,70.5039241 L69.3453773,32.2519224 Z"
                                        fill="url(#linearGradient-1)" opacity="0.2"></path>
                                    <polygon id="Path-2" fill="#000000" opacity="0.049999997"
                                        points="69.3922914 32.4202615 32.8435758 70.5039241 54.0490008 16.1851325">
                                    </polygon>
                                    <polygon id="Path-21" fill="#000000" opacity="0.099999994"
                                        points="69.3922914 32.4202615 32.8435758 70.5039241 58.3683556 20.7402338">
                                    </polygon>
                                    <polygon id="Path-3" fill="url(#linearGradient-2)" opacity="0.099999994"
                                        points="101.428699 0 83.0667527 94.1480575 130.378721 47.0740288"></polygon>
                                </g>
                            </g>
                        </g>
                    </svg>
                </span>
                <h2 class="brand-text">OPUS</h2>
            </a>
        </li>
        <li class="nav-item nav-toggle">
            <a class="nav-link modern-nav-toggle pe-0" data-bs-toggle="collapse">
                <i class="d-block d-xl-none text-primary toggle-icon font-medium-4" data-feather="x"></i><i
                    class="d-none d-xl-block collapse-toggle-icon font-medium-4 text-primary" data-feather="disc"
                    data-ticon="disc"></i>
            </a>
        </li>
    </ul>
</div>
<div class="shadow-bottom"></div>
<div class="main-menu-content">
    <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation">
        @can('dashboard')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == '/' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/') }}">
                    <i data-feather="home"></i><span class="menu-title text-truncate"
                        data-i18n="Dashboards">Dashboard</span>
                </a>
            </li>
        @endcan
        @can('properties')
            <li class="navigation-header"><span data-i18n="Attendance">User Management</span><i
                    data-feather="more-horizontal"></i></li>
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather="aperture"></i><span
                        class="menu-title text-truncate" data-i18n="aperture">Properties</span><span
                        class="badge badge-light-warning rounded-pill ms-auto me-1">2</span></a>
                <ul class="menu-content">
                    @can('properties_list')
                        <li class="{{ request()->is('properties/list*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('list.index') }}"><i
                                    data-feather="list"></i><span class="menu-item text-truncate"
                                    data-i18n="list">List</span></a>
                        </li>
                    @endcan
                </ul>
                <ul class="menu-content">
                    @can('properties_add')
                        <li class="{{ request()->is('properties/add*') ? ' active' : '' }}"><a class="d-flex align-items-center"
                                href="{{ route('add.index') }}"><i data-feather="plus"></i><span class="menu-item text-truncate"
                                    data-i18n="plus">Add</span></a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('property_setup')
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span
                        class="menu-title text-truncate" data-i18n="Dashboards">Property Setup</span><span
                        class="badge badge-light-warning rounded-pill ms-auto me-1">6</span></a>
                <ul class="menu-content">
                    @can('property_setup_city')
                        <li class="{{ request()->is('property/city*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('city.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">City</span></a>
                        </li>
                    @endcan
                    @can('property_setup_society')
                        <li class="{{ request()->is('property/society*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('society.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="eCommerce">Society</span></a>
                        </li>
                    @endcan
                    @can('property_setup_blocks')
                        <li class="{{ request()->is('property/block*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('block.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">Blocks</span></a>
                        </li>
                    @endcan
                    @can('property_setup_property_types')
                        <li class="{{ request()->is('property/property-types*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('property-types.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="eCommerce">Property Types</span></a>
                        </li>
                    @endcan
                    @can('property_setup_document_types')
                        <li class="{{ request()->is('property/doc-types*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('doc-types.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">Document Types</span></a>
                        </li>
                    @endcan
                    @can('property_setup_area_unit')
                        <li class="{{ request()->is('property/area-units*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('area-units.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="eCommerce">Area Units</span></a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('transfer_property')
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather="paperclip"></i><span
                        class="menu-title text-truncate" data-i18n="paperclip">Transfer Property</span><span
                        class="badge badge-light-warning rounded-pill ms-auto me-1">2</span></a>
                <ul class="menu-content">
                    @can('transfer_property_list')
                        <li class="{{ request()->is('transfer-property/List*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('List.index') }}"><i
                                    data-feather="list"></i><span class="menu-item text-truncate" data-i18n="list">Transfer
                                    List</span></a>
                        </li>
                    @endcan
                </ul>
                <ul class="menu-content">
                    @can('transfer_property_add')
                        <li class="{{ request()->is('transfer-property/Add*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('Add.index') }}"><i
                                    data-feather="plus"></i><span class="menu-item text-truncate"
                                    data-i18n="plus">Add</span></a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('billing')
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span
                        class="menu-title text-truncate" data-i18n="Dashboards">Billing</span><span
                        class="badge badge-light-warning rounded-pill ms-auto me-1">6</span></a>
                <ul class="menu-content">
                    @can('billing_advance_payment')
                        <li class="{{ request()->is('billing/advance-payment*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('advance-payment.index') }}"><i
                                    data-feather="zap"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">Advance Payment</span></a>
                        </li>
                    @endcan
                    @can('billing_monthly_bills')
                        <li class="{{ request()->is('billing/monthly-bills*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('monthly-bills.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">Monthly Bills</span></a>
                        </li>
                    @endcan
                    @can('billing_properties_bill')
                        <li class="{{ request()->is('billing/property-bills*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('property-bills.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="eCommerce">Properties Bill</span></a>
                        </li>
                    @endcan
                    @can('billing_services_bill')
                        <li class="{{ request()->is('billing/service-bills*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('service-bills.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">Services Bill</span></a>
                        </li>
                    @endcan
                    @can('collections')
                        <li class=" nav-item">
                            <a class="d-flex align-items-center" href="#"><i data-feather="home"></i><span
                                    class="menu-title text-truncate" data-i18n="Dashboards">Collections</span><span
                                    class="badge badge-light-warning rounded-pill ms-auto me-1">4</span></a>
                            <ul class="menu-content">
                                @can('collections_collection_types')
                                    <li class="{{ request()->is('collections/payment-type*') ? ' active' : '' }}"><a
                                            class="d-flex align-items-center" href="{{ route('payment-type.index') }}"><i
                                                data-feather="circle"></i><span class="menu-item text-truncate"
                                                data-i18n="Analytics">Collection Types</span></a>
                                    </li>
                                @endcan
                                @can('collection_head')
                                    <li class="{{ request()->is('collections/payment-code*') ? ' active' : '' }}"><a
                                            class="d-flex align-items-center" href="{{ route('payment-code.index') }}"><i
                                                data-feather="circle"></i><span class="menu-item text-truncate"
                                                data-i18n="eCommerce">Collection Heads</span></a>
                                    </li>
                                @endcan
                                @can('collection_plans')
                                    <li class="{{ request()->is('collections/payment-plan*') ? ' active' : '' }}"><a
                                            class="d-flex align-items-center" href="{{ route('payment-plan.index') }}"><i
                                                data-feather="circle"></i><span class="menu-item text-truncate"
                                                data-i18n="Analytics">Collection Plans</span></a>
                                    </li>
                                @endcan
                                @can('collection_assign_plan')
                                    <li class="{{ request()->is('collection/assign-plan*') ? ' active' : '' }}"><a
                                            class="d-flex align-items-center" href="{{ route('assign-plan.index') }}"><i
                                                data-feather="circle"></i><span class="menu-item text-truncate"
                                                data-i18n="Analytics">Assign Plan To Properties</span></a>
                                    </li>
                                @endcan
                            </ul>
                        </li>
                    @endcan
                    @can('billing_bank')
                        <li class="{{ request()->is('billing/bank*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('bank.index') }}"><i
                                    data-feather="circle"></i><span class="menu-item text-truncate"
                                    data-i18n="Analytics">Bank</span></a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('maintenance')
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="#"><i data-feather="inbox"></i><span
                        class="menu-title text-truncate" data-i18n="Maintenance">Maintenance</span><span
                        class="badge badge-light-warning rounded-pill ms-auto me-1">2</span></a>
                <ul class="menu-content">
                    @can('maintenance_category')
                        <li class="{{ request()->is('maintenance/categories*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('categories.index') }}"><i
                                    data-feather="map"></i><span class="menu-item text-truncate"
                                    data-i18n="Category">Category</span></a>
                        </li>
                    @endcan
                    @can('maintenance_services')
                        <li class="{{ request()->is('maintenance/services*') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ route('services.index') }}"><i
                                    data-feather="link"></i><span class="menu-item text-truncate"
                                    data-i18n="Services">Services</span></a>

                        </li>
                    @endcan
                </ul>
            </li>
        @endcan

        @can('reports')
            <li class=" nav-item">
                <a class="d-flex align-items-center" href="#"><i class="fa fa-book" aria-hidden="true"></i><span
                        class="menu-title text-truncate" data-i18n="reports">Reports</span><span
                        class="badge badge-light-warning rounded-pill ms-auto me-1">3</span></a>
                <ul class="menu-content">
                    {{-- @can('') --}}
                        <li class="{{ request()->is('property_ledger') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ url('property_ledger') }}"><i
                                    data-feather="map"></i><span class="menu-item text-truncate"
                                    data-i18n="property_ledger">Apartment Ledger</span></a>
                        </li>
                    {{-- @endcan --}}
                    {{-- @can('') --}}
                        <li class="{{ request()->is('monthly_maintenance_bill_report') ? ' active' : '' }}"><a
                                class="d-flex align-items-center" href="{{ url('monthly_maintenance_bill_report') }}"><i
                                    data-feather="map"></i><span class="menu-item text-truncate"
                                    data-i18n="monthly_maintenance_bill_report">Maintenance Bill Monthly</span></a>
                        </li>
                    {{-- @endcan --}}
                </ul>
            </li>
        @endcan

        @can('pos')
            <li class=" nav-item {{ request()->is('pos*') ? ' active' : '' }}"><a target="_blank"
                    class="d-flex
                align-items-center" href="{{ route('pos.index') }}"><i
                        data-feather="pocket"></i><span class="menu-title text-truncate" data-i18n="POS">POS</span></a>
            </li>
        @endcan
        @can('governing_body')
            <li class=" nav-item {{ request()->is('governing-body*') ? ' active' : '' }}"><a
                    class="d-flex align-items-center" href="{{ route('governing-body.index') }}"><i
                        data-feather="globe"></i><span class="menu-title text-truncate" data-i18n="globe">Governing
                        Body</span></a>
            </li>
        @endcan
        @can('member_ship')
            <li class=" nav-item {{ request()->is('member_ship/member_list*') ? ' active' : '' }}"><a
                    class="d-flex align-items-center" href="{{ route('member_list.index') }}"><i
                        data-feather="zap"></i><span class="menu-title text-truncate" data-i18n="zap">Member
                        Ship</span></a>
            </li>
        @endcan
        @can('staff')
            <li class=" nav-item {{ request()->is('staff*') ? ' active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ route('staff.index') }}"><i data-feather="layers"></i><span
                        class="menu-title text-truncate" data-i18n="layers">Staff</span></a>
            </li>
        @endcan
        @can('erp_entry_type')
            <li
                class=" nav-item {{ request()->is('entry_type/entry-list*') || request()->is('entry_type/entry-add*') ? ' active' : '' }}">
                <a class="d-flex align-items-center" href="{{ route('entry-list.index') }}"><i
                        data-feather="edit"></i><span class="menu-title text-truncate" data-i18n="edit">ERP Entry
                        Type</span></a>
            </li>
        @endcan
        @can('complain_types')
            <li class=" nav-item {{ request()->is('complain-types*') ? ' active' : '' }}"><a
                    class="d-flex align-items-center" href="{{ route('complain-types.index') }}"><i
                        data-feather="mail"></i><span class="menu-title text-truncate" data-i18n="layers">Complain
                        Types</span></a>
            </li>
        @endcan
        @can('complains')
            <li class=" nav-item {{ request()->is('complains*') ? ' active' : '' }}"><a class="d-flex align-items-center"
                    href="{{ route('complains.index') }}"><i data-feather="alert-triangle"></i><span
                        class="menu-title text-truncate" data-i18n="layers">Complains</span></a>
            </li>
        @endcan
        @can('role_and_permissions')
            <li class="nav-item {{ request()->is('roles*') || request()->is('permissions*') ? ' active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/roles') }}">
                    <i class="fa fa-tasks" aria-hidden="true"></i><span class="menu-title text-truncate"
                        data-i18n="Dashboards">Roles & Permissions</span>
                </a>
            </li>
        @endcan
        @can('users')
            <li class="nav-item {{ \Route::getFacadeRoot()->current()->uri() == 'users' ? 'active' : '' }}">
                <a class="d-flex align-items-center" href="{{ url('/users') }}">
                    <i data-feather="users"></i><span class="menu-title text-truncate"
                        data-i18n="Dashboards">Users</span>
                </a>
            </li>
        @endcan
    </ul>
</div>
