@extends('admin.layouts.master')
@section('title', 'Lists')
@section('content')
    <section id="basic-datatable">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Member Ship</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Member-Ship
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table" id="dataTable">
                        <thead>
                            <tr>
                                <th></th>
                                <th>Full Name</th>
                                <th>Father/Husband Number</th>
                                <th>Membership Number</th>
                                <th>Membership From Date</th>
                                <th>Membership To Date</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </section>

    <div class="toggle-between-modal">
        <!-- Modal 1-->
        <div class="modal fade" id="modalToggle" aria-labelledby="modalToggleLabel" tabindex="-1" style="display: none"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalToggleLabel">Member Details</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <div class="container" id="memberDetails">
                            <div class="row">
                                <div class="col-4 py-2"><strong>Name:</strong></div>
                                <div class="col-8 py-2" id="Name"></div>
                                <div class="col-4 py-2"><strong>Father / Husband Name:</strong></div>
                                <div class="col-8 py-2" id="Father_Name"></div>
                                <div class="col-4 py-2"><strong>Membership No:</strong></div>
                                <div class="col-8 py-2" id="MemberShip_No"></div>
                                <div class="col-4 py-2"><strong>GL Code:</strong></div>
                                <div class="col-8 py-2" id="GL_CODE"></div>
                                <div class="col-4 py-2"><strong>CNIC / NTN No:</strong></div>
                                <div class="col-8 py-2" id="CNIC"></div>
                                <div class="col-4 py-2"><strong>CNIC / NTN No Expiry Date:</strong></div>
                                <div class="col-8 py-2" id="EXP_CNIC"></div>
                                <div class="col-4 py-2"><strong>Current Address:</strong></div>
                                <div class="col-8 py-2" id="CURR_ADDRESS"></div>
                                <div class="col-4 py-2"><strong>Permanent Address:</strong></div>
                                <div class="col-8 py-2" id="PER_ADDRESS"></div>
                                <div class="col-4 py-2"><strong>Phone No / Mobile No:</strong></div>
                                <div class="col-8 py-2" id="PHONE_NUMBER"></div>
                                <div class="col-4 py-2"><strong>Secondary Phone No / Mobile No:</strong></div>
                                <div class="col-8 py-2" id="SEC_PHONE"></div>
                                <div class="col-4 py-2"><strong>Email:</strong></div>
                                <div class="col-8 py-2" id="EMAIL"></div>
                                <div class="col-4 py-2"><strong>Date of Enrolment as Member:</strong></div>
                                <div class="col-8 py-2" id="ENROLLMENT"></div>
                                <div class="col-4 py-2"><strong>Date of approval by the Managing Committee:</strong></div>
                                <div class="col-8 py-2" id="APPROVAL"></div>
                                <div class="col-4 py-2"><strong>Date of Confirmation by the General Body:</strong></div>
                                <div class="col-8 py-2" id="CONFIRMATION"></div>
                                <div class="col-4 py-2"><strong>Number of Shares :</strong></div>
                                <div class="col-8 py-2" id="NUMBER_SHARE"></div>
                                <div class="col-4 py-2"><strong>Value of Shares :</strong></div>
                                <div class="col-8 py-2" id="VALUE_SHARE"></div>
                                <div class="col-4 py-2"><strong>Member’s Vote :</strong></div>
                                <div class="col-8 py-2" id="VOTE"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-bs-target="#modalToggle2" data-bs-toggle="modal"
                            data-bs-dismiss="modal">
                            View Member Image
                        </button>
                        <button type="button" class="btn btn-primary" data-bs-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal 2-->
        <div class="modal fade" id="modalToggle2" aria-hidden="true" aria-labelledby="modalToggleLabel2" tabindex="-1">
            <div class="modal-dialog  modal-fullscreen" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalToggleLabel2">Modal 2</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">Hide this modal and show the first with the button below.</div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" data-bs-dismiss="modal">Back to Details</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts-foot')
    @if (Session::has('success_message'))
        <script>
            Toast.fire({
                icon: "success",
                title: "{!! Session::get('success_message') !!}"
            })
        </script>
    @endif
    @if (Session::has('error_message'))
        <script>
            Toast.fire({
                icon: "warning",
                title: "{!! Session::get('error_message') !!}"
            })
        </script>
    @endif
    <script>
        var Table = '';
        $(document).ready(function() {
            Table = $('#dataTable').DataTable({
                // processing: true,
                // serverSide: true,
                ajax: '{!! route('member_list.index') !!}',
                columns: [{
                        data: 'responsive_id'
                    },
                    {
                        data: 'name'
                    },
                    {
                        data: 'father_name'
                    },
                    {
                        data: 'membership_number'
                    },
                    {
                        data: 'membership_from_date'
                    },
                    {
                        data: 'membership_to_date'
                    },
                    {
                        @can('member_ship_status')
                            data: 'status',
                            "render": function(data, type, full, meta) {
                                if (data >= 1) {
                                    let str =
                                        `<span onclick="status(${full.id}, 1)" class="badge bg-info px-2 " style="white-space: nowrap; cursor: pointer;">active</span>`;
                                    `<span class="badge bg-info px-2 " style="white-space: nowrap;">active</span>`;
                                    return str;
                                } else {
                                    let str =
                                        `<span class="badge bg-danger px-1" style="white-space: nowrap;">inactive</span>`
                                    return str;
                                }
                            }
                        @endcan
                    },
                    {
                        data: ''
                    }
                ],
                columnDefs: [{
                        // For Responsive
                        className: 'control',
                        orderable: false,
                        searchable: false,
                        targets: 0
                    },
                    {
                        // Actions
                        targets: -1,
                        title: 'Actions',
                        orderable: false,
                        searchable: false,
                        render: function(data, type, full, meta) {
                            let btn = '';
                            @can('member_ship_edit')
                                btn +=
                                    `<a href="{{ url('member_ship/member_list/${full.id}') }}" class="item-edit">` +
                                    feather.icons['edit'].toSvg({
                                        class: 'font-medium-4'
                                    }) +
                                    '</a>';
                            @endcan
                            @can('member_ship_delete')
                                btn += `<a href="javascript:;" onclick="view_member(${full.id})">` +
                                    feather.icons['eye'].toSvg({
                                        class: 'font-medium-4 text-success'
                                    }) +
                                    '</a>';
                            @endcan
                            return btn;
                        }
                    },
                    {
                        "defaultContent": "-",
                        "targets": "_all"
                    }
                ],
                order: [
                    [1, 'asc']
                ],
                dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 pb-1 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
                displayLength: 7,
                lengthMenu: [7, 10, 25, 50, 75, 100],
                buttons: [{
                        extend: 'collection',
                        className: 'btn btn-outline-secondary dropdown-toggle me-2',
                        text: feather.icons['share'].toSvg({
                            class: 'font-small-4 me-50'
                        }) + 'Export',
                        buttons: [{
                                extend: 'print',
                                text: feather.icons['printer'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Print',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'csv',
                                text: feather.icons['file-text'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Csv',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'excel',
                                text: feather.icons['file'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Excel',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'pdf',
                                text: feather.icons['clipboard'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Pdf',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            },
                            {
                                extend: 'copy',
                                text: feather.icons['copy'].toSvg({
                                    class: 'font-small-4 me-50'
                                }) + 'Copy',
                                className: 'dropdown-item',
                                exportOptions: {
                                    columns: ':not(.not_include)'
                                }
                            }
                        ],
                        init: function(api, node, config) {
                            $(node).removeClass('btn-secondary');
                            $(node).parent().removeClass('btn-group');
                            setTimeout(function() {
                                $(node).closest('.dt-buttons').removeClass('btn-group')
                                    .addClass('d-inline-flex');
                            }, 50);
                        }
                    },
                    @can('member_ship_add')
                        {
                            text: feather.icons['plus'].toSvg({
                                class: 'me-50 font-small-4'
                            }) + 'ADD MEMBERSHIP',
                            className: 'create-new btn btn-primary member',
                            attr: {

                            },
                            init: function(api, node, config) {
                                $(node).removeClass('btn-secondary');
                            }
                        }
                    @endcan
                ],
                responsive: {
                    details: {
                        display: $.fn.dataTable.Responsive.display.childRowImmediate,
                        type: 'column',
                    }
                },
                language: {
                    paginate: {
                        // remove previous & next text from pagination
                        previous: '&nbsp;',
                        next: '&nbsp;'
                    }
                }
            });

            $(document).on('click', '.member', function() {
                var anchor = $('<a>', {
                    href: '{{ route('member_add.index') }}',
                    style: 'display:none;'
                });

                $('body').append(anchor);

                anchor[0].click();

                anchor.remove();
            });

            $('div.head-label').html('<h6 class="mb-0">List of Member Ship</h6>');
        });

        function view_member(id) {
            $('#modalToggle').modal('show');
            $.ajax({
                url: "{{ url('/member_ship/member_list') }}" + "/" + id + "/edit",
                method: 'GET',
                data: {
                    "_token": "{{ csrf_token() }}",
                    "check": "list"
                },
                success: function(response) {
                    $('#Name').text(response.data.name);
                    $('#Father_Name').text(response.data.father_name);
                    $('#MemberShip_No').text(response.data.membership_number);
                    $('#GL_CODE').text(response.data.gl_code);
                    $('#CNIC').text(response.data.cnic);
                    $('#EXP_CNIC').text(response.data.cnicExpiryDate);
                    $('#CURR_ADDRESS').text(response.data.address);
                    $('#PER_ADDRESS').text(response.data.permanent_address);
                    $('#PHONE_NUMBER').text(response.data.phone_number);
                    $('#SEC_PHONE').text(response.data.secondary_phone_number);
                    $('#EMAIL').text(response.data.email);
                    $('#ENROLLMENT').text(response.data.date_enrolment_member);
                    $('#APPROVAL').text(response.data.date_approval_managingCommitte);
                    $('#CONFIRMATION').text(response.data.date_confirmation_by_GB);
                    $('#NUMBER_SHARE').text(response.data.no_shares);
                    $('#VALUE_SHARE').text(response.data.value_shares);
                    $('#VOTE').text(response.data.member_vote);

                },
                error: function(xhr) {}
            });
        }

        function status(id, check) {
            let msg = check == 1 ? "Inactive" : "Active";
            $.confirm({
                icon: 'far fa-question-circle',
                title: 'Confirm!',
                content: `You want to ${msg} this!`,
                type: 'orange',
                typeAnimated: true,
                buttons: {
                    Confirm: {
                        text: 'Confirm',
                        btnClass: 'btn-orange',
                        action: function() {
                            $.ajax({
                                url: "{{ url('member_ship/member_list/') }}" + "/" + id + "/status",
                                method: 'POST',
                                data: {
                                    _token: '{{ csrf_token() }}',
                                    check: check,
                                },
                                success: function(response) {
                                    if (response.error_message) {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    } else if (response.code == 300) {
                                        $.alert({
                                            icon: 'far fa-times-circle',
                                            title: 'Oops!',
                                            content: response.message,
                                            type: 'red',
                                            buttons: {
                                                Okay: {
                                                    text: 'Okay',
                                                    btnClass: 'btn-red',
                                                }
                                            }
                                        });
                                    } else {
                                        Table.ajax.reload();
                                        Toast.fire({
                                            icon: 'success',
                                            title: `Member has been ${msg} Successfully!`
                                        })
                                    }
                                },
                                error: function(xhr) {
                                    var isRtl = $('html').attr('data-textdirection') === 'rtl';
                                    if (xhr.status === 422) {

                                        var errors = "";
                                        //loop through error
                                        $.each(xhr.responseJSON.errors, function(i, val) {
                                            //loop through inner array for each keys
                                            $.each(val, function(i, val1) {
                                                toastr['error'](
                                                    val1, {
                                                        closeButton: true,
                                                        tapToDismiss: false,
                                                        rtl: isRtl
                                                    });
                                            })
                                        })
                                    } else {
                                        Toast.fire({
                                            icon: 'error',
                                            title: 'An error has been occured! Please Contact Administrator.'
                                        })
                                    }
                                    Table.ajax.reload();
                                }
                            });
                        }
                    },
                    cancel: function() {
                        $.alert('Canceled!');
                    },
                }
            });
        }
    </script>
@endsection
