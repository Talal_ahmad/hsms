@extends('admin.layouts.master')
@section('title', 'Add')
@section('content')
    <section id="multiple-column-form">
        <div class="content-header row">
            <div class="content-header-left col-md-9 col-12 mb-2">
                <div class="row breadcrumbs-top">
                    <div class="col-12">
                        <h2 class="content-header-title float-start mb-0">Member-Ship</h2>
                        <div class="breadcrumb-wrapper">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Dashboard</a>
                                </li>
                                <li class="breadcrumb-item active">Add MemberShip
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <h4 class="card-title">Member-Ship</h4>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs justify-content-around" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="service-tab-center" data-bs-toggle="tab" href="#service-center"
                                aria-controls="service-center" role="tab" aria-selected="false">
                                <h5><strong><i data-feather="activity"></i>Assign Membership To Priviously Added
                                        Owners</strong></h5>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="account-tab-center" data-bs-toggle="tab" href="#account-center"
                                aria-controls="account-center" role="tab" aria-selected="false">
                                <h5><strong><i data-feather="user-plus"></i>Add New Owner To Assign Membership</strong></h5>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="service-center" aria-labelledby="service-tab-center"
                            role="tabpanel">
                            <form action="{{ route('member_add.store') }}" method="POST" id="add_form_one"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <input type="text" name="check" value="previous_owner" hidden>
                                    <div class="col-6 mb-1">
                                        <label class="form-label" for="property_no">Property</label>
                                        <select class="select2 select property_no form-select my_select" name="property_no"
                                            id="property_no" required>
                                            <option></option>
                                            @foreach ($properties as $property)
                                                <option value = "{{ $property->id }}">
                                                    {{ $property->property_number . '/ ' . $property->city_name . '/ ' . $property->society_name . '/ ' . $property->block_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-6 mb-1">
                                        <label class="form-label" for="owner">Select Owner</label>
                                        <select class="select2 select form-select my_select" name="owner" id="owner"
                                            disabled>
                                            <option></option>
                                        </select>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-1">
                                            <label class="form-label" for="member_image_one">Member Recent
                                                Photo:</label>
                                            <input type="file" name="member_image" id="member_image_one"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-1">
                                            <label class="form-label" for="front_image_one">Picture Of Valid
                                                CNIC-Front
                                                Side:</label>
                                            <input type="file" name="front_image" id="front_image_one"
                                                class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="mb-1">
                                            <label class="form-label" for="back_image_one">Picture Of Valid
                                                CNIC-Back
                                                Side:</label>
                                            <input type="file" name="back_image" id="back_image_one"
                                                class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12 text-end">
                                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                </div>
                            </form>
                        </div>
                        <div class="tab-pane" id="account-center" aria-labelledby="account-tab-center" role="tabpanel">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-body">
                                            <form action="{{ route('member_add.store') }}" method="POST" id="add_form"
                                                enctype="multipart/form-data">
                                                @csrf
                                                <div class="row">
                                                    <input type="text" name="check" value="new_owner" hidden>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="property_no_new">Property</label>
                                                        <select class="select2 select property_no form-select my_select" name="property_id"
                                                            id="property_no_new" required>
                                                            <option></option>
                                                            @foreach ($properties as $property)
                                                                <option value = "{{ $property->id }}">
                                                                    {{ $property->property_number . '/ ' . $property->city_name . '/ ' . $property->society_name . '/ ' . $property->block_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="membership_number">Membership
                                                                Number</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="membership_number"
                                                                placeholder="Enter Membership Number"
                                                                name="membership_number" aria-label="type" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="select2-basic-edit">Select
                                                            GL</label>
                                                        <select class="select2 select form-select my_select"
                                                            name="gl_code" id="select2-basic-edit" required>
                                                            @foreach ($gl_code as $gl)
                                                                <option value = "{{ $gl->account_code }}">
                                                                    {{ $gl->account_code }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="full_name">Full Name</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="full_name" placeholder="Enter Name" name="name"
                                                                aria-label="type" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="guardian_name">Father/Husband
                                                                Name</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="guardian_name" placeholder="Enter Father/Husband Name"
                                                                name="father_name" aria-label="guardian_name"
                                                                required />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="email">Email</label>
                                                            <input type="email" class="form-control dt-full-name"
                                                                id="email" placeholder="Enter Email" name="email"
                                                                aria-label="user-email" required />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="cnic">CNIC No./NTN No.</label>
                                                        <input type="text" class="form-control cnic-mask"
                                                            oninput="this.value = this.value.replace(/[^0-9]/g, '');"
                                                            placeholder="Enter Cnic" name="cnic" id="cnic"
                                                            required />
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="cnic_exp_date">CNIC/NTN EXPIRY
                                                            DATE:</label>
                                                        <input type="text" id="cnic_exp_date"
                                                            class="form-control flatpickr-basic" name="cnicExpiryDate"
                                                            placeholder="YYYY-MM-DD" required />
                                                    </div>

                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="curr_address">Current
                                                                Address:</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="curr_address" placeholder="Enter Current Address"
                                                                name="address" aria-label="curr_adress" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="permanent_address">Permanent
                                                                Address:</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="permanent_address"
                                                                placeholder="Enter Permanent Address"
                                                                name="permanent_address" aria-label="permanent_address" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="phone_number">Phone No./Mobile
                                                            No.</label>
                                                        <div class="input-group input-group-merge">
                                                            <span class="input-group-text">PK (+92)</span>
                                                            <input type="text" class="form-control phone-number-mask"
                                                                maxlength="10" placeholder="Enter Mobile No:"
                                                                name="phone_number" id="phone_number" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="sec_phone">Secondary Phone
                                                            No./Mobile No.</label>
                                                        <div class="input-group input-group-merge">
                                                            <span class="input-group-text">PK (+92)</span>
                                                            <input type="text" class="form-control phone-number-mask"
                                                                maxlength="10" placeholder="Enter Secondary Mobile No:"
                                                                name="secondary_phone_number" id="sec_phone" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="vote_number">Member
                                                                Vote</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="vote_number" placeholder="Enter Member Vote:"
                                                                name="member_vote" aria-label="vote_number" required />
                                                        </div>
                                                    </div>

                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="approval_date">Date Of approval by
                                                            the Managing
                                                            Committee</label>
                                                        <input type="text" id="approval_date"
                                                            class="form-control flatpickr-basic" name="date_approval_managingCommitte"
                                                            placeholder="YYYY-MM-DD" />
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="confirmation_date">Date Of
                                                            Confirmation by the General
                                                            Body</label>
                                                        <input type="text" id="confirmation_date"
                                                            class="form-control flatpickr-basic" name="date_confirmation_by_GB"
                                                            placeholder="YYYY-MM-DD" />
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="share_number">Number Of
                                                                Shares</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="share_number" placeholder="Enter Shares Number"
                                                                name="no_shares" aria-label="share_number" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="share_value">Value of
                                                                Shares</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="share_value" placeholder="Enter Value of Share"
                                                                name="value_shares" aria-label="share_value" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="share_capital">Share
                                                                Capitals</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="share_capital" placeholder="Enter Share Capitals"
                                                                name="share_capital" aria-label="share_capital" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="member_image">Member Recent
                                                                Photo:</label>
                                                            <input type="file" name="member_image" id="member_image"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="front_image">Picture Of Valid
                                                                CNIC-Front
                                                                Side:</label>
                                                            <input type="file" name="front_image" id="front_image"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="mb-1">
                                                            <label class="form-label" for="back_image">Picture Of Valid
                                                                CNIC-Back
                                                                Side:</label>
                                                            <input type="file" name="back_image" id="back_image"
                                                                class="form-control">
                                                        </div>
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="member_enroll_date">Date Of
                                                            Enrolment as Member</label>
                                                        <input type="text" id="member_enroll_date"
                                                            class="form-control flatpickr-basic" name="date_enrolment_members"
                                                            placeholder="YYYY-MM-DD" />
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="from_date">MemberShip From
                                                            Date:</label>
                                                        <input type="text" class="form-control flatpickr-basic"
                                                            id="from_date" name="membership_from_date" placeholder="YYYY-MM-DD" />
                                                    </div>
                                                    <div class="col-4 mb-1">
                                                        <label class="form-label" for="to_date">MemberShip To
                                                            Date:</label>
                                                        <input type="text" id="to_date"
                                                            class="form-control flatpickr-basic" name="membership_to_date"
                                                            placeholder="YYYY-MM-DD" />
                                                    </div>
                                                </div>
                                                <?php
                                                $count = 0;
                                                ?>
                                                <section id="accordion-with-margin">
                                                    <div class="row">
                                                        <div class="col-sm-12">
                                                            <div class="card">
                                                                <div class="card-body">
                                                                    <div class="accordion accordion-margin"
                                                                        id="accordionMargin">
                                                                        <div class="pb-3">
                                                                            <div class="accordion-item">
                                                                                <h2 class="accordion-header"
                                                                                    id="headingMarginOne">
                                                                                    <button
                                                                                        class="accordion-button collapsed"
                                                                                        type="button"
                                                                                        data-bs-toggle="collapse"
                                                                                        data-bs-target="#accordionMarginOne"
                                                                                        aria-expanded="false"
                                                                                        aria-controls="accordionMarginOne">
                                                                                        Legal Heirs:
                                                                                    </button>
                                                                                </h2>
                                                                                <div id="accordionMarginOne"
                                                                                    class="accordion-collapse collapse"
                                                                                    aria-labelledby="headingMarginOne"
                                                                                    data-bs-parent="#accordionMargin">
                                                                                    <div class="accordion-body">
                                                                                        <div class="ayat-repeater"
                                                                                            id="erp_type_repeater">
                                                                                            <div
                                                                                                data-repeater-list="member_ship_loop">
                                                                                                <div data-repeater-item>
                                                                                                    <h3 class="title">
                                                                                                        Legal Heir 1</h3>
                                                                                                    <div
                                                                                                        class="row d-flex align-items-end">
                                                                                                        <div
                                                                                                            class="col-6">
                                                                                                            <div
                                                                                                                class="mb-1">
                                                                                                                <label
                                                                                                                    class="form-label">Full
                                                                                                                    Name</label>
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    class="form-control dt-full-name"
                                                                                                                    placeholder="Enter Name"
                                                                                                                    name="heir_name"
                                                                                                                    aria-label="type" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6">
                                                                                                            <div
                                                                                                                class="mb-1">
                                                                                                                <label
                                                                                                                    class="form-label">Father/Husband
                                                                                                                    Name</label>
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    class="form-control dt-full-name"
                                                                                                                    placeholder="Enter Father/Husband Name"
                                                                                                                    name="heir_father_name"
                                                                                                                    aria-label="father_name" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6 mb-1">
                                                                                                            <label
                                                                                                                class="form-label">CNIC
                                                                                                                No./NTN
                                                                                                                No.</label>
                                                                                                            <input
                                                                                                                type="text"
                                                                                                                class="form-control cnic-mask"
                                                                                                                oninput="this.value = this.value.replace(/[^0-9]/g, '');"
                                                                                                                placeholder="Enter Cnic"
                                                                                                                name="heir_cnic" />
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6 mb-1">
                                                                                                            <label
                                                                                                                class="form-label">CNIC/NTN
                                                                                                                EXPIRY
                                                                                                                DATE:</label>
                                                                                                            <input
                                                                                                                type="text"
                                                                                                                class="form-control input-date flatpickr-basic"
                                                                                                                name="heir_cnicExpiryDate"
                                                                                                                placeholder="YYYY-MM-DD" />
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6">
                                                                                                            <div
                                                                                                                class="mb-1">
                                                                                                                <label
                                                                                                                    class="form-label">Email</label>
                                                                                                                <input
                                                                                                                    type="email"
                                                                                                                    class="form-control dt-full-name"
                                                                                                                    placeholder="Enter Email"
                                                                                                                    name="heir_email"
                                                                                                                    aria-label="heir_email" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6 mb-1">
                                                                                                            <label
                                                                                                                class="form-label">Date
                                                                                                                Of
                                                                                                                Birth</label>
                                                                                                            <input
                                                                                                                type="text"
                                                                                                                class="form-control flatpickr-basic"
                                                                                                                name="heir_date_of_birth"
                                                                                                                placeholder="YYYY-MM-DD" />
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6">
                                                                                                            <div
                                                                                                                class="mb-1">
                                                                                                                <label
                                                                                                                    class="form-label">Current
                                                                                                                    Address:</label>
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    class="form-control dt-full-name"
                                                                                                                    placeholder="Enter Current Address"
                                                                                                                    name="heir_address"
                                                                                                                    aria-label="curr_adress" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6">
                                                                                                            <div
                                                                                                                class="mb-1">
                                                                                                                <label
                                                                                                                    class="form-label">Permanent
                                                                                                                    Address:</label>
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    class="form-control dt-full-name"
                                                                                                                    placeholder="Enter Permanent Address"
                                                                                                                    name="heir_permanent_address"
                                                                                                                    aria-label="heir_permanent_address" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6 mb-1">
                                                                                                            <label
                                                                                                                class="form-label">Phone
                                                                                                                No./Mobile
                                                                                                                No.</label>
                                                                                                            <div
                                                                                                                class="input-group input-group-merge">
                                                                                                                <span
                                                                                                                    class="input-group-text">PK
                                                                                                                    (+92)</span>
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    class="form-control phone-number-mask"
                                                                                                                    maxlength="10"
                                                                                                                    placeholder="Enter Mobile No:"
                                                                                                                    name="heir_phone_number" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-6 mb-1">
                                                                                                            <label
                                                                                                                class="form-label">Secondary
                                                                                                                Phone
                                                                                                                No./Mobile
                                                                                                                No.</label>
                                                                                                            <div
                                                                                                                class="input-group input-group-merge">
                                                                                                                <span
                                                                                                                    class="input-group-text">PK
                                                                                                                    (+92)</span>
                                                                                                                <input
                                                                                                                    type="text"
                                                                                                                    class="form-control phone-number-mask"
                                                                                                                    maxlength="10"
                                                                                                                    placeholder="Enter Secondary Mobile No:"
                                                                                                                    name="heir_secondary_phone_number" />
                                                                                                            </div>
                                                                                                        </div>
                                                                                                        <div
                                                                                                            class="col-md-2 col-12 mb-50">
                                                                                                            <div
                                                                                                                class="mb-1">
                                                                                                                <button
                                                                                                                    class="btn btn-outline-danger text-nowrap px-1"
                                                                                                                    data-repeater-delete
                                                                                                                    type="button">
                                                                                                                    <i data-feather="x"
                                                                                                                        class="me-25"></i>
                                                                                                                    <span>Delete</span>
                                                                                                                </button>
                                                                                                            </div>
                                                                                                        </div>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                            <div class="row">
                                                                                                <div class="col-12">
                                                                                                    <button
                                                                                                        class="btn btn-icon btn-primary "
                                                                                                        type="button"
                                                                                                        data-repeater-create>
                                                                                                        <i data-feather="plus"
                                                                                                            class="me-25"></i>
                                                                                                        <span>Add New</span>
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </section>
                                                <div class="col-12 text-end">
                                                    <button type="submit" class="btn btn-primary me-1">Submit</button>
                                                    <button type="reset" class="btn btn-outline-success">Reset</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
@section('custom-scripts-foot')
    <script>
        $(document).ready(function() {
            // var cnic_mask = document.getElementsByClassName('cnic-mask');
            // var cnices = Array.from(cnic_mask);
            // cnices.forEach(function(text){
            //         new Cleave(cnic_mask, {
            //         delimiter: '-',
            //         blocks: [5, 7, 1],
            //         uppercase: true
            //     });
            // })
            var ayat = $('#erp_type_repeater');
            $(ayat).repeater({
                isFirstItemUndeletable: false,
                show: function() {
                    $(this).slideDown();
                    ayat.find('select').next('.select2-container').remove();
                    ayat.find('select').select2();
                    var rowIndex = $(this).index() + 1;
                    $(this).find(".title").text('Legal Heir ' + rowIndex);
                    var datesCollection = document.getElementsByClassName("flatpickr-basic");
                    var dates = Array.from(datesCollection);
                    dates.forEach(function(date) {
                        new Cleave(date, {
                            date: true,
                            delimiter: '-',
                            datePattern: ['Y', 'm', 'd']
                        })
                        datesCollection.flatpickr();
                    });
                    // var cnic_mask = document.getElementsByClassName('cnic-mask');
                    // var cnices = Array.from(cnic_mask);
                    // cnices.forEach(function(text){
                    //         new Cleave(cnic_mask, {
                    //         delimiter: '-',
                    //         blocks: [5, 7, 1],
                    //         uppercase: true
                    //     });
                    // })
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
        });
        $('.property_no').on('change', function() {
            var data = $(".property_no option:selected").val();
            if (data) {
                $("#owner").prop('disabled', false);
                $.ajax({
                    url: "{{ url('/member_ship/member_add') }}" + "/" + data + "/edit",
                    method: 'GET',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "check": "new_owner"
                    },
                    contentType: false,
                    processData: true,
                    success: function(response) {
                        var selectElement = $('#owner');
                        selectElement.empty();
                        selectElement.append('<option></option>');
                        for (var i = 0; i < response.data.length; i++) {
                            var owner = response.data[i];
                            selectElement.append(
                                `<option value="${owner.id}"> ${owner.name} S/O or D/O ${owner.father_name} / ${owner.cnic}</option>`
                            );
                        }
                        selectElement.select2();

                    },
                    error: function(xhr) {}
                });
            } else {
                $('#owner').val(null).trigger('change');
                $("#owner").prop('disabled', true);
            }
        });
    </script>
@endsection
