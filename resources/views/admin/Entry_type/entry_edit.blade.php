@extends('admin.layouts.master')
@section('title', 'Edit')
@section('content')
    <div class="content-body">
        <div class="card">
            <div class="card-bod">
                <div class="row">
                    <!-- Invoice repeater -->
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Update Entry Type</h4>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('entry-add.update', $erp->id) }}" method="POST" id="add_form">
                                    @method('PUT')
                                    @csrf
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="mb-1">
                                                <label class="form-label" for="type">Type</label>
                                                <input type="text" class="form-control dt-full-name" id="type"
                                                    placeholder="Enter Name" name="type" aria-label="type"
                                                    value="{{ $erp->type }}" required />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form mb-0">
                                                <label for="textarea-counter">Description</label>
                                                <textarea data-length="10000" class="form-control char-textarea" id="textarea-counter" rows="3"
                                                    placeholder="Enter Description" name="description" style="height: 100px">{{ $erp->description }}</textarea>
                                            </div>
                                            <small class="textarea-counter-value float-end"><span
                                                    class="char-count">0</span> / 10000 </small>
                                        </div>
                                    </div>
                                    <div class="ayat-repeater" id="ayat_repeat">
                                        <div data-repeater-list="erp_loop">
                                            <div data-repeater-item>
                                                <div class="row d-flex align-items-end">
                                                    <div class="col-5 mb-1">
                                                        <label class="form-label" for="select2-basic-edit">Select GL</label>
                                                        <select class="select2 select form-select my_select" name="gl_code"
                                                            id="select2-basic-edit">
                                                            @foreach ($gl_code as $gl)
                                                                <option
                                                                    value = "{{ $gl->account_code . '|' . $gl->account_name }}">
                                                                    {{ $gl->account_code . ' ' . $gl->account_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="col-3 mb-1">
                                                        <label class="form-label" for="select2-basic-1-edit">Select
                                                            Method</label>
                                                        <select class="select2 select form-select my_selects" name="method"
                                                            id="select2-basic-1-edit">
                                                            <option value = "Debit">Debit</option>
                                                            <option value = "Credit">Credit</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-4">
                                                        <div class="form mb-1">
                                                            <label class="form-label"
                                                                for="erp-description">Description</label>
                                                            <input type="text" class="form-control dt-full-name"
                                                                id="erp-description" placeholder="Enter description"
                                                                name="erp-description" aria-label="erp-description"
                                                                required />
                                                        </div>
                                                    </div>
                                                    <div class="col-md-2 col-12 mb-50">
                                                        <div class="mb-1">
                                                            <button class="btn btn-outline-danger text-nowrap px-1"
                                                                data-repeater-delete type="button">
                                                                <i data-feather="x" class="me-25"></i>
                                                                <span>Delete</span>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-12">
                                                <button class="btn btn-icon btn-primary" type="button"
                                                    data-repeater-create>
                                                    <i data-feather="plus" class="me-25"></i>
                                                    <span>Add New</span>
                                                </button>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="reset" class="btn btn-outline-success" aria-label="Close">
                                                Reset
                                            </button>
                                            <button type="submit" class="btn btn-primary">
                                                Submit
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('custom-scripts-foot')
    <script>
        $(document).ready(function() {
            var ayat = $('#ayat_repeat');
            $(ayat).repeater({
                isFirstItemUndeletable: true,
                show: function() {
                    $(this).slideDown();
                    ayat.find('select').next('.select2-container').remove();
                    ayat.find('select').select2();
                    // Feather Icons
                    if (feather) {
                        feather.replace({
                            width: 14,
                            height: 14
                        });
                    }
                },
                hide: function(deleteElement) {
                    if (confirm('Are you sure you want to delete this element?')) {
                        $(this).slideUp(deleteElement);
                    }
                }
            });
        })
    </script>
@endsection
