<?php

namespace Database\Seeders;

use Hash;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'first_name' => 'Main',
            'last_name' => 'User',
            'email' => 'Admin@gmail.com',
            'password' => Hash::make('12345678'),
        ]);
    }
}
