<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class ValidDateRule implements Rule
{
    protected $attr_name;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($attr_name)
    {
        $this->attr_name = $attr_name;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $date = Carbon::parse($value);
            return $date->isToday() || $date->isFuture();
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The {$this->attr_name} does not accept past date.";
    }
}
