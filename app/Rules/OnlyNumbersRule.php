<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class OnlyNumbersRule implements Rule
{
    protected $attributeName;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($attributeName)
    {
        $this->attributeName = $attributeName;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return preg_match('/^\d+(\.\d+)?$/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The {$this->attributeName} must only contain numbers";
    }
}
