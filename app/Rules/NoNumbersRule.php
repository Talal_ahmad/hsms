<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class NoNumbersRule implements Rule
{
    protected $attr_name;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($attr_name)
    {
        $this->attr_name = $attr_name;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        // Your validation logic here
        return !preg_match('/\d/', $value);
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The {$this->attr_name} field cannot contain numbers.";
    }
}
