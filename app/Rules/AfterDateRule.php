<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class AfterDateRule implements Rule
{
    protected $reference_date_lable;
    protected $previous_date_lable;
    protected $previous_date;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($current_date_lable, $previous_date_lable, $previous_date)
    {
        $this->reference_date_lable = $current_date_lable;
        $this->previous_date_lable = $previous_date_lable;
        $this->previous_date = request()->input($previous_date);
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        try {
            $date = Carbon::parse($value);
            $referenceDate = Carbon::parse($this->previous_date);
            return $date->isSameDay($referenceDate) || $date->isAfter($referenceDate);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The {$this->reference_date_lable} must be same or after the date of {$this->previous_date_lable}";
    }
}
