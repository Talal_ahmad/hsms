<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class BetweenZeroAndHundredRule implements Rule
{
    protected $name;
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct($val)
    {
        $this->name = $val;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        return $value >= 0 && $value <= 100;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return "The {$this->name} must be between 0 and 100.";
    }
}
