<?php

namespace App\Http\Requests;

use App\Rules\OnlyStringRule;
use Illuminate\Foundation\Http\FormRequest;

class GovernBodyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'name' => ['required', new OnlyStringRule('Governing Body   Name')],
            'cnic' => ['required', 'regex:/^\d{5}-\d{7}-\d$/'],
            'designation' => 'required',
            'phone' => 'nullable',
            'adress' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'is_member' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'is_member.required' => 'You must select one between "Member" and "Non Member" option',
        ];
    }
}
