<?php

namespace App\Http\Requests;

use App\Rules\NoNumbersRule;
use App\Rules\OnlyNumbersRule;
use App\Rules\OnlyStringRule;
use Illuminate\Foundation\Http\FormRequest;

class PaymentCodeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'code' => ['required', new OnlyNumbersRule('Code')],
            'name' => ['required'],
            'gl_code' => 'required',
            'payment_type_id' => 'required',
            'tax_rate' => ['nullable', new OnlyNumbersRule('Tax Field')],
            'taxation_body' => 'required',
            'image_path' => 'nullable',
            'description' => 'nullable',
        ];
    }
}
