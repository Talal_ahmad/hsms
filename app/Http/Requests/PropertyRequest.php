<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PropertyRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'type_id' => ['required'],
            'typesProperty' => ['required'],
            'city_id' => ['required'],
            'society_id' => ['required'],
            'block_id' => ['required'],
            'property_number' => ['required'],
            'purchased_price' => ['required', 'integer'],
            'mortage_document' => ['nullable', 'string'],
            'mortage_book' => ['nullable', 'string'],
            'mortage_volume' => ['nullable', 'string'],
            'mortage_pages' => ['nullable', 'string'],
            'mortage_sub_register' => ['nullable', 'string'],
            'nab_detail' => ['nullable', 'string'],

        ];
    }
}
