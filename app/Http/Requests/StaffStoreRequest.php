<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StaffStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'name' => 'required|string',
            'cnic' => ['required', 'regex:/^\d{5}-\d{7}-\d$/'],
            'designation' => 'nullable',
            'phone' => 'nullable',
            'address' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date|after:start_date',
            'remarks' => 'nullable',
        ];
    }
}
