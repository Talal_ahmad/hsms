<?php

namespace App\Http\Requests;

use App\Rules\OnlyNumbersRule;
use App\Rules\OnlyStringRule;
use Illuminate\Foundation\Http\FormRequest;

class SocietyStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'city_id' => 'required',
            'name' => ['required', new OnlyStringRule('City Name')],
            'number' => ['required', new OnlyNumbersRule('Society Number')],
        ];
    }
}
