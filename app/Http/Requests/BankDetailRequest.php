<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BankDetailRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        return [
            'bank_name' => 'required',
            'branch' => 'required',
            'branch_phone_no' => 'required',
            'branch_narration' => 'nullable',
            'branch_address' => 'required',
            'account_title' => 'required',
            'account_no' => 'required',
            'gl_no' => 'required',
            'city_id' => 'required',
            'society_id' => 'required',
            'image' => 'nullable',
        ];
    }
}
