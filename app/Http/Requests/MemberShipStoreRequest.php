<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MemberShipStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =  [
            'membership_number' => ['required', 'integer'],
            'gl_code' => "required",
            'full_name' => "required",
            'guardian_name' => "required",
            'cnic' => "required",
            'cnic_exp_date' => "required|date|after:today",
            'address' => "required",
            'permanent_address' => "required",
            // 'phone_number' => "required|regex:/^\[0-9]{10}$/",
            'vote_number' => "required",
            'approval_date' => "required|date",
            'confirmation_date' => "required|date",
            'share_number' => "required",
            'share_value' => "required",
            'share_capital' => "required",
            'member_image' => "required",
            'front_image' => "required",
            'back_image' => "required",
            'member_enroll_date' => "required",
            'from_date' => "required",
            'to_date' => "required|after:from_date",
        ];

        if ($this->getMethod() == 'POST') {
            $rules += ['email' => "required|email:rfc,dns|unique:member_ships,email,id"];
        }
        if ($this->getMethod() == 'PUT' || $this->getMethod() == 'PATCH') {
            $id = $this->request->get('member_id');
            $rules += ['email' => "unique:member_ships,email,$id,id"];
        }
        return $rules;
    }
}
