<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $rules =  [
            'name' => ['required', 'regex:/^[\pL\s\-]+$/u'],
            // 'type' => "required",
            // 'role_id' => "required",
        ];

        if ($this->getMethod() == 'POST') {
            $rules += ['password' => 'required|min:6'];
            $rules += ['email' => "required|email:rfc,dns|unique:users,email,id"];
        }
        if ($this->getMethod() == 'PUT' || $this->getMethod() == 'PATCH') {
            $id = $this->request->get('user_id');
            $rules += ['password' => 'nullable'];
            $rules += ['email' => "unique:users,email,$id,id"];
        }
        return $rules;
    }
}
