<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VerifyPaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules =  [
            'payment_date' => 'required',
            'paid_amount' => 'required|integer',
            'waived_off' => 'nullable|integer',
            'payment_reference' => 'nullable',
            'remarks' => 'nullable',
        ];
        if ($this->input('waived_off') > 0) {
            $rules += [
                'waived_off_reference' => 'required',
            ];
        }
        // if ($this->input('payment_reference') != null) {
        //     $rules += [
        //         'paid_slip' => 'required'
        //     ];
        // }
        return $rules;
    }
}
