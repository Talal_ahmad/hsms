<?php

namespace App\Http\Requests;

use App\Rules\AfterDateRule;
use App\Rules\BetweenZeroAndHundredRule;
use App\Rules\OnlyNumbersRule;
use App\Rules\OnlyStringRule;
use App\Rules\ValidDateRule;
use Illuminate\Foundation\Http\FormRequest;

class PaymentPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        // , new AtLeastOneRepeaterRule('Collection Heads')
        $rules = [
            'plan_name' => ['required'],
            // , new OnlyNumbersRule('Due Amount'), new BetweenZeroAndHundredRule('Due Amount Percentage')
            'amount_after_due_date' =>  ['required'],
            // , new ValidDateRule('From Date')
            'from_date' => ['required', 'date'],
            // , new AfterDateRule('To Date', 'From Date', 'from_date', 'to_date')
            'to_date' => ['required', 'date'],
            // , new ValidDateRule("Bill's Start Date")
            'start_date' => ['required', 'date'],
            // , new AfterDateRule("Bill's End Date", "Bill's From Date", "start_date")
            'end_date' => ['required', 'date'],
            'image_path' => 'nullable',
            'description' => 'nullable',
        ];

        return $rules;
    }
}
