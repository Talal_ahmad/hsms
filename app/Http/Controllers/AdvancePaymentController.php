<?php

namespace App\Http\Controllers;

use App\Models\AdvancePayment;
use App\Models\Property;
use App\Models\PropertyOwner;
use App\Models\Tenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class AdvancePaymentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = AdvancePayment::select(
                'properties.property_number as property_number',
                'advance_payments.advance_amount as advance_payment',
                'advance_payments.remaining_amount as remaining_amount',
                'advance_payments.billing_month as billing_month',
                'advance_payments.billing_year as billing_year',
                DB::raw("CASE
                WHEN advance_payments.payment_type = 'tenant' THEN tenants.name
                ELSE property_owners.name
                END AS payment_by"),
                'advance_payments.payment_type'
            )
                ->leftJoin('properties', 'properties.id', '=', 'advance_payments.property_id')
                ->leftJoin('tenants', function ($join) {
                    $join->on('advance_payments.paid_by', '=', 'tenants.id');
                })
                ->leftJoin('property_owners', function ($join) {
                    $join->on('advance_payments.paid_by', '=', 'property_owners.id');
                })
                ->whereIn('advance_payments.id', function ($query) {
                    $query->select(DB::raw('MAX(id)'))
                        ->from('advance_payments')
                        ->groupBy('property_id', 'payment_type');
                });
            return DataTables::of($data)->addIndexColumn()->make(true);
        }
        $data = Property::select(
            'properties.id as id',
            'cities.name as city_name',
            'societies.name as society_name',
            'property_blocks.name as block_name',
            'property_types.type as billing_type',
            'properties.property_number as property_number'
        )
            ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
            ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
            ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'properties.type_id')
            ->get();
        return view(
            'admin.Billing.advance_payment',
            [
                'properties' => $data,
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $paid_by = NULL;
                $property = AdvancePayment::where('property_id', $request->property_id)->get()->toArray();
                if ($request->payment_type == "tenant") {
                    $paid_by = Tenant::where('property_id', $request->property_id)->value('id');
                } else {
                    $paid_by = PropertyOwner::where('property_id', $request->property_id)->value('id');
                }
                if ($property == []) {
                    AdvancePayment::create([
                        'property_id' => $request->property_id,
                        'payment_type' => $request->payment_type,
                        'paid_by' => $paid_by,
                        'advance_amount' => $request->advance_amount,
                        'remaining_amount' => $request->advance_amount,
                        'billing_month' => Carbon::now()->format('m'),
                        'billing_year' => Carbon::now()->format('Y'),
                    ]);
                } else {
                    foreach ($property as $single_property) {
                        $total_advance_amount = 0;
                        $total_remaining_amount = 0;
                        if ($single_property['payment_type'] == "tenant") {
                            $active_tenant = Tenant::where('id', $single_property['paid_by'])->where('status', 1);
                            if ($request->payment_type == "tenant") { // so we could make sure that paid_by has the id of tennat
                                if ($paid_by == $active_tenant->value('id')) {
                                    $total_advance_amount = $single_property['advance_amount'] + $request->advance_amount;
                                    $total_remaining_amount = $single_property['remaining_amount'] + $request->advance_amount;
                                } else {
                                    $total_advance_amount = $request->advance_amount;
                                    $total_remaining_amount = $request->advance_amount;
                                }
                            }
                        } else {
                            $active_owner = PropertyOwner::where('id', $single_property['paid_by'])->where('status', 1);
                            if ($request->payment_type == "owner") {
                                if ($paid_by == $active_owner->value('id')) {
                                    $total_advance_amount = $single_property['advance_amount'] + $request->advance_amount;
                                    $total_remaining_amount = $single_property['remaining_amount'] + $request->advance_amount;
                                } else {
                                    $total_advance_amount = $request->advance_amount;
                                    $total_remaining_amount = $request->advance_amount;
                                }
                            }
                        }
                        AdvancePayment::create([
                            'property_id' => $single_property['property_id'],
                            'payment_type' => $request->payment_type,
                            'paid_by' => $paid_by,
                            'advance_amount' => $total_advance_amount == 0 ? $request->advance_amount : $total_advance_amount,
                            'remaining_amount' => $total_remaining_amount == 0 ? $request->advance_amount :  $total_remaining_amount,
                            'billing_month' => Carbon::now()->format('m'),
                            'billing_year' => Carbon::now()->format('Y'),
                        ]);
                    }
                }
                return ["code" => "200", 'message' => "success"];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ["code" => "422", '"errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ["code" => "500", "error_message" => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
