<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\Property;
use App\Models\PropertyBill;
use App\Models\PropertyOwner;
use Hash;
use Auth;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except('updateBills');
    }

    public function index(Request $request)
    {
        $total_properties = Property::count();
        $total_paid_bills = PropertyBill::where('is_paid', 1)->count();
        $total_unpaid_bills = PropertyBill::where('is_paid', 0)->count();
        $total_partially_paid_bills = PropertyBill::where('is_paid', 0)->where('paid_amount', '>', 0)->count();

        $current_month_amounts = PropertyBill::join('payment_plans', 'property_bills.payment_plan_id', 'payment_plans.id')->select('total_amount_after_due_date', 'paid_amount', 'total_amount_within_due_date', 'payment_date', 'billing_month', 'payment_plans.end_date')
            ->where('billing_month', date('m'))
            ->where('billing_year', date('Y'));

        $current_month_total_amount_within_due_date = $current_month_amounts->sum('total_amount_within_due_date');
        $current_month_total_paid_amount = $current_month_amounts->sum('paid_amount');
        $current_month_total_amount_after_due_date = $current_month_amounts
            ->whereRaw('payment_date > end_date')
            ->sum('total_amount_after_due_date');
        $current_month_total_outstanding_amount = ($current_month_total_amount_within_due_date + $current_month_total_amount_after_due_date) - $current_month_total_paid_amount;

        $arrears_amounts = PropertyBill::join('payment_plans', 'property_bills.payment_plan_id', 'payment_plans.id')->select(
            'total_amount_after_due_date',
            'paid_amount',
            'total_amount_within_due_date',
            'payment_date',
            'billing_month',
            'billing_year',
            'payment_plans.end_date'
        )
            ->where('billing_month', '!=', date('m'));
        // ->where('billing_year', '!=', date('Y'));
        // dd($arrears_amounts->get());

        $arrears_total_amount_within_due_date = $arrears_amounts->sum('total_amount_within_due_date');
        // dd($arrears_total_amount_within_due_date);
        $arrears_total_paid_amount = $arrears_amounts->sum('paid_amount');
        $arrears_total_amount_after_due_date = $arrears_amounts
            ->whereRaw('payment_date > end_date')
            ->sum('total_amount_after_due_date');
        // dd($arrears_total_amount_after_due_date);
        $arrears_total_outstanding_amount = ($arrears_total_amount_within_due_date + $arrears_total_amount_after_due_date) - $arrears_total_paid_amount;

        $monthly_amounts = array_fill(1, 12, 0);
        $all_months_total_amounts = PropertyBill::join('payment_plans', 'property_bills.payment_plan_id', 'payment_plans.id')->select(
            DB::raw('MONTH(payment_plans.end_date) as month'),
            DB::raw('SUM((CASE WHEN payment_date < end_date THEN total_amount_after_due_date ELSE 0 END) + total_amount_within_due_date) as total_amount')
        )
            ->groupBy('month')
            ->orderBy('month')
            ->pluck('total_amount', 'month')
            ->toArray();

        // dd($all_months_total_amounts);

        foreach ($all_months_total_amounts as $months => $all_months_total_amount) {
            $monthly_amounts[(int) $months] = $all_months_total_amount;
        }
        $monthly_total_amounts = $monthly_amounts;
        // dd($monthly_total_amounts);

        $all_months_total_paid_amounts = PropertyBill::join('payment_plans', 'property_bills.payment_plan_id', 'payment_plans.id')->select(
            DB::raw('MONTH(payment_plans.end_date) as month'),
            DB::raw('SUM(paid_amount) as total_paid_amount')
        )
            ->groupBy('month')
            ->orderBy('month')
            ->pluck('total_paid_amount', 'month')
            ->toArray();
            
            // dd($all_months_total_paid_amounts);
        foreach ($all_months_total_paid_amounts as $months => $all_months_total_paid_amounts) {
            $monthly_amounts[(int) $months] = $all_months_total_paid_amounts;
        }
        $all_months_total_paid_amounts = $monthly_amounts;

        $all_months_total_outstanding_amounts = PropertyBill::join('payment_plans', 'property_bills.payment_plan_id', 'payment_plans.id')->select(
            DB::raw('MONTH(payment_plans.end_date) as month'),
            DB::raw('(SUM((CASE WHEN payment_date != end_date THEN total_amount_after_due_date ELSE 0 END) + total_amount_within_due_date) - SUM(paid_amount)) as total_outstanding_amount')
        )
            ->groupBy('month')
            ->orderBy('month')
            ->pluck('total_outstanding_amount', 'month')
            ->toArray();

        foreach ($all_months_total_outstanding_amounts as $months => $all_months_total_outstanding_amount) {
            $monthly_amounts[(int) $months] = $all_months_total_outstanding_amount;
        }
        $all_months_total_outstanding_amounts = $monthly_amounts;
        // dd($request->ajax());
        if ($request->ajax()) {
            $dashboard = PropertyBill::join('properties', 'property_bills.property_id', 'properties.id')
                ->leftJoin('property_owners', 'property_bills.property_id', 'property_owners.property_id')
                ->leftJoin('tenants', 'property_bills.property_id', 'tenants.property_id')
                ->select(
                    'properties.property_number',
                    'properties.id as property_id',
                    'properties.common_area',
                    'property_owners.name as property_owner_name',
                    'tenants.name as tenant_name',
                    'property_owners.phone_number as property_owners_phone_number',
                    'tenants.phone as tenants_phone_number',
                    'property_bills.billing_month',
                    'property_bills.billing_year',
                    'property_bills.paid_amount',
                    'property_bills.is_paid',
                    'property_bills.assigned_to as assigned_to',
                    'property_bills.total_amount_within_due_date',
                    'property_bills.total_amount_after_due_date',
                )
                // ->where('properties.property_number', '7F-63')
                ->where(function ($assigned_to) {
                    $assigned_to->whereRaw('property_bills.assigned_to = property_owners.name')
                        ->orWhereRaw('property_bills.assigned_to = tenants.name');
                })
                ->where('property_bills.billing_year', date('Y'))
                ->orderBy('properties.property_number')
                ->get()
                ->groupBy('property_number');
            // dd($dashboard);

            $dashboard = $dashboard->map(function ($group, $property_number) {
                $total_paid_amount = $group->sum('paid_amount');
                $total_amount_within_due_date = $group->sum('total_amount_within_due_date');
                $all_billing_months = $group->pluck('is_paid', 'billing_month')->toArray();
                // $all_billing_months_is_paid = $group->pluck('is_paid')->toArray();
                // $total_amount_after_due_date = $group->sum('total_amount_after_due_date');
                $total_amount_after_due_date = $group->filter(function ($item) {
                    return \Carbon\Carbon::parse($item->payment_date)->month != $item->billing_month;
                })->sum('total_amount_after_due_date');
                $first_item = $group->first();

                return [
                    'property_number' => $property_number,
                    'name' => $first_item->common_area == 'owner' ? $first_item->property_owner_name : $first_item->tenant_name,
                    'phone_number' => $first_item->common_area == 'owner' ? $first_item->tenants_phone_number : $first_item->property_owners_phone_number,
                    'total_paid_amount' => $total_paid_amount,
                    'billing_month_is_paid' => $all_billing_months,
                    // 'is_paid' => $all_billing_months_is_paid,
                    'total_amount_within_due_date' => $total_amount_within_due_date,
                    'total_amount_after_due_date' => $total_amount_after_due_date,
                ];
            })->values();
            // dd($dashboard);
            return DataTables::of($dashboard)->addIndexColumn()->make(true);
        }
        return view('admin.index', get_defined_vars());
    }

    public function profile()
    {
        return view('admin.users.profile');
    }

    public function updateUserProfile(Request $request)
    {
        if (request()->query('type') == 'changePassowrd') {
            $this->validate($request, [
                'old_password' => 'required',
                'password' => 'required|min:8|max:20|confirmed',
            ]);
            $user = User::find(Auth::user()->id);
            if (Hash::check($request->old_password, $user->password)) {
                $user->fill([
                    'password' => Hash::make($request->password)
                ])->save();
                return redirect('profile?type=changePassowrd')->with('success_message', 'Password has been Updated Successfully');
            } else {
                return redirect('profile?type=changePassowrd')->with('error_message', 'Old Password didn\'t match');
            }
        } else {
            $user = User::find(Auth::user()->id);
            if ($request->hasFile('profile_pic')) {
                if (!empty($user->profile_photo_path)) {
                    unlink(public_path() . '/profile_pics/' . $user->profile_photo_path);
                }
                $profile_pic = User::InsertImage($request->file('profile_pic'));
            }
            $user->first_name = $request->firstName;
            $user->last_name = $request->lastName;
            $user->email = $request->email;
            if (isset($profile_pic)) {
                $user->profile_photo_path = $profile_pic;
            }
            $user->update();
            return redirect('profile?type=general')->with('success_message', 'Profile has been Updated Successfully');
        }
    }

    public function updateBills()
    {
        $bills = PropertyBill::get();
        foreach ($bills as $key => $bill) {
            $owner = PropertyOwner::where([['property_id', $bill->property_id], ['name', '=', $bill->assigned_to]])->first();
            if (!empty($owner)) {
                $bill->assigned_to = $owner->id;
                $bill->update();
            }
        }
        return 1;
    }
}
