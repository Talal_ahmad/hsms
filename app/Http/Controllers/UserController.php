<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserStoreRequest;
use App\Models\City;
use App\Models\PropertyBlock;
use App\Models\Society;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use Spatie\Permission\Models\Role;
use Yajra\DataTables\Facades\DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $cities = City::all();
        $societies = Society::all();
        $blocks = PropertyBlock::all();
        $roles = Role::all();
        if ($request->ajax()) {
            $user = DB::table('users')->select([

                'users.name',
                'users.id',
                'users.email',
                'users.society_id',
                'users.city_id',
                'users.block_id',
                DB::raw('COALESCE(GROUP_CONCAT(DISTINCT cities.name), "-") as city_name'),
                DB::raw('COALESCE(GROUP_CONCAT(DISTINCT societies.name), "-") as society_name'),
                DB::raw('COALESCE(GROUP_CONCAT(DISTINCT property_blocks.name), "-") as block_name')
            ])
                ->leftJoin('cities', function ($join) {
                    $join->on(DB::raw('FIND_IN_SET(cities.id, users.city_id)'), '>', DB::raw('0'));
                })
                ->leftJoin('societies', function ($join) {
                    $join->on(DB::raw('FIND_IN_SET(societies.id, users.society_id)'), '>', DB::raw('0'));
                })
                ->leftJoin('property_blocks', function ($join) {
                    $join->on(DB::raw('FIND_IN_SET(property_blocks.id, users.block_id)'), '>', DB::raw('0'));
                })
                ->groupBy('users.name', 'users.id', 'users.email', 'users.society_id', 'users.city_id', 'users.block_id')
                ->get();

            return DataTables::of($user)->make(true);
        }
        return view(
            'admin.users.index',
            [
                'cities' => $cities,
                'societies' => $societies,
                'blocks' => $blocks,
                'roles' => $roles
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserStoreRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                if ($request->has('city_id')) {
                    $request->merge(['city_id' => implode(',', $request->city_id)]);
                }
                if ($request->has('society_id')) {
                    $request->merge(['society_id' => implode(',', $request->society_id)]);
                }
                if ($request->has('block_id')) {
                    $request->merge(['block_id' => implode(',', $request->block_id)]);
                }
                $request['password'] = Hash::make($request->password);
                $user = User::create($request->all());
                $user->assignRole($request->role_id);
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {

        $entry = User::FindOrFail($id);
        $roles = DB::table('model_has_roles')->where('model_id', $id)->pluck('role_id')->toArray();
        $society = explode(',', $entry->society_id);
        $city = explode(',', $entry->city_id);
        $block = explode(',', $entry->block_id);
        return response([
            'data' => $entry,
            'society' => json_encode($society),
            'city' => json_encode($city),
            'block' => json_encode($block),
            'role' => json_encode($roles)
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserStoreRequest $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                if ($request->has('city_id')) {
                    $request->merge(['city_id' => implode(',', $request->city_id)]);
                }
                if ($request->has('society_id')) {
                    $request->merge(['society_id' => implode(',', $request->society_id)]);
                }
                if ($request->has('block_id')) {
                    $request->merge(['block_id' => implode(',', $request->block_id)]);
                }
                $user = User::findOrFail($id);
                if ($request->password == null) {
                    $user->update($request->except(['password']));
                } else {
                    $request['password'] = Hash::make($request->password);
                    $user->update($request->all());
                }
                if ($request->role_id) {
                    $user->roles()->detach();
                    $user->assignRole($request->role_id);
                } else {
                    $user->roles()->detach();
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            DB::transaction(function () use ($id) {
                $user = User::findOrFail($id);
                $user->roles()->detach();
                $user->delete();
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
}
