<?php

namespace App\Http\Controllers;

use App\Http\Requests\BankDetailRequest;
use App\Models\BankDetail;
use App\Models\City;
use App\Models\Society;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationData;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class BankDetailController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = BankDetail::select(
                'bank_details.*',
                'cities.name as city_name',
                'societies.name as society_name'
            )
                ->join('cities', 'bank_details.city_id', '=', 'cities.id')
                ->join('societies', 'bank_details.society_id', '=', 'societies.id');
            return DataTables::of($data)->make(true);
        }
        $city = City::all();
        $society = Society::all();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://194.163.145.90/eperp/api/getGlAccounts?data=gl_codes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $gl_entries = json_decode($response);
        return view(
            'admin.bank',
            [
                'gl_code' => $gl_entries->data,
                'cities' => $city,
                'societies' => $society
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(BankDetailRequest $request)
    {
        try {
            $requestData = $request->all();
            if ($request->hasFile('image')) {
                $image = $request->image;
                $name = time() . rand(1, 100) . "." . $image->extension();
                $image->move(public_path('payment_code'), $name);
                $requestData['image'] = $name;
            }
            $gl_no = explode('|', $requestData['gl_no']);
            $requestData['gl_code'] = $gl_no[0];
            $requestData['gl_name'] = $gl_no[1];
            BankDetail::create($requestData);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $details = BankDetail::findOrFail($id);
        return response([
            'data' => $details,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BankDetailRequest $request, string $id)
    {
        try {
            $bank = BankDetail::findOrFail($id);
            $requestData = $request->all();
            if ($request->hasFile('image')) {
                if (file_exists(public_path('payment_code/' . $bank->image))) {
                    unlink(public_path('payment_code/' . $bank->image));
                } else {
                    return response()->json(['message' => 'Cannot Update!  Contact Administration'], 500);
                }
                $image = $request->image;
                $name = time() . rand(1, 100) . "." . $image->extension();
                $image->move(public_path('payment_code'), $name);
                $requestData['image'] = $name;
            }
            $gl_no = explode('|', $requestData['gl_no']);
            $requestData['gl_code'] = $gl_no[0];
            $requestData['gl_name'] = $gl_no[1];
            $bank->update($requestData);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
