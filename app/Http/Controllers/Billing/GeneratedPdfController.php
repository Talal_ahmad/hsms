<?php

namespace App\Http\Controllers\Billing;

use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\GeneratedPdf;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class GeneratedPdfController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $city = City::all();
        if ($request->ajax()) {
            $where =  "";
            if ($request->city_id) {
                $toInt = (int)$request->city_id;
                $where .= "generatedpdf.city_name = $toInt";

                if ($request->society_id) {
                    $toInt = (int)$request->society_id;
                    $where .= " AND generatedpdf.society_name = $toInt";

                    if ($request->block_id) {
                        $toInt = (int)$request->block_id;
                        $where .= " AND generatedpdf.block_name = $toInt";
                    }
                }
            }
            $data = GeneratedPdf::select('generatedpdf.*', 'users.name as user_name')
                ->join('users', 'generatedpdf.user_id', '=', 'users.id');
            if (!empty($where)) {
                $data->whereRaw($where);
                return DataTables::of($data)->make(true);
            } else {
                return DataTables::of($data)->make(true);
            }
        }
        return view('admin.Billing.pdf', ['cities' => $city]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == "society") {
            $society = DB::table('societies')->select('id', 'name')->where('city_id', $id)->get();
            return response([
                'data' => $society
            ]);
        }
        if ($request->check == "block") {
            $block = DB::table('property_blocks')->select('id', 'name')->where('society_id', $id)->get();
            return response([
                'data' => $block
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
