<?php

namespace App\Http\Controllers\Billing;

use App\Http\Controllers\Controller;
use App\Jobs\BillCreateJob;
use App\Models\AdvancePayment;
use App\Models\City;
use App\Models\OldPropertyBill;
use App\Models\OldPropertyBillDetail;
use App\Models\PaymentCode;
use App\Models\PaymentPlan;
use App\Models\PaymentPlanDetail;
use App\Models\Property;
use App\Models\PropertyBill;
use App\Models\PropertyBillDetail;
use App\Models\PropertyOwner;
use App\Models\Tenant;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Validation\ValidationException;

class PropertyBillController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $city = City::all();
        if ($request->ajax()) {
            $where = "";
            if ($request->city_id) {
                $toInt = (int) $request->city_id;
                $where .= "properties.city_id = $toInt";

                if ($request->society_id) {
                    $toInt = (int) $request->society_id;
                    $where .= " AND properties.society_id = $toInt";

                    if ($request->block_id) {
                        $toInt = (int) $request->block_id;
                        $where .= " AND properties.block_id = $toInt";
                    }
                }
            }
            $data = Property::select(
                'type_id',
                DB::raw('MIN(cities.name) as city_name'),
                DB::raw('MIN(societies.name) as society_name'),
                DB::raw('MIN(property_blocks.name) as block_name'),
                DB::raw('MIN(property_types.type) as type_name'),
                DB::raw('COALESCE(GROUP_CONCAT(DISTINCT payment_plans.plan_name), "-") as plan')
            )
                ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                ->leftjoin('property_types', 'property_types.id', '=', 'properties.type_id')
                ->leftJoin('payment_plans', function ($join) {
                    $join->on(DB::raw('FIND_IN_SET(payment_plans.id, properties.payment_plan_id)'), '>', DB::raw('0'));
                })
                ->whereNotNull('properties.payment_plan_id')
                ->where('properties.type_id', '!=', 0)
                ->groupBy('type_id');
            if (!empty($where)) {
                $data->whereRaw($where);
                // dd($data);
                return DataTables::of($data)->make(true);
            } else {
                // dd($data->pluck('plan'));
                return DataTables::of($data)->make(true);
            }
        }
        return view('admin.Billing.monthly_bills', ['cities' => $city]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        return ['data' => Bus::findBatch($id)];
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == "society") {
            $society = DB::table('societies')->select('id', 'name')->where('city_id', $id)->get();
            return response([
                'data' => $society
            ]);
        }
        if ($request->check == "block") {
            $block = DB::table('property_blocks')->select('id', 'name')->where('society_id', $id)->get();
            return response([
                'data' => $block
            ]);
        }
    }

    public function requiredDays($bill)
    {
        $code_id = PaymentPlanDetail::where('payment_plan_id', $bill->value('payment_plan_id'))->first();
        if (!empty($code_id)) {
            $days = 0;
            $days = PaymentCode::where('payment_codes.id', $code_id->value('payment_code_id'))
                ->select('payment_types.num_of_days as total_days')
                ->leftJoin('payment_types', 'payment_types.id', '=', 'payment_codes.payment_type_id')
                ->value('total_days');
            $date = Carbon::parse($bill->value('start_date'))->addDays($days + $days);
            $date = ($days > 0) ? $date->subDays($date->day - 1) : Carbon::parse($bill->value('start_date')); // collection types like government tax usually don't have days-span, so handling them using "0"
            return $date;
        }
        return false;
    }
    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        // dd($request->all());
        try {
            // DB::transaction(function () use ($request, $id) {
            $bills_batch = Bus::batch([])->dispatch();
            if ($request->plans) {
                $properties = Property::where('type_id', $id)->whereIn('id', $request->plans)->whereNotNull('properties.payment_plan_id')->get();
                // dd($properties . 'if');
            } else {
                $properties = Property::where('type_id', $id)->whereNotNull('properties.payment_plan_id')->get();
                // dd($properties . 'else');
            }
            // $i = 0;
            $random_number = str_pad(mt_rand(1000, 9999), 4, '0', STR_PAD_LEFT);
            $unit_area = 0;
            foreach ($properties as $property) {
                $is_challan = PropertyBill::where('property_id', $property->id)->value('challan_no');
                if (PropertyBill::count() == 0) {
                    $challan_no = $random_number;
                    $random_number++;
                } else if ($is_challan != null) {
                    $is_challan = explode('-', $is_challan);
                    $challan_no = $is_challan[0];
                } else {
                    $challan_no = $random_number;
                    $random_number++;
                }
                $challan_no = $challan_no . '-' . str_pad($request->billing_month, 2, '0', STR_PAD_LEFT) . '-' . $request->billing_year;
                $assigned_to = '';
                $advance_payment = 0;
                $temp = null;
                if ($property->common_area == "tenant") {
                    $assigned_to = Tenant::where('tenants.property_id', $property->id)->value('name');
                    $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $property->id)->value('id'))->latest()->first();
                    $advance_payment = $temp == null ? 0 : $temp['remaining_amount'];
                } else {
                    $assigned_to = PropertyOwner::where('property_owners.property_id', $property->id)->value('name');
                    $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $property->id)->value('id'))->latest()->first();
                    $advance_payment = $temp == null ? 0 : $temp['remaining_amount'];
                }
                // check if any values of payment_plan_id is present in property_bills table...
                $to_string = explode(',', str_replace("'", '', $property->payment_plan_id));
                $int_array = array_map(function ($id) {
                    return (int) trim($id, "'");
                }, $to_string);
                $plans = PaymentPlan::whereIn('id', $int_array)->get();
                // Now we have to check each value of (payment_plan_id)'s so avoid duplicate bills
                foreach ($plans as $plan) {
                    $billing_month = $request->billing_month;
                    $billing_year = $request->billing_year;
                    $check_bill = PropertyBill::select(
                        'property_bills.*',
                        'payment_plans.start_date as start_date',
                        'payment_plans.end_date as end_date'
                    )
                        ->leftJoin('payment_plans', 'payment_plans.id', '=', 'property_bills.payment_plan_id')
                        ->where('property_id', $property->id)
                        ->orderBy('billing_month', 'desc')
                        ->limit(1);
                    // ->where('payment_plan_id', $plan->id);
                    $plan_details = PaymentPlanDetail::where('payment_plan_id', $plan->id);
                    $total_amount = 0;
                    $unit_area = 0;
                    foreach ($plan_details->get() as $details) {
                        $tax_for_each_head = 0;
                        if ($details->calculation_unit == 'marla') {
                            $tax_for_each_head = ((PaymentCode::where('id', $details->payment_code_id)->value('tax_rate') / 100) * ($details->amount * $property->landAreaMarla));
                            $total_amount += ($details->amount * $property->landAreaMarla) + $tax_for_each_head;
                            $unit_area = $property->landAreaMarla;
                        } else if ($details->calculation_unit == 'kanal') {
                            $tax_for_each_head = ((PaymentCode::where('id', $details->payment_code_id)->value('tax_rate') / 100) * ($details->amount * $property->landAreaKanal));
                            $total_amount += ($details->amount * $property->landAreaKanal) + $tax_for_each_head;
                            $unit_area = $property->landAreaKanal;
                        } else if ($details->calculation_unit == 'fixed') {
                            $tax_for_each_head = ((PaymentCode::where('id', $details->payment_code_id)->value('tax_rate') / 100) * ($details->amount));
                            $total_amount += $details->amount + $tax_for_each_head;
                        } else {
                            $tax_for_each_head = ((PaymentCode::where('id', $details->payment_code_id)->value('tax_rate') / 100) * ($details->amount * $property->landAreaSqfeet));
                            $total_amount += ($details->amount * $property->landAreaSqfeet) + $tax_for_each_head;
                            $unit_area = $property->landAreaSqfeet;
                        }
                    }
                    // only insert bill if doesn't exist
                    if (!$check_bill->exists()) {
                        $payment_codes = PaymentPlanDetail::where('payment_plan_id', $plan->id)->pluck('payment_code_id');
                        foreach ($payment_codes as $key => $value) {
                            $code = PaymentPlanDetail::where('payment_code_id', $value);
                            $amount = $code->value('calculation_unit') == 'fixed' ? $code->value('amount') : round(PaymentPlanDetail::where('payment_code_id', $value)->value('amount') * $property->landAreaSqfeet);
                            PropertyBillDetail::create([
                                'property_bill_id' => $challan_no,
                                'payment_code_id' => $value,
                                'payment_plan_id' => $plan->id,
                                'amount' => $amount,
                                'pra' => $amount * (PaymentCode::where('id', $value)->value('tax_rate') / 100),
                            ]);
                        }
                        $bills_batch->add(
                            new BillCreateJob(
                                $property->id,
                                $plan->id,
                                $property->city_id,
                                $property->society_id,
                                $property->block_id,
                                $billing_month,
                                $billing_year,
                                round($total_amount),
                                round($total_amount * ($plan->amount_after_due_date / 100)),
                                $challan_no,
                                $assigned_to,
                                0,
                                Auth::id(),
                                0,
                                round($total_amount) - round($advance_payment) <= 0 ? 1 : 0,
                                round($total_amount) - round($advance_payment) <= 0 ? round($total_amount) : round($advance_payment),
                                $temp == null ? 0 : ($temp['remaining_amount'] == 0 ? 0 : 1),
                                round($total_amount) - round($advance_payment) <= 0 ? Carbon::now() : NULL
                            )
                        );
                        if ($property->common_area == "tenant") {
                            $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $property->id)->value('id'))->latest()->first();
                            if ($temp != null) {
                                AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => round($total_amount) - round($advance_payment) < 0 ? (round($total_amount) - round($advance_payment)) * -1 : 0]);
                            }
                        } else {
                            $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $property->id)->value('id'))->latest()->first();
                            if ($temp != null) {
                                AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => round($total_amount) - round($advance_payment) < 0 ? (round($total_amount) - round($advance_payment)) * -1 : 0]);
                            }
                        }
                        $bill_history = OldPropertyBill::create([
                            'challan_no' => $challan_no,
                            'name' => $assigned_to,
                            'unit_no' => $property->property_number,
                            'unit_area' => $unit_area == null ? 0 : $unit_area,
                            'billing_month' => $billing_month,
                            'billing_year' => $billing_year,
                            'arrears' => 0,
                            'due_amount' => round($total_amount),
                            'amount_after_due_date' => round($total_amount * ($plan->amount_after_due_date / 100)),
                            'start_end' => $plan->start_date,
                            'due_date' => $plan->end_date
                        ]);
                        $payment_names = $plan_details->select('payment_codes.name as name', 'payment_codes.tax_rate as tax', 'payment_plan_details.*')
                            ->leftJoin('payment_codes', 'payment_codes.id', '=', 'payment_plan_details.payment_code_id');
                        foreach ($payment_names->get() as $details) {
                            OldPropertyBillDetail::create([
                                'old_property_bill_id' => $bill_history->id,
                                'name' => $details->name,
                                'amount_per_unit' => $details->amount,
                                'total_amount' => $details->value('calculation_unit') == 'fixed' ? $details->value('amount') : round($details->value('amount') * $property->landAreaSqfeet),
                                'pra_percent' => $details->tax
                            ]);
                        }
                    } else if ($check_bill->exists() && ($request->billing_month > $check_bill->value('billing_month')) && ($request->billing_year >= $check_bill->value('billing_year'))) {
                        // $currentDate->copy()->startOfDay()->isAfter(Carbon::parse($property_bills_cont->requiredDays($check_bill))) || $currentDate->toDateString() === Carbon::parse($property_bills_cont->requiredDays($check_bill))->toDateString()
                        if ($check_bill->value('is_paid')) {
                            $bills_batch->add(
                                new BillCreateJob(
                                    $property->id,
                                    $plan->id,
                                    $property->city_id,
                                    $property->society_id,
                                    $property->block_id,
                                    $billing_month,
                                    $billing_year,
                                    round($total_amount),
                                    round($total_amount * ($plan->amount_after_due_date / 100)),
                                    $challan_no,
                                    $assigned_to,
                                    0,
                                    Auth::id(),
                                    0,
                                    round($total_amount) - round($advance_payment) <= 0 ? 1 : 0,
                                    round($total_amount) - round($advance_payment) <= 0 ? round($total_amount) : round($advance_payment),
                                    $temp == null ? 0 : ($temp['remaining_amount'] == 0 ? 0 : 1),
                                    round($total_amount) - round($advance_payment) <= 0 ? Carbon::now() : NULL
                                )
                            );
                            $payment_codes = PaymentPlanDetail::where('payment_plan_id', $plan->id)->pluck('payment_code_id');
                            foreach ($payment_codes as $key => $value) {
                                PropertyBillDetail::create([
                                    'property_bill_id' => $challan_no,
                                    'payment_code_id' => $value,
                                    'payment_plan_id' => $plan->id,
                                    'amount' => round(PaymentPlanDetail::where('payment_code_id', $value)->value('amount') * $property->landAreaSqfeet),
                                    'pra' => PaymentCode::where('id', $value)->value('tax_rate'),
                                ]);
                            }
                            if ($property->common_area == "tenant") {
                                $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $property->id)->value('id'))->latest()->first();
                                if ($temp != null) {
                                    AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => round($total_amount) - round($advance_payment) < 0 ? (round($total_amount) - round($advance_payment)) * -1 : 0]);
                                }
                            } else {
                                $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $property->id)->value('id'))->latest()->first();
                                if ($temp != null) {
                                    AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => round($total_amount) - round($advance_payment) < 0 ? (round($total_amount) - round($advance_payment)) * -1 : 0]);
                                }
                            }
                            $bill_history = OldPropertyBill::create([
                                'challan_no' => $challan_no,
                                'name' => $assigned_to,
                                'unit_no' => $property->property_number,
                                'unit_area' => $unit_area == null ? 0 : $unit_area,
                                'billing_month' => $billing_month,
                                'billing_year' => $billing_year,
                                'arrears' => 0,
                                'due_amount' => round($total_amount),
                                'amount_after_due_date' => round($total_amount * ($plan->amount_after_due_date / 100)),
                                'start_end' => $plan->start_date,
                                'due_date' => $plan->end_date
                            ]);
                            $payment_names = $plan_details->select('payment_codes.name as name', 'payment_codes.tax_rate as tax', 'payment_plan_details.*')
                                ->leftJoin('payment_codes', 'payment_codes.id', '=', 'payment_plan_details.payment_code_id');
                            foreach ($payment_names->get() as $details) {
                                OldPropertyBillDetail::create([
                                    'old_property_bill_id' => $bill_history->id,
                                    'name' => $details->name,
                                    'amount_per_unit' => $details->amount,
                                    'total_amount' => $details->value('calculation_unit') == 'fixed' ? $details->value('amount') : round($details->value('amount') * $property->landAreaSqfeet),
                                    'pra_percent' => $details->tax
                                ]);
                            }
                        } else {
                            $total_arrears = $check_bill->value('arrears') == null ? 0 : $check_bill->value('arrears');
                            $waived_off = $check_bill->value('waived_off') == null ? 0 : $check_bill->value('waived_off');
                            $final_amount = $check_bill->value('total_amount_within_due_date') + ($check_bill->value('total_amount_after_due_date') == null ? 0 : $check_bill->value('total_amount_after_due_date'));
                            $paid_amount = $check_bill->value('paid_amount') == null ? 0 : $check_bill->value('paid_amount');
                            $final_arrears = (($total_arrears - $waived_off) + $final_amount) - $paid_amount;
                            $bills_batch->add(
                                new BillCreateJob(
                                    $property->id,
                                    $plan->id,
                                    $property->city_id,
                                    $property->society_id,
                                    $property->block_id,
                                    $billing_month,
                                    $billing_year,
                                    round($total_amount),
                                    round($total_amount * ($plan->amount_after_due_date / 100)),
                                    $challan_no,
                                    $assigned_to,
                                    round($final_arrears) > -1 ? round($final_arrears) : 0,
                                    Auth::id(),
                                    $waived_off,
                                    (round($final_arrears) + round($total_amount)) - round($advance_payment) <= 0 ? 1 : 0,
                                    (round($final_arrears) + round($total_amount)) - round($advance_payment) <= 0 ? round($final_arrears) + round($total_amount) : round($advance_payment),
                                    $temp == null ? 0 : ($temp['remaining_amount'] == 0 ? 0 : 1),
                                    (round($final_arrears) + round($total_amount)) - round($advance_payment) <= 0 ? Carbon::now() : NULL,
                                )
                            );
                            $payment_codes = PaymentPlanDetail::where('payment_plan_id', $plan->id)->pluck('payment_code_id');
                            foreach ($payment_codes as $key => $value) {
                                PropertyBillDetail::create([
                                    'property_bill_id' => $challan_no,
                                    'payment_code_id' => $value,
                                    'payment_plan_id' => $plan->id,
                                    'amount' => round(PaymentPlanDetail::where('payment_code_id', $value)->value('amount') * $property->landAreaSqfeet),
                                    'pra' => PaymentCode::where('id', $value)->value('tax_rate'),
                                ]);
                            }
                            if ($property->common_area == "tenant") {
                                $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $property->id)->value('id'))->latest()->first();
                                if ($temp != null) {
                                    AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => (round($final_arrears) + round($total_amount)) - round($advance_payment) < 0 ? (round(($final_arrears) + round($total_amount)) - round($advance_payment)) * -1 : 0]);
                                }
                            } else {
                                $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $property->id)->value('id'))->latest()->first();
                                if ($temp != null) {
                                    AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => (round($final_arrears) + round($total_amount)) - round($advance_payment) < 0 ? (round(($final_arrears) + round($total_amount)) - round($advance_payment)) * -1 : 0]);
                                }
                            }
                            $bill_history = OldPropertyBill::create([
                                'challan_no' => $challan_no,
                                'name' => $assigned_to,
                                'unit_no' => $property->property_number,
                                'unit_area' => $unit_area == null ? 0 : $unit_area,
                                'billing_month' => $billing_month,
                                'billing_year' => $billing_year,
                                'arrears' => round($final_arrears) > -1 ? round($final_arrears) : 0,
                                'due_amount' => round($total_amount),
                                'amount_after_due_date' => round($total_amount * ($plan->amount_after_due_date / 100)),
                                'start_end' => $plan->start_date,
                                'due_date' => $plan->end_date
                            ]);
                            $payment_names = $plan_details->select('payment_codes.name as name', 'payment_codes.tax_rate as tax', 'payment_plan_details.*')
                                ->leftJoin('payment_codes', 'payment_codes.id', '=', 'payment_plan_details.payment_code_id');
                            foreach ($payment_names->get() as $details) {
                                OldPropertyBillDetail::create([
                                    'old_property_bill_id' => $bill_history->id,
                                    'name' => $details->name,
                                    'amount_per_unit' => $details->amount,
                                    'total_amount' => $details->value('calculation_unit') == 'fixed' ? $details->value('amount') : round($details->value('amount') * $property->landAreaSqfeet),
                                    'pra_percent' => $details->tax
                                ]);
                            }
                        }
                    }
                }
            }
            return ['data' => $bills_batch->id];
            // });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                // DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function get_plans($id)
    {
        $properties = Property::leftJoin('payment_plans', 'payment_plans.id', 'payment_plan_id')->where('type_id', $id)->whereNotNull('properties.payment_plan_id')
            ->select(
                'properties.*',
                'properties.id as property_id',
                'payment_plans.plan_name as plan_name'
            )
            ->get();
        // dd($properties);
        return response()->json(['properties' => $properties]);
    }
}
