<?php

namespace App\Http\Controllers\Billing;

use App\Http\Controllers\Controller;
use App\Http\Requests\VerifyPaymentRequest;
use App\Models\City;
use App\Models\PaymentCode;
use App\Models\PaymentPlan;
use App\Models\PaymentPlanDetail;
use App\Models\Property;
use App\Models\PropertyBill;
use App\Models\PropertyBillDetail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;
use Illuminate\Validation\ValidationException;

class BillController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $city = City::all();
        if ($request->ajax()) {
            // dd($request->all());
            $data = PropertyBill::select(
                'property_bills.*',
                'property_bills.challan_no as challan_number',
                'properties.property_number as plot_number',
                'cities.name as city_name',
                'societies.name as society_name',
                'property_blocks.name as block_name',
                // 'property_bill_details.pra as pra_amount'
            )
                ->join('cities', 'property_bills.city_id', '=', 'cities.id')
                ->join('societies', 'property_bills.society_id', '=', 'societies.id')
                ->join('property_blocks', 'property_bills.block_id', '=', 'property_blocks.id')
                ->join('properties', 'property_bills.property_id', '=', 'properties.id');
            // ->join('property_bill_details', 'property_bill_details.property_bill_id', '=', 'property_bills.challan_no');
            // dd($data->get());
            if ($request->city_id) {
                $data->where('property_bills.city_id', (int) $request->city_id);
                if ($request->society_id) {
                    $data->where('property_bills.society_id', (int) $request->society_id);
                    if ($request->block_id) {
                        $data->where('property_bills.block_id', (int) $request->block_id);
                    }
                }
            }
            if ($request->is_paid) {
                $request->is_paid == 'paid' ? $data->where('property_bills.is_paid', 1) : $data->where('property_bills.is_paid', 0);
            }
            if ($request->billing_month) {
                $data->where('property_bills.created_at', '>=', $request->billing_month);
            }
            if ($request->billing_year) {
                $data->where('property_bills.created_at', '<=', $request->billing_year);
            }
            if ($request->property_number) {
                $data->where('properties.property_number', 'LIKE', '%' . $request->property_number . '%');
            }
            return DataTables::of($data)->make(true);
        }
        return view('admin.Billing.property_bills', ['cities' => $city]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request, string $id)
    {
        $bill = PropertyBill::where('id', $id);
        // dd($bill->get());
        $assigned_to = $bill->value('assigned_to');
        // $hmonth = date("F", mktime(0, 0, 0, 5, 1)); // Previously code for month
        $hmonth = date("F", mktime(0, 0, 0, $request->billing_month, 1));
        $year = $bill->value('billing_year');
        $challan_no = $bill->value('challan_no');
        $total_amount_after_due_date = $bill->value('total_amount_after_due_date');
        $total_amount_within_due_date = $bill->value('total_amount_within_due_date');
        $arrears = $bill->value('arrears');
        $plan = PaymentPlan::where('id', $bill->value('payment_plan_id'));
        $issue_date = Carbon::parse($plan->value('start_date'))->format('d-M-Y');
        $due_date = Carbon::parse($plan->value('end_date'))->format('d-M-Y');
        $total_amount = $plan->value('total_amount');
        $plan_name = $plan->value('plan_name');

        $plan_details = PaymentPlanDetail::where('payment_plan_details.payment_plan_id', $plan->value('id'));
        $code_details = PaymentCode::whereIn('id', $plan_details->pluck('payment_code_id'))->get();
        $tax_rate = $code_details[0]->value('tax_rate');
        $taxation_body = $code_details[0]->value('taxation_body');
        $data = [];
        foreach ($plan_details->get() as $index => $pay) {
            $amount = 0;
            if ($pay->calculation_unit == 'marla') {
                $amount += ($pay->amount * Property::where('id', $bill->value('property_id'))->value('landAreaMarla'));
            } else if ($pay->calculation_unit == 'kanal') {
                $amount += ($pay->amount * Property::where('id', $bill->value('property_id'))->value('landAreaKanal'));
            } else if ($pay->calculation_unit == 'fixed') {
                $amount += $pay->amount;
            } else {
                $amount += ($pay->amount * Property::where('id', $bill->value('property_id'))->value('landAreaSqfeet'));
            }
            $data[] = [
                'name' => PaymentCode::where('id', $pay->payment_code_id)->value('name'),
                'amount' => round($amount)
            ];
        }
        $property = Property::where('properties.id', $bill->value('property_id'))->select(
            'properties.*',
            'cities.name as city_name',
            'societies.name as society_name',
            'property_blocks.name as block_name',
            'bank_details.*',
        )
            ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
            ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
            ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
            ->leftJoin(
                'bank_details',
                function ($join) {
                    $join->on('bank_details.city_id', '=', 'properties.city_id')
                        ->on('bank_details.society_id', '=', 'properties.society_id');
                }
            )
            ->first();
        $curr_month = (int) $bill->value('billing_month');
        $curr_year = (int) $bill->value('billing_year');
        // $total = $bill->value('arrears') + $bill->value('total_amount_after_due_date') + $bill->value('total_amount_within_due_date');
        // $balance = $bill->value('total_amount_within_due_date');
        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $monthName = Carbon::create()->addMonths($i)->shortMonthName;
            // $arrears_check = PropertyBill::where('billing_year', $curr_year)->where('billing_month', $i + 1);
            $arrears_check = PropertyBill::where('billing_year', $curr_month == 0 ? $curr_year - 1 : $curr_year)->where('billing_month', $i + 1);
            $months[$i] = [
                'month_name' => $monthName,
                'month_number' => $i + 1,
                // 'year' => $curr_month == 0 ? $curr_year - 1 : $curr_year,
                'year' => $curr_year,
                'total' => $arrears_check->first() == null ? 0 : $arrears_check->value('arrears') + $arrears_check->value('total_amount_after_due_date') + $arrears_check->value('total_amount_within_due_date'),
                // 'total' => $arrears_check->first() == null ? 0 : $total,
                'waived_off' => 0,
                'bill_paid' => 0,
                // 'balance' => $arrears_check->first() == null ? 0 : $balance,
                'balance' => $arrears_check->first() == null ? 0 : $arrears_check->value('total_amount_within_due_date'),
            ];
            if ($curr_month > 0) {
                $curr_month--;
                // dd($curr_month);
            }
            // dd($arrears_check->get());
        }
        $var_data = get_defined_vars();
        $pdfContents = PDF::loadHTML(view('admin.pdf.bills', $var_data)->render())
            ->setOption('margin-top', 5)
            ->setOption('margin-right', 5)
            ->setOption('margin-bottom', 15)
            ->setOption('margin-left', 5)
            ->setOption('page-width', 350)
            ->setOption('page-height', 420)
            ->inline();
        return $pdfContents;
        // return view('admin.pdf.bills', $var_data);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == "society") {
            $society = DB::table('societies')->select('id', 'name')->where('city_id', $id)->get();
            return response([
                'data' => $society
            ]);
        }
        if ($request->check == "block") {
            $block = DB::table('property_blocks')->select('id', 'name')->where('society_id', $id)->get();
            return response([
                'data' => $block
            ]);
        }
    }

    public function verify_payment(VerifyPaymentRequest $request, string $id)
    {
        // dd($request->all());
        try {
            DB::transaction(function () use ($request, $id) {
                $waived_off_image = NULL;
                $paid_slip = NULL;
                if ($request->waived_off_reference) {
                    $image = $request->waived_off_reference;
                    $waived_off_image = 'waived_off_' . time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('property_bills_payments'), $waived_off_image);
                }
                if ($request->paid_slip) {
                    $image = $request->paid_slip;
                    $paid_slip = 'paid_slip_' . time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('property_bills_payments'), $paid_slip);
                }
                $currentDate = Carbon::parse($request->payment_date);
                $bill = PropertyBill::select(
                    'property_bills.*',
                    'payment_plans.end_date',
                )
                    ->leftJoin('payment_plans', 'payment_plans.id', '=', 'property_bills.payment_plan_id')
                    ->where('property_bills.id', $id);
                $total_arrears = $bill->value('arrears') == null ? 0 : $bill->value('arrears');
                $total_paid = ($request->paid_amount == null ? 0 : $request->paid_amount) + $bill->value('paid_amount');
                $advance_payment = $bill->value('advance_paid_amount');
                $total_waived_off = ($bill->value('waived_off') == null ? 0 : $bill->value('waived_off')) + ($request->waived_off == null ? 0 : $request->waived_off);
                $current_amount = ($bill->value('total_amount_within_due_date') + ($currentDate->copy()->startOfDay()->isAfter(Carbon::parse($bill->value('end_date'))) == true ? $bill->value('total_amount_after_due_date') : 0));
                $remaining_amount = ($total_arrears - $total_waived_off - ($bill->value('advance_payment_status') == 1 ? $advance_payment : 0)) + $current_amount;
                $remaining_amount -= $total_paid;
                $bill->update([
                    'paid_amount' => $total_paid,
                    'waived_off' => $total_waived_off,
                    'payment_date' => $currentDate,
                    'waived_off_reference' => $waived_off_image,
                    'paid_slip' => $paid_slip,
                    'remarks' => $request->remarks
                ]);
                $challan_no = explode('-', $bill->value('challan_no'));
                if ($remaining_amount <= 0) {
                    PropertyBill::where('challan_no', 'LIKE', $challan_no[0] . '%')
                        ->update([
                            'is_paid' => 1,
                        ]);
                }
                if (($total_arrears - $total_waived_off) - $total_paid <= 0) {
                    PropertyBill::where('challan_no', 'LIKE', $challan_no[0] . '%')
                        ->where('id', '!=', $id)
                        ->update([
                            'is_paid' => 1,
                        ]);
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $bill = PropertyBill::where('id', $id);
            PropertyBillDetail::where('property_bill_id', $bill->value('challan_no'))->delete();
            $bill->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
