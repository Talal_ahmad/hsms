<?php

namespace App\Http\Controllers;

use App\Http\Requests\ComplainRequest;
use App\Models\City;
use App\Models\Complain;
use App\Models\ComplainType;
use App\Models\Property;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class ComplainController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $cities = City::all();
        $properties = Property::select(
            'properties.*',
            'cities.name as city_name',
            'societies.name as society_name',
            'property_blocks.name as block_name'
        )
            ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
            ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
            ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id');
        if (!User::find(Auth::id())->roles()->where('name', 'admin')->exists()) {
            $properties->where('added_by', Auth::id());
        }
        $complain_types = ComplainType::all();
        if ($request->ajax()) {
            $data = Complain::select(
                'complains.*',
                'complain_types.type as complain_type',
                'properties.property_number as house_number',
                'cities.name as city_name',
                'societies.name as society_name',
                'property_blocks.name as block_name'
            )
                ->leftJoin('complain_types', 'complain_types.id', '=', 'complains.comp_type_id')
                ->leftJoin('properties', 'properties.id', '=', 'complains.property_id')
                ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id');
            return DataTables::of($data)->make(true);
        }
        return view('admin.Complains.complains', ['cities' => $cities, 'properties' => $properties->get(), 'complain_types' => $complain_types]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ComplainRequest $request)
    {
        try {
            $requestData = $request->all();
            if ($request->attachment) {
                $image =  $request->attachment;
                $name = time() . rand(1, 100) . '.' . $image->extension();
                $image->move(public_path('members'), $name);
                $requestData['attachment'] = $name;
            }
            Complain::create($requestData);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ComplainRequest $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Complain::where('id', $id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
