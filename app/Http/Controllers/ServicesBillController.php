<?php

namespace App\Http\Controllers;

use App\Http\Requests\VerifyPaymentRequest;
use App\Models\AdvancePayment;
use App\Models\Cart;
use App\Models\City;
use App\Models\Property;
use App\Models\PropertyOwner;
use App\Models\ServicesBill;
use App\Models\ServicesBillDetail;
use App\Models\Tenant;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;
use Barryvdh\Snappy\Facades\SnappyPdf as PDF;

class ServicesBillController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $city = City::all();
        if ($request->ajax()) {
            $where =  "";
            if ($request->city_id) {
                $toInt = (int)$request->city_id;
                $where .= "properties.city_id = $toInt";

                if ($request->society_id) {
                    $toInt = (int)$request->society_id;
                    $where .= " AND properties.society_id = $toInt";

                    if ($request->block_id) {
                        $toInt = (int)$request->block_id;
                        $where .= " AND properties.block_id = $toInt";
                    }
                }
            }
            $data = ServicesBill::select(
                'services_bills.*',
                'cities.name as city_name',
                'societies.name as society_name',
                'property_blocks.name as block_name',
                'properties.property_number as property_number',
            )
                ->leftJoin('properties', 'properties.id', 'services_bills.property_id')
                ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id');
            $user = User::find(Auth::id());
            if (!($user->roles()->where('name', 'admin')->exists())) {
                $data->where('services_bills.added_by', Auth::id());
            }
            // ->where('added_by', Auth::id());
            if (!empty($where)) {
                $data->whereRaw($where);
                return DataTables::of($data)->make(true);
            } else {
                return DataTables::of($data)->make(true);
            }
        }
        return view('admin.Billing.service_bill', ['cities' => $city]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    private function getChallanNumber()
    {
        do {
            $challanNumber = str_pad(mt_rand(1, 9999), 4, '0', STR_PAD_LEFT);
        } while (ServicesBill::where('challan_no', 'LIKE', $challanNumber . '%')->exists());

        return $challanNumber;
    }
    public function store(Request $request)
    {
        // $billing_month = Carbon::now()->subMonth()->format('m');
        // if ($billing_month == '12') {
        //     $billing_year = Carbon::now()->subYear()->format('Y');
        // } else {
        //     $billing_year = Carbon::now()->format('Y');
        // }
        try {
            DB::transaction(function () use ($request) {
                $billing_month = Carbon::now()->format('m');
                $billing_year = Carbon::now()->format('Y');
                // If bill associated to that property doesn't exist.
                $is_property = ServicesBill::where('property_id', $request->prop_id);
                $advance_payment = 0;
                if (Property::where('id', $request->prop_id)->value('maintenance') == 'tenant') {
                    $assigned_to = Tenant::where('id', $request->assigned_to)->value('name');
                    $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $request->prop_id)->value('id'))->latest()->first();
                    $advance_payment = $temp == null ? 0 : $temp['remaining_amount'];
                } else {
                    $assigned_to = PropertyOwner::where('id', $request->assigned_to)->value('name');
                    $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $request->prop_id)->value('id'))->latest()->first();
                    $advance_payment = $temp == null ? 0 : $temp['remaining_amount'];
                }
                if ($is_property->first() == NULL) {
                    $challan_no = $this->getChallanNumber();
                    $total_amount = ($request->subtotal + $request->tax) - $request->discount;
                    ServicesBill::create([
                        'property_id' => $request->prop_id,
                        'assigned_to' => $assigned_to,
                        'sub_total' => $request->subtotal,
                        'gst' => $request->tax,
                        'discount' => $request->discount,
                        'payment_method' => $request->payment_type,
                        'billing_month' => $billing_month,
                        'billing_year' => $billing_year,
                        'total_amount' => $total_amount,
                        'challan_no' => $challan_no . '-' . $billing_month . '-' . $billing_year,
                        'issue_date' => Carbon::now(),
                        'added_by' => Auth::id(),
                        'is_paid' => $total_amount - $advance_payment <= 0 ? 1 : 0,
                        'advance_paid_amount' => $total_amount - $advance_payment < 0 ? $total_amount : $advance_payment,
                        'payment_date' => $total_amount - $advance_payment <= 0 ? Carbon::now() : NULL,
                        'advance_payment_status' => $temp == null ? 0 : ($temp['remaining_amount'] == 0 ? 0 : 1),
                    ]);
                    if (Property::where('id', $request->prop_id)->value('maintenance') == 'tenant') {
                        $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $request->assigned_to)->value('id'))->latest()->first();
                        if ($temp != null) {
                            AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => $total_amount - $advance_payment < 0 ? ($total_amount - $advance_payment) * -1  : 0]);
                        }
                    } else {
                        $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $request->assigned_to)->value('id'))->latest()->first();
                        if ($temp != null) {
                            AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => $total_amount - $advance_payment < 0 ? ($total_amount - $advance_payment) * -1  : 0]);
                        }
                    }
                    foreach ($request->cart as $cart) {
                        ServicesBillDetail::create([
                            'services_bills_id' => ServicesBill::latest()->value('id'),
                            'category_id' => $cart['category_id'],
                            'service_id' => $cart['service_id'],
                            'quantity' => $cart['quantity']
                        ]);
                    }
                    Cart::where('user_id', Auth::id())->delete();
                } else {
                    $last_bill_month = $is_property->value('billing_month');
                    if (Carbon::now()->format('m') > $last_bill_month) { // new month bill
                        // paid or unpaid
                        $challan = explode('-', $is_property->value('challan_no'));
                        $arrears = 0; // if paid
                        $waived_off = 0;
                        if ($is_property->value('is_paid') == 0) {
                            $arrears = ((($is_property->value('arrears') == null ? 0 : $is_property->value('arrears')) - $waived_off) + $is_property->value('total_amount')) - $is_property->value('paid_amount');
                            $waived_off = $is_property->value('waived_off') == null ? 0 : $is_property->value('waived_off');
                            $paid_amount = $is_property->value('paid_amount');
                        }
                        $total_amount = ($arrears - $waived_off) + (($request->subtotal + $request->tax) - $request->discount) - $paid_amount;
                        ServicesBill::create([
                            'property_id' => $request->prop_id,
                            'assigned_to' => $assigned_to,
                            'sub_total' => $request->subtotal,
                            'gst' => $request->tax,
                            'discount' => $request->discount,
                            'payment_method' => $request->payment_type,
                            'billing_month' => $billing_month,
                            'billing_year' => $billing_year,
                            'issue_date' => Carbon::now(),
                            'arrears' => round($arrears > -1 ? $arrears : 0),
                            'total_amount' => round(($request->subtotal + $request->tax) - $request->discount),
                            'challan_no' => $challan[0] . '-' . $billing_month . '-' . $billing_year,
                            'added_by' => Auth::id(),
                            'waived_off' => $waived_off,
                            'is_paid' => $total_amount - $advance_payment <= 0 ? 1 : 0,
                            'advance_paid_amount' => round($total_amount - $advance_payment < 0 ? $total_amount : $advance_payment),
                            'payment_date' => $total_amount - $advance_payment <= 0 ? Carbon::now() : NULL,
                            'advance_payment_status' => $temp == null ? 0 : ($temp['remaining_amount'] == 0 ? 0 : 1),
                        ]);
                        if (Property::where('id', $request->prop_id)->value('maintenance') == 'tenant') {
                            $temp = AdvancePayment::where('payment_type', "tenant")->where('paid_by', Tenant::where('tenants.property_id', $request->assigned_to)->value('id'))->latest()->first();
                            if ($temp != null) {
                                AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => round($total_amount) - round($advance_payment) < 0 ? (round($total_amount) - round($advance_payment)) * -1  : 0]);
                            }
                        } else {
                            $temp = AdvancePayment::where('payment_type', "owner")->where('paid_by', PropertyOwner::where('property_owners.property_id', $request->assigned_to)->value('id'))->latest()->first();
                            if ($temp != null) {
                                AdvancePayment::where('id', $temp['id'])->update(['remaining_amount' => round($total_amount) - round($advance_payment) < 0 ? (round($total_amount) - round($advance_payment)) * -1  : 0]);
                            }
                        }
                        foreach ($request->cart as $cart) {
                            ServicesBillDetail::create([
                                'services_bills_id' => ServicesBill::latest()->value('id'),
                                'category_id' => $cart['category_id'],
                                'service_id' => $cart['service_id'],
                                'quantity' => $cart['quantity']
                            ]);
                        }
                        Cart::where('user_id', Auth::id())->delete();
                    } else { // update old bill
                        ServicesBill::where('id', $is_property->value('id'))
                            ->update([
                                'sub_total' => $is_property->value('sub_total') + $request->subtotal,
                                'gst' => $is_property->value('gst') + $request->tax,
                                'discount' => $is_property->value('discount') + $request->discount,
                                'total_amount' => round(($request->subtotal + $request->tax) - $request->discount) + ServicesBill::where('id', $is_property->value('id'))->value('total_amount'),
                                'payment_method' => $request->payment_type,
                                'issue_date' => Carbon::now(),
                                'added_by' => Auth::id(),
                            ]);
                        foreach ($request->cart as $cart) {
                            $service_bill = ServicesBillDetail::where('services_bills_id', $is_property->value('id'))->where('category_id', $cart['category_id'])->where('service_id', $cart['service_id']);
                            $service_bill->update(['quantity' => $service_bill->value('quantity') + $cart['quantity']]);
                        }
                        Cart::where('user_id', Auth::id())->delete();
                    }
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception $e) {
            DB::rollBack();
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $bill = ServicesBill::where('id', $id);
        $assigned_to = $bill->value('assigned_to');
        $hmonth = date("F", mktime(0, 0, 0, 5, 1));
        $year = $bill->value('billing_year');
        $challan_no = $bill->value('challan_no');
        $arrears = $bill->value('arrears');
        $total_bill = $bill->value('total_amount');
        $issue_date = $bill->value('issue_date');

        $services_bill_details = ServicesBillDetail::select(
            'services_bills_details.*',
            'services.name as service_name',
            'services.price as service_price',
            'services.gst as service_gst',
            'services.discount as service_discount'
        )
            ->leftJoin('services', 'services.id', '=', 'service_id')
            ->where('services_bills_id', $id)
            ->get();
        $data = [];
        foreach ($services_bill_details as $services) {
            $data[] = [
                'name' => $services['service_name'],
                'quantity' => $services['quantity'],
                'gst' => $services['service_gst'],
                'discount' => $services['service_discount'],
                'price' => $services['service_price'],
                'total_amount' => (($services['service_price'] + ($services['service_price'] * ($services['service_gst'] / 100))) - ($services['service_price'] * ($services['service_discount'] / 100))) * $services['quantity'],
            ];
        }

        $property = Property::where('properties.id', $bill->value('property_id'))->select(
            'properties.*',
            'cities.name as city_name',
            'societies.name as society_name',
            'property_blocks.name as block_name',
        )
            ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
            ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
            ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
            ->first();
        $curr_month = (int)$bill->value('billing_month');
        $curr_year = (int)$bill->value('billing_year');
        $months = [];
        for ($i = 0; $i < 12; $i++) {
            $monthName = Carbon::create()->addMonths($i)->shortMonthName;
            $arrears_check = ServicesBill::where('billing_year', $curr_month == 0 ? $curr_year - 1 : $curr_year)->where('billing_month', $i + 1);
            $months[$i] = [
                'month_name' => $monthName,
                'month_number' => $i + 1,
                'year' => $curr_month == 0 ? $curr_year - 1 : $curr_year,
                'total' => $arrears_check->first() == null ? 0 : $arrears_check->value('arrears') + $arrears_check->value('total_amount'),
                'waived_off' => 0,
                'bill_paid' => 0,
                'balance' => $arrears_check->first() == null ? 0 : $arrears_check->value('total_amount'),
            ];
            if ($curr_month > 0) {
                $curr_month--;
            }
        }
        $var_data = get_defined_vars();
        $pdfContents = PDF::loadHTML(view('admin.pdf.service_bill', $var_data)->render())
            ->setOption('margin-top', 5)
            ->setOption('margin-right', 5)
            ->setOption('margin-bottom', 15)
            ->setOption('margin-left', 5)
            ->setOption('page-width', 350)
            ->setOption('page-height', 420)
            ->inline();
        return $pdfContents;
        // $pdfContents = Browsershot::html(view('admin.pdf.service_bill', $var_data)->render())
        //     ->margins(5, 5, 5, 15)  // Set margins
        //     ->paperSize(350, 420)
        //     ->pdf();
        // return Response::make($pdfContents, 200, [
        //     'Content-Type' => 'application/pdf',
        //     'Content-Disposition' => 'attachment; filename="' . "Service-Bill-" . $property->city_name . '-' . $property->society_name . '-' . $property->block_name . '-' . $property->property_number . '.pdf"',
        // ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == "society") {
            $society = DB::table('societies')->select('id', 'name')->where('city_id', $id)->get();
            return response([
                'data' => $society
            ]);
        }
        if ($request->check == "block") {
            $block = DB::table('property_blocks')->select('id', 'name')->where('society_id', $id)->get();
            return response([
                'data' => $block
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $waived_off_image = NULL;
                $paid_slip = NULL;
                if ($request->waived_off_reference) {
                    $image = $request->waived_off_reference;
                    $waived_off_image = "waived_off_" . time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('service_bills_payments'), $waived_off_image);
                }
                if ($request->paid_slip) {
                    $image = $request->paid_slip;
                    $paid_slip = "paid_slip_" . time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('service_bills_payments'), $paid_slip);
                }
                $bill = ServicesBill::where('id', $id);
                $challan_id = explode('-', $bill->value('challan_no'));
                $total_arrears = $bill->value('arrears') == null ? 0 : $bill->value('arrears');
                $total_paid = ($request->paid_amount == null ? 0 : $request->paid_amount) + $bill->value('paid_amount');
                $total_waived_off = ($bill->value('waived_off') == null ? 0 : $bill->value('waived_off')) + ($request->waived_off == null ? 0 : $request->waived_off);
                $current_amount = $bill->value('total_amount');
                $remaining_amount = ($total_arrears - $total_waived_off - ($bill->value('advance_payment_status') == 1 ? $bill->value('advance_paid_amount') : 0)) + $current_amount;
                $remaining_amount -= $total_paid;
                $bill->update([
                    'paid_amount' => $total_paid,
                    'waived_off' => $total_waived_off,
                    'payment_date' => $request->payment_date,
                    'waived_off_reference' => $waived_off_image,
                    'paid_slip' => $paid_slip
                ]);
                if ($remaining_amount <= 0) {
                    ServicesBill::where('challan_no', 'LIKE', $challan_id[0] . '%')
                        ->update([
                            'is_paid' => 1,
                        ]);
                }
                if (($total_arrears - $total_waived_off) - $total_paid <= 0) {
                    ServicesBill::where('challan_no', 'LIKE', $challan_id[0] . '%')
                        ->where('id', '!=', $id)
                        ->update([
                            'is_paid' => 1,
                        ]);
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception $e) {
            DB::rollBack();
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }

    public function verify_payment(VerifyPaymentRequest $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $waived_off_image = NULL;
                $paid_slip = NULL;
                if ($request->waived_off_reference) {
                    $image = $request->waived_off_reference;
                    $waived_off_image = "waived_off_" . time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('service_bills_payments'), $waived_off_image);
                }
                if ($request->paid_slip) {
                    $image = $request->paid_slip;
                    $paid_slip = "paid_slip_" . time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('service_bills_payments'), $paid_slip);
                }
                $bill = ServicesBill::where('id', $id);
                $challan_id = explode('-', $bill->value('challan_no'));
                $total_arrears = $bill->value('arrears') == null ? 0 : $bill->value('arrears');
                $total_paid = ($request->paid_amount == null ? 0 : $request->paid_amount) + $bill->value('paid_amount');
                $total_waived_off = ($bill->value('waived_off') == null ? 0 : $bill->value('waived_off')) + ($request->waived_off == null ? 0 : $request->waived_off);
                $current_amount = $bill->value('total_amount');
                $remaining_amount = ($total_arrears - $total_waived_off - ($bill->value('advance_payment_status') == 1 ? $bill->value('advance_paid_amount') : 0)) + $current_amount;
                $remaining_amount -= $total_paid;
                $bill->update([
                    'paid_amount' => $total_paid,
                    'waived_off' => $total_waived_off,
                    'payment_date' => $request->payment_date,
                    'waived_off_reference' => $waived_off_image,
                    'paid_slip' => $paid_slip,
                    'remarks' => $request->remarks
                ]);
                if ($remaining_amount <= 0) {
                    ServicesBill::where('challan_no', 'LIKE', $challan_id[0] . '%')
                        ->update([
                            'is_paid' => 1,
                        ]);
                }
                if (($total_arrears - $total_waived_off) - $total_paid <= 0) {
                    ServicesBill::where('challan_no', 'LIKE', $challan_id[0] . '%')
                        ->where('id', '!=', $id)
                        ->update([
                            'is_paid' => 1,
                        ]);
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            DB::transaction(function () use ($id) {
                ServicesBillDetail::where('services_bills_id', $id)->delete();
                ServicesBill::where('id', $id)->delete();
                return ["code" => "200", 'message' => 'success'];
            });
        } catch (\Exception $e) {
            DB::rollBack();
            return ["code" => '500', 'error_message' => $e->getMessage()];
        }
    }
}
