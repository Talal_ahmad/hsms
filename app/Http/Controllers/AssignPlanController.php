<?php

namespace App\Http\Controllers;

use App\Http\Requests\AssignPlanRequest;
use App\Models\City;
use App\Models\PaymentPlan;
use App\Models\Property;
use App\Models\PropertyType;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class AssignPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = Property::select(
                'properties.id as id',
                'cities.name as city_name',
                'societies.name as society_name',
                'property_blocks.name as block_name',
                'property_types.type as type_name',
                'properties.assigned_date as assigned_date',
                'users.name as user_name',
                'payment_plans.plan_name as plan_name',
            )
                ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                ->leftJoin('property_types', 'property_types.id', '=', 'properties.type_id')
                ->leftJoin('payment_plans', 'payment_plans.id', '=', 'properties.payment_plan_id')
                ->leftJoin('users', 'users.id', '=', 'payment_plans.assigned_by')
                ->where('properties.payment_plan_id', '!=', 0);
            return DataTables::of($data)->addIndexColumn()->make(true);
        }
        return view(
            'admin.Collections.assign_plan',
            [
                'cities' => City::all(),
                'property_types' => PropertyType::all(),
                'payment_plans' => PaymentPlan::where('status', 1)->get(),
                'prop' => Property::select(
                    'properties.id as property_id',
                    'properties.property_number as property_number',
                    'cities.name as city_name',
                    'societies.name as society_name',
                )
                    ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                    ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                    ->get(),
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignPlanRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $property_ids = array_map(function ($id) {
                    return (int)trim($id, "'");
                }, $request->input('property_id'));
                $properties = Property::whereIn('id', $property_ids)->get();

                foreach ($properties as $property) {
                    $property->update(['payment_plan_id' => $request->payment_plan_id, 'assigned_date' => Carbon::now()]);
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == 'view') {
            $property = Property::select(
                'properties.id as property_id',
                'cities.name as city_name',
                'societies.name as society_name',
                'properties.payment_plan_id as plan_id',
                'properties.property_number as flat_number',
            )
                ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                ->where('properties.id', $id);
            $plans = explode(',', $property->value('plan_id'));
            return response([
                'data' => $property->first(),
                'payment_plans' => json_encode($plans),
            ]);
        }
        if ($request->check == "property") {
            $billing = Property::select(
                'properties.id as id',
                'properties.property_number as prop_num',
                'cities.name as city_name',
                'societies.name as society_name',
                'property_blocks.name as block_name'
            )
                ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                ->where('properties.city_id', $request->input('city_id'))
                ->where('properties.society_id', $request->input('society_id'))
                ->where('properties.block_id', $request->input('block_id'))
                ->where('properties.type_id', $id)
                ->get();
            return response([
                'data' => $billing,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            Property::where('id', $id)
                ->update([
                    'payment_plan_id' => $request->plan_id ? implode(',', $request->plan_id) : null,
                    'assigned_date' => Carbon::now()
                ]);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ["code" => "422", "errors" => $e->errors()];
            } else {
                return ["code" => "500", 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Property::where('id', $id)->update(['payment_plan_id' => NULL]);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
