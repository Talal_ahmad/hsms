<?php

namespace App\Http\Controllers\Properties;

use App\Http\Controllers\Controller;
use App\Http\Requests\PropertyRequest;
use App\Models\AreaUnit;
use App\Models\City;
use App\Models\DocumentType;
use App\Models\Property;
use App\Models\PropertyBlock;
use App\Models\PropertyDealer;
use App\Models\PropertyDocument;
use App\Models\PropertyOwner;
use App\Models\PropertyType;
use App\Models\Society;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class PropertyController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->getPathInfo() == '/properties/list') {
            if ($request->ajax()) {
                $user = User::find(Auth::id());
                $data = Property::select(
                    'properties.id',
                    'properties.landAreaKanal',
                    'properties.landAreaMarla',
                    'properties.landAreaSqfeet',
                    'properties.covered_area',
                    'properties.typesProperty',
                    'properties.property_number',
                    'cities.name as city_name',
                    'societies.name as society_name',
                    'property_blocks.name as block_name',
                    'property_types.type as type_name',
                    DB::raw('GROUP_CONCAT(property_owners.name) as owner_names')
                )
                    ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                    ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                    ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                    ->leftJoin('property_types', 'property_types.id', '=', 'properties.type_id')
                    ->leftJoin('property_owners', 'property_owners.property_id', '=', 'properties.id');
                if ($request->city_id) {
                    $data->where('properties.city_id', (int)$request->city_id);
                    if ($request->society_id) {
                        $data->where('properties.society_id', (int)$request->society_id);
                        if ($request->block_id) {
                            $data->where('properties.block_id', (int)$request->block_id);
                        }
                    }
                }
                if ($request->type_id) {
                    $data->where('properties.type_id', (int)$request->type_id);
                }
                if ($request->typesProperty) {
                    $data->where('properties.typesProperty', ucfirst($request->typesProperty));
                }
                if ($request->property_number) {
                    $data->where('properties.property_number', ucfirst($request->property_number));
                }
                if ($request->owner_name) {
                    $entry = PropertyOwner::where('name', $request->owner_name)->pluck('property_id')->toArray();
                    $String = implode(',', $entry);
                    $data->whereIn('properties.id', [$String]);
                }
                if (!$user->roles()->where('name', 'admin')->exists()) {
                    $data->where('added_by', Auth::id()); // Only show the user's properties
                }
                $data->groupBy(
                    'properties.id',
                    'city_name',
                    'society_name',
                    'block_name',
                    'type_name',
                    'properties.landAreaKanal',
                    'properties.landAreaMarla',
                    'properties.landAreaSqfeet',
                    'properties.covered_area',
                    'properties.typesProperty',
                    'properties.property_number'
                );
                return DataTables::eloquent($data)->make(true);
            }
            return view(
                'admin.Properties.list',
                [
                    'property_types' => PropertyType::all(),
                    'cities' => City::all(),
                    'societies' => Society::all(),
                    'blocks' => PropertyBlock::all()
                ]
            );
        }
        if ($request->getPathInfo() == '/properties/add') {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://194.163.145.90/eperp/api/getGlAccounts?data=gl_codes',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $gl_entries = json_decode($response);
            return view('admin.Properties.add', [
                'gl_code' => $gl_entries->data,
                'property_types' => PropertyType::all(),
                'cities' => City::all(),
                'societies' => Society::all(),
                'blocks' => PropertyBlock::all(),
                'units' => AreaUnit::all()
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PropertyRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $requestData = $request->except(['maintenance', 'common_area_bill']);
                if ($request->litigation_image) {
                    $image = $request->litigation_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('image'), $name);
                    $requestData['litigation_image'] = $name;
                }
                if ($request->mortage_image) {
                    $image =  $request->mortage_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('image'), $name);
                    $requestData['mortage_image'] = $name;
                }
                if ($request->nab_image) {
                    $image =  $request->nab_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('image'), $name);
                    $requestData['nab_image'] = $name;
                }
                Property::create(
                    $requestData +
                        [
                            'added_by' => Auth::id(),
                            'maintenance' => $request->maintenance == 'on' ? "tenant" : "owner",
                            'common_area' => $request->common_area_bill == 'on' ? "tenant" : 'owner'
                        ]
                );
                $property_id = DB::table('properties')->latest('id')->pluck('id')->first();
                if ($request->dealer_name && $request->tenant_name) {
                    PropertyDealer::create([
                        'name' => $request->dealer_name,
                        'address' => $request->dealer_address,
                        'phone' => $request->dealer_phone
                    ]);
                    if ($request->tenant_image) {
                        $image = $request->tenant_image;
                        $name = time() . rand(1, 100) . '.' . $image->extension();
                        $image->move(public_path('image'), $name);
                    }
                    Tenant::create([
                        'property_id' => $property_id,
                        'dealer_id' => PropertyDealer::latest('id')->pluck('id')->first(),
                        'name' => $request->tenant_name,
                        'father_name' => $request->tenant_father_name,
                        'cnic' => $request->tenant_cnic,
                        'email' => $request->tenant_email,
                        'cnic_image' => $name ? $name : null,
                        'address' => $request->tenant_address,
                        'phone' => $request->tenant_phone,
                        'rent' => $request->tenant_rent,
                        'increment' => $request->tenant_increment,
                        'from_date' => $request->tenant_from_date,
                        'to_date' => $request->tenant_to_date,
                    ]);
                }
                if (!empty($request->property_owner)) {
                    foreach ($request->property_owner as $owner) {
                        $owner += ["property_id" => $property_id];
                        PropertyOwner::create($owner);
                    }
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $data = Property::select(
            'properties.*',
            'cities.id',
            'cities.name as city_name',
            'societies.name as society_name',
            'property_blocks.name as block_name',
            'property_types.type as type_name',
            'property_owners.*'
        )
            ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
            ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
            ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
            ->leftJoin('property_types', 'property_types.id', '=', 'properties.type_id')
            ->leftJoin('property_owners', 'property_owners.property_id', '=', 'properties.id')
            ->where('properties.id', $id)
            ->get();
        $tenant = Tenant::where('property_id', $id);
        $doc_details = PropertyDocument::select('document_types.type as name', 'property_documents.document_image as image')
            ->leftJoin('document_types', 'document_types.id', '=', 'property_documents.document_type_id')
            ->where('property_documents.property_id', $id)
            ->get();
        if ($tenant) {
            $tenant = $tenant->select(
                'tenants.name as tenant_name',
                'tenants.address as tenant_address',
                'tenants.cnic as tenant_cnic',
                'tenants.email as tenant_email',
                'tenants.phone as tenant_phone',
                'tenants.rent as tenant_rent',
                'tenants.increment as tenant_increment',
                'tenants.from_date as from_date',
                'tenants.to_date as to_date',
                'property_dealers.name as dealer_name',
                'property_dealers.address as dealer_address',
                'property_dealers.phone as dealer_phone',
            )
                ->leftJoin('property_dealers', 'property_dealers.id', '=', 'tenants.dealer_id')
                ->first();
        }
        return view(
            'admin.Properties.view',
            [
                'data' => $data,
                'tenant' => $tenant,
                'document_types' => DocumentType::all(),
                'property_documents' => $doc_details,
                'property' => Property::where('id', $id)->first()
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == "society") {
            $society = DB::table('societies')->select('id', 'name')->where('city_id', $id)->get();
            return response([
                'data' => $society
            ]);
        }
        if ($request->check == "block") {
            $block = DB::table('property_blocks')->select('id', 'name')->where('society_id', $id)->get();
            return response([
                'data' => $block
            ]);
        }
    }

    public function view(string $id)
    {
        $property = Property::where('id', $id)->first();
        $owners = PropertyOwner::where('property_id', $property->id)->get();
        $tenant = Tenant::where('property_id', $id)->first();
        $dealer = '';
        if ($tenant) {
            $dealer = PropertyDealer::where('id', $tenant->dealer_id)->first();
        }
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://194.163.145.90/eperp/api/getGlAccounts?data=gl_codes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $gl_entries = json_decode($response);
        return view(
            'admin.Properties.edit',
            [
                'gl_code' => $gl_entries->data,
                'property_types' => PropertyType::all(),
                'cities' => City::all(),
                'societies' => Society::all(),
                'blocks' => PropertyBlock::all(),
                'units' => AreaUnit::all(),
                'property' => $property,
                'owners' => $owners,
                'tenant' => $tenant,
                'dealer' => $dealer
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $property = Property::where('id', $id)->first();
                $requestData = $request->except([
                    '_token',
                    '_method',
                    'property_owner',
                    'secondary_phone_number',
                    'phone_number',
                    'tenant_name',
                    'tenant_father_name',
                    'tenant_cnic',
                    'tenant_email',
                    'tenant_image',
                    'tenant_address',
                    'tenant_phone',
                    'tenant_rent',
                    'tenant_increment',
                    'tenant_from_date',
                    'tenant_to_date',
                    'dealer_name',
                    'dealer_address',
                    'dealer_phone',
                    'tenant_remarks',
                    'maintenance',
                    'common_area_bill',
                ]);
                if ($request->litigation_image) {
                    if ($property->litigation_image && file_exists(public_path('image/' . $property->image_path))) {
                        unlink(public_path('image/' . $property->image_path));
                    }
                    $image = $request->litigation_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('image'), $name);
                    $requestData['litigation_image'] = $name;
                }
                if ($request->mortage_image) {
                    if ($property->mortage_image && file_exists(public_path('image/' . $property->mortage_image))) {
                        unlink(public_path('image/' . $property->mortage_image));
                    }
                    $image =  $request->mortage_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('image'), $name);
                    $requestData['mortage_image'] = $name;
                }
                if ($request->nab_image) {
                    if ($property->nab_image && file_exists(public_path('image/' . $property->nab_image))) {
                        unlink(public_path('image/' . $property->nab_image));
                    }
                    $image =  $request->nab_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('image'), $name);
                    $requestData['nab_image'] = $name;
                }
                Property::where('id', $id)->update(
                    $requestData +
                        [
                            'added_by' => Auth::id(),
                            'maintenance' => $request->maintenance == 'on' ? "tenant" : "owner",
                            'common_area' => $request->common_area_bill == 'on' ? "tenant" : 'owner'
                        ]
                );
                if (!empty($request->property_owner)) {
                    if ($request->property_owner[0]['membership_number']) {
                        PropertyOwner::where('property_id', $id)->delete();
                    }
                    foreach ($request->property_owner as $owner) {
                        $owner += ["property_id" => $id];
                        PropertyOwner::create($owner);
                    }
                }
                if ($request->tenant_name) {
                    if ($request->dealer_name != null) {
                        PropertyDealer::where(
                            'property_dealers.id',
                            Tenant::where('tenants.property_id', $id)->pluck('dealer_id')->first()
                        )->delete();
                        PropertyDealer::create([
                            'name' => $request->dealer_name,
                            'address' => $request->dealer_address,
                            'phone' => $request->dealer_phone
                        ]);
                    }
                    Tenant::where('property_id', $id)->delete();
                    if ($request->tenant_image) {
                        $image = $request->tenant_image;
                        $name = time() . rand(1, 100) . '.' . $image->extension();
                        $image->move(public_path('image'), $name);
                    }
                    Tenant::create([
                        'property_id' => $id,
                        'dealer_id' => PropertyDealer::latest('id')->pluck('id')->first(),
                        'name' => $request->tenant_name,
                        'father_name' => $request->tenant_father_name,
                        'cnic' => $request->tenant_cnic,
                        'email' => $request->tenant_email,
                        'cnic_image' => isset($name) ? $name : null,
                        'address' => $request->tenant_address,
                        'phone' => $request->tenant_phone,
                        'rent' => $request->tenant_rent,
                        'increment' => $request->tenant_increment,
                        'from_date' => $request->tenant_from_date,
                        'to_date' => $request->tenant_to_date,
                        'maintenance' => $request->maintenance == 'on' ? 1 : 0,
                        'common_area_bill' => $request->common_area_bill == 'on' ? 1 : 0,
                    ]);
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    function addDocument(Request $request, string $id)
    {
        try {
            $image =  $request->document_image;
            $name = "property_document_" . time() . rand(1, 100) . '.' . $image->extension();
            $image->move(public_path('image'), $name);

            PropertyDocument::create([
                'property_id' => $id,
                'document_type_id' => $request->document_type_id,
                'document_image' => $name
            ]);
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    function downloadImage($fileName)
    {
        $path = public_path('image/' . $fileName);
        $extension = pathinfo($fileName, PATHINFO_EXTENSION);
        $doc_name = PropertyDocument::select('document_types.type as document_name')
            ->leftJoin('document_types', 'property_documents.document_type_id', '=', 'document_types.id')
            ->where('document_image', $fileName)
            ->pluck('document_name')
            ->first();
        $newFileName = 'Property_Document_' . $doc_name . "." . $extension;
        $headers = [
            'Content-Type' => 'image/jpeg',
            'Content-Disposition' => 'attachment; filename="' . $newFileName,
        ];

        return response()->download($path, $newFileName, $headers);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
    }
}
