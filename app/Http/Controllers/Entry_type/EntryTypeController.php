<?php

namespace App\Http\Controllers\Entry_type;

use App\Http\Controllers\Controller;
use App\Http\Requests\EntryTypeRequest;
use App\Models\EntryType;
use App\Models\EntryTypeDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class EntryTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->getPathInfo() == '/entry_type/entry-list') {
            if ($request->ajax()) {
                return DataTables::of(EntryType::query())->make(true);
            }
            return view('admin.Entry_type.entry_list');
        }
        if ($request->getPathInfo() == '/entry_type/entry-add') {
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://194.163.145.90/eperp/api/getGlAccounts?data=gl_codes',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $gl_entries = json_decode($response);
            return view('admin.Entry_type.entry_add', ['gl_code' => $gl_entries->data]);
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(EntryTypeRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $entry = new EntryType();
                $entry->type = $request->type;
                $entry->description = $request->description;
                $entry->save();
                $id = DB::table('entry_types')->latest('id')->pluck('id');
                foreach ($request->erp_loop as $erp) {
                    $data = explode('|', $erp['gl_code']);
                    $code = (int)$data[0];
                    $string = $data[1];
                    $detail = new EntryTypeDetail();
                    $detail->entry_type_id = (int)$id->first();
                    $detail->gl_code = $code;
                    $detail->gl_name = $string;
                    $detail->method = $erp['method'];
                    $detail->description = $erp['erp-description'];
                    $detail->save();
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $erp = EntryType::findOrFail($id);
        $erp_details = DB::table('entry_type_details')->where('entry_type_id', $id)->get();
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://194.163.145.90/jchserp/api/getGlAccounts?data=gl_codes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $gl_entries = json_decode($response);
        return view('admin.Entry_type.entry_edit', ['erp' => $erp, 'details_loop' => $erp_details, 'gl_code' => $gl_entries->data]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $item = DB::table('entry_type_details')->where('entry_type_id', $id)->get();
        return response([
            'data' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(EntryTypeRequest $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $entry = EntryType::findOrFail($id);
                $entry->type = $request->type;
                $entry->description = $request->description;
                $entry->save();
                DB::table('entry_type_details')->where('entry_type_id', $id)->delete();
                foreach ($request->erp_loop as $erp) {
                    $data = explode('|', $erp['gl_code']);
                    $code = (int)$data[0];
                    $string = $data[1];
                    $detail = new EntryTypeDetail();
                    $detail->entry_type_id = $id;
                    $detail->gl_code = $code;
                    $detail->gl_name = $string;
                    $detail->method = $erp['method'];
                    $detail->description = $erp['erp-description'];
                    $detail->save();
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_messgae' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
