<?php

namespace App\Http\Controllers\Collections;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentPlanRequest;
use App\Models\PaymentCode;
use App\Models\PaymentPlan;
use App\Models\PaymentPlanDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\DataTables;

class PaymentPlanController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $collection = PaymentCode::where('status', 1);
        if ($request->ajax()) {
            $data = PaymentPlan::select('payment_plans.*', 'users.name as user_name')
                ->join('users', 'users.id', '=', 'payment_plans.assigned_by');
            return DataTables::of($data)->addIndexColumn()->make(true);
        }
        return view('admin.Collections.plan', ['collections' => $collection->get()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PaymentPlanRequest $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $requestData = $request->all();
                if ($request->image_path) {
                    $image = $request->image_path;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('payment_code'), $name);
                    $requestData['image_path'] = $name;
                }
                $value = 0;
                if ($request->member_ship_loop) {
                    foreach ($request->member_ship_loop as $total) {
                        $value += $total['amount'];
                    }
                }
                $requestData['total_amount'] = $value;
                $requestData['assigned_by'] = Auth::user()->id;
                $requestData['assigned_date'] = date('Y-m-d');
                PaymentPlan::create($requestData);
                $plan_id = DB::table('payment_plans')->latest('id')->pluck('id')->first();
                if ($request->member_ship_loop) {
                    $requestData = $request->member_ship_loop;
                    foreach ($requestData as $data) {
                        PaymentPlanDetail::create([
                            "payment_plan_id" => $plan_id,
                            "payment_code_id" => $data['payment_code_id'],
                            "amount" => $data['amount'],
                            "calculation_unit" => $data['calculation_unit']
                        ]);
                    }
                }
                // return response()->json(['code' => '200', 'message' => 'success', 'collections' => PaymentCode::all()]);
            });
            return response([
                'collections' => PaymentCode::all(),
                'code' => '200',
                'message' => 'success', 'collections'
            ]);
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ["code" => "422", 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ["code" => "500", 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Request $request, string $id)
    {
        if ($request->check == 'edit') {
            $item = DB::table('payment_plans')->where('id', $id)->get();
            $details = PaymentPlanDetail::where('payment_plan_id', $id)
                ->select('payment_plan_details.*', 'payment_codes.*')
                ->leftJoin('payment_codes', 'payment_codes.id', '=', 'payment_plan_details.payment_code_id')
                ->get();
            return response([
                'data' => $item,
                'details' => $details,
                'collections' => PaymentCode::all()
            ]);
        } else if ($request->check == 'view') {
            $plan = DB::table('payment_plans')->select('plan_name', 'total_amount')->where('id', $id)->get();
            $plan_details = PaymentPlanDetail::select('payment_plan_details.*', 'payment_codes.name as head_name')
                ->join('payment_codes', 'payment_plan_details.payment_code_id', '=', 'payment_codes.id')
                ->where('payment_plan_id', $id);
            return response([
                'data' => $plan,
                'details' => $plan_details->get(),
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PaymentPlanRequest $request, string $id)
    {
        // dd($request->all());
        try {
            DB::transaction(function () use ($request, $id) {
                $requestData = $request->all();
                $prev = PaymentPlan::findOrFail($id);
                if ($request->image_path) {
                    if (file_exists(public_path('payment_code/' . $prev->image_path))) {
                        unlink(public_path('payment_code/' . $prev->image_path));
                    }
                    $image = $request->image_path;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('payment_code'), $name);
                    $requestData['image_path'] = $name;
                }
                if ($request->member_ship_loop) {
                    $value = 0;
                    foreach ($request->member_ship_loop as $total) {
                        $value += $total['amount'];
                    }
                    $requestData['total_amount'] = $value;
                }
                $requestData['assigned_by'] = Auth::user()->id;
                $prev->update($requestData);
                if ($request->member_ship_loop) {
                    DB::table('payment_plan_details')->where('payment_plan_id', $id)->delete();
                    $requestData = $request->member_ship_loop;
                    foreach ($requestData as $data) {
                        PaymentPlanDetail::create([
                            "payment_plan_id" => $id,
                            "payment_code_id" => $data['payment_code_id'],
                            "amount" => $data['amount'],
                            "calculation_unit" => $data['calculation_unit']
                        ]);
                    }
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function status(Request $request, string $id)
    {
        try {
            $obj = PaymentPlan::findOrFail($id);
            if ($request->check == 1) {
                $obj->status = 0;
            } else {
                $obj->status = 1;
            }
            $obj->update();
            return response()->json(['message' => 'Successfully Updated'], 200);
        } catch (\Exception $e) {

            return $e->getMessage();
        }
    }
}
