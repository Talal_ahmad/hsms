<?php

namespace App\Http\Controllers\Collections;

use App\Http\Controllers\Controller;
use App\Http\Requests\PaymentCodeRequest;
use App\Models\PaymentCode;
use App\Models\PaymentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class PaymentCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PaymentCode::select('payment_codes.*', 'payment_types.type as type_id')
                ->join('payment_types', 'payment_codes.payment_type_id', '=', 'payment_types.id');
            return DataTables::of($data)->addIndexColumn()->make(true);
        }
        $collection = PaymentType::where('status', 1);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://194.163.145.90/eperp/api/getGlAccounts?data=gl_codes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $gl_entries = json_decode($response);
        return view('admin.Collections.head', ['collections' => $collection->get(), 'gl_code' => $gl_entries->data,]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(PaymentCodeRequest $request)
    {
        try {
            $requestData = $request->all();
            if ($request->hasFile('image_path')) {
                $image = $request->image_path;
                $name = time() . rand(1, 100) . '.' . $image->extension();
                $image->move(public_path('image'), $name);
                $requestData['image_path'] = $name;
            }
            PaymentCode::create($requestData);
            return response()->json(['message' => 'Successfully Added'], 200);
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' =>  $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $item = DB::table('payment_codes')->where('id', $id)->first();
        return response([
            'data' => $item,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(PaymentCodeRequest $request, string $id)
    {
        try {
            $requestData = $request->all();
            if ($request->image_path) {
                $image = $request->image_path;
                $name = time() . rand(1, 100) . '.' . $image->extension();
                $image->move(public_path('image'), $name);
                $requestData['image_path'] = $name;
            }
            PaymentCode::findOrFail($id)->update($requestData);
            return response()->json(['message' => 'Successfully Updated'], 200);
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }

    public function status(Request $request, string $id)
    {
        try {
            $obj = PaymentCode::findOrFail($id);
            if ($request->check == 1) {
                $obj->status = 0;
            } else {
                $obj->status = 1;
            }
            $obj->update();
            return response()->json(['message' => 'Successfully Updated'], 200);
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
