<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use App\Http\Requests\CityStoreRequest;
use App\Models\City;
use App\Models\Society;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Validation\ValidationException;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(City::query())->make(true);
        }
        return view('admin.property.city');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CityStoreRequest $request)
    {
        try {
            City::create($request->all());
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CityStoreRequest $request, string $id)
    {
        try {
            City::findOrFail($id)->update($request->all());
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | validationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $city = City::findOrFail($id);
            $foreignKey = Society::where('city_id', '=', $id)->get();
            if ($foreignKey->isEmpty()) {
                $city->delete();
                return ['code' => '200', 'message' => 'success'];
            } else {
                return ['code' => '300', 'message' => 'There are societies associated with this city. Delete them first.'];
            }
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
