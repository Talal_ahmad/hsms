<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use App\Http\Requests\BlockStoreRequest;
use App\Models\City;
use App\Models\PropertyBlock;
use App\Models\PropertyType;
use App\Models\Society;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class BlockController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            $data = PropertyBlock::select('property_blocks.*', 'property_blocks.name as block_name', 'cities.name as city_name', 'societies.name as society_name')
                ->leftJoin('societies', 'societies.id', '=', 'property_blocks.society_id')
                ->leftJoin('cities', 'cities.id', '=', 'societies.city_id');
            return DataTables::of($data)->make(true);
        }
        return view('admin.property.blocks', ['cities' => City::all()]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

        /**
     * Store a newly created resource in storage.
     */
    public function store(BlockStoreRequest $request)
    {
        try {
            PropertyBlock::create($request->all());
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        $entry = PropertyBlock::where('id', $id);
        $city = Society::where('id', $entry->value('society_id'));
        return response(['data' => $entry->get(), 'society' => $entry->value('society_id'), 'city' => $city->value('city_id')]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(BlockStoreRequest $request, string $id)
    {
        try {
            PropertyBlock::findOrFail($id)->update($request->all());
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            PropertyBlock::findOrFail($id)->delete();
            return ['code' => '200', 'messsge' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
