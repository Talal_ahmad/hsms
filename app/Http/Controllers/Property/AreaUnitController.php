<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreaUnitStoreRequest;
use App\Models\AreaUnit;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class AreaUnitController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(AreaUnit::query())->make(true);
        }
        return view('admin.property.area');
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(AreaUnitStoreRequest $request)
    {
        try {
            AreaUnit::create($request->all());
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(AreaUnitStoreRequest $request, string $id)
    {
        try {
            AreaUnit::findOrFail($id)->update($request->all());
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception | validationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            AreaUnit::findOrFail($id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
