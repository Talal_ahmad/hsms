<?php

namespace App\Http\Controllers;

use App\Models\PaymentCode;
use App\Models\Property;
use App\Models\PropertyBill;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;

class ReportController extends Controller
{
    public function index()
    {
        return view('admin.reports.property_ledger_report', get_defined_vars());
    }
    public function monthly_maintenance_bill_report(Request $request)
    {
        // dd($request->all());
        $collection_heads = PaymentCode::pluck('name')->toArray();
        $properties = Property::select('id', 'property_number')->get();
        // dd($collection_heads);
        $monthly_bills = PropertyBill::join('properties', 'property_bills.property_id', 'properties.id')
            ->leftJoin('property_owners', 'property_bills.property_id', 'property_owners.property_id')
            ->leftJoin('tenants', 'property_bills.property_id', 'tenants.property_id')
            ->join('property_types', 'properties.type_id', 'property_types.id')
            ->join('payment_plans', 'property_bills.payment_plan_id', 'payment_plans.id')
            ->join('payment_plan_details', 'property_bills.payment_plan_id', 'payment_plan_details.payment_plan_id')
            ->join('payment_codes', 'payment_plan_details.payment_code_id', 'payment_codes.id')
            ->select(
                'properties.id as property_id',
                'properties.property_number',
                'properties.landAreaSqfeet',
                'properties.landAreaMarla',
                'properties.landAreaKanal',
                'properties.common_area',
                'property_types.type as property_unit_type',
                'payment_plans.start_date as bill_issued_on',
                'payment_plans.end_date as bill_end_date',
                'payment_plans.total_amount as total_bill_without_pra_sales_tax',
                'payment_plans.amount_after_due_date as total_bill_without_pra_sales_tax_amount_after_due_date',
                'payment_codes.name as collection_head',
                'payment_codes.tax_rate as tax_rate',
                'property_owners.name as property_owner_name',
                'tenants.name as tenant_name',
                'property_owners.phone_number as property_owners_phone_number',
                'tenants.phone as tenants_phone_number',
                'payment_plan_details.amount as payment_plan_details_amount',
                'payment_plan_details.calculation_unit as payment_plan_details_calculation_unit',
                'property_bills.billing_month',
                'property_bills.billing_year',
                'property_bills.is_paid',
                'property_bills.assigned_to as assigned_to',
                'property_bills.total_amount_within_due_date',
                'property_bills.total_amount_after_due_date',
                'property_bills.paid_amount',
                'property_bills.arrears',
                'property_bills.payment_date',
                'property_bills.waived_off',
                'property_bills.remarks',
                // 'property_bills.created_at as property_bills_created_at',
                // 'property_bills.updated_at as property_bills_updated_at',
            )
            // ->where('properties.id', 4)
            // ->where('properties.property_number', '7F-63')
            // ->where('property_bills.billing_year', date('Y'))
            ->where(function ($where) {
                $where->whereRaw('property_bills.assigned_to = property_owners.name')
                ->orWhereRaw('property_bills.assigned_to = tenants.name');
            });
            if ($request->fromdate) {
                $monthly_bills = $monthly_bills->where('property_bills.created_at', '>=', $request->fromdate);
            }
            if ($request->todate) {
                $monthly_bills = $monthly_bills->where('property_bills.created_at', '<=', $request->todate);
            }
            if ($request->possesion == 'owner') {
                $monthly_bills = $monthly_bills->where('properties.common_area', $request->possesion);
            }
            if ($request->possesion == 'tenant') {
                $monthly_bills = $monthly_bills->where('properties.common_area', $request->possesion);
            }
            if ($request->properties) {
                $monthly_bills = $monthly_bills->whereIn('properties.id', $request->properties);
            }
        $monthly_bills = $monthly_bills->orderBy('properties.id', 'asc')
            ->get()
            ->groupBy('property_number');
        // dd($monthly_bills);

        $monthly_bills = $monthly_bills->map(function ($group, $property_number) {
            $collection_heads_and_calculation_units = $group->pluck('payment_plan_details_calculation_unit', 'collection_head')->toArray();
            // $collection_heads_and_calculation_units = $group->mapToGroups(function ($item) {
            //     return [$item['payment_plan_details_calculation_unit'] => $item['collection_head']];
            // })->toArray();
            $collection_heads_payment_plan_details_amount = $group->pluck('payment_plan_details_amount', 'collection_head')->toArray();
            $first_item = $group->first();
            return [
                'property_id' => $first_item->property_id,
                'property_number' => $property_number,
                'name' => $first_item->common_area == 'owner' || $first_item->common_area == null ? $first_item->property_owner_name : $first_item->tenant_name,
                'final_area_sft' => $first_item->landAreaSqfeet,
                'final_area_kanal' => $first_item->landAreaKanal,
                'final_area_marla' => $first_item->landAreaMarla,
                'unit_type' => $first_item->property_unit_type,
                'bill_issued_on' => $first_item->bill_issued_on,
                // 'total_amount_receivable' => $first_item->total_amount_within_due_date + $first_item->total_amount_after_due_date,
                'total_amount_receivable' => $first_item->total_amount_within_due_date + (Carbon::parse($first_item->bill_end_date)->lessThan(Carbon::parse($first_item->payment_date)) ? $first_item->total_amount_after_due_date : 0),
                'arrears' => $first_item->arrears,
                // 'total_amount_receivable_with_arrears' => $first_item->total_amount_within_due_date + $first_item->total_amount_after_due_date + $first_item->arrears,
                'total_amount_receivable_with_arrears' => $first_item->total_amount_within_due_date + (Carbon::parse($first_item->bill_end_date)->lessThan(Carbon::parse($first_item->payment_date)) ? $first_item->total_amount_after_due_date : 0) + $first_item->arrears,
                'amount_received_on_date' => Carbon::parse($first_item->payment_date)->format('d-M-Y'),
                'total_amount_received' => $first_item->paid_amount,
                'total_waived_off' => $first_item->waived_off,
                // 'balance_due' => ($first_item->total_amount_within_due_date + $first_item->total_amount_after_due_date + $first_item->arrears) - $first_item->paid_amount,
                'balance_due' => ($first_item->total_amount_within_due_date + (Carbon::parse($first_item->bill_end_date)->lessThan(Carbon::parse($first_item->payment_date)) ? $first_item->total_amount_after_due_date : 0) + $first_item->arrears) - $first_item->paid_amount,
                'remarks' => $first_item->remarks,
                'possession_given/not_given' => $first_item->common_area == 'owner' || $first_item->common_area == null ? 'Given' : 'Not Given(Rented)',
                'collection_heads_and_calculation_units' => $collection_heads_and_calculation_units,
                'collection_heads_payment_plan_details_amount' => $collection_heads_payment_plan_details_amount,
                'total_bill_without_pra_sales_tax' => $first_item->total_bill_without_pra_sales_tax,
                // 'total_bill_without_pra_sales_tax' => $first_item->total_bill_without_pra_sales_tax + $first_item->total_bill_without_pra_sales_tax_amount_after_due_date,
                'pra_tax_rate' => $first_item->tax_rate,
                // 'created_at' => $first_item->property_bills_created_at,
                // 'updated_at' => $first_item->property_bills_updated_at,
            ];
        })->values();
        // dd($monthly_bills);
        //     return DataTables::of($monthly_bills)->make(true);
        // }
        return view('admin.reports.monthly_maintenance_bill_report', get_defined_vars());
    }
}
