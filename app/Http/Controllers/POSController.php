<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Category;
use App\Models\Property;
use App\Models\PropertyOwner;
use App\Models\Service;
use App\Models\Tenant;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;

class POSController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::find(Auth::id());
        $properties = Property::select(
            'properties.*',
            'cities.name as city_name',
            'societies.name as society_name',
            'property_blocks.name as block_name'
        )
            ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
            ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
            ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id');
        if (!($user->roles()->where('name', 'admin')->exists())) {
            $properties = $properties->where('added_by', Auth::id());
        }
        $categories = Category::all();
        $result = $categories->map(function ($category) {
            return [
                'category_name' => $category->name,
                'services' => Service::where('category_id', $category->id)->get()->toArray(),
            ];
        })->toArray();
        return view('admin.Services.order', ['categories' => $result, 'properties' => $properties->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response([
            'data' => Cart::select(
                'services.*',
                'carts.*'
            )
                ->leftJoin('services', 'carts.service_id', '=', 'services.id')
                ->where('is_ordered', 0)
                ->where('user_id', Auth::id())
                ->get()->toArray()
        ]);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            Cart::where('is_ordered', 0)
                ->where('user_id', Auth::id())
                ->update([
                    'is_ordered' => 1,
                    'is_credit' => $request->check == "cash" ? 0 : 1
                ]);
            return $this->create();
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if ($request->quantity > 0) {
            $actual_price = Service::find(Cart::find($id)['service_id'])['price'];
            $new_price = $actual_price;
            for ($i = 1; $i < $request->quantity; $i++) {
                $new_price += $actual_price;
            }
            $gst_amount = (Service::find(Cart::find($id)['service_id'])['gst'] * $new_price) / 100;
            $discount = (Service::find(Cart::find($id)['service_id'])['discount'] * $new_price) / 100;
            Cart::where('id', $id)->update([
                'quantity' => $request->quantity,
                'gst_amount' => $gst_amount,
                'discount_amount' => $discount,
                'total_amount' => ($gst_amount + $new_price) - $discount,
                'sub_total' => $new_price,
                'user_id' => Auth::id()
            ]);
        } else {
            Cart::where('id', $id)->delete();
        }
        return $this->create();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Request $request)
    {
        if ($request->check == 'new_owner') {
            if (Property::where('id', $id)->value('maintenance') == 'tenant') {
                $entry = Tenant::where('property_id', $id)->get();
            } else {
                $entry = PropertyOwner::where('property_id', $id)->get();
            }
            return response([
                'data' => $entry,
            ]);
        } else {
            return response([
                'data' => Service::where('id', $id)->get()->first()
            ]);
        }
    }

    function addToCart(Request $request)
    {
        if (Cart::where('service_id', $request->service_id)->first() == null) {
            $service = Service::where('id', $request->service_id)->get()->first();
            $gst_amount = ($service->gst * $service->price) / 100;
            $discount = ($service->price * $service->discount) / 100;
            Cart::create([
                'service_id' => $request->service_id,
                'quantity' => $request->quantity,
                'gst_amount' => $gst_amount,
                'discount_amount' => $discount,
                'total_amount' => ($gst_amount + $service->price) - $discount,
                'sub_total' => $service->price,
                'user_id' => Auth::id()
            ]);
        }
        return $this->create();
    }

    function updateQuantity(Request $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $service = Service::where('id', Cart::where('id', $id)->value('service_id'));
                $total_price = $request->quantity * $service->value('price');
                $total_gst = ($service->value('gst') * $total_price) / 100;
                $total_discount = ($service->value('discount') * $total_price) / 100;
                Cart::where('id', $id)->update([
                    'quantity' => $request->quantity,
                    'gst_amount' => $total_gst,
                    'discount_amount' => $total_discount,
                    'total_amount' => ($total_gst + $total_price) - $total_discount,
                    'sub_total' => $total_price
                ]);
                $this->create();
            });
        } catch (\Exception $e) {
            DB::rollBack();
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            Cart::find($id)->delete();
            return ['code' => '200', 'message' => 'success'];
        } catch (\Exception $e) {
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
