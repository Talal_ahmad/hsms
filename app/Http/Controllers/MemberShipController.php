<?php

namespace App\Http\Controllers;

use App\Models\City;
use App\Models\GoverningBody;
use App\Models\LegalHeir;
use App\Models\MemberShip;
use App\Models\Property;
use App\Models\PropertyBlock;
use App\Models\PropertyOwner;
use App\Models\Society;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Yajra\DataTables\Facades\DataTables;

class MemberShipController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        if ($request->getPathInfo() == '/member_ship/member_list') {
            if ($request->ajax()) {
                $user = User::find(Auth::id());
                if (!$user->roles()->where('name', 'admin')->exists()) {
                    $data = MemberShip::select('member_ships.*')
                        ->leftJoin('properties', 'properties.member_added', '=', 'member_ships.id')
                        ->where('properties.added_by', Auth::id());
                    return DataTables::of($data)->make(true);
                } else {
                    return DataTables::of(MemberShip::query())->make(true);
                }
            }
            return view('admin.Member_ship.member_list');
        }
        if ($request->getPathInfo() == '/member_ship/member_add') {
            $user = User::find(Auth::id());
            if ($user->roles()->where('name', 'admin')->exists()) {
                $properties = Property::select(
                    'properties.*',
                    'cities.name as city_name',
                    'societies.name as society_name',
                    'property_blocks.name as block_name'
                )
                    ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                    ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                    ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                    ->where('member_added', 0)->get();
            } else {
                $properties = Property::select(
                    'properties.*',
                    'cities.name as city_name',
                    'societies.name as society_name',
                    'property_blocks.name as block_name'
                )
                    ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                    ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                    ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                    ->where('added_by', Auth::id())
                    ->where('member_added', 0)->get();
            }
            $cities = City::all();
            $societies = Society::all();
            $blocks = PropertyBlock::all();
            $authorities = GoverningBody::all();
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => 'http://194.163.145.90/eperp/api/getGlAccounts?data=gl_codes',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
                ),
            ));
            $response = curl_exec($curl);
            curl_close($curl);
            $gl_entries = json_decode($response);
            return view(
                'admin.Member_ship.member_add',
                compact(
                    'cities',
                    'societies',
                    'blocks',
                    'properties',
                    'authorities'
                ),
                [
                    'gl_code' => $gl_entries->data
                ]
            );
        }
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            DB::transaction(function () use ($request) {
                $requestData = $request->all();
                $image = $request->member_image;
                $name = time() . rand(1, 100) . '.' . $image->extension();
                $image->move(public_path('members'), $name);
                $requestData['member_image'] = $name;
                $image =  $request->front_image;
                $name = time() . rand(1, 100) . '.' . $image->extension();
                $image->move(public_path('members'), $name);
                $requestData['front_image'] = $name;
                $image =  $request->back_image;
                $name = time() . rand(1, 100) . '.' . $image->extension();
                $image->move(public_path('members'), $name);
                $requestData['back_image'] = $name;
                if ($request->check == 'previous_owner') {
                    $data = PropertyOwner::findOrFail($request->owner)->toArray();
                    MemberShip::create(
                        $data +
                            [
                                'owner_id' => $request->owner,
                                'member_image' => $requestData['member_image'],
                                'front_image' => $requestData['front_image'],
                                'back_image' => $requestData['back_image']
                            ]
                    );
                    Property::where('id', $request->property_no)->update([
                        'member_added' => MemberShip::latest()->pluck('id')->first()
                    ]);
                } else {
                    PropertyOwner::create($request->all());
                    MemberShip::create($requestData +
                        [
                            'owner_id' => PropertyOwner::latest()->pluck('id')->first()
                        ]);
                    Property::where('id', $request->property_id)->update([
                        'member_added' => MemberShip::latest()->pluck('id')->first()
                    ]);
                    $membership_number = $request->membership_number;
                    if ($request->member_ship_loop && !empty(array_filter(array_values($request->member_ship_loop[0])))) {
                        foreach ($request->member_ship_loop as $member) {
                            $detail = new LegalHeir();
                            $detail->membership_number = $membership_number;
                            $detail->name = $member['heir_name'];
                            $detail->father_name = $member['heir_father_name'];
                            $detail->date_of_birth = $member['heir_date_of_birth'];
                            $detail->cnic = $member['heir_cnic'];
                            $detail->cnicExpiryDate = $member['heir_cnicExpiryDate'];
                            $detail->email = $member['heir_email'];
                            $detail->address = $member['heir_address'];
                            $detail->permanent_address = $member['heir_permanent_address'];
                            $detail->phone_number = $member['heir_phone_number'];
                            $detail->secondary_phone_number = $member['heir_secondary_phone_number'];
                            $detail->save();
                        }
                    }
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $member = MemberShip::findOrFail($id);
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => 'http://194.163.145.90/jchserp/api/getGlAccounts?data=gl_codes',
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => '',
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => 'GET',
            CURLOPT_HTTPHEADER => array(
                'Cookie: ci_session=a32r6av7gph2s93g40bgo2a6dqe628ac'
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $gl_entries = json_decode($response);
        return view('admin.Member_ship.member_edit', ['member' => $member, 'gl_code' => $gl_entries->data]);
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id, Request $request)
    {
        if ($request->check == 'new_owner') {
            $entry = PropertyOwner::where('property_id', $id)->get();
            return response([
                'data' => $entry,
            ]);
        }
        if ($request->check == 'list') {
            $entry = MemberShip::findOrFail($id);
            return response([
                'data' => $entry,
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $delID = MemberShip::where('id', $id)->pluck('membership_number')->toArray();
                LegalHeir::where('membership_number', (int)$delID)->delete(); // new
                $requestData = $request->all();
                if ($request->member_image) { // if not found, then hold previous images in DataBase
                    $image = $request->member_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('members'), $name);
                    $requestData['member_image'] = $name;
                }
                if ($request->front_image) {
                    $image =  $request->front_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('members'), $name);
                    $requestData['front_image'] = $name;
                }
                if ($request->back_image) {
                    $image =  $request->back_image;
                    $name = time() . rand(1, 100) . '.' . $image->extension();
                    $image->move(public_path('members'), $name);
                    $requestData['back_image'] = $name;
                }
                MemberShip::FindOrFail($id)->update($requestData); // changed
                $ownerData = $request->except([
                    '_token',
                    '_method',
                    'member_image',
                    'front_image',
                    'back_image',
                    'member_ship_loop',
                    'member_id'
                ]);
                PropertyOwner::where('id', MemberShip::where('id', $id)->pluck('owner_id')->toArray())->update($ownerData);
                $membership_number = $request->membership_number;
                if ($request->member_ship_loop && !empty(array_filter(array_values($request->member_ship_loop[0])))) {
                    foreach ($request->member_ship_loop as $member) {
                        $detail = new LegalHeir();
                        $detail->membership_number = $membership_number;
                        $detail->name = $member['heir_name'];
                        $detail->father_name = $member['heir_father_name'];
                        $detail->date_of_birth = $member['heir_date_of_birth'];
                        $detail->cnic = $member['heir_cnic'];
                        $detail->cnicExpiryDate = $member['heir_cnicExpiryDate'];
                        $detail->email = $member['heir_email'];
                        $detail->address = $member['heir_address'];
                        $detail->permanent_address = $member['heir_permanent_address'];
                        $detail->phone_number = $member['heir_phone_number'];
                        $detail->secondary_phone_number = $member['heir_secondary_phone_number'];
                        $detail->save();
                    }
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception | ValidationException $e) {
            if ($e instanceof ValidationException) {
                return ['code' => '422', 'errors' => $e->errors()];
            } else {
                DB::rollBack();
                return ['code' => '500', 'error_message' => $e->getMessage()];
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $id)
    {
        try {
            DB::transaction(function () use ($request, $id) {
                $obj = MemberShip::findOrFail($id);
                if ($request->check >= 1) {
                    $obj->status = 0;
                    $obj->update();
                    Property::where('member_added', $id)->update(['member_added' => 0]);
                }
                return ['code' => '200', 'message' => 'success'];
            });
        } catch (\Exception $e) {
            DB::rollBack();
            return ['code' => '500', 'error_message' => $e->getMessage()];
        }
    }
}
