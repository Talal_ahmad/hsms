<?php

namespace App\Jobs;

use App\Models\GeneratedPdf;
use App\Models\PaymentCode;
use App\Models\PaymentPlan;
use App\Models\PaymentPlanDetail;
use App\Models\Property;
use App\Models\PropertyBill;
use App\Models\PropertyOwner;
use Barryvdh\DomPDF\Facade\Pdf;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Auth;

class GeneratePdfJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $id;
    protected $property_details;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $property_details)
    {
        $this->id = $id;
        $this->property_details = $property_details;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $properties = Property::where('type_id', $this->id)->whereNotNull('properties.payment_plan_id')->get();
        $property_details = [];
        foreach ($properties as $key => $property) {
            $bill = PropertyBill::where('property_id', $property->id);
            $plan = PaymentPlan::where('id', $bill->value('payment_plan_id'));
            $plan_details = PaymentPlanDetail::where('payment_plan_details.payment_plan_id', $plan->value('id'));
            $code_details = PaymentCode::whereIn('id', $plan_details->pluck('payment_code_id'))->get();
            $data = [];
            foreach ($plan_details->get() as $index => $pay) {
                $data[] = [
                    'name' => $code_details[$index]->name,
                    'amount' => $pay->amount
                ];
            };
            $property_details[] = [
                'nmonth' => date("m", strtotime($bill->value('billing_month'))),
                'hmonth' => date("M", strtotime($bill->value('billing_month'))),
                'year' => $bill->value('billing_year'),
                'challan_no' => $bill->value('challan_no'),
                'total_amount_after_due_date' => $bill->value('total_amount_after_due_date'),
                'total_amount_within_due_date' => $bill->value('total_amount_within_due_date'),
                'issue_date' => $plan->value('start_date'),
                'due_date' => $plan->value('end_date'),
                'total_amount' => $plan->value('total_amount'),
                'plan_name' => $plan->value('plan_name'),
                'prop' => PropertyOwner::where('property_owners.property_id', Property::where('properties.id', $bill->value('property_id'))->value('id'))->first(),
                'sub_property' => Property::where('properties.id', $bill->value('property_id'))->select(
                    'properties.*',
                    'cities.name as city_name',
                    'societies.name as society_name',
                    'property_blocks.name as block_name',
                    'property_types.type as type_name',
                    'bank_details.*',
                )
                    ->leftJoin('cities', 'cities.id', '=', 'properties.city_id')
                    ->leftJoin('societies', 'societies.id', '=', 'properties.society_id')
                    ->leftJoin('property_blocks', 'property_blocks.id', '=', 'properties.block_id')
                    ->leftJoin('property_types', 'property_types.id', '=', 'properties.type_id')
                    ->leftJoin(
                        'bank_details',
                        function ($join) {
                            $join->on('bank_details.city_id', '=', 'properties.city_id')
                                ->on('bank_details.society_id', '=', 'properties.society_id');
                        }
                    )
                    ->first(),
                'data' => $data,
            ];
        }
        $contxt = stream_context_create([
            'ssl' => [
                'verify_peer' => FALSE,
                'verify_peer_name' => FALSE,
                'allow_self_signed' => TRUE,
            ]
        ]);
        $pdf = PDF::setOptions(['isHTML5ParserEnabled' => true, 'isRemoteEnabled' => true]);
        $pdf->getDomPDF()->setHttpContext($contxt);
        $file_name = $property_details[0]['sub_property']['city_name'] . '-' . $property_details[0]['sub_property']['society_name'] . '-' . $property_details[0]['sub_property']['block_name'] . '-' . $property_details[0]['sub_property']['type_name'] . '-' . Carbon::now()->format('M') . '-' . Carbon::now()->year . '-' . Carbon::now();
        $path = public_path('PropertyBills');
        $pdf->setPaper('a4', 'landscape');
        $pdf = Pdf::loadView('admin.pdf.multipleBill', get_defined_vars())->save('' . $path . '/' . $file_name . '.pdf');

        $generatedpdf = new GeneratedPdf();
        $generatedpdf->user_id = Auth::id();
        $generatedpdf->city_name = $property_details[0]['sub_property']['city_name'];
        $generatedpdf->society_name = $property_details[0]['sub_property']['society_name'];
        $generatedpdf->block_name = $property_details[0]['sub_property']['block_name'];
        $generatedpdf->month_year = Carbon::now()->format('Y-m');
        $generatedpdf->file_name = $file_name;
        $generatedpdf->file_path = 'PropertyBills/' . $file_name . '.pdf';
        $generatedpdf->save();
    }
}
