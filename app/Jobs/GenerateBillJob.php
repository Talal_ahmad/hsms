<?php

namespace App\Jobs;

use App\Http\Controllers\Billing\PropertyBillController;
use App\Models\PaymentPlan;
use App\Models\Property;
use App\Models\PropertyBill;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class GenerateBillJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $id;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // creating the instance of my controller to use its functions
        $property_bills_cont = new PropertyBillController();

        $properties = Property::where('type_id', $this->id)->whereNotNull('properties.payment_plan_id')->get();
        $currentDate = Carbon::now();
        foreach ($properties as $property) {
            // check if any values of payment_plan_id is present in property_bills table...
            // convert the (payment_plan_id) string into array of ids, so we can get data from payment_plans table
            $to_string = explode(',', str_replace("'", '', $property->payment_plan_id));
            $int_array =  array_map(function ($id) {
                return (int)trim($id, "'");
            }, $to_string);

            $plans = PaymentPlan::whereIn('id', $int_array)->get();

            // Now we have to check each value of (payment_plan_id)'s so avoid duplicate bills
            foreach ($plans as $plan) {
                $check_bill = PropertyBill::select(
                    'property_bills.*',
                    'payment_plans.start_date as start_date',
                    'payment_plans.end_date as end_date'
                )
                    ->leftJoin('payment_plans', 'payment_plans.id', '=', 'property_bills.payment_plan_id')
                    ->where('property_id', $property->id)
                    ->where('payment_plan_id', $plan->id);
                // only insert bill if doesn't exist
                if (!$check_bill->exists()) {
                    $challan_no = PropertyBill::count() === 0 ? str_pad(mt_rand(1000, 9999), 4, '0', STR_PAD_LEFT) : str_pad(strval(intval(PropertyBill::where('id', PropertyBill::count())->value('challan_no')) + 1), 4, '0', STR_PAD_LEFT);
                    PropertyBill::create([
                        'property_id' => $property->id,
                        'payment_plan_id' => $plan->id,
                        'city_id' => $property->city_id,
                        'society_id' => $property->society_id,
                        'block_id' => $property->block_id,
                        'billing_month' =>  Carbon::now()->format('m'),
                        'billing_year' => Carbon::now()->format('Y'),
                        'total_amount_within_due_date' => $plan->total_amount,
                        'total_amount_after_due_date' => $plan->amount_after_due_date,
                        'challan_no' => $challan_no
                    ]);
                } else if ($check_bill->exists() && !$check_bill->value('is_paid')) { // if previous version is present and still marked unpaid, then also calculate previous amount
                    if ($property_bills_cont->requiredDays($check_bill) == Carbon::parse($check_bill->value('start_date')) && $currentDate->gt(Carbon::parse($check_bill->value('end_date')))) {
                        PropertyBill::where('id', $check_bill->value('id'))->update([
                            'total_amount' => $plan->total_amount + $plan->amount_after_due_date
                        ]);
                    } else if ($currentDate->gt($property_bills_cont->requiredDays($check_bill))) {
                        PropertyBill::where('id', $check_bill->value('id'))->update([
                            'total_amount' => $plan->total_amount + $plan->amount_after_due_date,
                            'start_date' => Carbon::parse($check_bill->value('start_date'))->addDays($property_bills_cont->requiredDays($check_bill)),
                            'end_date' => Carbon::parse($check_bill->value('end_date'))->addDays($property_bills_cont->requiredDays($check_bill))
                        ]);
                    }
                }
            }
        }
    }
}
