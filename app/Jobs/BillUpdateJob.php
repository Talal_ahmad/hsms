<?php

namespace App\Jobs;

use App\Models\PropertyBill;
use Illuminate\Bus\Batchable;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class BillUpdateJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $bill_id,
        $total_amount,
        $amount_after_due_date,
        $start_date,
        $end_date,
        $challan_no,
        $arrears;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($id, $amount, $after_date, $start_date, $end_date, $challan_no, $arrears)
    {
        $this->bill_id = $id;
        $this->total_amount = $amount;
        $this->amount_after_due_date = $after_date;
        $this->start_date = $start_date;
        $this->end_date = $end_date;
        $this->challan_no = $challan_no;
        $this->arrears = $arrears;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        PropertyBill::where('id', $this->bill_id)->update([
            'total_amount' => $this->total_amount + $this->amount_after_due_date,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'challan_no' => $this->challan_no,
            'arrears' => $this->arrears
        ]);
    }
}
