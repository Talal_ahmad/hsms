<?php

namespace App\Jobs;

use App\Models\PropertyBill;
use Illuminate\Bus\Queueable;
use Illuminate\Bus\Batchable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;

class BillCreateJob implements ShouldQueue
{
    use Batchable, Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    protected $property_id,
        $payment_plan_id,
        $city_id,
        $society_id,
        $block_id,
        $billing_month,
        $billing_year,
        $total_amount_within_due_date,
        $total_amount_after_due_date,
        $challan_no,
        $assigned_to,
        $arrears,
        $auth_id,
        $waived_off,
        $is_paid,
        $paid_amount,
        $advance_payment_status,
        $payment_date;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($property_id, $payment_plan_id, $city_id, $society_id, $block_id, $billing_month, $billing_year, $total_amount_within_due_date, $total_amount_after_due_date, $challan_no, $assigned_to, $arrears, $auth_id, $waived_off, $is_paid, $paid_amount, $advance_payment_status, $payment_date)
    {
        $this->property_id = $property_id;
        $this->payment_plan_id = $payment_plan_id;
        $this->city_id = $city_id;
        $this->society_id = $society_id;
        $this->block_id = $block_id;
        $this->billing_month = $billing_month;
        $this->billing_year = $billing_year;
        $this->total_amount_within_due_date = $total_amount_within_due_date;
        $this->total_amount_after_due_date = $total_amount_after_due_date;
        $this->challan_no = $challan_no;
        $this->assigned_to = $assigned_to;
        $this->arrears = $arrears;
        $this->auth_id = $auth_id;
        $this->waived_off = $waived_off;
        $this->is_paid = $is_paid;
        $this->paid_amount = $paid_amount;
        $this->advance_payment_status = $advance_payment_status;
        $this->payment_date = $payment_date;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            PropertyBill::create([
                'property_id' => $this->property_id,
                'payment_plan_id' => $this->payment_plan_id,
                'city_id' => $this->city_id,
                'society_id' => $this->society_id,
                'block_id' => $this->block_id,
                'billing_month' =>  $this->billing_month,
                'billing_year' => $this->billing_year,
                'total_amount_within_due_date' => $this->total_amount_within_due_date,
                'total_amount_after_due_date' => $this->total_amount_after_due_date,
                'challan_no' => $this->challan_no,
                'assigned_to' => $this->assigned_to,
                'arrears' => $this->arrears,
                'auth_id' => $this->auth_id,
                'waived_off' => $this->waived_off,
                'is_paid' => $this->is_paid,
                'advance_paid_amount' => $this->paid_amount,
                'advance_payment_status' => $this->advance_payment_status,
                'payment_date' => $this->payment_date,
            ]);
        } catch (\Exception $e) {
            Log::error('Error handling BillCreateJob: ' . $e->getMessage(), [
                'property_id' => $this->$this->property_id,
                // Add more variables here
            ]);
        }
    }
}
