<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdvancePayment extends Model
{
    use HasFactory;
    protected $table = 'advance_payments';
    protected $fillable = ['property_id', 'paid_by', 'advance_amount', 'remaining_amount', 'billing_month', 'billing_year', 'payment_type'];
}
