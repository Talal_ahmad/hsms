<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesBill extends Model
{
    use HasFactory;

    protected $table = 'services_bills';
    protected $fillable = [
        'property_id',
        'assigned_to',
        'sub_total',
        'gst',
        'discount',
        'arrears',
        'billing_month',
        'billing_year',
        'is_paid',
        'challan_no',
        'payment_method',
        'total_amount',
        'issue_date',
        'added_by',
        'waived_off',
        'paid_amount',
        'advance_payment_status',
        'payment_date',
        'advance_paid_amount',
        'paid_slip',
        'waived_off_reference',
        'remarks'
    ];
}
