<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyDealer extends Model
{
    use HasFactory;

    protected $table = 'property_dealers';
    protected $fillable = ['name', 'address', 'phone'];
}
