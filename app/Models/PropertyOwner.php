<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyOwner extends Model
{
    use HasFactory;
    protected $table = 'property_owners';
    protected $guarded = [
        'owner_cnic_old',
        'gender',
        'ntn',
        'transfer_letter',
        'date_of_transfer',
        'date_of_allotment',
        'date_of_possession',
        'date_of_map_approval',
        'date_of_noc',
        'date_of_noc_for_mortgage',
        'member_photo',
        'cnic_front',
        'cnic_back',
        'corruption_detail',
        'corruption_image',
        'mortgage_bank',
        'mortgage_bank_branch',
        'mortgage_document_no',
        'mortgage_book_no',
        'mortgage_volume_no',
        'mortgage_page',
        'mortgage_subregistrar',
        'mortgage_date',
        'mortgage_image',
        'suite',
        'court_name',
        'date_litigation',
        'litigation_status',
        'litigation_image'
    ];
}
