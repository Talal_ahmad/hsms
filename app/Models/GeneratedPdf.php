<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GeneratedPdf extends Model
{
    use HasFactory;
    protected $table = 'generatedpdf';
    protected $fillable = [
        'user_id',
        'city_name',
        'society_name',
        'block_name',
        'month_year',
        'file_name',
        'file_path'
    ];
}
