<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyBill extends Model
{
    use HasFactory;

    protected $table = 'property_bills';

    protected $fillable = [
        'property_id',
        'payment_plan_id',
        'assigned_to',
        'arrears',
        'city_id',
        'society_id',
        'waived_off',
        'block_id',
        'billing_month',
        'billing_year',
        'total_amount_within_due_date',
        'total_amount_after_due_date',
        'challan_no',
        'added_by',
        'advance_payment_status',
        'paid_amount',
        'advance_paid_amount',
        'waived_off_reference',
        'payment_date',
        'is_paid',
        'remarks',
    ];
}
