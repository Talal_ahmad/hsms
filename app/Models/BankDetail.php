<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankDetail extends Model
{
    use HasFactory;
    protected $table = 'bank_details';
    protected $fillable = [
        'image',
        'bank_name',
        'gl_code',
        'gl_name',
        'branch_phone_no',
        'branch_address',
        'branch_narration',
        'branch',
        'account_title',
        'account_no',
        'city_id',
        'society_id'
    ];
    protected $appends = ['gl_no'];

    public function getGlNoAttribute()
    {
        return $this->attributes['gl_code'] . '/' . $this->attributes['gl_name'];
    }
}
