<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentCode extends Model
{
    use HasFactory;
    protected $table = 'payment_codes';
    protected $fillable = [
        'code',
        'name',
        'gl_code',
        'gl_name',
        'payment_type_id',
        'from_date',
        'to_date',
        'description',
        'image_path',
        'status',
        'tax_rate',
        'taxation_body'
    ];
}
