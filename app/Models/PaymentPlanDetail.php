<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentPlanDetail extends Model
{
    use HasFactory;
    protected $table = 'payment_plan_details';
    protected $fillable = ['payment_plan_id', 'payment_code_id', 'amount', 'calculation_unit'];
}
