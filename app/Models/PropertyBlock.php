<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyBlock extends Model
{
    use HasFactory;

    protected $fillable = ['name', 'society_id', 'description'];
}
