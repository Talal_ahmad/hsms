<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $table = 'carts';
    protected $fillable = [
        'service_id',
        'quantity',
        'gst_amount',
        'discount_amount',
        'total_amount',
        'sub_total',
        'is_ordered',
        'user_id',
        'is_credit'
    ];
}
