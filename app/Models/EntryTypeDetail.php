<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EntryTypeDetail extends Model
{
    use HasFactory;
    protected $fillable = ['entry_type_id', 'gl_code', 'gl_name', 'method', 'description'];
}
