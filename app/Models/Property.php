<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $table = 'properties';
    protected $fillable = [
        'added_by',
        'member_added',
        'maintenance',
        'common_area',
        'property_number',
        'type_id',
        'typesProperty',
        'city_id',
        'society_id',
        'society_number',
        'block_id',
        'payment_plan_id',
        'assigned_date',
        'purchased_price',
        'landAreaKanal',
        'landAreaMarla',
        'landAreaSqfeet',
        'land_area_unit_id',
        'covered_area',
        'covered_area_unit_id',
        'alloted_area',
        'possession_area',
        'extra_area',
        'extra_area_unit_id',
        'dimention_length',
        'dimention_width',
        'scaned_iamges_added',
        'category_id',
        'share',
        'pending_dues',
        'litigation_cout_name',
        'litigation_suite',
        'litigation_date',
        'litigation_image',
        'litigation_remarks',
        'mortage_bank',
        'mortage_branch',
        'mortage_document',
        'mortage_book',
        'mortage_volume',
        'mortage_pages',
        'mortage_sub_register',
        'mortage_date',
        'mortage_image',
        'nab_detail',
        'nab_image'
    ];
}
