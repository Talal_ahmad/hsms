<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PropertyBillDetail extends Model
{
    use HasFactory;
    protected $table = 'property_bill_details';
    protected $fillable = ['property_bill_id', 'payment_code_id', 'payment_plan_id', 'amount', 'pra'];
}
