<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MemberShip extends Model
{
    use HasFactory;
    protected $fillable = [
        'owner_id',
        'cnic',
        'phone_number',
        'email',
        'address',
        'membership_number',
        'member_vote',
        'membership_from_date',
        'membership_to_date',
        'share_capital',
        'gl_code',
        'name',
        'father_name',
        'cnicExpiryDate',
        'permanent_address',
        'secondary_phone_number',
        'date_approval_managingCommitte',
        'date_confirmation_by_GB',
        'no_shares',
        'value_shares',
        'member_image',
        'front_image',
        'back_image',
        'date_enrolment_member'
    ];
}
