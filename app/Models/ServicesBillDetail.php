<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ServicesBillDetail extends Model
{
    use HasFactory;

    protected $table = 'services_bills_details';
    protected $fillable = ['services_bills_id', 'category_id', 'service_id', 'quantity'];
}
