<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tenant extends Model
{
    use HasFactory;
    protected $table = 'tenants';
    protected $fillable = [
        'property_id',
        'dealer_id',
        'name',
        'father_name',
        'cnic',
        'email',
        'cnic_image',
        'address',
        'phone',
        'rent',
        'increment',
        'from_date',
        'to_date',
        'status',
    ];
}
