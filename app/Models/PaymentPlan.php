<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentPlan extends Model
{
    use HasFactory;
    protected $table = 'payment_plans';

    protected $fillable = [
        'plan_name',
        'image_path',
        'total_amount',
        'amount_after_due_date',
        'from_date',
        'to_date',
        'start_date',
        'end_date',
        'description',
        'status',
        'assigned_by',
    ];
}
