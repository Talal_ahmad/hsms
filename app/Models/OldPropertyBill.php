<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OldPropertyBill extends Model
{
    use HasFactory;

    protected $table = 'old_property_bills';
    protected $fillable = ['challan_no', 'name', 'unit_no', 'unit_area', 'billing_month', 'billing_year', 'arrears', 'due_amount', 'amount_after_due_date', 'start_end', 'due_date'];
}
