<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OldPropertyBillDetail extends Model
{
    use HasFactory;

    protected $table = 'old_property_bill_details';
    protected $fillable = ['old_property_bill_id', 'name', 'amount_per_unit', 'total_amount', 'pra_percent'];
}
