<?php

use App\Http\Controllers\AdvancePaymentController;
use App\Http\Controllers\AssignPlanController;
use App\Http\Controllers\BankDetailController;
use App\Http\Controllers\Billing\BillController;
use App\Http\Controllers\Billing\GeneratedPdfController;
use App\Http\Controllers\Billing\PropertyBillController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\Collections\PaymentCodeController;
use App\Http\Controllers\Collections\PaymentPlanController;
use App\Http\Controllers\Collections\PaymentTypeController;
use App\Http\Controllers\ComplainController;
use App\Http\Controllers\ComplainTypeController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\Entry_type\EntryTypeController;
use App\Http\Controllers\GoverningBodyController;
use App\Http\Controllers\MemberShipController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\POSController;
use App\Http\Controllers\Properties\PropertyController;
use App\Http\Controllers\Property\AreaUnitController;
use App\Http\Controllers\Property\BlockController;
use App\Http\Controllers\Property\CityController;
use App\Http\Controllers\Property\DocumentTypeController;
use App\Http\Controllers\Property\PropertyTypeController;
use App\Http\Controllers\Property\SocietyController;
use App\Http\Controllers\ReportController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ServiceController;
use App\Http\Controllers\ServicesBillController;
use App\Http\Controllers\StaffController;
use App\Http\Controllers\TransferController;
use App\Http\Controllers\UserController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'DashboardController@index')->name('dashboard');
// Profile
Route::get('profile', 'DashboardController@profile');
Route::post('updateUserProfile', 'DashboardController@updateUserProfile');

// Role
Route::resource('roles', RoleController::class);
// Permission
Route::resource('permissions', PermissionController::class);

// Route accessed only by admin
// Route::group(['middleware' => 'admin'], function () {
// Users
Route::resource('users', UserController::class);
// PROPERTY SETUP
Route::resource('property/city', CityController::class);
Route::resource('property/society', SocietyController::class);
Route::resource('property/block', BlockController::class);
Route::resource('property/property-types', PropertyTypeController::class);
Route::resource('property/doc-types', DocumentTypeController::class);
Route::resource('property/area-units', AreaUnitController::class);
// TRANSFER PROPERTY
Route::resource('transfer-property/List', TransferController::class);
Route::resource('transfer-property/Add', TransferController::class);
// Maintenance
Route::resource('maintenance/categories', CategoryController::class);
Route::resource('maintenance/services', ServiceController::class);
// POS
Route::resource('pos', POSController::class);
Route::get('pos/{id}/show', [POSController::class, 'show']);
// GOVERNING-BODY
Route::resource('/governing-body', GoverningBodyController::class);
Route::post('/governing-body/{id}/status', [GoverningBodyController::class, 'status']);
// STAFF
Route::resource('/staff', StaffController::class);
//  ERP ENTRY-TYPES
Route::resource('entry_type/entry-list', EntryTypeController::class);
Route::resource('entry_type/entry-add', EntryTypeController::class);
// COMPLAIN TYPES
Route::resource('/complain-types', ComplainTypeController::class);
// });

Route::get('/notification', function () {
    return view('admin.notifications.notification');
});

require __DIR__ . '/auth.php';
// PROPERTIES
Route::resource('properties/list', PropertyController::class);
Route::post('properties/add/{id}/edit', [PropertyController::class, 'view']);
Route::post('properties/add/{id}/doc', [PropertyController::class, 'addDocument']);
Route::resource('properties/add', PropertyController::class);
Route::get('/download-image/{filename}', [PropertyController::class, 'downloadImage'])->name('download.image');
// Billing
Route::resource('billing/monthly-bills', PropertyBillController::class);
Route::get('billing/monthly-bills-get-plans/{id}', [PropertyBillController::class, 'get_plans']);
Route::resource('billing/property-bills', BillController::class);
Route::post('billing/property-bills/{id}/verify-payment', [BillController::class, 'verify_payment']);
Route::resource('billing/service-bills', ServicesBillController::class);
Route::post('billing/service-bills/{id}/verify-payment', [ServicesBillController::class, 'verify_payment']);
Route::resource('billing/bill-pdf', GeneratedPdfController::class);
Route::resource('billing/advance-payment', AdvancePaymentController::class);
// COLLECTIONS
Route::resource('collections/payment-type', PaymentTypeController::class);
Route::post('collections/payment-type/{id}/status', [PaymentTypeController::class, 'status']);
Route::resource('collections/payment-code', PaymentCodeController::class);
Route::post('collections/payment-code/{id}/status', [PaymentCodeController::class, 'status']);
Route::resource('collections/payment-plan', PaymentPlanController::class);
Route::resource('collections/payment-code', PaymentCodeController::class);
Route::post('collections/payment-plan/{id}/status', [PaymentPlanController::class, 'status']);
Route::resource('collection/assign-plan', AssignPlanController::class);
// MEMBER-SHIP
Route::resource('member_ship/member_list', MemberShipController::class);
Route::post('member_ship/member_list/{id}/status', [MemberShipController::class, 'destroy']);
Route::resource('member_ship/member_add', MemberShipController::class);
// BANK
Route::resource('billing/bank', BankDetailController::class);
// COMPLAINS
Route::resource('/complains', ComplainController::class);

// Services
Route::post('/pos/addtocart', [POSController::class, 'addToCart']);
Route::post('/pos/updateQuantity/{id}', [POSController::class, 'updateQuantity']);

//Reports
Route::get('property_ledger', [ReportController::class, 'index']);
Route::get('monthly_maintenance_bill_report', [ReportController::class, 'monthly_maintenance_bill_report'])->name('monthly_maintenance_bill_report');